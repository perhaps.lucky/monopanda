# MonoPanda

## Introduction

MonoPanda was created as personal project, to put some basic game engine ideas together. The most important part turned out to be the simple modability through json files and content files loaded from outside of pipeline.

MonoPanda is meant to to be used as "base engine project" rather than framework. There are many things that are not the strong part, and many things that could use some personal changes. In the end, what seems easy and simple to me, might seem like unnecessary complication to other person.

## Used technologies

- [MonoGame](https://www.monogame.net/)
- [NoPipeline](https://github.com/Martenfur/NoPipeline) - configured to simply copy every file inside content directory (no need for compilation since everything gets loaded from raw files)
- [Penumbra](https://github.com/discosultan/penumbra) - 2D lighting component
- [Humper](https://github.com/dotnet-ad/Humper) - collision detection / movement
- [Spine](http://esotericsoftware.com/) - 2D skeletal animation (requires spine license to be used)
- [ini-parser](https://github.com/rickyah/ini-parser)
- [Newtonsoft.Json](https://www.newtonsoft.com/json)
- [OptimizedPriorityQueue](https://github.com/BlueRaja/High-Speed-Priority-Queue-for-C-Sharp)
- [TextCopy](https://github.com/CopyText/TextCopy)

## Documentation

1. Basics:

    1.1. [Configuration and user settings](chapters/Basics/Configuration and user settings.md)
    * 1.1.1. [Graphics Configuration](chapters/Basics/Graphics Configuration.md)

    * 1.1.2. [Logger](chapters/Basics/Logger.md)

    1.2. Managers

    * 1.2.1. [Content Manager](chapters/Basics/Managers/Content.md)

    * 1.2.2. [Input Manager](chapters/Basics/Managers/Input.md)

    * 1.2.3. [Languages](chapters/Basics/Managers/Languages.md)

    * 1.2.4. [Mods Manager](chapters/Basics/Managers/Mods.md)

    * 1.2.5. [Sound Manager](chapters/Basics/Managers/Sound.md)

    * 1.2.6. [State Manager](chapters/Basics/Managers/State Manager.md)

    1.3. [Thread Request Systems](chapters/Basics/ThreadRequestSystem.md)

    1.4. [Repositories](chapters/Basics/Repositories.md)

    1.5. [Time](chapters/Basics/Time.md)

2. Entity Component System

    2.1. [Introduction](chapters/ECS/Entity Component System.md)

    2.2. MonoPanda Systems

    * 2.2.1. [Draw Systems](chapters/ECS/Systems/DrawSystem and SpineDrawSystem.md)

    * 2.2.2. [CameraSystem](chapters/ECS/Systems/CameraSystem.md)

    * 2.2.3. [CollisionSystem](chapters/ECS/Systems/CollisionSystem.md)

    * 2.2.4. [BehaviourSystem](chapters/ECS/Systems/BehaviourSystem.md)

    * 2.2.5. [GravitySystem](chapters/ECS/Systems/GravitySystem.md)

    * 2.2.6. [LightSystem](chapters/ECS/Systems/LightSystem.md)

    * 2.2.7. [MoveSystem](chapters/ECS/Systems/MoveSystem.md)

    * 2.2.8. [SoundSystem](chapters/ECS/Systems/SoundSystem.md)

    * 2.2.9. [SpineAnimationSystem](chapters/ECS/Systems/SpineAnimationSystem.md)

    * 2.2.10. [SpriteAnimationSystem](chapters/ECS/Systems/SpriteAnimationSystem.md)

    * 2.2.11. [TextSystem](chapters/ECS/Systems/TextSystem.md)
    
    2.3. Old Systems
    
    * 2.3.1. [CollisionSystem](chapters/ECS/Old Systems/CollisionSystem.md)
    
    * 2.3.2. [MoveSolidCollisionSystem](chapters/ECS/Old Systems/MoveSolidCollisionSystem.md)

3. Other features

    3.1. [Bitmap Fonts](chapters/Other features/Bitmap Fonts.md)

    3.2. [Command system and console](chapters/Other features/CommandSystem.md)

    3.3. [OneTimeFlag](chapters/Other features/OneTimeFlag.md)

    3.4. [Timers](chapters/Other features/Timers.md)

    3.5. [Optional](chapters/Other features/Optional.md)

    3.6. [SpriteSheets](chapters/Other features/SpriteSheets.md)

    3.7. [UI](chapters/Other features/UI.md)

    3.8. [Tiled Integration](chapters/Other features/Tiled Integration.md)

    3.9. [CachedTextureSprite](chapters/Other%20features/CachedTextureSprite.md)

4. [Spine](chapters/Spine.md)
