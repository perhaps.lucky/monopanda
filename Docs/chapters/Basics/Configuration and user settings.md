# Configuration and user settings

## Introduction

Configuration in MonoPanda is done with ini files. Values stored within files are passed to the objects that reflect them inside game engine. Passing the values is done by reflection and is easy to edit and extend.

Everything refered as `Configuration` / `Config` is purely "framework" settings - their saved values should not be changed by game at any point.

Everything refered as `Settings` is user settings - these values can be saved and edited. Furthermore, they reflect values stored in `Configuration` in a way that their default value is one taken from configuration json. So loading value from `Configuration` should always return default value, while loading value from `Settings` should return value set by the player, or - if it wasn't set yet - default value.

By default both files are located under `Content/MonoPanda`. Their location can be changed by editic static strings in their classes (`Config` and `Settings`).

## Structure

Each section of ini file reflects as different class object, stored as private field of singleton object of main class (`Config` or `Settings`). In main classes, there are also static fields that point to these objects inside the instance. Each property is a public field in the section object (in case of `Config` they are read-only).

For example, in section `[Window]` we have a property named `Width` - we can refer to it simply using `Config.Window.Width`. This property is also stored as user setting, to which we can refer using `Settings.Window.Width`.

We can add additional properties by simply adding another field to a section class. Furthermore if we want to make it an setting, we can add it to settings class as well - no further actions are needed.

We can add more `Config` sections by creating new class that extends `DefaultIniSectionLoader` and extends base constructor with given section name as second parameter. After that we have to add it as field to the main `Config` and initialize that field in private constructor, using `IniData` object. For example let's add new section with one property:
```ini
[RandomCats]
; Time in ms after which we show a new cat picture
TimeBetweenCatPictures=3000
```
We add the class for section with this property:
```c#
// You can use your own namespace to keep it clear from "framework" part
namespace Cats.Configuration {
  public class RandomCatsConfig : DefaultIniSectionLoader {

    // property name must be exactly the same as it is in ini file
    public readonly int TimeBetweenCatPictures;

    public RandomCatsConfig(IniData iniData) : base(iniData, "RandomCats") { }
  }
```
And finally we add it to `Config.cs`:
```diff
// You can probably keep it away from this file by some C# magic
namespace MonoPanda.Configuration {
  public class Config {
    (...)
    public static GraphicsConfig Graphics => getInstance().graphics;
++  public static RandomCatsConfig RandomCats => getInstance().randomCats;
    (...)
    private readonly GraphicsConfig graphics;
++  private readonly RandomCatsConfig randomCats;
    (...)
    private Config() {
        Log.log(LogCategory.Config, LogLevel.Info, "Initializing engine config.");
        IniData configIniData = IniUtils.getIniData(CONFIG_LOCATION);
        (...)
        graphics = new GraphicsConfig(configIniData);
++      randomCats = new RandomCatsConfig(configIniData);
    (...)
```
And that's it, the value we put in the new field (currently `3000`) will be available in any class using `Config.RandomCats.TimeBetweenCatPictures`.

To create new section and field in user settings, we have to first follow steps above, then create a new class extending `SettingsIniSectionLoader`. The rest of procedure is very similar:

Note: We don't need to edit `user-settings.ini`. This file will be filled with default settings when game starts for the first time (or first time after adding new property/section).

We create new class for the section:
```c#
namespace Cats.UserSettings {
  public class RandomCatsSettings : SettingsIniSectionLoader {

    // note how this isn't readonly and can be edited at any point
    public int TimeBetweenCatPictures { get; set; }

    public RandomCatsSettings(IniData iniData) : base(iniData, "RandomCatsSettings") { }
  }
}
```
And we add it to `Settings.cs`:
```diff
namespace MonoPanda.UserSettings {
  public class Settings {
    (...)
    public static LanguageSettings Language => getInstance().languageSettings;
++  public static RandomCatsSettings RandomCats => getInstance().randomCatsSettings;
    (...)
    private readonly LanguageSettings languageSettings;
++  private readonly RandomCatsSettings randomCatsSettings;
    (...)
    private Settings() {
      IniData settingsIniData = IniUtils.getIniData(SETTINGS_LOCATION);
      (...)
      languageSettings = new LanguageSettings(settingsIniData);
++    randomCatsSettings = new RandomCatsSettings(settingsIniData);
    (...)
```
Now we can access and edit user value for `TimeBetweenCatPictures` using `Settings.RandomCats.TimeBetweenCatPictures`. If we want to save the value for the next time, we should call `Settings.Save()`. This operation will save ALL settings out there, so paying attention while designing is necessary.

For some of existing parts of engines there are already methods that call saving. These methods will be mentioned in related chapters.


