# Graphics Configuration

## Related configuration

```ini
[Window]
Width=800
Height=600
Fullscreen=false
AllowUserSettings=true

[Graphics]
IsFixedTimeStep=false
TargetElapsedTimeMillis=16
SynchronizeWithVerticalRetrace=false

[Mouse]
CursorVisibleAtStart=true
DefaultCursor=true
CursorSpriteContentId=example-cursor
```
- `Width`, `Height`, `Fullscreen` - basically what it says; the `Width` and `Height` values are considered "default" and in some cases they are used for scaling
- `AllowUserSettings` - that's just additional protection, if set to `false`, values from user settings will not be loaded
- `IsFixedTimeStep` - setting to `true` causes the game to have exactly same amount of updates in every second (removing a need to consider a time passed within every update); however, it might cause the game to run slower on the bad computers, as it will have to "catch up" with updates (there is more to it, google it :D)
- `TargetElapsedTimeMillis` - if above is set to `true`, this value will be amount of milliseconds that one update should take (16 = 60 updates in second = 60 fps). This is actually useful to test low/high fps situations
- `SynchrinozeWithVerticalRetrace` - Vsync
- `CursorVisibleAtStart` - basically what it says, if set to `true`, then cursor will be visible
- `DefaultCursor` - if set to `true`, then default OS cursor will be used
- `CursorSpriteContentId` - if `DefaultCursor` is set to `false`, game will use sprite with this `ContentId`

## Changing resolution/fullscreen mode on runtime

For changing resolution and fullscreen mode use: `GameSettings.SetResolution(width, height)` and `GameSettings.SetFullscreen(true/false)`. These methods will change values in both user settings and inner monogame classes and also call the `GraphicsDeviceManager.ApplyChanges()`. However, these methods will not save changes in settings - you still need to call `Settings.Save()`

## Changing cursor mode on runtime

You can changes cursor visibility and type of cursor, using methods:

- `GameSettings.SetCursorVisible(bool)`
- `GameSettings.SetDrawCustomCursor(bool)`
- `GameSettings.SetCustomCursorContentId(string)`

Changes are applied instantly. They are not part of user settings so they can't be saved in any way (you can easily add that if needed).

