# Logger

## Introduction

MonoPanda allows logging various events in your game. Aside from debug console, logs can be saved to the file or show in the console inside the game.

You can configure various categories and set different levels of logged messages.

## Logging a message

There are four `log` methods we can call:

1. `Log.log(LogCategory category, LogLevel level, string message)` - this method will simply log a message in specified category with specified level
2. `Log.log(LogCategory category, LogLevel level, object o)` - this method will use `o.ToString()` as the message string
3. `Log.log<T>(LogCategory category, LogLevel level, List<T> list)` - this method will log a list of object, calling `ToString()` on each of them; objects will be separed by `,` and surround by square brackets
4. `Log.log<T>(LogCategory category, LogLevel level, string msg, List<T> list)` - this method will log a message followed by a list of objects (logic is the same as in previous method)

## LogCategory

LogCategory is a enum used to specify to which category should the message be logged. Name of category is also added as a tag to logged message. Each category has a configurable minimal `LogLevel` of saved messages. You can add new category by adding new enum value and a configuration (more about it in next sections).

## LogLevel

By default there are five levels:

1. `Off` - this level should not be used to logging, it is used in configuration when we don't want to receive any logs from the category (we usually want at least errors though?)
2. `Error` - events of this level are critical damage that should not happen. Usually they cause the application to crash or worse.
3. `Warn` - events of this level mean that something is probably wrong, but the engine will continue to work regardless. Usually there is an information of what should be fixed and/or how game will behave in current state.
4. `Info` - any informational event, that isn't too detailed
5. `Debug` - detailed informations, usually just for developper builds

Setting more detailed category level cause engine to log every message from that and less detailed level. E.g. setting category level to `Info` will log every message with `Error`, `Warn` and `Info` level, but will ignore `Debug` level.

New levels can be added by simply adding them to the enum. What you have to keep in mind is that placement in enum defines which levels will log the message, e.g.:
```C#
public enum LogLevel {
    Off, Error, Warn, NEW_LEVEL, Info, Debug
}
```
Message with `NEW_LEVEL` level will be logged when category is set to `NEW_LEVEL` or `Info` or `Debug`. Category with `NEW_LEVEL` will log messages with `Off`, `Error`, `Warn` and `NEW_LEVEL` levels.

## Configuration

All configuration happens outside the main file, due to fact that logger is initialized **before** other modules. Logger on it's own has also separated log category and logging file for his inner errors and other events.

Location of logger configuration, inner logger file and its timestamp format can be changed in `Logger.cs`. By default inner loggers go to `logger.txt` file.

By default category configuration is loaded from `Content/MonoPanda/logger.json`. Configuration contains several fields:
```json
{
    "FileOutputEnabled": true,
    "FileOutputDirectory": "Logs/",
    "MaxLogFiles": 4,
    "FileSaveInterval": 5000,
    "Timestamps": true,
    "TimestampsFormat": "HH:mm:ss",
    "LevelSettings":
    [
        {
            "Category":"Startup",
            "Level":"Debug"
        }
    ]
}
```
- `FileOutputEnabled` - if set to `true`, logs will be logged to the file
- `FileOutputDirectory` - location where logs will be saved to (aside from `logger.txt`)
- `MaxLogFiles` - what it says - after reaching maximum, oldest file will be deleted
- `FileSaveInterval` - how often should the file be saved (in milliseconds)
- `Timestamps` - should the timestamps be added to the logged messages
- `TimestampsFormat` - format of timestamps
- `LevelSettings` - array of objects that consist of `Category` (a category which we want to set, one category should be used only once) and `Level` (how detailed messages we want to save for that category)

## Adding new category

To add new category:

1. Add new value in `LogCategory` enum.
2. Add new level setting to `logger.json` file with that category name and desired level. If you don't do that, a logger will log a warning and set category level to lowest one (by default `Off`).

## Show logs in in-game console

You can make logger send messages to in-game console using the `showlogs on` command.
