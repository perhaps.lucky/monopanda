# Content

## Introduction

Content in MonoPanda is loaded from raw, non-compiled files. For that reason, I added [NoPipeline](https://github.com/Martenfur/NoPipeline) to the project. It is configured to simply copy every single file that is located in Content directory.

Each of content items (let's say, sprites) must be defined inside json file (path specified by `ContentItemsJsonPath` in config, by default it's `{Mod folder}/content-items.json`) to be loaded and used by the game.

Example defition:
```json
{
    "Id":"monopanda-logo",
    "ItemType":"Texture2D",
    "FilePath":"Textures/MonoPanda.png",
    "NeverUnload": false,
}
```
- `FilePath` is a destination to main item file, **inside mod folder**.
- `NeverUnload` is optional (by default `false`) - if set to `true`, item will not be unloaded from memory (more about it in next sections).

## Content Item Types

Types of content supported by default in MonoPanda:
- `Texture2D` - graphics file, usually in png format
- `SoundEffect` - SFX file, usually wav
- `Song` - music file, ogg / mp3
- `BitmapFont` - a font, `FilePath` should lead to `.fnt` file, additionaly requires texture file, created with [BMFont](https://www.angelcode.com/products/bmfont/)
- `SpriteSheet` - a spritesheet, `FilePath` should lead to texture file, additionaly requires `json` file, containing definitions created with [TexturePacker](http://www.codeandweb.com/texturepacker)
- `SpineAsset` - spine graphics + skeletal/animation data, `FilePath` should lead to `.atlas` file, additional requires `json` and texture files, exported from [Spine](http://esotericsoftware.com/)

Additional files should have the same name as file in `FilePath`

## Content Loading

Content is loaded using `Id` defined in `content-items.json`. Generally loading happens in a separed thread, but it is possible (and in some cases necessary) to force load on main thread.

To load an item, we call `ContentManager.Get<T>(id)` where `T` is the class of item:
- if item is already loaded, it will be returned by the method
- if item isn't loaded, method will return `default` value (most likely `null`) - code where item is loaded should be prepared for that case (or use forced loading)
- additionally if item isn't loaded and queued for loading, a request will be created and put in queue

To force load the item, we set 2nd parameter of `Get` method to true.

## Content Packages

We can create content packages, that can load many items with a single call. Content packages are defined in `content-packages.json` file (just like with items, path is configurable).

Example definition:
```json
{
    "Id":"example-package",
    "LoadOnStart":true,
    "Items":[
        "monopanda-logo",
        "example-sfx",
        "example-song",
        "example-font",
        "example-square",
        "example-big-file"
    ]
}
```
- `LoadOnStart` - if set to `true`, package will be loaded on start of the game (force loading), this can be used for menus and similar.
- `Items` - list of Ids of content items that should be loaded with this package.

We can load package by calling `ContentManager.RequestPackageLoad(id)`. Method will return `ContentRequest` object which will be queued to `ContentLoader` thread. We can preview progress by checking `ItemsLoaded` and `ItemsToLoad` values. We can check if it's currently processed with `Processing` value and if it's done with `Finished` value (more details in chapter about `ThreadRequestSystem`).

While it's suggested to do package loading on separated thread, we can also call `ContentLoader.loadPackage(ContentManager.getContentPackage(id))` to do it on the main thread.

## Item unloading

Item unloading removes loaded items from the memory if they weren't used (method `Get` wasn't called) for a set amount of time. This feature can be turned off and time after which item gets removed can be set in configuration. Settings `NeverUnload` to `true`, in item definition, prevents unloading.

## Modding

1. If file specified by `FilePath` exists inside active mod, it will be loaded from the mod (every next mod overrides "final" path from which the item gets loaded). Note that it only checks for the main file, but the path gets changed for all of them - so in case of multiple files per item (e.g. `SpineAsset`) mods should contain all of them.
2. New items can be added by creating `content-items.json` file with definitions. New items should have unique id or they will override already existing items.
3. Already existing items can be overrided but this will most likely break the game (it can be used for preventing unload or "forcing" certain item to be loaded from other directory though)
4. Items added by mods[2] can be overrided by another mods[1] - as long as overriding mod is loaded **after** one that added the item.
5. Packages work in a same way that items - if package with same id is already existing, it will be overrided, otherwise a new package will be created.

## Related configuration

```ini
[Content]
ContentItemsJsonPath=content-items.json
ContentPackagesJsonPath=content-packages.json
ContentUnloadActive=true
ContentUnloadTime=300000
```
- `ContentItemsJsonPath` / `ContentPackagesJsonPath` - paths to item/package json files, relative to mod folder
- `ContentUnloadActive` - if set to `false`, item unloading feature will be deactivated and items will not unload on their own
- `ContentUnloadTime` - time in miliseconds after which item gets unloaded (as long as previous property is set to `true`)

## Adding new item type

To add new type follow the next steps:

1. Add new enum value in `ContentItemType.cs`
2. In `ContentItem.cs` method `load()`: add new case for your item type. Item object should be placed in `item` field. Remember that type of object you put there, will be used as `T` in `ContentManager.Get<T>`
3. If your items need special unloading, implement `IDisposable` interface on its object (`Dispose()` will be called automatically).
4. If under some specific circumstances your item should not be unloaded (and doesn't call `Get` to prevent it), add a specific case to `unload()` method (that's not really a nice way, but it's also not common thing ;) )
