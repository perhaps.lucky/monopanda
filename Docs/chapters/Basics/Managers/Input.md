# Input

## Introduction

Inputs are handled by `InputManager` class. All necessary methods are static. Each of buttons handles states like Pressed, Held, Released. States are updated in main game loop.

Most of methods contains `context` parameter that is by default set to `null` - it should be only changed to different value when used inside UI object class. It is used as a way to block game inputs when for example writing inside text box. More about it in UI chapter.

## Keyboard Input

Inputs for keyboards are defined in keybinds json file. There is a separate file that stores default settings and one that can be used to store user settings. In project we only edit default keybindings. Location of files can be configured.

Definition for single keybinding looks like:
```json
{
	"Id":"example-input-1",
	"Key":"A"
}
```
- `Id` will be used everywhere we want to check this input state (it is suggested to place ids inside some class with constants)
- `Key` is enum name from [MonoGame Keys](https://www.monogame.net/docs-tmp/api/Microsoft.Xna.Framework.Input.Keys.html)

Keyboard inputs can be checked by calling `InputManager.GetKeyInput(id)`. Returned object contains three properties:
- `IsPressed` - value `true` means that button got pressed in last update (becomes `false` if button is held in next update)
- `IsHeld` - value `true` means that button is down (either just pressed or isn't up yet)
- `IsRelease` - value `true` means that button got released in last update

Additionally there is a method `IsHeldDelay(int delay)` - this method will return `true` every [`delay`] milliseconds. Method returns `true` only once per update, so it can be used in a single place only (for additional places, you can easily build mechanism like that with use of `RepeatingTimer`).

### User keybinds
To change user keybind we can use method `InputManager.SetUserKeybind(string id, Keys key)`. Change will be made on runtime and will not be saved automatically.

To save user keybinds, we should call `InputManager.SaveUserKeybinds()`. This will create a file for user settings, which will be loaded on the next startup.

We can also use `InputManager.RestoreDefaultKeybinds()` to restore settings from default keybinds json. Changes will not be saved on their own.

### Keyboard states

You can get keyboard states for current and previous update, using `GetCurrentKeyboardState()` and `GetOldKeyboardState()`. Both methods return [KeyboardState from MonoGame](http://rbwhitaker.wikidot.com/basic-keyboard-input).

## Mouse Input

Mouse inputs can be read using methods:
- `GetLeftMouseButton()`
- `GetRightMouseButton()`
- `GetMiddleMouseButton()`

Each of them returns related object with states (just like with keyboard).

To read mouse position **inside the window**, use `InputManager.GetMouseWindowPosition()`. For world position(if you are using ECS and camera movement), you should call `ECS.Camera.ScreenToWorld(InputManager.GetMouseWindowPosition())` - ECS is available in many places like system or entity objects.

Methods `ScrolledUp()` and `ScrolledDown()` will return `true` if user scrolled in named direction.

## Related configuration
```ini
[Input]
DefaultKeybindsJson=Content/MonoPanda/keybinds_default.json
UserKeybindsJson=Content/MonoPanda/keybinds.json
```
Configuration allows developer to set location of json files for default and user keyboard bindings json files.

## Modding

Only way to add new keybindings is to edit json files (both default and user files must be included) which are game-level. It is up to game design to give purpose for buttons though (e.g. there is nothing stopping developer from storing id of used keybinds as property of some modable object).
