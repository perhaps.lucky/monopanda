# Languages

## Introduction

Language allows simple translation by adding new ini file containing mapping from id to translated text. 

All files must be located in folder specified by configuration. Name of file (without extension) is used as identification of language (important for modding).

## Using language

Let's say we defined a single line in file named `en.ini` inside languages directory:
```ini
example=This is text loaded from en.ini file
```
We can then use it value in any place by calling `Language.Get("example")` (it's suggested to put id as const in special class).

Translation will be loaded from currently active language. If given id is missing, its id will be returned instead. If language got set and then file got removed, configured default language will be loaded instead.

## Changing language

On startup the default/active language will be loaded.

You can change the language later on by calling `Language.LoadLanguage(id)` where `id` is name of language file.

You can get list of all available `ids` using `Language.GetLanguagesNames()`. Returned values can be used in `Settings.Language.Language` to set user language for next startup (more about it in [Configuration and user settings chapter](../Configuration%20and%20user%20settings.md)).

## Related configuration

```ini
[Language]
LanguagesDirectory=Languages/
Language=en
```
- `LanguagesDirectory` - directory **inside mod folder** which will be scanned for language files
- `Language` - default language or - in case of user settings - currently active language

## Modding

1. Mods can add new languages or "fix" existing one. Each line with already existing id will be overrided.
2. Mods should always provide default language of the game. If that one is missing and user has different language than one provided by mod, nothing will be loaded. If default language is provided and user is using language that isn't in mod, default language file will be loaded.
3. If mod adds new lines with language that doesn't exist in previously loaded mods, the language will be added to the list of available languages, but loading in previously loaded mods will load the default language instead. User can workaround that by copying their desired "2nd" language under name of "1st" language.

## Modding - examples

In these examples, `en` is the default language.

#### 1. Mod changes already existing line:
- Mod_1/Language/en.ini:
```ini
test=mod1
```
- Mod_2/Language/en.ini:
```ini
test=mod2
```
`Get("test")` returns `"mod2"`.

#### 2. Mod adds new translation to existing line
- Mod_1/Language/en.ini:
```ini
test=mod1
```
- Mod_2/Language/en_but_better.ini:
```ini
test=mod2
```
If user language is set to `en_but_better`, `Get("test")` returns `"mod2"`. If user language is set to `en`, `Get("test")` returns "`mod1`".

#### 3. Mod with new line doesn't provide default language:
- Mod_3/Language/en_different.ini:
```ini
test_2=mod3
```
If user language is set to `en_but_better` (which is not provided by `Mod_3`), engine will try to load default language `en`. Because it is not provided, no line will be loaded to engine memory and calling `Get("test_2")` will return `"test_2"` (and log a warning).

#### 4. Mod provides default language:
- Mod_3/Language/en_different.ini:
```ini
test_2=mod3
```
- Mod_3/Language/en.ini:
```ini
test_2=mod3_en
```
If user language is set to `en_but_better` (which is not provided by `Mod_3`), engine will try to load default language `en`. Engine will load the language and calling `Get("test_2")` will return `"mod3_en"`.

#### 5. Mod adds new language which doesn't exist in previously loaded mod:
- Mod_3/Language/en_different.ini:
```ini
test_2=mod3
```
- Mod_3/Language/en.ini:
```ini
test_2=mod3_en
```
- Mod_4/Language/en_the_best.ini:
```ini
test_3=mod4
```
If user language is set to `en_different` calling `Get("test_2")` will return `"mod3"`. However, after changing language to `en_the_best`, `Mod_3` will use default language instead (as `en_the_best` is not provided there), so calling `Get("test_2")` will return `"mod3_en"`. If user desires to use `en_different` instead, they can achieve that by renaming `Mod_3/Language/en_different.ini` to `en_the_best.ini`.
