# Mods

## Introduction

MonoPanda is designed to allow easy creation of mods. Every content item, system, parameter or entity prototype can be edited by adding mod. As long as developer sticks to using some classes / patterns, modders should not have problem to add their changes.

Adding/modyfing code is currently not possible, but might be added using libraries like [Harmony](https://github.com/pardeike/Harmony). You can also create commands that will be called within objects, allowing easy script language.

## Mod loading

Mods are loaded in order stored in json file, by default: `Content/MonoPanda/mods.json`. File should be left empty in project, as it gets edited on user side. 

By default, game engine will scan for new mods at startup (mods are identified by having their own folder in `Content/Mods/` with json file that contains informations like full name or description. Within the engine, mods are identified by their folder name). New mods will be added at bottom of the list and will be deactivated by default. During startup, engine will perform mod loading for globally shared classes like `ContentManager` or `Language`. A specific classes like `EntityFactory` or `Repository` will perform mod loading on their initialization.

In most of scenarios, engine will perform the same set of action for each mod:
1. Check if object that is being added by mod is already existing(loaded by another mod).
2. If no, add new object.
3. If yes, override existing object or its parameters.

#### Please refer to documentation of specific classes in order to understand how each of them performs mod loading.

## Core mod

Core mod is a mod with name specified by `CoreModFolderName` in config. This is a main mod, that contains content for the base game. It will not be listed in `mods.json` file and it will always be active and loaded before other mods.

## ContentManager class

List of useful methods from this class. All methods are static.

### List<ModSettings> GetAllMods()

Returns list of all mods (does not include core mod). `ModSettings` objects contain two fields:
- `Name` - string - id of the mod / folder name
- `Active` - bool - information if mod is currently activated.

Note that these information might not reflect current state of game, if changes were saved without performing restart.

### List<ModSettings> GetActiveMods(bool includeCoreMod = false)

Returns list of currently activated mods. Used mostly in initialization of classes that load content from several mods (in that case, `includeCoreMod` should be `true`). Core mod will be returned as first on the list.

### SaveMods() and SaveMods(List<ModSettings> modSettings)

These methods save changes on the mod list to the file. In case of first one, saved list will be the one that is stored in instance of `ContentManager` ( aka one returned by `GetAllMods()`). In case of second one, list stored in class will be replaced by one provided by the parameter.

Save does not reload mods. For that, a restart will be necessary (although some of changes might be reloaded).

### List<ModSettings> ScanForNewMods(bool save = false)

Scans for new mods and adds them to the list of mods stored in instance of `ContentManager`. If `save` is set to true, list will be saved to the file. Method returns list with newly found mods.

This can be used to allow player to scan for new mods (and activate them) without turning game off and on.

### RemoveDeletedMods()

This method removes mods that no longer exist in folder structure. Similary to previous method, this can be used to update list without need for restart (although removing mod with game turned on might cause unexpected errors if loading will be performed afterwards).

### ModInfo GetModInfo(string modName)

Returns detailed mod info for mod with specified name. All informations are optional. Current fields are: `FullName`, `Description`, `Author`, `Mail`, `Website`, `Image`, `ReadmePath`, `InstallScriptPath` and `UninstallScriptPath`.

`Image` should be an image encoded to base64 string. In Mod Manager the maximum (visible) resolution of image is 300x150.

`ReadmePath`, `InstallScriptPath` and `UninstallScriptPath` should be paths refering to mod directory. More about them in next chapter below.

## MonoPanda Mod Manager

MonoPanda Mod Manager is a separe windows form application, that allows user to modify their mod list outside the game.

Mod Manager allows moving mods up/down the list, activating and deactivating them and reading their description from `mod-info.json` file. List can be reloaded from inside the program to update to external changes (e.g. mod got deleted from directory - reloading will delete mod from the list).

Mod Manager uses same configuration as one used by game engine. For that reason, executable file should be placed in main directory of the game. In case of different directory (or ini configuration having changed path), path to configuration can be changed by editing const value in `Configuration.cs` file. Other than that, Mod Manager project does not require additional changes and one executable should be easily portable between project of same structure.

Mod Manager allows creating packed mod files. Actually they are simple zip files with exactly same content as the mod directory. Using this button ensures that it will be packed correctly (e.g. directly in archive, not additional folders etc). Aside from that there is nothing special in that function.

Mod Manager allows installation and uninstallation of mods. 
- Installation will ask user to point the zip file that then will be unpacked in mod folder with the same name. If directory already exists, it will be first fully removed and recreated. After unpacking, if `mod-info.json` defines `InstallScriptPath`, it will be executed. If installation script process returns different code than `0` (success), mod will be uninstalled. Otherwise, mod installation will be completed. If `ReadmePath` is provided in `mod-info.json`, readme file will be automatically opened.
- Uninstallation will ensure user wants to delete the mod and entire directory. First, the `UninstallScriptPath` from `mod-info.json` will be executed (if provided). If process returns error value, uninstallation will not be continued. Otherwise, entire mod folder will be deleted and removed from the mods list.

Install script can be a batch file, used to make external changes (outside the mod directory). In most of cases it should not be needed. Uninstall script should revert these changes, and clear everything that might corrupt the game later on.

## Configs
``` ini
[Mods]
ModsFolder=Content/Mods/
CoreModFolderName=_Main
ModsSettingsJson=Content/MonoPanda/mods.json
ModInfoJson=mod-info.json
```
Most of these got already mentioned in text above.
- *ModsFolder* - path to folder with all of mods.
- *CoreModFolderName* - name of the core mod folder / id of core mod in engine.
- *ModsSettingsJson* - path to json file where list with mods is stored.
- *ModInfoJson* - name of the json file (inside mod folder) with detailed informations about the mod.




