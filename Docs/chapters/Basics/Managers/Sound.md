# Sound

## Introduction

MonoPanda uses basic MonoGame sfx/music methods, wrapped around in sound manager.

Music files are loaded with content manager. There are two content types:
- `SoundEffect` - usually in wav format, it's a sound effect played in the world (3D or not). Many sound effects can be played at once.
- `Song` - ogg/mp3, music played in the background. Only one song can be played at once.

## Sound volume dictionary

Sound volume dictionary is a dictionary used to adjust volume of individual SFX and Song files. Each key in dictionary is ID of sound loaded in game content. Each value is a value higher than 0, that shows percentage by which the in-game volume will be changed (e.g. 100 is sound playing at its normal volume, 50 at its half, 200 is two times louder).

By default file is located inside mod folder and named `sound-volume.json`. This can be changed in engine configuration.

## Playing sound

To play a sound effect, we call `SoundManager.PlaySFX(id)` with id from Content Manager. Method returns SoundEffectInstance, which can be used to manage the sound later on.

Similary, we call `SoundManager.PlayMusic(id)` to play music. This method does not return any object. By default, the music is looped, which can be changed by changing second method parameter to `false` (`SoundManager.PlayMusic(id, false)`). Music can be stopped using `SoundManager.StopMusic()`. You can get id of currently played song with `SoundManager.GetCurrentlyPlayedSongId()`.

## Global volume

Global volume is handled separately for music and sound effects. Both values are loaded into settings at the start of game. Values can be changed with SoundManager using methods:
- `SetSFXVolume(volume)`
- `SetMusicVolume(volume)`
- `AdjustSFXVolume(value)`
- `AdjustMusicVolume(value)`

First two methods change volume to provided value, while other two are changing them by that value. Values are changed in Settings class, but they are not saved - meaning that call to `Settings.save()` needs to be performed in order to restore new values after restarting the game.

## Modding

1. Music and sfx can be overrided like any other item in Content Manager. Refer to Content chapter.
2. Individual sound volume can be changed by creating sound volume dictionary inside mod folder. Existing keys will be updated, new keys will be added.

## Related configuration


```ini
[Sound]
SoundVolumeJsonPath=sound-volume.json
SFXVolume=100
MusicVolume=100
```
- `SoundVolumeJsonPath` - path to file with sound volume dictionary, relative to mod folder
- `SFXVolume` / `MusicVolume` - default value for sound effect and music volume. It's used only on the first startup to create default values in user settings.

