# State Manager

## Introduction

State manager is responsible for switching and handling different states of game. States of game are a way to divide different parts of game into separated classes. Good examples of different states are: main menu, select level screen, game, credits - generally anything that has unique mechanic that isn't related to other part of it. In most of cases this should not be used to handle different levels of game - however, it can be.

## States json map

States are initially loaded from map json file (by default `Content/MonoPanda/states.json`). Keys in map are later on used as `ids` of states. Values in the map are full names of classes, e.g. `Example.States.ExampleStatesMainState` that extends `State` class. State objects are loaded into list of states at initialization of engine.

## Changining state

Changing state can be triggered by calling `StateManager.SetActiveState(id,terminateCurrentState,reinitialize)`:

- `id` is obviously, id of state that should be loaded next
- `terminateCurrentState` is a boolean value (defaults to `false`); if it's set to `true`, currently active state will be terminated
- `reinitialize` is also a boolean value (defaults to `false`); if it's set to `true`, state with `id` will be reinitialized before engine will switch to it

You can also initialize state in separated thread, using `StateManager.PrepareState(id, reinitialize)` method.

## State lifecycle

State has 3 different lifecycle states:
- **Uninitialized** - state wasn't loaded yet or got terminated by setting `terminatCurrentState` to `true` - next time it will be loaded, it will first be initialized
- **Initialized** - state is initialized, engine can freely switch back and forth to/from this state
- **Active** - only one state can be active at once - engine will call `Update` and `Draw` only for that state; there is **always one active state**.

## State class

There are four methods that should be implemented in each State class:
- `Initialize()` - this method will be called once at the initialization of state, and will not get called again until state is terminate
- `Terminate()` - this is the opposite, it will be called on the termination of state
- `Update()` and `Draw()` - these are basic monogame loop method, only difference is that they no longer have `GameTime` parameter - it got moved to `Time` class instead.

## Related Configuration

```ini
[States]
StatesJsonPath=Content/MonoPanda/states.json
InitialStateId=example-pathfinding
```
- `StatesJsonPath` - path to json with map of states
- `InitialStateId` - this state will be initialized at the start of the game
