# Repositories

## Introduction

Repositories are idea similar in a way to entity factories. In case of repositories, we don't store models for entities, but rather any data models we want to. They are still modable with similar logic.

Note that on json level, repositories are storing dictionaries from string to object, rather than objects. Nested objects will not work correctly, so you should mostly store basic data types (or you can use Parameter Conversion Attributes - they work here just the same way as in Entity Factories / Entity Component System - read more about them in [Entity Component System chapter](../Entity%20Component%20System.md).

## How-to
In case of repositories we have two abstract classes that we need to extend.

### RepositoryEntity 
Class present one single object stored inside repository. Object is identified by field `Id`.

Aside from `Id`, there is a virtual method `Initialize()` that can be overrided. Method runs after repository is initialized.

In Spine Example, we have a repository of available fur colors, the object in json goes like:
```json
{
    "Id":"001",
    "Label":"1",
    "Color":"242, 172, 12"
}
```
and class:
```csharp
public class ColorEntity : RepositoryEntity, ISelectionItem {
    public string Label { get; set; }
    [ColorParameter]
    public Color Color { get; set; }

    public string GetLabel() => Label;
}
```
`Color` field is converted using `ColorParameter` attribute.

### Repository<T>
`T extends RepositoryEntity`

Repository extended class will have to implement `void setInstance(Repository<T> repository)` method. The purpose is to store the instance in static field inside the new class.

Repository needs to be initialized using static method CreateRepository<R>(string jsonPath) (where R extends Repository), for example: `ColorsRepository.CreateRepository<ColorsRepository>("Spine_Example/colors.json");` (yes looks slighty weird, but `ColorsRepository.CreateRepository` refers to `Repository.CreateRepository` method).

Repository extended class will have access to `entities` list. It's up to that class to define the way the entities should be accessed. It's suggested to define static methods for easy access later on. For example:
```csharp
public static List<ColorEntity> GetAll() {
      return instance.entities;
    }

public static ColorEntity Get(string id) {
    return instance[id];
}

public static int Amount() {
    return instance.entities.Count;
}

public static ColorEntity Get(int i) {
    return instance.entities[i];
}
```

## Modding

For all active mods that have same json file (path to json in `CreateRepository` is relative to mod folder):
1. If entity with same `Id` was loaded before, each of present fields will replace the old value.
2. If entity with same `Id` doesn't exist, it will be simply added.
