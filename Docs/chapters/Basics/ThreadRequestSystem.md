# ThreadRequestSystem

## Introduction

`ThreadRequestSystem` is an abstract class that manages queueing and resolving requests. An example of implementation of this class is `ContentLoader`


## Example is better than theory

A good example is `PathfindingSystem`. It does a lot of crazy math that we want to be done in the background, without slowing down the main thread. A class on it's own has a lot of logic but most important there is the overrided `Process(ThreadRequest request)` method:
```csharp
    protected override void Process(ThreadRequest request) {
      if (request is PathfindingRequest) {
        // pathfinding logic
        var pathfindingRequest = request as PathfindingRequest;
        SearchInstance searchInstance = new SearchInstance(pathfindingRequest.Start, pathfindingRequest.Target, this, heuristic);
        searchInstance.Search();

        // saving results to the request
        pathfindingRequest.FoundPath = searchInstance.FoundPath;
        pathfindingRequest.Path.Clear();
        foreach (int nodeID in searchInstance.Path) {
          pathfindingRequest.Path.Add(GetNode(nodeID));
        }
      }
    }
```
In this example we first check if request we received is indeed the type this system should handle. Then there is the logic part and we save the results back to the `ThreadRequest` object.

`ThreadRequest` is another class that has 3 boolean fields that describe the state of request:
- `InQueue` - request got queued to `ThreadRequestSystem` using `QueueRequest` method
- `Processing` - request is currently handled (`Process` method is ran at this moment)
- `Finished` - `Process` method finished its working, request is no longer in system and it's ready to be read

In case of `PathfindingRequest`, aside from these fields we get data like starting and target point, plus fields that will keep the results after pathfinding is done.

### So how to use these?

A full example of pathfinding can be found in `Example.PathfindExample` package. Let's focus on multithreading part.

In `TileSystem` we create an instance of `PathfindSystem`. The `PathfindRequest` object is stored inside `PawnComponent`. There is a single "main" `PawnComporent` stored in field of `TileSystem`.

In `Update` method, system checks for left mouse click, after which current and target tile is stored inside the `PathfindRequest` and queued afterwards:
```csharp
          pawn.PathfindingRequest.Target = component.PathfindingNode;
          pawn.PathfindingRequest.Start = findStartTile(pawn).PathfindingNode;
          PathfindingSystem.QueueRequest(pawn.PathfindingRequest);
```

Also in `Update` method, the `readPath` method is called:
```csharp
    private void readPath() {
      var pathfindingRequest = pawn.PathfindingRequest;
      // check if pathfinding request is finished
      if (pathfindingRequest.Finished
        && readPathFlag.Check()) {
        
        (...) // do stuff with results
        
      }
    }
```
In this part of `Update`, we check if pathfinding request of "main" `PawnComponent` is finished. If yes, we use data from the result to perform next actions on normal `Update` route.

In other words we create requests, queue them and then keep on checking on them each `Update` if they are done already. And that's it. Probably not the best of solutions out there, but it's simple to use.
