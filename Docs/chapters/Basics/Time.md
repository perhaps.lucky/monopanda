# Time

## Introduction

Time is a single class that main purpose is to simplify working with MonoGame `GameTime`. Main target was to remove `GameTime` parameter from `Draw` and `Update` calls in other classes (like States or EntitySystems).

## Static parameters

- `CurrentGameTime` - a GameTime object passed from main MonoGame `Update` call.
- `ElapsedMillis` - a shortcut to `CurrentGameTime.ElapsedGameTime.TotalMilliseconds`, returns number of milliseconds that passed since last `Update` call.
- `ElapsedCalculated` - number of milliseconds that passed since last `Update` multiplied by configurable multiplier (by default it's set to `0.001`, meaning that this value will return how many seconds passed since last `Update` call). Usage example:

`position += new Vector2(100, 0) * Time.ElapsedCalculated;` - this will change `position` at speed of 100 pixels per second.

- `WorldTimeSpeed` - defaults to `1.0f`, this value is used in various systems, changing it can effectively slow the world (as long as it is used in new systems as well). While slightly unrelated, it feels like right place to put that value.
- `TotalElapsedMillis` - shortcut to `gameTime.TotalGameTime.TotalMilliseconds`

## TimeComponent
(ECS Stuff)

Similar to `WorldTimeSpeed`, if change-in-time happens in reference to some entity, we can instead use `EntityTimeSpeed` method. Method takes either `Entity` or `EntityComponent` as the parameter. The return value is affect by two factors:
- `WorldTimeSpeed`
- `TimeMultiplier` from `TimeComponent` (if entity doesn't have `TimeComponent`, this value defaults to `1.0f`)

## Usage

Generally references to time should happen everywhere where time matters, e.g. object movement. We can skip it if `IsFixedTimeStep` is set to `true` - in that case, MonoGame will force amount of updates happening in one second to always be the same. That might however cause fps drops when engine is trying to catch up with number of updates before `Draw` call.

While using `IsFixedTimeStep=false`, number of updates and draws will be the same, which might cause them to happen slower or faster depending on cpu abilities and load at the time. As such, the value stored in `GameTime` object will be different each time. We can use that value to adjust the changes in game to time that passed since last update. 

### Example:

Object should move at speed of 100 pixels each second, `position += new Vector2(100, 0) * Time.ElapsedCalculated;` was used:
- `Update` took 1 second and object was moved by 100 pixels (`Time.ElapsedCalculated` was equal to 1)
- `Update` took 2 seconds and object was moved by 200 pixels (`Time.ElapsedCalculated` was equal to 2)

In case of using `IsFixedTimeStep=true`, the above would cause game to start catching up with a missing update that happened in second case (assuming update was set to happen once per second). In that case `Time.ElapsedCalculated` had value equal 1 in each of updates.

## Related configuration
```ini
[Time]
ElapsedCalculatedMultiplier=0.001
```
It's suggedsted to not change the above value.
