# Entity Component System

## Introduction

Entity Component System is one of design widely used in games. Some people hate it, some people love it. I will not go here in details and discussion, feel free to google it :)

Very basic concept goes like this: `EntityComponentSystem` contais many `Entities` and `Systems`. Each of `Systems` has its purpose, most likely managing `Components`. `Components` are like parts of `Entity` - each of them can have many of them, then can be freely added and removed on the run. `Components` are also storing all necessary data.

Aside from that there are some helper modules like `Camera` or `Observers`.

## Creating Entity Component System

Suggested way to create an Entity Component System is to use the static method `EntityComponentSystem.LoadFromJson(jsonPath)` - aside from moving all systems creation to separate file, it allows the modability.

An example of json file looks like this:
```json
[
    {
        "ClassName": "MonoPanda.Systems.DrawSystem,
        "UpdateInterval": 0,
        "Parameters": {
            "Active": true
            "BlendState": "AlphaBlend",
            "SortMode": "BackToFront"
        }
    }
]
```
In human-words: it should contain a list with objects that describe each of systems that should be created. Each system is described by the fields:
- `ClassName` - a full name of system class (extends `EntitySystem`)
- `UpdateInterval` - optional, if it's higher than 0, a `RepeatingTimer` will be initialized, causing the `Update` methods run every `[UpdateInterval]` milliseconds instead of running every frame
- `Parameters` - parameters is a map of parameters specific of each system - they can modify how system works. Additionaly every system has an `Active` field, allowing for deactivation of system (by mod or on the run)

### Modding

Path to json in `EntityComponentSystem.LoadFromJson(jsonPath)` is relative to mod folder. Other active mods will be loaded with these rules:

1. If system is already loaded (by core mod or another mod), parameters of the system will get replaced by new values (not present values will remain as they were loaded before). If `UpdateInterval` is present and higher than `0`, it will replace `UpdateInterval` value (changing down to `0` is not possible, but value like `1` should give similar effect anyway).
2. If system is not loaded, it will be added to the list (mods that are further in loading order will be able to replace its parameters)


## EntityComponentSystem methods

- `CreateEntity(string id, Vector2 position)` - creates new empty entity in system and returns it
- `CreateEntity(Vector2 position)` - same as above, id becomes a number
- `DestroyEntity(string id)` - destroys entity by `id`, returns `true` if entity was found and destroyed
- `DestroyEntity(Entity entity)` - as above, but takes parameter object instead, returns `true` or throws exception - technically it can destroy entity that belongs to another `EntityComponentSystem` as well
- `GetEntity(string id)` - now that's obvious and chill compared to last one (returns null if not found)
- `GetEntityOptional(string id)` - as above, just wrapped in an [Optional](../Other%20features/Optional.md)
- `GetEntityByPrefix(string prefix)` - returns list of entities that `Id` starts with prefix
- `AddSystem(EntitySystem entitySystem)` - adds the system (system object should already have all parameters set, as he is going straight to the frontline)
- `GetSystem<T>()` - T is class that extends `EntitySystem`, method returns object of that class that belongs to the system (or null if not found)
- `GetSystemOptional<T>()` - as above, just wrapped in an [Optional](../Other%20features/Optional.md)
- `GetSystemByIdentifier(string identifier)` - returns system by it's `ShortIdentifier` (by default short class name)
- `GetAllSystems()` - returns list with all systems
- `RemoveSystem<T>()` - removes system, calls `entitySystem.Destroy()`
- `Update()` - this should be called in `State` `Update` or whereever it's used (aside from calling `Update` for each of systems, it also calls `Update` on `InnerEntityUpdateSystem` - more about it later)
- `Draw()` - same but this is `Draw`
- `Destroy()` - calls destroy on every `EntitySystem` and every `Entity` in the object
- `GetAllEntities()` - returns list with all entities, *try not to iterate through this, it might be big*
- `GetAllComponents<T>()` - retuns list of one type of `EntityComponent`, these list are cached by different operations

These methods should probably not be used outside MonoPanda code:
- `RemoveEntity(Entity entity)` - this only **removes** entity from the system, it is called in `Destroy()` method of `Entity` object (so effectively in previous two methods as well). As it only removes `Entity`, the `EntityComponents` that belong to it will still remain in the system. However, every `EntitySystem` will still call `OnEntityDestroy(entity)`. *Better just ignore its existence...*
- `ComponentAdded(EntityComponent component)` - called from `Entity` after adding new component, adds component to component lists cache and calls `system.OnComponentAdd(component)`
- `ComponentRemoved(EntityComponent component)` - called from `Entity` after removing component or during `Destroy()` call, removes component from cache and calls `system.OnComponentRemove(component)`


## EntitySystem

Main purpose of classes that extend abstract `EntitySystem` is to manage `EnityComponent` and `Entity` objects. 

It's important to create a constructor with two parameters: `(string id, int updateInterval = -1)` as these parameters will be used in creation of entity system from json file (MonoPanda uses reflection for that). An example of such constructor:
```csharp
    public SoundSystem(string id, int updateInterval = -1) : base(id, updateInterval) {}
```


Virtual methods that can (or should) be overrided are:
- `Update()` - called on every frame or every `UpdateTimer` returns `true` (related to `UpdateInterval` parameter above)
- `Draw()` - called on every frame (timer doesn't affect it because it is `Draw` after all - it **should** work on every frame)
- `OnComponentAdd(EntityComponent component)` - as it says, added when component is added to entity
- `OnComponentRemove(EntityComponent component)`
- `OnEntityCreate(Entity entity)`
- `OnEntityDestroy(Entity entity)`
- `Destroy()` - called in `RemoveSystem<T>()` and `Destroy()` in `EntityComponentSystem`
- `Initialize(Dictionary<string, object> parameters)` - called when system is created from json, this method should be used to apply parameters from json, that cannot be directly converted


## Entity

`Entity` is basically every object that is used in the game. Each `Entity` can have several `EntityComponents`. `Entity` object has some properties, often used inside component-related methods and systems.

Properties of `Entity`:
- `Id` - id of entity
- `Position` - position of the object in the world
- `Scale` - scale of the object
- `Rotation` - rotation of the object
- `IsInVisibleArea` - is the object visible on the screen (handled by `InnerEntityUpdateSystem`)
- `IsDestroyed` - if `true`, it means that object was destroyed and is no longer a part of `EntityComponentSystem` (it can remain in memory if for example it was stored in private field of some other object)
- `EntityComponentSystem` - reference to ECS that object is a part of

**Important**: `Entity` should be created by calling `CreateEntity` method on `EntityComponentSystem` object! 

Methods of `Entity` class:
- `Destroy()` - calls `Destroy()` on each of components and removes entity from ECS
- `AddComponent(EntityComponent component)` - adds component to entity
- `RemoveComponent<T>()` - T extends `EntityComponent` - removes component of given class
- `RemoveComponent(EntityComponent component) - removes component by object (use if multiple components of same class are present)
- `HasComponent<T>()` - returns `true` if component is present
- `GetComponent<T>()` - returns component of given class
- `GetComponentOptional<T>()` - returns [Optional](../Other%20features/Optional.md) of component
- `GetComponentByIdentifier(string identifier)` - returns component by its `ShortIdentifier` (by default it's class name)
- `GetAllComponents<T>()` - returns list of all components of given class
- `GetAllComponents()` - returns copy of components list
- `GetComponentById<T>(string id)` - returns component by class and id (useful with multiple components of same class)
- `RegisterObserver(EntityObserver observer)` - registers `EntityObserver`
- `UnregisterObserver(EntityObserver observer)` - unregisters `EntityObserver`
- `PostInit()` - calls `PostInit()` method in every component (this method is called automatically if entity was created with factory)

## EntityComponent

`EntityComponent` is a part of `Entity` that indicates it should be affected by certain actions caused by one or more of `EntitySystems`. It also stores all data specific to each type of component. It also can have some own methods.

Parameters shared across component classes:
- `Active` - indicates if component is activated - changing this value also calls method `SetActive()` or `SetInactive()`
- `Entity` - reference to `Entity` object to which this component was added
- `Id` - string value that can be used if multiple components of same type are added to single entity (value should be unique within single entity)
- `TypeIdentifier` - value used for caching lists of components inside `EntityComponentSystem` class (it's set to class name when component object is added to entity)

Virtual methods that can be overrided:
- `Destroy()` - called on destruction or removal or component. **This should not be used outside Entity class!** You can dispose objects or perform other actions in this method
- `Init(Dictionary<string, object> parameters)` - called after creation of object, can be used to convert parameters from json `EntityFactory`
- `PostInit()` - called after all components are created and added to entity object, can be used to cache connections between different components
- `SetActive()` - called when property `Active` is set to `true`
- `SetInactive()` - called when property `Active` is set to `false`

## EntityFactory

`EntityFactory` is a class which purpose is to create entities based on configured prototypes. Similar to `EntityComponentSystem`, prototypes are loaded from modable json. Example:

```json
{
	"Default": {
		"Components": {
			"MonoPanda.Components.DrawComponent": {
				"ContentId": "example-square",
				"Size": 50
			}
		}
	},
	"Models": [
		{
			"Id": "controlled-entity",
			"Components": {
				"MonoPanda.Components.DrawComponent": {
					"Color": "LimeGreen",
					"Depth": 0.5
				}
			}
		},
		{
			"Id": "gravity-source",
			"Components": {
				"MonoPanda.Components.DrawComponent": {
					"Color": "Navy",
					"Depth": 0.6
				},
				"gravity_component": {
				    "ClassName": "MonoPanda.Components.GravityFieldComponent",
					"Radius": 300,
					"Power": 200
				}
			}
		}
    ]
}
```
Factory fields:
- `Default` - this model is a "base" model for every model in the factory - that means its components will be used in every of instances created by the factory
- `Models` - a list of models that can be used to create an entity
Model fields:
- `Id` - identification of model (setting it in `Default` model has no effects)
- `Components` - this is map where:
  - `key` is identification of component used for modding (stored in `ShortIdentifier` field later on), you can also put full class name instead of using `ClassName` parameter
  - `value` is a map of properties applied to created component
  
About `ClassName` parameter: You can use it when you have more than one component of a single class type or if you prefer different `ShortIdentifiers` for your components (by default they are set to short class name). The way it works is:
1) First `ClassName` parameter presence is checked. If it's there, component is created using that class name, and `ShortIdentifier` is set to value of `key`.
2) If `ClassName` parameter isn't present, component is created using value of `key`. `ShortIdentifier` is later on set to short name of used class.

Using `ClassName` parameter allows you to create more than one component of same type. Check tree in examples.

First step to create `EntityFactory` is to extend the abstract class. There is a `SetAsInstance()` method which needs to be implemented. It is a way to force behaviour in which a created object is stored in the private static field in a new class (there is no way to force child class to have such field). 
`SetAsInstance()` will be called from static method `CreateInstance<T>(string jsonPath, EntityComponentSystem ECS)` - this method should be called somewhere in the state, after `EntityComponentSystem` is created. As always, `jsonPath` parameter is relative to mod folder. Technically we can call that method on any class that extends `EntityFactory` - what's important is the `T` class.
Last thing we need is a way to create entity. Here again, since we can't force child class to have private field, we also can't refer to it from parent class. We also can't force child have to have a static method. As such, this step isn't forced at all, but it's more like suggested by the design of this class (ofc that can be always changed ;) ). What is provided is a `CreateEntityInner(string modelId, Vector2 position, string id)` method. All that needs to be created is a static method that will call it on the previously stored instance.
Full example of implementation of `EntityFactory`:
```csharp
  public class ExampleFactory : EntityFactory {

    private static ExampleFactory instance;

    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(string modelId, Vector2 position, string id = null) {
      Entity entity = instance.CreateEntityInner(modelId, position, id);
      return entity;
    }
  }
```


### Modding

Other mods will be loaded with these rules:
1. If `Default` model is present:
    - If `Default` model was loaded with one of previous mods - for each present component, each present parameter will be updated (you can "remove" component by setting `Active` to `false`)
    - If `Default` model was not loaded - `Default` model will be set to this one

2. Models on the `Models` list will be loaded in similar way.
3. After all mods are loaded in, all models are recreated by combining together `Default` model and each model from the list.



## EntityObserver

`EntityObserver` is an object registered in entity. When one of entity parameters: `Position`, `Scale`, `Rotation` changes, all of observers are invoking the action through the `Inform()` method. This is not quite observer pattern should work, but it does the job. 
It answers a design problem: we have two systems, let's use light system and draw system. Light system can update the position of light (`Light.Position = Entity.Position + Offset`), but what if another system changes the position again? We can avoid that by placing light system right above the draw system, but less buggy way is to just use `EntityObserver` which will update the `Light.Position` every time it changes.
`EntityObservers` should be registered in `Init` method of `EntityComponent`.

## Camera

`Camera` is a class used in drawing systems. The object on its own is stored in `EntityComponentSystem` field (so its available from any system and component).
Properties of `Camera`:
- `Position`
- `Zoom`
- `Origin` - used in matrix calculation, set by resolution settings
- `Rotation` - this one is buggy, but it's there
- `TransformationMatrix` - `Matrix` object, recalculated each time any of values above changes

Methods:
- `ReinitializeOrigin()` - recalculates origin based on settings, call it if you changed resolution
- `WorldToScreen(Vector2 worldPosition)` - counts position on screen (on window) based on provided position in world
- `ScreenToWorld(Vector2 screenPosition)` - the other way around
- `GetVisibleArea()` - returns `Rectangle` object that describes visible area (of the world)

## InnerEntityUpdateSystem

This class got mentioned few times on this page, but what is it exactly?

Well, it's a helper class that updates entity field `IsInVisibleArea` - it's a bit that didn't exactly fit draw system, so it's placed outside the `EntitySystem` list. Additionally it draws visiblity rectangles if `VisibilityRectangles` is set to `true` in config.

Let's just accept it, okay?

## Parameter Conversion Attributes

Additionally to `ApplyParameters` methods, parameter conversion attributes can be used. For example, if your components contains `Color` property, you can do:
```csharp
[ColorParameter]
public Color Color { get; set; }
```
With that attribute, MonoPanda will try to convert `string` stored in json to `Color` object without the need for call in `ApplyParameters` method.

Available conversion attributes are:
- `ColorParameter` - converts XNA Color name or RGB(A) string to `Color`
- `DirectionVectorParameter` - converts direction string ("Up", "Down" and so on) to `Vector2`
- `EnumParameter(enumType)` - converts string to enum value (`enumType` should be type of the enum, e.g. `[EnumParameter(typeof(SpriteSortMode)]`)
- `RectangleParameter` - converts string with 4 values to `Rectangle`, for example `"0, 0, 50, 50"` is `Rectangle` with `{X: 0, Y: 0, Width: 50, Height: 50}`
- `StaticFieldParameter(classType)` - converts string to value stored in static field of `classType` (field name is value of json string field)
- `VectorParameter` - converts string with 2 (or 1) values to `Vector`, for example `"25,30"` is `Vector` with `{X: 25, Y: 30}`; `"10"` is `{X: 10, Y: 10}`

### Adding new conversion attributes
You can create new conversion attribute by extending `ParameterConvertAttribute` class. It will require you to implement a single method `public abstract object Convert(string parameterValue)` - this method takes json string (or `.ToString()`) value and returns expected object value.