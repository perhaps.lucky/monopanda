# CollisionSystem

## Why outdated?

Replaced by new system that uses [Humper library](https://github.com/dotnet-ad/Humper).

This system has some bugs that - not gonna lie - I was lazy to fix. It still is a bit simplier in use because it doesn't require declaring "world" size (which is not a big issue at all). 

## Purpose

Updating `CollisionMovementParameters` for each of `CollisionComponents`. Update happens when flag `UpdateRequested` in `CollisionComponent` is raised - that happens when entity observer is informed.

## Parameters

None

## Related components

### **CollisionComponent**

Component contains `CollisionMovementParameters` object, which contains most important information about current and previous collision bounding box.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`BoundingRectangle`|`Width` and `Height` works as size of bounding rectangle, while `X` and `Y` are offset of center of rectangle, from entity position|`Rectangle`|"0, 0, 50, 50"


#### *Methods*

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|IsColliding|- `CollisionComponent` other<br>- `bool` precise = `true`|`bool`|Returns `true` if two `CollisionComponents` are colliding. `precise` causes it to also check movement rectangles, rather than just final position in frame.|
|IsColliding|- `Entity` entity<br>- `bool` precise = `true`|`bool`|Same as previous method. If `entity` doesn't have `CollisionComponent`, this method returns `false`|
|GetCollisionDetails|`CollisionComponent` other|`CollisionDetails`|Returns `CollisionDetails` object with more informations about collision between two `CollisionComponent` objects.|
|GetCollisionDetails|`Entity` entity|`CollisionDetails`|Same as previous method. If `entity` doesn't have `CollisionComponent`, this method returns empty `CollisionDetails` object.
|Contains|`Vector2` position|bool|Returns `true` if `position` is inside bounding box of component.|

## Related classes

### CollisionDetails

Class contains informations about collision between two objects. In constructor, first parameter is `this` rectangle, while second one is `other` rectangle. These two names are used in object fields. Constructor takes `CollisionMovementParameters`.

All fields:
- `IsColliding` - `true` if objects collided
- `ThisCollisionRectangle` - rectangle describing position of `this` bounding box at which it collided with `other` bounding box
- `OtherCollisionRectangle` - rectangle describing position of `other` bounding box at which it collided with `this` bounding box
- `ThisPreviousRectangle` - rectangle describing position of `this` bounding box in previous frame
- `OtherPreviousRectangle` - rectangle describing position of `other` bounding box in previous frame
- `CollisionDirection` - (enum value) direction of collision, e.g `Top` means that `this` object hit `other` object from top

### CollisionMovementParameters

Describes position and movement of bounding box of `CollisionComponent`.

Fields of object:
- `CurrentCollisionRectangle` - current position, as in: this frame
- `PreviousCollisionRectangle` - position in previous frame
- `NoMovement` - `true` if entity doesn't have `MoveComponent`
- `MovementRectangle` - a rectangle made from `PreviousCollisionRectangle` and `CurrentCollisionRectangle` rectangles
- `MovementDirection` - a `Vector2` that describes direction of movement
- `Distance` - distance of the movement
