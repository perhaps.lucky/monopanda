# MoveSolidCollisionSystem

## Why outdated?

Replaced by new system that uses [Humper library](https://github.com/dotnet-ad/Humper).

This system has some bugs that - not gonna lie - I was lazy to fix. It still is a bit simplier in use because it doesn't require declaring "world" size (which is not a big issue at all).

## Purpose

Checking collisions between `MoveComponents` and `SolidComponents`. Stopping `MoveComponent` from futher movement. `CollisionComponent` is necessary for collision check.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`PlatformerTopCollision`|Causes mimicking when `MoveComponent` hits `SolidComponent` from the top side|`bool`|false|

## Related components

### **MoveComponent**

Parts of [MoveSystem](../Systems/MoveSystem.md)

### **SolidComponent**

Indicates that object is a solid and cannot be passed through.