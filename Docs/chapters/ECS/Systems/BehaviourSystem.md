# BehaviourSystem

## Purpose

Performing `Update` methods on `Behaviour` classes.

Main purpose is to not create separate system for simple entity behaviours, e.g. if entity should move in circle, we can simply extend the `Behaviour` class to perform it and then apply `BehaviourComponent` with our new class.

## Related components

### **BehaviourComponent**

Component with `Behaviour` object

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|BehaviourClass|Class name of class that extends `Behaviour` which will be used for entity|classname `string`|Example.ECSExample.ExampleBehaviour
|BehaviourParameters|Parameters applied to `Behaviour`|Dictionary<string, object>|See below

Example of `BehaviourParameters`:
```json
"BehaviourParameters": {
    "Acceleration": 30,
    "DirectionChangeTime": 3000,
    "Horizontal": true
}
```

## Related classes

### Behaviour

Class contains a field `Entity`, which will be set on initialization of `BehaviourComponent`.

Using `ApplyParameters` method, we can apply unusual parameters to `Behaviour` class.

