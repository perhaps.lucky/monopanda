# CameraSystem

## Purpose

Adding smooth movement to camera, with transition between followed objects and so on.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`FollowAfterDistance`|Value of distance between camera position and expected center after which camera will start following. Defaults to `50`|`float`|100
|`FollowUntilDistance`|Value of distance between camera position and expected center until which camera will follow. Defaults to `20`|`float`|30
|`CameraSpeedMultiplier`|Multiplies camera movement speed. Defaults to `1.0`|`float`|`2.5`
|`SmoothZoomSpeed`|Speed of smooth zoom feature. Defaults to `2.0`|`float`|`1.5`
|`AutoZoomMargin`|Distance to end of visible area that triggers auto zoom out if one of followed objects crosses it. Defaults to `10`|`float`|50
|`AutoZoomSpeed`|Speed of auto zoom. Defaults to `1`|`float`|0.5


## Methods

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`GetCameraState`||`CameraState`|Returns `CameraState` object that can be used later on
|`JumpToCameraState`|`CameraState` cameraState|`void`|Instantly sets camera to `CameraState`
|`GoTo`|`Vector2` position|`void`|Creates new `FollowedStaticObject` with provided position and sets it as only followed object
|`FollowEntity`|`Entity` entity|`void`|Creates new `FollowedEntityObject` with provided entity and sets it as only followed object
|`AddFollowedObject`|`FollowedObject` o|`void`|Adds `FollowedObject` to list of followed objects. Camera will go to position in center of all objects
|`RemovedFollowedObject`|`FollowedObject` o|`void`|Removes `FollowedObject` from list of followed objects
|`SmoothZoomTo`|`float` value|`void`|Sets smooth zoom target to provided value
|`SmoothZoomIn`|`float` value|`void`|Sets smooth zoom to zoom in by provided value
|`SmoothZoomOut`|`float` value|`void`|Sets smooth zoom to zoom out by provided value

## Related classes

### FollowedObject

Abstract class that provides three methods to camera system: `GetPosition()`, `GetSize()`, `GetVelocity()`. All methods return `Vector2` values that are used to handle `CameraSystem` features.

### FollowedStaticObject

Extends `FollowedObject`. Takes one parameter: `Vector2 position`. Size and Velocity of objects are always `Vector2.Zero`.

### FollowedEntityObject

Extends `FollowedObject`. Takes one parameter: `Entity entity`. Position is value of `entity.Position`. Size is `Size` value from `DrawComponent` - if not present at the moment of creation, then it's `Vector2.Zero`. Velocity is `Velocity` value from `MoveComponent` - if not present, then it's `Vector2.Zero` - this value is updated dynamically.

### FollowedCursorObject

Extends `FollowedObject`. Takes three parameters: `Camera camera`, `Entity rangeLimitEntity = null`, `float rangeLimit = 300f`. Position is value of the cursor position in the world. If `rangeLimitEntity` is provided and distance between entity and cursor position in the world is greater than `rangeLimit`, returned position will be calculated from direction between entity and cursor and `rangeLimit` value. Size and Velocity are always `Vector2.Zero`.

### CameraState

Contains two values that describe the state of camera: `Vector2 Position` and `float Zoom`.
