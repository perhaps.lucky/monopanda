# CollisionSystem

## Purpose

Updating position of each collision box in world object from [Humper library](https://github.com/dotnet-ad/Humper). Reflecting updated position from world object to entity position. Handling collisions between `MoveComponent` and `SolidComponent`.


## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`WorldBounds`|A rectangle representing bounds of virtual Humper world object. Write only value.|`string` rectangle|`"-200,-200,400,400"`|
|`CellSize`|Size of cell in Humper world. Defaults to `64F`|`float`|`32.0`
|`WorldOffset`|`Vector2` used to calculate relation between objects in Humper world and ECS. Read only|`Vector2`|
|`WorldSize`|`Vector2` representing world size. Read only, used in methods|`Vector2`


## Methods

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`RecreateWorld`|- `Vector2` offset<br>- `Vector2` size<br>- `float` cellSize|`void`| Creates new world and removes `HumperBox` data from all `CollisionComponents`. Boxes will be recreated on the next update.
|`RecreateWorld`|- `Vector2` offset<br>- `Vector2` size|`void`|Uses stored `CellSize`
|`RecreateWorld`|- `Vector2` offset|`void`|Uses stored `WorldSize` and `CellSize`
|`RemoveBox`|- `IBox` box|`bool`|Calls `Remove` method from `World` class 

## Related components

### **CollisionComponent**

Component contains `IBox` object, which is a reflection of the entity in Humper world.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`BoundingRectangle`|`Width` and `Height` works as size of bounding rectangle, while `X` and `Y` are offset of center of rectangle, from entity position|`Rectangle`|"0, 0, 50, 50"


#### *Methods*

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`IsColliding`|`CollisionComponent` other|`bool`|Returns `true` if two `CollisionComponents` are colliding|
|`IsColliding`|`Entity` entity|`bool`|Same as previous method. If `entity` doesn't have `CollisionComponent`, this method will return `false`|
|`Contains`|`Vector2` position|`bool`|Returns `true` if `position` is inside bounding box of the component|
|`UpdateBoundingRectangle`|`Rectangle` rectangle|`void`|Updates stored bounding rectangle and forces system to recreate `IBox`
|`RefreshBoundingRectangle`| |`void`|Forces system to recreate `IBox`. Use after changing scale of entity - can't use `EntityObserver` for this.
