# DrawSystem / SpineDrawSystem

## Purpose

Drawing entities, integrating with other systems like animation.

### Differences

Aside from obvious one (`SpineDrawSystem` also handles Spine objects), `DrawSystem` allows changes of `SpriteSortMode`, while `SpineDrawSystem` is always set to `Deferred`.

`SpineDrawSystem` uses two separate renderers - for Spine objects it uses `SkeletonRender` while other objects are drew with `SpriteBatch`. Order of drawing is handled inside the system, then it sort of rotates between two renderers, switching them on and off when they are needed.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`Effect`|Spritebatch parameter from MonoGame|`Effect`||
|`RasterizerState`|Spritebatch parameter from MonoGame|One of: `CullClockwise`, `CullCounterClockwise`, `CullNone`|"CullNone"
|`DepthStencilState`|Spritebatch parameter from MonoGame|One of: `Default`, `DepthRead`, `None`|"None"
|`SamplerState`|Spritebatch parameter from MonoGame|One of: `AnisotropicClamp`, `AnisotropicWrap`, `LinearClamp`, `LinearWrap`, `PointClamp`, `PointWrap`|"LinearClamp"
|`BlendState`|Spritebatch parameter from MonoGame|One of: `Additive`, `AlphaBlend`, `NonPremultiplied`, `Opaque`|"AlphaBlend"
|`SpriteSortMode`|[DrawSystem only] Spritebatch parameter from MonoGame|One of: `Deferred`, `Immediate`, `Texture`, `BackToFront`, `FrontToBack`| "Deferred"

## Related components

### **DrawComponent**

Component contains data used for drawing.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`ContentId`|Id used to get content item used to draw|`string`|"my-texture"
|`Origin`|Origin position, defaults to center of `Size` if not set|`Vector2`|"0, 0"
|`Color`|Color of texture|[XNA Color Name](http://www.foszor.com/blog/xna-color-chart/) or RGBA `string`|"Aqua" "125,0,125"
|`Rotation`|Rotation of texture(separate from rotation of entity)|`float`|0
|`Scale`|Scale of texture(separate from scale of entity)|`float`|1.5
|`SpriteEffects`|Effect on the sprite|One of: `None`, `FlipHorizontally`, `FlipVertically`||
|`Depth`|Depth used to draw texture|`float`|0.5
|`DrawTarget`|Gets set on init. One of `IDraw` classes|`IDraw`||
|`IsVisible`|If `false`, texture won't be drew|`bool`|true|
|`Size`|Size of texture - used to hide texture when it's off the screen|`Vector2`|"64,64"


### **SpineComponent**

Compononent responsible for spine features.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`AnimationState`|`AnimationState` object from Spine. Cannot be set|`AnimationState`||
|`DefaultAnimationMix`|AnimationMix value if it's not provided by `PlayAnimation` call. Defaults to `0.2f`|`float`|0.25|

#### *Methods*
|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`ApplyColor`|- `string` slotName<br>- `Color` color|`void`|Applies colour to specified Spine slot|
|`ClearColor`|`string` slotName|`void`|Restores default colour for specified Spine slot|
|`ApplySkin`|- `string` slotId<br>- `string` skinId|`void`|`slotId` is not related to Spine slots. Read description below.|
|`AddSkin`|`string` skinId|`void`|Adds skin with specified id to list of used skins (skin is reloaded on next update)|
|`RemoveSkin`|`string` skinId|`void`|Removes skin with specified id from list of used skins|
|`SetSlotVisible`|- `string` slotName<br>- `bool` visible|`void`|Sets visiblity of slot|
|`SetSlotsVisibleByPrefix`|- `string` prefix<br>- `bool` visible|`void`|Sets visiblity of many slots by prefix in their names (e.g. you can turn off all slots that name starts with "front_" - useful if you have many directions in your spine project)|
|`PlayAnimation`|- `int` trackNumber<br>- `string` animationName<br>- `bool` loop = `false`<br>- `float` mixIn = `-1`<br>- `float` mixOut = `-1`<br>- `bool` forceRestart = `false`|`void`|Plays animation on specified `trackNumber` (lower tracks are prioritized, if track is taken then other animation will be stopped). `mixIn` and `mixOut` describe speed at which animation mixes with previous animation in track. If `forceRestart` is left as `false`, animation will not be played again in case it's already playing on track with `trackNumber`
|`GetAnimationName`|`int` trackNumber|`Optional<string>`|Returns `Optional` containing animation name that is played on the track with `trackNumber`. Returns empty `Optional` if no animation is played
|`IsAnimationPlayed`|- `int` trackNumber<br>- `string` animationName|`bool`|Checks if animation with `animationNumber` is played on the track with `trackNumber`

|`ApplyState`||`void`|Calls `sprite.ApplyState(AnimationState)`. Method used inside `SpineAnimationSystem`|

**Note about skins:** 

Spine libraries allow to create a skin which contains other skins (aka is a mix of few skins). When we use `AddSkin` and `RemoveSkin`, we operate on the list of skins that will get mixed together into single skin. Each time the skin list inside `SpineSprite` is modified, main skin is recreated and skins from the list are getting mixed in. `ApplySkin` works on level of `SpineComponent` and it additionally allows skins to be bound into "slots" - the main idea here was to create something that can work like clothes, e.g. inside our Spine project we have skins that change only hat and other set of skins changing boots - we use `ApplySkin("hat", "some-hat-skin-from-spine")` to apply change of hat - after another call of `ApplySkin` with `"hat"` as `slotId`, old skin will be removed from the list inside `SpineSprite`, effectively causing our hat to switch to other one.

Additionaly if we use any `skinId` that doesn't exist, a warn will be logged, but slot will still get changed (to basically empty skin). We can use `EmptySkinName` from `[Spine]` configuration to not log the warn - the idea here is to have "empty" skin, as in example above: no hat is on.



- `SetSlotVisible(string slotName, bool visible)` - sets slot to visible or not
- `SetSlotsVisibleByPrefix(string prefix, bool visible)` - sets many slots (picked by prefix in their name) to visible or not