# GravitySystem

## Purpose

Adding gravity effect to `MoveComponents` of entities with `GravityComponent`.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`GlobalGravityActive`|Is global gravity active? Defaults to `true`|`bool`|false|
|`GlobalGravityPower`|Power of global gravity power. It is value by which velocity gets affected each second. Defaults to `100.0f`|`float`|500
|`AffectSourcePowerByDistance`|If `true`, gravity sources will have stronger influence on objects that are closer to them. Defaults to `true`|`bool`|true
|`MaxPowerDistanceMultiplier`|Value between `0` and `1.0` that describes how close max power should work on object in gravity source area. Defaults to `0.4f`|`float`|0.5
|`GlobalDirection`|From which side should the global gravity work. Defaults to down|direction `string`, one of: `"Down"`, `"Up"`, `"Left"`, `"Right"`|"Up"

## Methods

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`SetGlobalGravityDirection`|`string` direction|`void`|Converts string to `Vector2` and sets the direction of global gravity. Valid values are: `"Down"`, `"Up"`, `"Left"`, `"Right"`

## Related components

### **GravityComponent**

Component of entity which movement should be affected by the gravity.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`GravityMultiplier`|Multiplier by how strong gravity is, defaults to `1.0`|`float`|0.5


### **GravityFieldComponent**

Component that pulls in other components affected by the gravity. Pretty primitive but working.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`Radius`|Radius at which component affects other components|`float`|300
|`Power`|Power used to pull other components in|`float`|100
