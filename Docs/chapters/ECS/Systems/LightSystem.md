# LightSystem

## Purpose

Integration with [Penumbra library](https://github.com/discosultan/penumbra). Allows calling `BeginDraw()` method from outside of this system (from `DrawSystem`).

**System should be placed lower than system which calls `BeginDraw`**

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`AmbientColor`|Ambient color inside penumbra module|[XNA Color Name](http://www.foszor.com/blog/xna-color-chart/) or RGBA `string`|"Aqua" "125,0,125"
|`DefaultShadowType`|If `LightComponent` doesn't have `ShadowType` set, it will be set to this value on first `Update`|One of: `Illuminated`, `Solid`, `Occluded`. More info on [Penumbra github](https://github.com/discosultan/penumbra)|"Solid"|

## Related components

### **LightComponent**

Component contains `Light` object from penumbra library, which is used inside the system to draw lights.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`Type`|Type of light, more info on [Penumbra github](https://github.com/discosultan/penumbra)|One of: `PointLight`, `Spotlight`, `TexturedLight`|"PointLight"|
|`ShadowType`|Type of shadow casted by light, more info on [Penumbra github](https://github.com/discosultan/penumbra)|One of: `Illuminated`, `Solid`, `Occluded`|"Solid"|
|`OffsetPosition`|Offset added to light position|`Vector2`|"0, 0"|
|`ContentId`|If `Type` is `TexturedLight`, this texture will be used|Content id string|"my-textured-light"|
|`ConeDecay`|Penumbra light parameter. Important only for `Spotlight`|`float`|50.5|
|`Intensity`|Penumbra light parameter|`float`|0.5|
|`Radius`|Penumbra light parameter|`float`|100|
|`Scale`|Penumbra light parameter|`Vector2`|"25, 25"|
|`Color`|Penumbra light parameter|[XNA Color Name](http://www.foszor.com/blog/xna-color-chart/) or RGBA `string`|"Aqua" "125,0,125"

### **ShadowHullComponent**

Component contains `Hull` object from penumbra library, which is used inside the system to draw hulls (objects that block the light).

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`PointsOffset`|Offsets from `Entity.Position` that will create a hull. Must have minimum 3 points that form a polygon|Array of `Vector2`|["-25, -25", "-25, 25", "25, 25", "25, -25"]|

#### *Methods*
|Method name|Parameters|Return type|Description|
|---|---|---|---|
|ChangePoints|- `List<Vector2>` points|`void`|Changes points of `Hull` object|

