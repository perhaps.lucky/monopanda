# MoveSystem

## Purpose

Changing position of entities with `MoveComponent`. Managing velocity loss and gain from acceleration.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`MaxAccelerationToVelocity`|Max velocity value that can be reached through acceleration. This value gets multiplied by `MaxAccelerationToVelocityMultiplier` from `MoveComponent`|`float`|100|
|`VelocityLoss`|Velocity lost in each update. This value gets multiplied by `VelocityLossMultiplier` from `MoveComponent`, `Time.ElapsedCalculated` and `Time.WorldTimeSpeed`|`float`|100|

## Related components

### **MoveComponent**

Component contains all data necessary to move object. Most important is `Acceleration`, which changes the `Velocity` through the system. `LockVelocity` can be used to turn velocity loss off.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`Acceleration`|A value by which `Velocity` gets changed each update (multiplied by passing time)|`Vector2`|"100,0"
|`Velocity`|A value by which object gets moved each update|`Vector2`|"100,100"
|`MaxAccelerationToVelocityMultipler`|Multiplier for `MaxAccelerationToVelocity`|`float`|1.5
|`VelocityLossMultiplier`|Multiplier for `VelocityLoss`|`float`|0.5
|`LockVelocity`|Prevents velocity loss|`bool`|false
|`LastPosition`|Position from last update (used in other systems). Cannot be set on creation through entity factory|`Vector2`|
|`Mimicking`|If not `null`, entity will move the same way as entity from component in this field. After movement this field is set back to `null`. Cannot be set on creation through entity factory|`MoveComponent`|
