# SoundSystem

## Purpose

Handling 3D sound effects. Effects that should be affected by this system should be triggered through system methods instead of `SoundManager`.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`DefaultDistanceOfMaxVolume`|Below this distance, 3D effects will play at max volume|`float`|50|
|`DefaultDistanceOfMinVolume`|Above this distance, 3D effects are not playing|`float`|1000|
|`DistanceScaleMultiplier`|Multiplier used to count "virtual distance" between emitter and listener (distance in MonoGame doesn't scale well with ECS distance)|`float`|20
|`ActiveAudioListener`|Audio listener which is used to listen. It will be set automatically to first entity with `SoundListenerComponent`|`AudioListener`|

## Methods

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|PlaySFX|- `string` id<br>- `AudioEmitter` audioEmitter<br>- `float` pitch = `0f`|`SoundEffectInstance`|Plays sound effect with provided `AudioEmitter`
|PlaySFX|- `string` id<br>- `Vector3` position<br>- `float` pitch = `0f`|`SoundEffectInstance`|Creates new `AudioEmitter` with provided position and plays the sound
|PlaySFX|- `string` id<br>- `Vector2` position<br>- `float` pitch = `0f`|`SoundEffectInstance`|Creates new `AudioEmitter` with `Vector3` position created from provided `Vector2` and plays the sound
|PlaySFX|`string` id|`SoundEffectInstance`|Proxy to `SoundManager` method
|PlayMusic|- `string` id<br>- `bool` loop = `true`|void|Proxy to `SoundManager` method
|StopMusic||`void`|Proxy to `SoundManager` method
|GetCurrentlyPlayedSongId||`string`|Proxy to `SoundManager` method
|SetSFXVolume|`int` volume|`void`|Proxy to `SoundManager` method
|SetMusicVolume|`int` volume|`void`|Proxy to `SoundManager`  method
|AdjustSFXVolume|`int` change|`void`|Proxy to `SoundManager` method
|AdjustMusicVolume|`int` change|`void`|Proxy to `SoundManager` method

## Related components

### **SoundListenerComponent**

Component will be used to specify place from which the sound will be "listened" to.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`AudioListener`|MonoGame `AudioListener` object. Created on init, cannot be set|`AudioListener`||
|`PositionZ`|Virtual `Z` position|`float`|0|

#### *Methods*

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`Activate`||`void`|Sets this component as `ActiveAudioListener` in `SoundSystem` (if system is present in ECS)

### **SoundEmitterComponent**

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`AudioEmitter`|MonoGame `AudioEmitter` object. Created on init, cannot be set|`AudioEmitter`||
|`PositionZ`|Virtual `Z` position|`float`|0|
|`DistanceOfMaxVolume`|Overrides `DefaultDistanceOfMaxVolume` from `SoundSystem`. Defaults to `-1`|`float`|10|
|`DistanceOfMinVolume`|Overrides `DefaultDistanceOfMinVolume` from `SoundSystem`. Defaults to `-1`|`float`|500|

#### *Methods*

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|`PlaySFX`|`string` SFX|`SoundEffectInstance`|Calls `PlaySFX` on `SoundSystem` with provided sfx id and `AudioEmitter` from component. If `SoundSystem` is not present in ECS, this method will do nothing
