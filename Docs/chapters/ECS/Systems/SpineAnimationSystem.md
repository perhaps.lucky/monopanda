# SpineAnimationSystem

## Purpose

Updates `AnimationState` in `SpineComponent`. Calls `ApplyState` if entity is in visible area.

## Parameters

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`MaxAccelerationToVelocity`|Max velocity value that can be reached through acceleration. This value gets multiplied by `MaxAccelerationToVelocityMultiplier` from `MoveComponent`|`float`|100|
|`VelocityLoss`|Velocity lost in each update. This value gets multiplied by `VelocityLossMultiplier` from `MoveComponent`, `Time.ElapsedCalculated` and `Time.WorldTimeSpeed`|`float`|100|

## Related components

### **SpineComponent**

Described inside [DrawSystem and SpineDrawSystem](DrawSystem and SpineDrawSystem.md).

