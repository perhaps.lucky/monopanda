# SpriteAnimationSystem

## Purpose

Updating animation states inside `SpriteSheetAnimationComponents`.

## Related components

### **SpriteSheetAnimationComponent**

Component contains animation defined for entity and information about current state (with timer).

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|`Animations`|A dictionary where `key` is name of animation and `value` is `SpriteSheetAnimation` object that describes animation|`Dictionary<string, SpriteSheetAnimation>`|See below
|`StartAnimation`|If present, animation with this name will be started right after component creation|`string`|"idle"
|`CurrentAnimation`|`SpriteSheetAnimation` object with currently played animation, cannot be set on creation|`SpriteSheetAnimation`||
|`CurrentFrame`|Frame number within currently played animation|`int`||
|`FrameTimer`|Timer used to change frame of animation|`RepeatingTimer`||
|`AnimationFinished`|If `true` then animation got finished|`bool`||


Example of animations json:

```json
"Animations": {
    "running": {
        "FrameDuration": 1000,
        "Frames": [0, 1, 2, 3],
        "IsLooping": false,
        "IsReversing": true
    },
    "otherAnimation": {
        "FrameDuration": 500,
        "Frames": [0, 3],
        "IsLooping": true
    }
}
```

#### *Methods*

|Method name|Parameters|Return type|Description|
|---|---|---|---|
|PlayAnimation|`string` id|`void`|Plays animation with provided id
|StopAnimation||`void`|Stops animation 
|UpdateSpriteSheetSprite||`void`|Updates `sourceRectangle` inside related `SpriteSheetSprite` - called by `SpriteAnimationSystem`


## Related classes

### SpriteSheetSprite

Class contains `TextureId` and `SourceRectangle` - rectangle that describes which part of texture should be used in `Draw`. Class extends `IDraw`, meaning it has a method `Draw(SpriteBatch spriteBatch, DrawComponent drawComponent)`. 

Other methods are: `UpdateSourceRectangle(string sourceRectangleFilename)` and `UpdateSourceRectangle(int frame)`. Both methods are changing source rectangle parameters to ones that are loaded from `SpriteSheet` - one is using `Filename` parameter and other is using list index. 

After a tiny bit of work this class can be used for static non-animated objects too (e.g. many sprites from single spritesheet) - but it is not implemented at this moment.

### SpriteSheetAnimation

Properties:

- `int[] Frames` - list of indexes of frames from `SpriteSheet` that will be played in order
- `int FrameDuration` - duration of one frame in millis
- `bool IsLooping` - if `true`, animation will start again from the first frame after reaching the end
- `bool IsReversing` - if `true`, animation will play in reversed order after reaching the end. Makes animation looped regardless of `IsLooping`
- `bool IsReversed` - indicates if animation is currently played in reversed order. This property is used inside `SpriteSheetAnimationComponent`
