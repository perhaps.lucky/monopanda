# TextSystem

## Purpose

Creating `DrawComponent` with `TextSprite`, based on `TextComponent` data.

`TextSprite` is drew by `DrawSystem` or `SpineDrawSystem`. It requires `TextComponent` to be present.

## Related components

### **TextComponent**

Component contains data used for drawing.

#### *Parameters*

|Parameter name|Description|Value type|Example|
|---|---|---|---|
|PositionOffset|Offset position from entity.Position|`Vector2`|"25,25"
|Text|Text drew by `FontRenderer`|`string`|"test"
|Font|Content id of font|`string`|"my-special-font"
|FontSize|Size of text|`int`|32
|FontColor|Colour of text|[XNA Color Name](http://www.foszor.com/blog/xna-color-chart/) or RGBA `string`|"Aqua" "125,0,125"
|Depth|Depth used in `DrawSystem`/`SpineDrawSystem`|float|0.5

## Related classes

### TextSprite

Implements `IDraw`. Doesn't store any data, all parameters used for drawing are coming from `TextComponent`.
