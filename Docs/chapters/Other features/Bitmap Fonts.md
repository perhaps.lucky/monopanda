# Bitmap Fonts

## Introduction

Bitmap Fonts are a way to work with modable content manager and fonts (there is no way to load non-compiled font files during runtime). Bitmap fonts are created using [BMFont](https://www.angelcode.com/products/bmfont/).

Output from the program are two files: 
- `.fnt` file, which is xml with all data about font
- `.png` file, which contains font texture

You need to ensure that program creates only one texture file (many pages are not handled).

## FontRenderer

`FontRenderer` is main class responsible for drawing text.

It contains one static method:

`Vector2 DrawText(string fontId, Vector2 position, string txt, Color color, int fontSize = 32, bool scaleToWindowSize = true, int maxLineWidth = -1, float depth = 0, SpriteBatch spriteBatch = null, bool ignoreColorTags = false)`

- `fontId` - content id of font
- `position` - position of start of the text, if `scaleToWindowSize` is `true` it will be scaled from engine configuration to user settings
- `txt` - text to draw
- `color` - color of text
- `fontSize` - size of text
- `scaleToWindowSize` - if `true` then text position, size and word-wrapping will be scaled to user settings
- `maxLineWidth` - max width of line of text, if left at default `-1`, text width will be not limited, otherwise word-wrapping will be used. *Warning: If word doesn't fit in single line, it will stop drawing and log a warn!*
- `depth` - `depth` value passed to `SpriteBatch`
- `spriteBatch` - `SpriteBatch` used to draw the text, if left at default `null`, `GameMain.SpriteBatch` will be used
- `ignoreColorTags` - if set to `true`, color tags will be ignored. Color tags are described in [Console chapter](CommandSystem.md), it works just the same way in any text-drawing.

Method is already used in features used in UI like `Text`, `TextButton` etc
