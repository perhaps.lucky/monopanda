# CachedTextureSprite

CachedTextureSprite is a class that implements `IDraw` interface. It purpose is to store the `Texture2D` reference. 

It can be set as `DrawTarget` property of `DrawComponent`. In case of component destruction or change of `DrawTarget` property, stored texture will be automatically disposed.

Object doesn't allow a change of stored texture.