# CommandSystem and Console

## Introduction

`CommandSystem` allows creating commands that can take parameters. On initialization, `CommandSystem` searches for all classes that expand `Command` class and adds them to list of command. Command get called by their class name (minus `Command` - if present, this word gets removed).

`CommandSystem` is created in `ConsoleSystem` when console is enabled in engine configuration. While `ConsoleSystem` is singleton, there can be many `CommandSystems` - for example it can be used as sort of scripting language, moved away from code to json content.

Important method in both of classes is `SetECS` - all commands run on active ECS, so make sure it is set there.

## Command

`Command` contains abstract method `string Execute(List<string> parameters)` - method should perform command actions and return some string with information what was done. `parameters` is list of provided parameters.

Aside from above method, `Command` contains help-related properties:
- `ShortHelpDescription` - description on the list returned from `help` command
- `ExtendedHelp` - help returned from `help [command name]` command
- `ShowInHelp` - if set to `false`, this command will not be returned by `help` command
- `HelpCategoryName` - list from `help` is sorted by categories, commands with same value in this field will go together

## Command syntax

A single command call goes always the same way: `[command name] (parameter1) (parameter2)` etc - parameters are separated by space symbol, unless they were put inside quotation mark, e.g. `print "Hello World"` will print a single string `"Hello World"`, while `print Hello World` will print two strings: `"Hello"` and `"World"` (`print` returns all parameters separated by new line symbol).

Many commands can be separated using `;` - they will be executed in order. Example: `print Hello; print World;` will execute two separated `prints`. Similar to space, if this symbol is used inside quotation marks, it will be passed on as part of argument.

Quote symbols can be escaped using `\`, for example: `print \"Hello World\"` will return two strings: `""Hello"` and `"World""`.

### Color tags

Color tags can be used in returned value to specify that text should have changed colour. Syntax of tag: `%{XNA_COLOR_NAME}%`, for example: `%{Cyan}%` or `%{R_VALUE, G_VALUE, B_VALUE}%`, for example: `%{255,0,0}%`.

[XNA Color Chart](http://www.foszor.com/blog/xna-color-chart/)

## Console

By default console can be opened with `~` key. While console is open, all input goes into console, not into `InputManager`.

Using up and down arrow keys, user can go through executed command history. 

Pressing page up and down will scroll the console view.

Pressing shift + backspace (shift + delete) will remove entire word.

## Macros

User can create macros that will be stored after closing up the game. Macros can contain several commands separated by `;`. Related commands are:
- `listmacros` - prints list of saved macros
- `addmacro [macro name] [macro command] - saves new macro, `macro command` is a single parameter so use quoation marks
- `removemacro [macro name]` - removes macro

Macro can be called like any command using its `[macro name]`. Because of that, macros should not be called like already existing commands (commands have priority in execution).

Macros cannot call other macros!

## Related configuration

```ini
[Console]
ConsoleActive=true
ConsoleButton=OemTilde
ConsoleFont=example-font
ConsoleFontSize=16
ConsoleDefaultHeight=300
ConsoleMaxLines=256
CommandHistoryMax=64
UserMacrosJson=Content/MonoPanda/macros.json
```
- `ConsoleActive` - if `false`, all console features will be disabled
- `ConsoleButton` - button name from MonoGame Keys enum, used to open/close console
- `ConsoleFont` - content id of font used inside console
- `ConsoleFontSize` - size of text inside console
- `ConsoleDefaultHeight` - height of the console (it can be changed during runtime using `consoleheight` command)
- `ConsoleMaxLines` - number of lines stored in console memory
- `CommandHistoryMax` - number of executed commands stored in console memory
- `UserMacrosJson` - path to json file with stored macros
