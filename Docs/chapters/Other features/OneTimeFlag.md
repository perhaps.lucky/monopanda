# OneTimeFlag

## Introduction

`OneTimeFlag` is a class meant to simplify triggering one time events. Normally the most obvious way to do that would be to create a `boolean` and then check it and change its value inside the `if` statement.

Tldr - it removes some lines from code

## Example

Before:

```csharp
private bool someFlag = false;

private void someUpdate() {
    if(!someFlag) {
        Log.log(LogCategory.Flags, LogLevel.Debug, "A flag was checked.");
        executeOnce();
        someFlag = true;
    }
}
```

After:

```csharp
private OneTimeFlag someFlag = new OneTimeFlag();

private void someUpdate() {
    if(someFlag.Check()) {
        executeOnce();
    }
}
```

## But I want it to execute again!

You can call `Reinitialize()` method and it will go back to state from before `Check()`.


## Logging state

There are two messages that are logged on `Debug` level in `Flags` category. One for when flag is checked and one for when flag is reinitialized.

You can use optional constructor parameters to configure the category, logging level and id of the flag. If no id is provided, flag will be called anonymous.
