# Optional

## Wtf

Optional is an utility class. People who know Optionals from Java will probably know how it works. For others, to put it it simply, Optionals are containers with or without other objects inside. Optionals are meant to replace `null` values in a more safe and nice-looking way.

For MonoPanda I planned to switched to Optionals at some points. However, it didn't go well. There were many places with `if(x==null)` that were hard to redesign, so in the end the class is only used in few places and left there, for future (or to just be there for nothing).

Example `null` vs `Optional`:
```csharp
// Method that retuns macro if its present
// First let's see how it would look with null:
public static string GetMacro(string name) {
    if (macros.ContainsKey(name))
        return macros[name];
    else
        return null;
}

// And an example call:
public void Update() {
    string macro = MacroManager.GetMacro("macro");
    if (macro != null)
        doExecute(macro); // some method
}

// Now the same method with optional (so how it is now)
public static Optional<string> GetMacro(string name) {
    if (macros.ContainsKey(name))
        return Optional<string>.Of(macros[name]);
    else
        return Optional<string>.Empty();
}

// And the same example with Optional features:
public void Update() {
    MacroManager.GetMacro("macro").IfPresent(macro => doExecute(macro));
    // No ugly null-checking, woohoo
}
```

## Working with optionals

### Constructors

All constructors are `private`. There are three static methods used to create `Optional` objects:
- `Of(T value)` - returns optional with `value` inside (can be `null` - in that case it will be empty optional)
- `Of(Func<T> funcValue)` - retuns optional after executing the function provided (catches NullReferenceException - returns empty optional). *This method turned out to be slow and it is suggested to use `IfPresentGet` instead!*
- `Empty()` - returns empty optional

## Methods

#### IsPresent()
Returns `true` if there is value present inside optional objects (equal to `x!=null`).

#### IfPresent(Action<T> action)
If value is present, perform action on the value (example above).

### IfPresentAnd(Func<T, bool> condition, Action<T> action)
It's the same as above, but aside from being present, the object gets checked for condition.

#### IfPresentGet<Y>(Func<T,Y> getFunc)
This is my invention, sort of "optional inside optional". 

Method returns optional of `Y` class, which is obtained using provided method on the value inside parent optional. If parent optional isn't present, it will return empty optional.

Example (getting Velocity from MoveComponent if its present):
```csharp
public override Vector2 GetVelocity() {
      return Optional<MoveComponent>
        .Of(entity.GetComponent<MoveComponent>())
        .IfPresentGet(moveComponent => moveComponent.Velocity)
        .OrElse(Vector2.Zero);
    }
```
The main plus here is that in the end we have Optional object, which can be used to call other methods (in example `orElse`). Speaking of...

### OrElse(T another)
If value is not present, return provided value instead.

### OrElseGet(Func<T> func)
If value is not present, get value from provided function.

### OrElseThrow(Exception e)
If value is not present, throw exception.

### Get()
Returns value, possibly evil `null` that in the end we can't avoid no matter what ;)
