# SpriteSheets

Spritesheets are single files containing many sprites together. In MonoPanda they are mostly used for animations of single object and skins in Spine features. There shouldn't be much problem to use single spritesheet for many different, non-animated objects as well.

Spritesheets are created using  [TexturePacker](http://www.codeandweb.com/texturepacker). Program should be set to ouput `json` and `png` files (different png quality should work just fine). `Json` file contains description of every sprite inside the spritesheet.

More about usage of spritesheets:
- [SpriteAnimationSystem](../ECS/Systems/SpriteAnimationSystem.md)
- [Spine](../Spine.md) (part about external skins)
