# Tiled Integration

## Introduction

Tiled is a program used to create tile-based maps. You can download it on its page [here](https://www.mapeditor.org/).

The integration is slighty limited and requires a bit of work on code side, e.g. you need to map each object to factory method that will be used (same goes for properties, if you don't map the usage of each property, it will simply be ignored). The examples below should make it simple.

There are few rules/limitations that need to be followed while creating Tiled projects:

- project must be saved in json file
- tilesets should be set to embedded (single file for all)
- tilesets and layers are identified by their names, so it's recommended to use unique values (unless the loaded should behave same way in both cases)

Tiled projects are handled by `ContentManager`.

## TiledMapLoader

Tiled map loader is class used to load tiled project. Loader uses registered handlers for each type of object (layer, tile, object). Example loading call can look like:
```csharp
new TiledMapLoader()
    .AddTileHandler("tileset",
        (tile, tileMapPosition, worldPosition, layer) => TiledFactory.CreateTile(position, tile, layer))
    .AddObjectHandlerByName("house",
          ((position, o, layer) => TiledFactory.CreateHouse(position, o, layer)))
    .AddObjectHandlerByType("colorParameterSpriteThing",
          postInit: ((entity, o, layer) =>
            entity.GetComponent<DrawComponent>().Color = o.GetColorProperty("color", Color.White)))
    .SetMapPositionOffset(new Vector2(100, 100)
    .LoadMap("tiled-map");
```
In above example we load map with content Id: `tiled-map`. We register a single tile handler for tileset named `tiledset`. For each tile, loader will call `TiledFactory.CreateTile` with data from map: calculated position of tile (includes offset set in the method, layer offset from project and calculated X/Y from project tile position and tile width/height), id of tile within tileset and layer data from project. 

Each of handlers has two methods: one is called on creation (in case of tile and object this method is required to return `Entity` object) and post init method. Post init methods are called after all of entities are creates and can be used to create relations between objects by e.g. object parameter in Tiled.

## Layer Handler

Layer handlers are called first, before loader checks if layer is a tile layer or an object layer.

Both create and postInit methods are using only `TiledLayer` object. Layers in Tiled can have custom properties, which can be added to tiles separately (however, layer object is also available in methods of tile handler).

As opposite to rest of handlers creation method of layer handler is optional and does not require `Entity` returned.

## Tile handler

Creation method uses 4 arguments that can be used to create tile entity:
- `int tile` - an index of used tile from tile set, can be used to set sprite index from spritesheet (as long as the same tile/spritesheet is used)
- `Point tileMapPosition` - position of tile in the layer
- `Vector2 worldPosition` - final, calculated position of entity to be created
- `TiledLayer layer` - layer data, can be used to read properties (custom or built-in Tiled), there are few properties added like `LayerIndex` which can be used to calculate depth of tile.

Example of usage (method referenced via action):
```csharp
public static Entity CreateTile(Vector2 position, int id, TiledLayer layer) {
  Entity entity = instance.CreateEntityInner("tile", position); // factory id of tile
  var drawComponent = entity.GetComponent<DrawComponent>();
  drawComponent.SetSprite(id);
  drawComponent.Color = ColorUtils.HexStringToColor(layer.TintColor); // built-in Tiled property
  drawComponent.Depth -= 0.01f * layer.LayerIndex; // LayerIndex added by loader
  return entity;
}
```

## Object handler

For object handling there are two separate handlers. One is based on `Type` of object and other is based on `Name` of object. User can use either or both of them. However, in case of creation, only one will be used - if object has both matching `Type` and `Name` handlers, `Name` handler will be used for creation and both will used in PostInit.

Creation method takes 3 arguments:
- `Vector2` position - calculated position of entity, calculated from position and width/height settings from Tiled (+ offset as in tile layer)


**IMPORTANT**: position is calculated to match objects like rectangles or text object. By default the origin of placed tile-objects is set to the left-up corner. To match correct position, you need to go in properties of used tileset and change `Object Alignment` to `Top Left`.

- `TiledObject` o - object json data
- `TiledLayer` layer - layer data


PostInit method takes 3 arguments:
- `Entity` entity - previously created entity
- `TiledObject` o - object json data
- `TiledLayer` layer - layer data


## Data classes

### TiledMap
The top class where everything is stored.

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|IdToEntity|Dictionary<int, Entity>|No|Dictionary from Id of TiledObject to Entity created by loader (only objects can be handled this way, tiles can be referenced in their layer object)
|Height|int|Yes|Height of the map (in tiles)
|Width|int|Yes|Width of the map (in tiles)
|Layers|List<TiledLayer>|Yes|List of layers in order from bottom one
|Orientation|string|Yes|Map orientation setting
|RenderOrder|string|Yes|RenderOrder setting
|TileHeight|int|Yes|Height of tile (in pixels)
|TileWidth|int|Yes|Width of tile (in pixels)
|Tilesets|List<TiledTileSet>|Yes|List of tilesets
|Properties|List<TiledProperty>|Yes|List of custom properties added to map

### TiledLayer

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|Map|TiledMap|No|Reference to TiledMap object added by loader
|IndexToCreatedentity|Dictionary<int,Entity>|No|Dictionary from index (in Data array) of tile to Entity create by loader
|Data|int[]|Yes|Array of ids from Tiled (these ids use global ids, not ids within their tileset)
|Height|float|Yes|Height of layer(in tiles)
|Width|float|Yes|Width of layer(in tiles)
|Id|int|Yes|Id of layer
|Name|string|Yes|Name of layer
|Type|TiledLayerType|Yes|Either `tilelayer` or `objectgroup`
|Visible|bool|Yes|Visible setting from Tiled
|TintColor|string|Yes|Tint color setting from Tiled (you can get `Color` type using `GetTintColor()` method)
|OffsetX|float|Yes|X value of layer offset (you can get `Vector2` using `GetOffset()` method)
|OffsetY|float|Yes|Y value of layer offset
|Opacity|float|Yes|Opacity value from Tiled
|Objects|List<TiledObject>|Yes|List of objects on the layer (if type is `objectgroup`)
|Properties|List<TiledProperty>|Yes|List of custom properties added to layer
|LayerIndex|int|No|Index of layer in `Layers` list of `TiledMap` object, added by loader.

### TiledObject

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|Map|TiledMap|No|Reference to TiledMap object added by loader
|Height|float|Yes|Height of object
|Width|float|Yes|Width of object
|Id|int|Yes|Id of object
|Rotation|float|Yes|Rotation of object
|Type|string|Yes|Type, used to determine which handler should be used
|Name|string|Yes|Name, used to determine which handler should be used (priority above Type)
|Visible|bool|Yes|Visible parameter
|X|float|Yes|Position X
|Y|float|Yes|Position Y
|Text|TiledText|Yes|If present, then object is text
|GID|int|Yes|Global id of tile that got used to create object (you can retreive `TiledTile` object using `GetTileFromTileset()` method - it contains some data like original size of tile and so on)
|Properties|List<TiledProperty|Yes|List of custom properties added to object

### TiledText

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|Text|string|Yes|Text value of object
|Wrap|bool|Yes|Does the text wrap?
|Color|string|Yes|Hex value of color (you can get `Color` object using `GetColor()` method)
|PixelSize|int|Yes|Size of font, probably?
|FontFamily|string|Yes|Font name from Tiled

Text is kinda tricky to use, but example below worked almost good (no wrapping):
```csharp
public static Entity CreateTextObject(Vector2 position, TiledObject o, TiledLayer layer) {
  Entity entity = instance.CreateEntityInner("text-object", position);
  var textComponent = entity.GetComponent<TextComponent>();
  textComponent.Depth -= 0.01f * layer.LayerIndex; // can ignore this bit, layer sorting stuff
  textComponent.Text = o.Text.Text;
  textComponent.FontColor = o.Text.GetColor();
  textComponent.FontSize = o.Text.PixelSize;
  textComponent.PositionOffset = new Vector2(-o.Width / 2, -o.Height / 2); // since entity position is in the middle of object in Tiled
  return entity;
}
```

### TiledTileSet

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|FirstGid|int|Yes|Global id of first tile of the tile set (global ids are used in `Data` property of `TiledLayer`)
|TileCount|int|Yes|Tile count
|TileHeight|int|Yes|Height of tile (in pixels)
|TileWidth|int|Yes|Width of tile (in pixels)
|Name|string|Yes|Name of tileset
|Columns|int|Yes|Number of columns in tileset

### TiledProperty

|Property name|Type|Is from Tiled?|Description
|---|---|---|---|
|Name|string|Yes|Name of property
|Type|string|Yes|Type of property
|Value|object|Yes|Value

Values as stored as `object`. However, each class that contains list of `TiledProperty`, extends `TiledClassWithProperties` class and has methods that allow simple extraction of property value:
- `GetBoolProperty`
- `GetColorProperty` - returns `Color` object (translated from hex)
- `GetIntProperty`
- `GetFloatProperty`
- `GetObjectProperty` - retuns `Entity` object (based on `IdToEntity` dictionary in `TiledMap`)
- `GetStringProperty`

There is no get file property method. Such field stores string value with path to file (can be read using `GetStringProperty` and then used as desired).
