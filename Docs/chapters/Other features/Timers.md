# Timers

## Introduction

Timers are helpers classes that are supposed to simplify process of delaying or making "cooldowns" on methods. Regardless of type of timer, they will all have 3 important methods:

- `Initialize(silent)` - method will start (or restart) timer; if `silent` is set to `true`, timer messages will not get logged
- `Check()` - this method is used in `if` statement to check if timer conditions are met
- `Dispose()` - this method will remove the timer from system, `Check()` will keep returning `false` until next initialization

### SingleTimeTimer

`SingleTimeTimer(int waitTime)`

This timer will return `true` after the time set by `waitTime` has passed. It will return it only once and after that, another `Initialize()` should be called.

Example: `SingleTimeTimer(3000)` - after 3sec, timer will return `true` once

### RepeatingTimer

`RepeatingTimer(int repeatTime)`

This timer will return `true` every time `repeatTime` has passed. It will not stop until `Dispose()` is called.

Example: `RepeatingTimer(4000)` - timer will return `true` every 4sec


### PeriodTimer

`PeriodTimer(int waitTime, int workTime, int cooldown = 0)`

This time will return `true` only within a certain period of time. It will first wait for `waitTime`, then keep returning `true` until `workTime` has passed (`workTime` should be higher than `waitTime`, in other words time of actual working = `workTime - waitTime`). `cooldown` is used in inner `RepeatTimer` that starts after the `waitTime` has passed.

Example: `PeriodTimer(1000, 3000, 500)` - timer will first wait for 1sec, then start returning `true` every 0.5sec for next 2sec

After `workTime` has passed, another reinitialization will be needed.

### Common constructor parameters

All timers have 3 common constructor parameters. All of them have default values.

- `id` - id of timer, used in logs if present, otherwise a number id will be set
- `affectedByWorldTime` - if `true`, timer work speed will be changed by `Time.WorldTimeSpeed` multiplier; by default it's set to `false`
- `entity` - if `affectedByWorldTime` is true and this parameter is present, timer will be also affected by change in `TimeComponent` of entity (if present)
- `logCategory` - enum of category under which the timer logs will be logged, by default it's `Timers` category
