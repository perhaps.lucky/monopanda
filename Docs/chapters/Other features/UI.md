# UI

Disclaimer: UI features are limited and that probably won't change. However, integration with InputManager is an important bit here.

## UISystem

`UISystem` is main class responsible for updating and drawing UI. It has a list of `UIObject`s. For it to work properly, we need to create an object inside the `State` and then add calls to `Update` and `Draw` in correct methods (probably at the end since it's UI).

Other methods:
- `AddObject(UIObject UIObject) - adds an object to the list
- `Clear()` - removes all objects (can be called from `Update` of one of objects)
- `ForceOnChangeActions()` - calls `ForceOnChange()` in objects that implement `IForceOnChange` - useful for forcing behaviour that should otherwise happen on a change, e.g. after initial draw

## UIObject implementations

A common property from `UIObject` class is `Visible` - defaults to `true`, if set to `false`, object won't call `Update` and `Draw`.

### UIfocusableObject

`UIFocusableObject` is an abstract class that implements `IFocus` (more about it below). It contains fields:
- `Focused` - a bool that changes a value in `IFocus.Focus`. Setting to `true` cause the field to refer to `this` object. Setting to `false` sets the field to `null`.
- `Active` - just a help field, needs further implementations

On it's own, object doesn't grant any logic outside the changing of `Focus` value. It has two virtual empty methods: `OnFocused` and `OnUnfocused` that are triggered when value of `Focus` changes (as long as it is done by `Focused` field of this or other object).

### Text

As the name says, it is the text that shows on the screen.

Properties:
- `FontId` - contentId of font
- `Position` - position on the screen
- `Txt` - text to draw
- `Color` - color of text
- `FontSize` - font size, defaults to 32

`Position` and `FontSize` are scalled with the resolution, meaning that these values should be set in relation to resolution set in engine **Configuration**.

### TextButton

A text that changes colour when mouse cursor hovers over and when clicked.

Properties:
- `FontId` - contentId of font
- `Position` - position on the screen
- `Txt` - text to draw
- `FontSize` - font size, defaults to 32
- `DefaultColor` - normal colour
- `HoverColor` - colour when mouse cursor is above the text
- `ClickColor` - colour after clicking the text
- `Action` - `Action` object triggered after click

Just like with `Text`, `Position` and `FontSize` are scalling with resolution.

### TextBox

A box where user can write.

Properties:
- `Text` - current content
- `FontId` - contentId of font
- `Position` - position on the screen
- `Color` - color of text
- `FontSize` - font size, defaults to 32
- `ClickToFocus` - if `true`, clicking the TextBox will set the focus to it (needs a width set)
- `Width` - `int` value describing how wide is the TextBox. Important for focusing. Causes text to "scroll" if it takes more space (however calculations are imperfect)
- `Height` - `int` value describing how high is the the TextBox. Important for focusing and nothing else.
- `IsFocused` - if `true`, input from keyboard will go there
- `OnEnter` - `Action` object triggered after pressing enter key; if empty, pressing enter will either add new line or unfocus the TextBox.
- `NewLineOnEnter` - if action above is empty, this bool decides what should be done - `true` = add new line symbol to text box, `false` = unfocus text box
- `OnTab` - `Action` object triggered after pressing tab key

TextBox should be disposed.

### TextSelection

Implements `IForceOnChange`.

A simple list of options that can be select by pressing right/left buttons.

Properties:
- `FontId` - contentId of font
- `Position` - position on the screen
- `FontSize` - font size, defaults to 32
- `DefaultColor` - normal colour
- `HoverColor` - colour when mouse cursor is above the text
- `ClickColor` - colour after clicking the text
- `OnChange` - `Action` invoked when value gets changed
- `LeftButton`, `RightButton`, `DescriptionText` - these are references to objects that it is made of
- `Items` - list of `ISelectionItem`s that can be picked from the list

`ISelectionItem` is interface that forces method `GetLabel()` - method is used to set text visible to user.

## IFocus

`IFocus` is an interface with a single static field `IFocus Focus`. This field purpose is to refer to object that is currently "focused". At this moment this can be only some `TextBox` object.

Value is used in `InputManager` methods. It is compared with `context` parameter (by default it's null) and if the context is different than current focus, then returned values are all set to false (controls are ignored). Only `GetLeftMouseButton` method can ignore that check by setting `ignoreFocus` parameter to `true` (assuming left mouse button is used to change focus).

In simple words, `Focus` describes what control is currently "active", so that when we press buttons, only active control is able to read them. It also stops game from reacting to controls, when for example player writes something that used movement keys. `Focus` set to `null` means that no UI object is focused and game can read keys as controls (reason why by default `context` is set to `null`).

### Integration with external libraries

`IFocus` can be simply integrated with external libraries, as long as they provide some way to trigger a method on focus/unfocus.

Example below is integration of `TextBox` from [Myra](https://github.com/rds1983/Myra)

```C#
public class MyraTestTextBox : Myra.Graphics2D.UI.TextBox, IFocus { // extend object and add IFocus interface
    
    public override void OnGotKeyboardFocus() {
      base.OnGotKeyboardFocus(); // do base action
      
      // this can be skipped if we don't want to use MonoPanda UI objects
      if (IFocus.Focus != null && IFocus.Focus != this && IFocus.Focus is UIFocusableObject) {
        ((UIFocusableObject)IFocus.Focus).OnUnfocused();
      }
      
      // the important bit, set focus to this
      IFocus.Focus = this;
    }

    public override void OnLostKeyboardFocus() {
      base.OnLostKeyboardFocus(); // do base action
      // set focus back to game
      IFocus.Focus = null;
    }
  }
```