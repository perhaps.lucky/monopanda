# Spine

## Introduction

Spine is a software for 2D skeletal animation. You can read more about on their official site: http://esotericsoftware.com/. Spine runtimes are added to project in `MonoPanda/Spine/` directory. They were slighty edited to allow working with both spine and normal objects. Aside from that, MonoPanda allows adding external skins (also through modding), not just skins that are defined in spine project.

## License - important

Runtimes license:
> You are welcome to evaluate the Spine Runtimes and the examples we provide in this repository free of charge.
>
> You can integrate the Spine Runtimes into your software free of charge, but users of your software must have their own [Spine license](https://esotericsoftware.com/spine-purchase). Please make your users aware of this requirement! This option is often chosen by those making development tools, such as an SDK, game toolkit, or software library.
>
> In order to distribute your software containing the Spine Runtimes to others that don't have a Spine license, you need a [Spine license](https://esotericsoftware.com/spine-purchase) at the time of integration. Then you can distribute your software containing the Spine Runtimes however you like, provided others don't modify it or use it to create new software. If others want to do that, they'll need their own Spine license.
>
> For the official legal terms governing the Spine Runtimes, please read the [Spine Runtimes License Agreement](http://esotericsoftware.com/spine-runtimes-license) and Section 2 of the [Spine Editor License Agreement](http://esotericsoftware.com/spine-editor-license#s2).

The important part here is that if you don't own the spine software license, you cannot create new software using the runtimes. In such case, you should not use spine version of MonoPanda.


## MonoPanda classes related to Spine

### SpineAsset

This is an asset used in `ContentManager`. Path to such asset should lead to `.atlas` file, additionally there are required two other files: `json` and texture, exported from Spine.

For extenal skins you need to also create a `json` file ending with `_skins` in its name, for example: we add `funnyguy.atlas` atlas, with `funnyguy.json` and `funnyguy.png` - if we want external textures, we need to create `funnyguy_skins.json` file. This file is loaded from all of mods separately from other 3 files - meaning mod can add external skins without having to override all other files. However if mod changes texture from main file, it will still need to contain other 2 files, as they are loaded together. In such case, external skins from main mod are not overrided in content loading (they might however be overrided by their id).

External skins file example:
```json
[
	{
		"Name": "mod-hat-skin",
		"Attachments": 
		[
			{
				"SlotName": "hat",
				"X": 25.6,
				"Y": 0,
				"Rotation": -90,
				"ScaleX": 1.3,
				"ScaleY": 1.3,
				"Width": 193,
				"Height": 89,
				"ContentId": "mod-hat-skin",
                "AtlasFileName": "this wasn't here",
                "MeshSkinName": null
			}
		]
		
	}
]
```
File should contain list of `ExternalSkin` objects. Their properties are:
- `Name` - name of the skin, just like in Spine skin
- `Attachments` - list of `ExternalAttachment` objects that will be used in the skin
`ExternalAttachment` properties:
- `SlotName` - name of the slot in the Spine project
- `X`, `Y`, `Rotation`, `ScaleX`, `ScaleY`, `Width`, `Height` - names say it all, these are skin properties (they can be tested inside Spine by simply adding the new texture as attachment in any skin). `X`, `Y`, `Rotation` default to `0`. `ScaleX`, `ScaleY` defaults to `1`. For `Width` and `Height` - if they are not present, they will be read from content item - however, that force loads content items and will slow down the process, so it's suggested to keep these values in this json as well.
- `ContentId` - contentId from which the skin texture will be loaded (you need to add it as new content item - more about it in `ContentManager` chapter)
- `AtlasFileName` - if content item is a `SpriteSheet`, this filename is used to pick right part of the file (if its empty, `SlotName` will be used instead)
- `MeshSkinName` - see Mesh Skin section below

#### Mesh External Skin

It is possible to create external skin that will use one of existing meshes from spine project. For that, all you have to do is to put name of existing skin in `MeshSkinName`. Note that all properties will be replaced by properties of existing skin. Additionally, new skin should be exactly same size (and rotation) as frame inside spine atlas.

At this point it is not possible to use custom UVs, edges etc. It is not planned for now.


### HackRendererObject

This is used in "hack" that allows drawing external textures inside the spine libraries (they work quite different than normal `MonoPanda` drawing). It also has `CountUvUs` method, where it counts `UVs`, but y'know, UwU hehe

Leave me alone.

### SpineSprite

This is part of draw system, but I will describe it here in case you are one of these ECS haters.

`SpineSprite` is created from `assetId`, linking it to main asset. Important methods:
- `ApplyState(AnimationState state)` - applies animation state to skeleton, animation state is an object which needs to be `Updated` on every frame. Applying it cause the skeleton to update to correct position. These actions are handled by `SpineAnimationSystem` in ECS.
- `ApplyColor(string slotId, Color color)` - applies color to texture on slot with provided id
- `ClearColor(string slotId)` - returns colour to default
- `AddSkin(string skinId)` - adds skin to skin list, multiple skins are possible (if they are not overriding each other, otherwise part loaded as last will override other) - changes are not applied until `Update` is called
- `RemoveSkin(string skinId)` - removes skin from skin list
- `SetSlotVisible(string slotName, bool visible)` - sets slot to visible or not
- `SetSlotsVisibleByPrefix(string prefix, bool visible)` - sets many slots (picked by prefix in their name) to visible or not
- `Draw` - ironically this is empty because `SkeletonRenderer` must be used - use `DrawSpine` for that.
- `DrawSpine(SkeletonRenderer render)` - basically calls `render.Draw(Skeleton)` if asset is loaded(`Skeleton` object is forcelly loaded on few other methods like `Update`, so it should be created at this point)
- `Update(DrawComponent component)` - it does several things:
    - creates skeleton if it's not there (also recreates it if texture got disposed)
    - applies rotation, position, scale, main colour channel, flip effects from `DrawComponent`
    - applies skins - it is done by creating new empty `Skin` and adding each of skins from the list to it (sub-skins, a cool feature from spine)
    - applies colors - other than main channel, colour for each of parts
    - calls `Skeleton.UpdateWorldTransform()`

I suppose this is chaotic, but these methods are wrapped in draw systems and components, where they are probably easier to use - more about it in related chapters.

## Modding

1. You can override spine project asset by adding 3 main files to your mod (like with other content files).
2. You can add new skins by adding json file with `_skins` suffix in name.
3. You can add existing external skins by adding skin with same `Name` - only last loaded mod will be used.
4. If in external skin you use `Name` that is used by Spine project, it will not work, as skin from project will be found first.
