﻿using IniParser;
using IniParser.Model;
using System.Text;

namespace MonoPanda_ModManager {
  public class Configuration {
    private const string MONOPANDA_CONFIGURATION_FILE= "Content/MonoPanda/panda-config.ini";
    private const string MODS_SECTION_NAME = "Mods";

    private static Configuration instance;

    public string ModsFolder { get; set; }
    public string ModsSettingsJson { get; set; }
    public string ModInfoJson { get; set; }

    private Configuration() {
      IniData configIniData = new FileIniDataParser().ReadFile(MONOPANDA_CONFIGURATION_FILE, Encoding.UTF8);
      ModsFolder = configIniData[MODS_SECTION_NAME]["ModsFolder"];
      ModsSettingsJson = configIniData[MODS_SECTION_NAME]["ModsSettingsJson"];
      ModInfoJson = configIniData[MODS_SECTION_NAME]["ModInfoJson"];
    }

    public static Configuration GetInstance() {
      if (instance == null)
        instance = new Configuration();
      return instance;
    }
  }
}
