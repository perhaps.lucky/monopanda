﻿namespace MonoPanda_ModManager {
  partial class Form1 {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
      this.modsList = new System.Windows.Forms.ListBox();
      this.modsListObjectContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.activateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deactivateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.uninstallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.packToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.moveUpButton = new System.Windows.Forms.Button();
      this.moveDownButton = new System.Windows.Forms.Button();
      this.enableDisableButton = new System.Windows.Forms.Button();
      this.modsListLabel = new System.Windows.Forms.Label();
      this.saveButton = new System.Windows.Forms.Button();
      this.saveAndQuitButton = new System.Windows.Forms.Button();
      this.rescanButon = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.readmeButton = new System.Windows.Forms.Button();
      this.websiteLinkLabel = new System.Windows.Forms.LinkLabel();
      this.emailLinkLabel = new System.Windows.Forms.LinkLabel();
      this.authorValueLabel = new System.Windows.Forms.Label();
      this.websiteLabel = new System.Windows.Forms.Label();
      this.emailLabel = new System.Windows.Forms.Label();
      this.authorLabel = new System.Windows.Forms.Label();
      this.descriptionLabel = new System.Windows.Forms.Label();
      this.modPictureBox = new System.Windows.Forms.PictureBox();
      this.modNameLabel = new System.Windows.Forms.Label();
      this.descriptionTextBox = new System.Windows.Forms.TextBox();
      this.installButton = new System.Windows.Forms.Button();
      this.packButton = new System.Windows.Forms.Button();
      this.uninstallButton = new System.Windows.Forms.Button();
      this.modsListObjectContextMenuStrip.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.modPictureBox)).BeginInit();
      this.SuspendLayout();
      // 
      // modsList
      // 
      this.modsList.ContextMenuStrip = this.modsListObjectContextMenuStrip;
      this.modsList.FormattingEnabled = true;
      this.modsList.Location = new System.Drawing.Point(13, 23);
      this.modsList.Name = "modsList";
      this.modsList.Size = new System.Drawing.Size(238, 446);
      this.modsList.TabIndex = 0;
      this.modsList.SelectedIndexChanged += new System.EventHandler(this.modsList_SelectedIndexChanged);
      // 
      // modsListObjectContextMenuStrip
      // 
      this.modsListObjectContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem,
            this.activateToolStripMenuItem,
            this.deactivateToolStripMenuItem,
            this.toolStripSeparator1,
            this.uninstallToolStripMenuItem,
            this.packToolStripMenuItem});
      this.modsListObjectContextMenuStrip.Name = "modsListObjectContextMenuStrip";
      this.modsListObjectContextMenuStrip.Size = new System.Drawing.Size(139, 142);
      this.modsListObjectContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.modsListObjectContextMenuStrip_Opening);
      // 
      // moveUpToolStripMenuItem
      // 
      this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
      this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.moveUpToolStripMenuItem.Text = "Move Up";
      this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.moveUpToolStripMenuItem_Click);
      // 
      // moveDownToolStripMenuItem
      // 
      this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
      this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.moveDownToolStripMenuItem.Text = "Move Down";
      this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.moveDownToolStripMenuItem_Click);
      // 
      // activateToolStripMenuItem
      // 
      this.activateToolStripMenuItem.Name = "activateToolStripMenuItem";
      this.activateToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.activateToolStripMenuItem.Text = "Activate";
      this.activateToolStripMenuItem.Click += new System.EventHandler(this.activateToolStripMenuItem_Click);
      // 
      // deactivateToolStripMenuItem
      // 
      this.deactivateToolStripMenuItem.Name = "deactivateToolStripMenuItem";
      this.deactivateToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.deactivateToolStripMenuItem.Text = "Deactivate";
      this.deactivateToolStripMenuItem.Click += new System.EventHandler(this.deactivateToolStripMenuItem_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
      // 
      // uninstallToolStripMenuItem
      // 
      this.uninstallToolStripMenuItem.Name = "uninstallToolStripMenuItem";
      this.uninstallToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.uninstallToolStripMenuItem.Text = "Uninstall";
      this.uninstallToolStripMenuItem.Click += new System.EventHandler(this.uninstallToolStripMenuItem_Click);
      // 
      // packToolStripMenuItem
      // 
      this.packToolStripMenuItem.Name = "packToolStripMenuItem";
      this.packToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
      this.packToolStripMenuItem.Text = "Pack";
      this.packToolStripMenuItem.Click += new System.EventHandler(this.packToolStripMenuItem_Click);
      // 
      // moveUpButton
      // 
      this.moveUpButton.Enabled = false;
      this.moveUpButton.Location = new System.Drawing.Point(13, 482);
      this.moveUpButton.Name = "moveUpButton";
      this.moveUpButton.Size = new System.Drawing.Size(75, 23);
      this.moveUpButton.TabIndex = 1;
      this.moveUpButton.Text = "/\\";
      this.moveUpButton.UseVisualStyleBackColor = true;
      this.moveUpButton.Click += new System.EventHandler(this.moveUpButton_Click);
      // 
      // moveDownButton
      // 
      this.moveDownButton.Enabled = false;
      this.moveDownButton.Location = new System.Drawing.Point(95, 482);
      this.moveDownButton.Name = "moveDownButton";
      this.moveDownButton.Size = new System.Drawing.Size(75, 23);
      this.moveDownButton.TabIndex = 2;
      this.moveDownButton.Text = "\\/";
      this.moveDownButton.UseVisualStyleBackColor = true;
      this.moveDownButton.Click += new System.EventHandler(this.moveDownButton_Click);
      // 
      // enableDisableButton
      // 
      this.enableDisableButton.Enabled = false;
      this.enableDisableButton.Location = new System.Drawing.Point(176, 482);
      this.enableDisableButton.Name = "enableDisableButton";
      this.enableDisableButton.Size = new System.Drawing.Size(75, 23);
      this.enableDisableButton.TabIndex = 3;
      this.enableDisableButton.Text = "-";
      this.enableDisableButton.UseVisualStyleBackColor = true;
      this.enableDisableButton.Click += new System.EventHandler(this.enableDisableButton_Click);
      // 
      // modsListLabel
      // 
      this.modsListLabel.AutoSize = true;
      this.modsListLabel.Location = new System.Drawing.Point(13, 5);
      this.modsListLabel.Name = "modsListLabel";
      this.modsListLabel.Size = new System.Drawing.Size(36, 13);
      this.modsListLabel.TabIndex = 4;
      this.modsListLabel.Text = "Mods:";
      // 
      // saveButton
      // 
      this.saveButton.Location = new System.Drawing.Point(378, 493);
      this.saveButton.Name = "saveButton";
      this.saveButton.Size = new System.Drawing.Size(99, 36);
      this.saveButton.TabIndex = 5;
      this.saveButton.Text = "Save";
      this.saveButton.UseVisualStyleBackColor = true;
      this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
      // 
      // saveAndQuitButton
      // 
      this.saveAndQuitButton.Location = new System.Drawing.Point(486, 493);
      this.saveAndQuitButton.Name = "saveAndQuitButton";
      this.saveAndQuitButton.Size = new System.Drawing.Size(106, 36);
      this.saveAndQuitButton.TabIndex = 6;
      this.saveAndQuitButton.Text = "Save and Quit";
      this.saveAndQuitButton.UseVisualStyleBackColor = true;
      this.saveAndQuitButton.Click += new System.EventHandler(this.saveAndQuitButton_Click);
      // 
      // rescanButon
      // 
      this.rescanButon.Location = new System.Drawing.Point(269, 493);
      this.rescanButon.Name = "rescanButon";
      this.rescanButon.Size = new System.Drawing.Size(75, 36);
      this.rescanButon.TabIndex = 7;
      this.rescanButon.Text = "Reload mods list";
      this.rescanButon.UseVisualStyleBackColor = true;
      this.rescanButon.Click += new System.EventHandler(this.rescanButon_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.readmeButton);
      this.panel1.Controls.Add(this.websiteLinkLabel);
      this.panel1.Controls.Add(this.emailLinkLabel);
      this.panel1.Controls.Add(this.authorValueLabel);
      this.panel1.Controls.Add(this.websiteLabel);
      this.panel1.Controls.Add(this.emailLabel);
      this.panel1.Controls.Add(this.authorLabel);
      this.panel1.Controls.Add(this.descriptionLabel);
      this.panel1.Controls.Add(this.modPictureBox);
      this.panel1.Controls.Add(this.modNameLabel);
      this.panel1.Controls.Add(this.descriptionTextBox);
      this.panel1.Location = new System.Drawing.Point(269, 5);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(319, 472);
      this.panel1.TabIndex = 8;
      // 
      // readmeButton
      // 
      this.readmeButton.Location = new System.Drawing.Point(238, 394);
      this.readmeButton.Name = "readmeButton";
      this.readmeButton.Size = new System.Drawing.Size(75, 23);
      this.readmeButton.TabIndex = 11;
      this.readmeButton.Text = "Readme";
      this.readmeButton.UseVisualStyleBackColor = true;
      this.readmeButton.Click += new System.EventHandler(this.readmeButton_Click);
      // 
      // websiteLinkLabel
      // 
      this.websiteLinkLabel.AutoSize = true;
      this.websiteLinkLabel.Location = new System.Drawing.Point(80, 435);
      this.websiteLinkLabel.Name = "websiteLinkLabel";
      this.websiteLinkLabel.Size = new System.Drawing.Size(55, 13);
      this.websiteLinkLabel.TabIndex = 9;
      this.websiteLinkLabel.TabStop = true;
      this.websiteLinkLabel.Text = "linkLabel1";
      this.websiteLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.websiteLinkLabel_LinkClicked);
      // 
      // emailLinkLabel
      // 
      this.emailLinkLabel.AutoSize = true;
      this.emailLinkLabel.Location = new System.Drawing.Point(80, 450);
      this.emailLinkLabel.Name = "emailLinkLabel";
      this.emailLinkLabel.Size = new System.Drawing.Size(55, 13);
      this.emailLinkLabel.TabIndex = 8;
      this.emailLinkLabel.TabStop = true;
      this.emailLinkLabel.Text = "linkLabel1";
      this.emailLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.emailLinkLabel_LinkClicked);
      // 
      // authorValueLabel
      // 
      this.authorValueLabel.Location = new System.Drawing.Point(80, 420);
      this.authorValueLabel.Name = "authorValueLabel";
      this.authorValueLabel.Size = new System.Drawing.Size(234, 13);
      this.authorValueLabel.TabIndex = 7;
      this.authorValueLabel.Text = "label1";
      // 
      // websiteLabel
      // 
      this.websiteLabel.AutoSize = true;
      this.websiteLabel.Location = new System.Drawing.Point(0, 435);
      this.websiteLabel.Name = "websiteLabel";
      this.websiteLabel.Size = new System.Drawing.Size(49, 13);
      this.websiteLabel.TabIndex = 6;
      this.websiteLabel.Text = "Website:";
      // 
      // emailLabel
      // 
      this.emailLabel.AutoSize = true;
      this.emailLabel.Location = new System.Drawing.Point(0, 450);
      this.emailLabel.Name = "emailLabel";
      this.emailLabel.Size = new System.Drawing.Size(35, 13);
      this.emailLabel.TabIndex = 5;
      this.emailLabel.Text = "Email:";
      // 
      // authorLabel
      // 
      this.authorLabel.AutoSize = true;
      this.authorLabel.Location = new System.Drawing.Point(0, 420);
      this.authorLabel.Name = "authorLabel";
      this.authorLabel.Size = new System.Drawing.Size(41, 13);
      this.authorLabel.TabIndex = 4;
      this.authorLabel.Text = "Author:";
      // 
      // descriptionLabel
      // 
      this.descriptionLabel.AutoSize = true;
      this.descriptionLabel.Location = new System.Drawing.Point(3, 214);
      this.descriptionLabel.Name = "descriptionLabel";
      this.descriptionLabel.Size = new System.Drawing.Size(63, 13);
      this.descriptionLabel.TabIndex = 2;
      this.descriptionLabel.Text = "Description:";
      // 
      // modPictureBox
      // 
      this.modPictureBox.Location = new System.Drawing.Point(9, 61);
      this.modPictureBox.Name = "modPictureBox";
      this.modPictureBox.Size = new System.Drawing.Size(300, 150);
      this.modPictureBox.TabIndex = 1;
      this.modPictureBox.TabStop = false;
      // 
      // modNameLabel
      // 
      this.modNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.modNameLabel.Location = new System.Drawing.Point(3, 0);
      this.modNameLabel.Name = "modNameLabel";
      this.modNameLabel.Size = new System.Drawing.Size(313, 58);
      this.modNameLabel.TabIndex = 0;
      this.modNameLabel.Text = "Example Mod EXAMPLE ";
      this.modNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // descriptionTextBox
      // 
      this.descriptionTextBox.Location = new System.Drawing.Point(4, 231);
      this.descriptionTextBox.Multiline = true;
      this.descriptionTextBox.Name = "descriptionTextBox";
      this.descriptionTextBox.ReadOnly = true;
      this.descriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.descriptionTextBox.Size = new System.Drawing.Size(309, 163);
      this.descriptionTextBox.TabIndex = 10;
      // 
      // installButton
      // 
      this.installButton.Location = new System.Drawing.Point(13, 511);
      this.installButton.Name = "installButton";
      this.installButton.Size = new System.Drawing.Size(75, 23);
      this.installButton.TabIndex = 9;
      this.installButton.Text = "Install";
      this.installButton.UseVisualStyleBackColor = true;
      this.installButton.Click += new System.EventHandler(this.installButton_Click);
      // 
      // packButton
      // 
      this.packButton.Enabled = false;
      this.packButton.Location = new System.Drawing.Point(95, 511);
      this.packButton.Name = "packButton";
      this.packButton.Size = new System.Drawing.Size(75, 23);
      this.packButton.TabIndex = 10;
      this.packButton.Text = "Pack";
      this.packButton.UseVisualStyleBackColor = true;
      this.packButton.Click += new System.EventHandler(this.packButton_Click);
      // 
      // uninstallButton
      // 
      this.uninstallButton.Enabled = false;
      this.uninstallButton.Location = new System.Drawing.Point(176, 511);
      this.uninstallButton.Name = "uninstallButton";
      this.uninstallButton.Size = new System.Drawing.Size(75, 23);
      this.uninstallButton.TabIndex = 11;
      this.uninstallButton.Text = "Uninstall";
      this.uninstallButton.UseVisualStyleBackColor = true;
      this.uninstallButton.Click += new System.EventHandler(this.uninstallButton_Click);
      // 
      // Form1
      // 
      this.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(604, 541);
      this.Controls.Add(this.uninstallButton);
      this.Controls.Add(this.packButton);
      this.Controls.Add(this.installButton);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.rescanButon);
      this.Controls.Add(this.saveAndQuitButton);
      this.Controls.Add(this.saveButton);
      this.Controls.Add(this.modsListLabel);
      this.Controls.Add(this.enableDisableButton);
      this.Controls.Add(this.moveDownButton);
      this.Controls.Add(this.moveUpButton);
      this.Controls.Add(this.modsList);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "Form1";
      this.Text = "MonoPanda Mod Manager";
      this.modsListObjectContextMenuStrip.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.modPictureBox)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

        #endregion

        private System.Windows.Forms.ListBox modsList;
    private System.Windows.Forms.ContextMenuStrip modsListObjectContextMenuStrip;
    private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem activateToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem deactivateToolStripMenuItem;
    private System.Windows.Forms.Button moveUpButton;
    private System.Windows.Forms.Button moveDownButton;
    private System.Windows.Forms.Button enableDisableButton;
    private System.Windows.Forms.Label modsListLabel;
    private System.Windows.Forms.Button saveButton;
    private System.Windows.Forms.Button saveAndQuitButton;
    private System.Windows.Forms.Button rescanButon;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label modNameLabel;
        private System.Windows.Forms.PictureBox modPictureBox;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label authorLabel;
    private System.Windows.Forms.Label websiteLabel;
    private System.Windows.Forms.LinkLabel websiteLinkLabel;
    private System.Windows.Forms.LinkLabel emailLinkLabel;
    private System.Windows.Forms.Label authorValueLabel;
    private System.Windows.Forms.TextBox descriptionTextBox;
    private System.Windows.Forms.Button installButton;
    private System.Windows.Forms.Button packButton;
    private System.Windows.Forms.Button uninstallButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem uninstallToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem packToolStripMenuItem;
    private System.Windows.Forms.Button readmeButton;
  }
}

