﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MonoPanda_ModManager {
  public partial class Form1 : Form {

    private const string DEACTIVATE_TEXT = "Deactivate";
    private const string ACTIVATE_TEXT = "Activate";

    private ModManager modManager;

    public Form1() {
      InitializeComponent();
      initializeNonGenerated();
      clearModInfo();
      modManager.ScanMods();
    }

    private void initializeNonGenerated() {
      modsList.MouseClick += modsList_MouseClick;
      loadMods("Content/MonoPanda/mods.json");
    }

    private void loadMods(string path) {
      modManager = new ModManager();
      modManager.Load();
      refreshModList();
    }

    // I love this lazy design, sorry not sorry
    private void refreshModList() {
      modsList.Items.Clear();
      foreach (ModSettings mod in modManager.GetModsList()) {
        modsList.Items.Add((mod.Active ? "✔  " : "      ") + mod.Name);
      }
    }

    private void modsList_SelectedIndexChanged(object sender, EventArgs e) {
      var index = modsList.SelectedIndex;
      if (index == -1)
        return;
    }

    private void moveSelectedItemDown() {
      var index = modsList.SelectedIndex;
      if (index == -1 || index == modsList.Items.Count - 1)
        return;
      modManager.MoveDown(index);
      refreshModList();
      modsList.SelectedIndex = index + 1;
    }

    private void moveSelectedItemUp() {
      var index = modsList.SelectedIndex;
      if (index < 1)
        return;
      modManager.MoveUp(index);
      refreshModList();
      modsList.SelectedIndex = index - 1;
    }

    private void activateSelectedItem() {
      var index = modsList.SelectedIndex;
      if (index == -1)
        return;
      modManager.SetModActive(index, true);
      refreshModList();
      modsList.SelectedIndex = index;
      enableDisableButton.Text = DEACTIVATE_TEXT;
    }

    private void deactivateSelectedItem() {
      var index = modsList.SelectedIndex;
      if (index == -1)
        return;
      modManager.SetModActive(index, false);
      refreshModList();
      modsList.SelectedIndex = index;
      enableDisableButton.Text = ACTIVATE_TEXT;
    }

    private void switchSelectedItemState() {
      var isActive = modManager.GetModActive(modsList.SelectedIndex);
      if (isActive)
        deactivateSelectedItem();
      else
        activateSelectedItem();

    }

    private void modsList_MouseClick(object sender, MouseEventArgs e) {
      var index = modsList.SelectedIndex;
      if (index == -1) {
        moveDownButton.Enabled = false;
        moveUpButton.Enabled = false;
        enableDisableButton.Enabled = false;
        uninstallButton.Enabled = false;
        packButton.Enabled = false;
        clearModInfo();
        return;
      } else {
        moveDownButton.Enabled = true;
        moveUpButton.Enabled = true;
        enableDisableButton.Enabled = true;
        uninstallButton.Enabled = true;
        packButton.Enabled = true;
        if (modManager.GetModActive(index)) {
          enableDisableButton.Text = DEACTIVATE_TEXT;
        } else {
          enableDisableButton.Text = ACTIVATE_TEXT;
        }
        loadModInfo();
      }
    }

    private void uninstallAction() {
      var uninstallMessageBox = MessageBox.Show(
        "Are you sure you want to uninstall " + modManager.GetModsList()[modsList.SelectedIndex].Name + "? This action will delete mod directory from your hard drive.",
        "Uninstall mod",
        MessageBoxButtons.YesNo,
        MessageBoxIcon.Exclamation);

      if (uninstallMessageBox == DialogResult.No)
        return;


      try {
        modManager.UninstallMod(modsList.SelectedIndex);
        MessageBox.Show("Mod uninstalled successfully.", "Uninstallation success", MessageBoxButtons.OK, MessageBoxIcon.Information);
      } catch (ExternalScriptException) {
        MessageBox.Show("External script failure. Uninstallation interrupted.", "External script failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      refreshModList();
      if (modsList.SelectedIndex == -1)
        clearModInfo();
    }

    private void packAction() {
      using (SaveFileDialog dialog = new SaveFileDialog()) {
        dialog.InitialDirectory = Directory.GetCurrentDirectory();
        dialog.Filter = "zip files(*.zip)|*.zip|All files(*.*)|*.*";
        dialog.RestoreDirectory = true;
        dialog.FileName = modManager.GetModsList()[modsList.SelectedIndex].Name + ".zip";

        if (dialog.ShowDialog() == DialogResult.OK) {
          var path = dialog.FileName;
          modManager.PackMod(modsList.SelectedIndex, path);
          MessageBox.Show("Mod packed succesfully to: " + path, "Pack success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }
    }

    private void installButton_Click(object sender, EventArgs e) {
      using (OpenFileDialog dialog = new OpenFileDialog()) {
        dialog.InitialDirectory = Directory.GetCurrentDirectory();
        dialog.Filter = "zip files(*.zip)|*.zip|All files(*.*)|*.*";
        dialog.RestoreDirectory = true;

        if (dialog.ShowDialog() == DialogResult.OK) {
          try {
            var modName = dialog.SafeFileName.Replace(".zip", "");
            if (modManager.ModDirectoryExists(modName)) {
              var overwriteMessageBox = MessageBox.Show(
                "Mod directory already exists. If you continue, directory will be deleted. Do you want to continue?",
                "Directory exists",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning);

              if (overwriteMessageBox == DialogResult.No)
                return;
            }
            modManager.InstallMod(dialog.FileName, modName);
            MessageBox.Show("Mod installed successfully.", "Installation success", MessageBoxButtons.OK, MessageBoxIcon.Information);
          } catch (ExternalScriptException) {
            MessageBox.Show("External script failure. Installation interrupted.", "External script failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
          } catch (Exception ex) {
            MessageBox.Show("Error during mod installation: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          refreshModList();
        }
      }
    }

    private void modsListObjectContextMenuStrip_Opening(object sender, CancelEventArgs e) {
      var index = modsList.SelectedIndex;
      modsListObjectContextMenuStrip.Enabled = index != -1;
      if (index == -1) {
        return;
      }
      var active = modManager.GetModActive(index);
      moveUpToolStripMenuItem.Enabled = index > 0;
      moveDownToolStripMenuItem.Enabled = index < modsList.Items.Count - 1;
      activateToolStripMenuItem.Visible = !active;
      deactivateToolStripMenuItem.Visible = active;
    }

    private void clearModInfo() {
      modNameLabel.Visible = false;
      modPictureBox.Visible = false;
      descriptionLabel.Visible = false;
      descriptionTextBox.Visible = false;
      websiteLabel.Visible = false;
      websiteLinkLabel.Visible = false;
      emailLabel.Visible = false;
      emailLinkLabel.Visible = false;
      authorLabel.Visible = false;
      authorValueLabel.Visible = false;
      readmeButton.Visible = false;
    }

    private void loadModInfo() {
      clearModInfo();
      ModInfo modInfo = modManager.GetModInfo(modsList.SelectedIndex);
      modNameLabel.Visible = true;
      if (modInfo.FullName != null) {
        modNameLabel.Text = modInfo.FullName;
      } else {
        modNameLabel.Text = modsList.Items[modsList.SelectedIndex].ToString();
      }

      if (modInfo.Description != null) {
        descriptionLabel.Visible = true;
        descriptionTextBox.Visible = true;
        descriptionTextBox.Text = modInfo.Description.Replace("\n", Environment.NewLine);
      }

      if (modInfo.Author != null) {
        authorLabel.Visible = true;
        authorValueLabel.Visible = true;
        authorValueLabel.Text = modInfo.Author;
      }

      if (modInfo.Website != null) {
        websiteLabel.Visible = true;
        websiteLinkLabel.Visible = true;
        websiteLinkLabel.Text = modInfo.Website;
      }

      if (modInfo.Mail != null) {
        emailLabel.Visible = true;
        emailLinkLabel.Visible = true;
        emailLinkLabel.Text = modInfo.Mail;
      }

      if (modInfo.Image != null) {
        modPictureBox.Visible = true;
        byte[] imageBytes = Convert.FromBase64String(modInfo.Image);
        using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length)) {
          modPictureBox.Image = Image.FromStream(ms, true);
        }
      }

      if (modInfo.ReadmePath != null) {
        readmeButton.Visible = true;
      }
    }

    private void moveUpToolStripMenuItem_Click(object sender, EventArgs e) {
      moveSelectedItemUp();
    }

    private void moveDownToolStripMenuItem_Click(object sender, EventArgs e) {
      moveSelectedItemDown();
    }

    private void activateToolStripMenuItem_Click(object sender, EventArgs e) {
      activateSelectedItem();
    }

    private void deactivateToolStripMenuItem_Click(object sender, EventArgs e) {
      deactivateSelectedItem();
    }

    private void moveUpButton_Click(object sender, EventArgs e) {
      moveSelectedItemUp();
    }

    private void moveDownButton_Click(object sender, EventArgs e) {
      moveSelectedItemDown();
    }

    private void enableDisableButton_Click(object sender, EventArgs e) {
      switchSelectedItemState();
    }

    private void saveButton_Click(object sender, EventArgs e) {
      modManager.SaveMods();
    }

    private void saveAndQuitButton_Click(object sender, EventArgs e) {
      modManager.SaveMods();
      Application.Exit();
    }

    private void rescanButon_Click(object sender, EventArgs e) {
      var index = modsList.SelectedIndex;
      modManager.ScanMods();
      refreshModList();
      
      if (index == -1)
        return;
      modsList.SelectedIndex = index;
      if (modManager.GetModActive(index)) {
        enableDisableButton.Text = DEACTIVATE_TEXT;
      } else {
        enableDisableButton.Text = ACTIVATE_TEXT;
      }
    }

    private void websiteLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
      System.Diagnostics.Process.Start(websiteLinkLabel.Text);
    }

    private void emailLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
      System.Diagnostics.Process.Start("mailto:" + emailLinkLabel.Text);
    }

    private void uninstallButton_Click(object sender, EventArgs e) {
      uninstallAction();
    }

    private void uninstallToolStripMenuItem_Click(object sender, EventArgs e) {
      uninstallAction();
    }

    private void packButton_Click(object sender, EventArgs e) {
      packAction();
    }

    private void packToolStripMenuItem_Click(object sender, EventArgs e) {
      packAction();
    }

    private void readmeButton_Click(object sender, EventArgs e) {
      ModInfo modInfo = modManager.GetModInfo(modsList.SelectedIndex);
      var readmeFullPath = Directory.GetCurrentDirectory() + "/" 
        + modManager.GetModPath(modsList.Items[modsList.SelectedIndex].ToString(), modInfo.ReadmePath);
      System.Diagnostics.Process.Start(readmeFullPath);
    }
  }
}
