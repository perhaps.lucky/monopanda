﻿namespace MonoPanda_ModManager {
  public class ModInfo {
    public string FullName { get; set; }
    public string Description { get; set; }
    public string Author { get; set; }
    public string Mail { get; set; }
    public string Website { get; set; }
    public string Image { get; set; }
    public string ReadmePath { get; set; }
    public string InstallScriptPath { get; set; }
    public string UninstallScriptPath { get; set; }

    public static ModInfo Error() {
      return new ModInfo { FullName = "Mod info file not found!" };
    }
  }
}
