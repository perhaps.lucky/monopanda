﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace MonoPanda_ModManager {
  public class ModManager {

    private List<ModSettings> mods;

    public void Load() {
      if (File.Exists(Configuration.GetInstance().ModsSettingsJson)) {
        mods = JsonConvert.DeserializeObject<List<ModSettings>>(File.ReadAllText(Configuration.GetInstance().ModsSettingsJson));
      } else
        mods = new List<ModSettings>();
    }

    public List<ModSettings> GetModsList() {
      return mods;
    }

    public void SaveMods() {
      File.WriteAllText(Configuration.GetInstance().ModsSettingsJson, JsonConvert.SerializeObject(mods, Formatting.Indented));
    }

    public bool GetModActive(int index) {
      return mods[index].Active;
    }

    public bool SetModActive(int index, bool value) {
      return mods[index].Active = value;
    }

    public void ScanMods() {
      Load(); // external changes to mods that are already in file
      ScanForNewMods();
      RemoveDeletedMods();
    }

    public void MoveUp(int index) {
      var mod = mods[index];
      mods.RemoveAt(index);
      mods.Insert(index - 1, mod);
    }

    public void MoveDown(int index) {
      var mod = mods[index];
      mods.RemoveAt(index);
      mods.Insert(index + 1, mod);
    }

    public ModInfo GetModInfo(int index) {
      var modName = mods[index].Name;
      string path = GetModPath(modName, Configuration.GetInstance().ModInfoJson);
      if (File.Exists(path))
        return JsonConvert.DeserializeObject<ModInfo>(File.ReadAllText(path));

      return ModInfo.Error();
    }

    public string GetModPath(string modName, string pathToFile) {
      return Configuration.GetInstance().ModsFolder + modName + "/" + pathToFile;
    }

    private void ScanForNewMods() {
      foreach (var folderName in Directory.EnumerateDirectories(Configuration.GetInstance().ModsFolder).Select(Path.GetFileName)) {
        if (File.Exists(GetModPath(folderName, Configuration.GetInstance().ModInfoJson))
            && mods.Find(m => m.Name == folderName) == null) {
          var mod = new ModSettings(folderName, false);
          mods.Add(mod);
        }
      }
      SaveMods();
    }

    public void RemoveDeletedMods() {
      List<ModSettings> remove = new List<ModSettings>();
      foreach (string modName in mods.Select(m => m.Name)) {
        if (!Directory.Exists(Configuration.GetInstance().ModsFolder + modName)
          || !File.Exists(GetModPath(modName, Configuration.GetInstance().ModInfoJson))) {
          var mod = mods.Find(m => m.Name == modName);
          remove.Add(mod);
        }
      }

      mods.RemoveAll(m => remove.Contains(m));
      SaveMods();
    }

    public void UninstallMod(int index) {
      ModInfo modInfo = GetModInfo(index);

      if (modInfo.UninstallScriptPath != null) {
        doExternalScript(index, modInfo.UninstallScriptPath);
      }

      Directory.Delete(Configuration.GetInstance().ModsFolder + "/" + mods[index].Name, true);
      RemoveDeletedMods();
    }

    public void PackMod(int index, string savePath) {
      if (File.Exists(savePath)) {
        File.Delete(savePath);
      }
      ZipFile.CreateFromDirectory(
        Configuration.GetInstance().ModsFolder + "/" + mods[index].Name,
        savePath);
    }

    public void InstallMod(string path, string modName) {
      if (ModDirectoryExists(modName)) {
        Directory.Delete(Configuration.GetInstance().ModsFolder + "/" + modName, true);
        RemoveDeletedMods();
      }
      ZipFile.ExtractToDirectory(path, Configuration.GetInstance().ModsFolder + "/" + modName);
      ScanForNewMods();
      int index = mods.FindIndex((m) => m.Name.Equals(modName));
      var modInfo = GetModInfo(index);
      
      if (modInfo.InstallScriptPath != null) {
        try {
          doExternalScript(index, modInfo.InstallScriptPath);
        } catch (ExternalScriptException e) {
          // Delete unpacked mod
          Directory.Delete(Configuration.GetInstance().ModsFolder + "/" + mods[index].Name, true);
          RemoveDeletedMods();
          throw e; // throw for "frontend"
        }
      }

      if (modInfo.ReadmePath != null) {
        var readmeFullPath = Directory.GetCurrentDirectory() + "/" + GetModPath(mods[index].Name, modInfo.ReadmePath);
        System.Diagnostics.Process.Start(readmeFullPath);
      }

      SetModActive(index, true);
    }

    private void doExternalScript(int modIndex, string path) {
      var uninstallScriptProcess = System.Diagnostics.Process.Start(
        Directory.GetCurrentDirectory() 
        + "/" 
        + GetModPath(mods[modIndex].Name, path));

      uninstallScriptProcess.WaitForExit();

      if (uninstallScriptProcess.ExitCode != 0)
        throw new ExternalScriptException();
    }

    public bool ModDirectoryExists(string modName) {
      return Directory.Exists(Configuration.GetInstance().ModsFolder + "/" + modName);
    }



  }
}
