﻿namespace MonoPanda_ModManager {
  public class ModSettings {
    public string Name { get; set; }
    public bool Active { get; set; }

    public ModSettings() { }
    public ModSettings(string name, bool active) {
      Name = name;
      Active = active;
    }

    public override string ToString() {
      return Name;
    }
  }
}
