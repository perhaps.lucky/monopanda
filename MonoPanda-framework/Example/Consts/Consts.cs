﻿namespace Consts {
  public class Fonts {
    public const string ExampleFont = "example-font";
  }

  public class Inputs {
    public const string Input1A = "example-input-1";
    public const string Input2S = "example-input-2";
    public const string Input3D = "example-input-3";
    public const string Input4F = "example-input-4";
    public const string Input5G = "example-input-5";
    public const string Input6W = "example-input-6";
    public const string Exit = "exit";
    public const string NextState = "next-state";
    public const string PreviousState = "previous-state";
  }

  public class States {
    public const string StatesMain = "example-states-main";
    public const string StatesSecond = "example-states-second";
    public const string Input = "example-input";
    public const string Content = "example-content";
    public const string Language = "example-language";
    public const string Mods = "example-mods";
    public const string Sound = "example-sound";
    public const string Settings = "example-settings";
    public const string Timers = "example-timers";
    public const string ECS = "example-ecs";
    public const string Spine = "example-spine";
    public const string Pathfinding = "example-pathfinding";
    public const string LoadingScreen = "example-loading-screen";
    public const string Tree = "example-tree";
    public const string Tiled = "example-tiled";
  }

  public class Textures {
    public const string Square = "example-square";
    public const string BigFile = "example-big-file";
    public const string MonoPandaLogo = "monopanda-logo";
  }

  public class ContentPackages {
    public const string Example = "example-package";
    public const string Loading = "example-loading";
  }

  public class SFX {
    public const string Example = "example-sfx";
  }

  public class Song {
    public const string Example = "example-song";
  }
}
