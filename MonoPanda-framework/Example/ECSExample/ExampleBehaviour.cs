﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.ECS.Components.BehaviourComponent;
using MonoPanda.Timers;
using System;
using System.Collections.Generic;

namespace Example.ECSExample {
  public class ExampleBehaviour : Behaviour {

    public float Acceleration { get; set; }
    public bool Horizontal { get; set; }

    private RepeatingTimer timer;
    private int direction = 1;
    private int directionChangeTime;

    public override void Init(Dictionary<string, object> parameters) {
      directionChangeTime = Convert.ToInt32(parameters["DirectionChangeTime"]);
    }

    public override void PostInit() {
      timer = new RepeatingTimer(directionChangeTime, affectedByWorldTime: true, entity: this.Entity);
      timer.Initialize();
    }

    public override void Update() {
      var moveComponent = Entity.GetComponent<MoveComponent>();

      if(!Horizontal)
        moveComponent.Acceleration = new Vector2(0, direction * Acceleration);
      else
        moveComponent.Acceleration = new Vector2(direction * Acceleration, 0);

      if (timer.Check()) {
        direction *= -1;
      }
    }
  }
}
