﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;

namespace Example.PathfindingExample {
  public class PathfindingExampleFactory : EntityFactory {

    private static PathfindingExampleFactory instance;

    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(string modelId, Vector2 position, string id = null) {
      Entity entity = instance.CreateEntityInner(modelId, position, id);
      return entity;
    }
  }
}
