﻿using MonoPanda.ECS;
using MonoPanda.Pathfinding;

namespace Example.PathfindingExample {
  public class PawnComponent : EntityComponent {
    public PathfindingRequest PathfindingRequest { get; set; }
    public float Acceleration { get; set; }

    public PawnComponent() {
      PathfindingRequest = new PathfindingRequest();
    }
  }
}
