﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.ECS;

namespace Example.PathfindingExample {
  public class PawnSystem : EntitySystem {
    public PawnSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (PawnComponent pawn in ECS.GetAllComponents<PawnComponent>()) {
        if (pawn.PathfindingRequest.Finished) {
          TileComponent currentTargetTile = getCurrentTargetTile(pawn);
          goTo(currentTargetTile, pawn);
          checkCollision(currentTargetTile, pawn);
        }

      }
    }

    private TileComponent getCurrentTargetTile(PawnComponent pawn) {
      if (pawn.PathfindingRequest.Path.Count > 0) {
        var node = pawn.PathfindingRequest.Path[0];
        return (TileComponent)node.Data;
      }
      return null;
    }

    private void goTo(TileComponent target, PawnComponent pawn) {
      var moveComponent = pawn.Entity.GetComponent<MoveComponent>();
      moveComponent.Acceleration = new Vector2();
      if (target == null)
        return;

      var targetPosition = target.Entity.Position;
      var pawnPosition = pawn.Entity.Position;
      
      if (pawnPosition.X > targetPosition.X)
        moveComponent.Acceleration = new Vector2(-pawn.Acceleration, moveComponent.Acceleration.Y);
      else
        moveComponent.Acceleration = new Vector2(pawn.Acceleration, moveComponent.Acceleration.Y);

      if (pawnPosition.Y > targetPosition.Y)
        moveComponent.Acceleration = new Vector2(moveComponent.Acceleration.X, -pawn.Acceleration);
      else
        moveComponent.Acceleration = new Vector2(moveComponent.Acceleration.X, pawn.Acceleration);
    }

    private void checkCollision(TileComponent target, PawnComponent pawn) {
      if (target == null)
        return;

      var targetCollision = target.Entity.GetComponent<CollisionComponent>();
      var pawnCollision = pawn.Entity.GetComponent<CollisionComponent>();

      if (pawnCollision.IsColliding(targetCollision)) {
        pawn.PathfindingRequest.Path.RemoveAt(0);
        target.Status = TileStatus.Active;
      }
    }


  }
}
