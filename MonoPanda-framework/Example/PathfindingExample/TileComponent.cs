﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;
using MonoPanda.Pathfinding;


namespace Example.PathfindingExample {
  public class TileComponent : EntityComponent {
    public Node PathfindingNode { get; set; }
    public TileStatus Status { get; set; }
    public Point Position { get; set; }
  }
}
