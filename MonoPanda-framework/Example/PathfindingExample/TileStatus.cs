﻿namespace Example.PathfindingExample {
  public enum TileStatus {
    Active, Inactive, OnPath, Target
  }
}
