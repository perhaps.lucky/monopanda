﻿using Example.States;
using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.Flag;
using MonoPanda.Input;
using MonoPanda.Pathfinding;
using System;

namespace Example.PathfindingExample {
  public class TileSystem : EntitySystem {

    public PathfindingSystem PathfindingSystem { get; set; }

    private PawnComponent pawn; // just single one
    private OneTimeFlag readPathFlag;

    private Random random;

    public TileSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      PathfindingSystem = new PathfindingSystem(new DistanceHeuristic(node => ((TileComponent)node.Data).Position));
      PathfindingSystem.Initialize(4, System.Threading.ThreadPriority.Lowest);
      readPathFlag = new OneTimeFlag("pathfinding_change_tiles");
      random = new Random();
    }

    public override void OnComponentAdd(EntityComponent component) {
      if (component is PawnComponent) {
        pawn = component as PawnComponent;
      }
    }

    public override void Update() {
      var mousePosition = ECS.Camera.ScreenToWorld(InputManager.GetMouseWindowPosition());
      foreach (TileComponent component in ECS.GetAllComponents<TileComponent>()) {
        updateTextComponent(component);
        updateMouseActions(component, mousePosition);
      }

      if(pawn != null)
        readPath();
    }

    private void updateTextComponent(TileComponent component) {
      component.Entity.GetComponent<TextComponent>().Text = component.PathfindingNode.IsleID.ToString();
    }

    private void updateMouseActions(TileComponent component, Vector2 mousePosition) {
      if (component.Entity.GetComponent<CollisionComponent>().Contains(mousePosition)) {
        component.Entity.GetComponent<DrawComponent>().Color = Color.Yellow;
        if (InputManager.GetRightMouseButton().IsPressed)
          changeStatus(component);

        if (InputManager.GetLeftMouseButton().IsPressed) {
          pawn.PathfindingRequest.Target = component.PathfindingNode;
          pawn.PathfindingRequest.Start = findStartTile(pawn).PathfindingNode;
          PathfindingSystem.QueueRequest(pawn.PathfindingRequest);
          readPathFlag.Reinitialize();

          foreach (PawnComponent pawnComponent in ECS.GetAllComponents<PawnComponent>()) {
            if (pawnComponent == pawn)
              continue;
            pawnComponent.PathfindingRequest.Target = getRandomTile().PathfindingNode;
            pawnComponent.PathfindingRequest.Start = findStartTile(pawnComponent).PathfindingNode;
            PathfindingSystem.QueueRequest(pawnComponent.PathfindingRequest);
          }
        }
      } else {
        setStatusColor(component);
      }
    }

    private void setStatusColor(TileComponent component) {
      var drawComponent = component.Entity.GetComponent<DrawComponent>();
      switch (component.Status) {
        case TileStatus.Active:
          drawComponent.Color = Color.LightGray;
          break;
        case TileStatus.Inactive:
          drawComponent.Color = Color.DarkGray;
          break;
        case TileStatus.OnPath:
          drawComponent.Color = Color.Green;
          break;
        case TileStatus.Target:
          drawComponent.Color = Color.Cyan;
          break;
      }
    }

    private void changeStatus(TileComponent component) {
      if (component.Status == TileStatus.Active) {
        component.Status = TileStatus.Inactive;
        PathfindingSystem.DeactivateNode(component.PathfindingNode);
      } else {
        component.Status = TileStatus.Active;
        PathfindingSystem.ActivateNode(component.PathfindingNode);
      }
    }

    private TileComponent findStartTile(PawnComponent pawnComponent) {
      Vector2 pawnPosition = pawnComponent.Entity.Position;
      pawnPosition += new Vector2(250);
      int X = (int)(pawnPosition.X / 50);
      int Y = (int)(pawnPosition.Y / 50);
      return ECS.GetEntity(X + "," + Y).GetComponent<TileComponent>();
    }

    private void readPath() {
      var pathfindingRequest = pawn.PathfindingRequest;
      if (pathfindingRequest.Finished
        && readPathFlag.Check()) {

        removeCurrentPath();

        if (pathfindingRequest.FoundPath) {
          foreach (Node node in pathfindingRequest.Path) {
            TileComponent tile = (TileComponent)node.Data;
            tile.Status = TileStatus.OnPath;
          }
          ((TileComponent)pathfindingRequest.Target.Data).Status = TileStatus.Target;
        }
      }
    }

    private void removeCurrentPath() {
      foreach (TileComponent tile in ECS.GetAllComponents<TileComponent>()) {
        if (tile.Status == TileStatus.OnPath || tile.Status == TileStatus.Target)
          tile.Status = TileStatus.Active;
      }
    }

    private TileComponent getRandomTile() {
      int randX = random.Next(0, ExamplePathfindingState.TILES_SIDE_LENGTH);
      int randY = random.Next(0, ExamplePathfindingState.TILES_SIDE_LENGTH);
      Entity tileEntity = ECS.GetEntity(randX + "," + randY);
      TileComponent tileComponent = tileEntity.GetComponent<TileComponent>();
      if (tileComponent.Status == TileStatus.Inactive)
        return getRandomTile();
      return tileComponent;
    }
  }
}
