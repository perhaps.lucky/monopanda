﻿using Microsoft.Xna.Framework;

using MonoPanda.ParameterConvert;
using MonoPanda.Repository;
using MonoPanda.UI;

namespace Example.SpineExample {
  public class ColorEntity : RepositoryEntity, ISelectionItem {
    public string Label { get; set; }
    [ColorParameter]
    public Color Color { get; set; }

    public string GetLabel() => Label;
  }
}
