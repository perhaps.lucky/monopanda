﻿using MonoPanda.Repository;
using System.Collections.Generic;

namespace Example.SpineExample {
  public class ColorsRepository : Repository<ColorEntity> {

    private static ColorsRepository instance;

    protected override void setInstance(Repository<ColorEntity> repository) {
      instance = (ColorsRepository)repository;
    }

    public static List<ColorEntity> GetAll() {
      return instance.entities;
    }

    public static ColorEntity Get(string id) {
      return instance[id];
    }

    public static int Amount() {
      return instance.entities.Count;
    }

    public static ColorEntity Get(int i) {
      return instance.entities[i];
    }
  }
}
