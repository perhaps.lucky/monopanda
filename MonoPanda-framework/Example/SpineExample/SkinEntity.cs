﻿using MonoPanda.Repository;
using MonoPanda.UI;

namespace Example.SpineExample {
  public class SkinEntity : RepositoryEntity, ISelectionItem {
    public string Slot { get; set; }
    public string Label { get; set; }
    public string SkinId { get; set; }

    public string GetLabel() => Label;
  }
}
