﻿using MonoPanda.Repository;
using System.Collections.Generic;

namespace Example.SpineExample {
  public class SkinsRepository : Repository<SkinEntity> {

    private static SkinsRepository instance;

    protected override void setInstance(Repository<SkinEntity> repository) {
      instance = (SkinsRepository)repository;
    }

    public static List<SkinEntity> GetAllBySlot(string slot) {
      return instance.entities.FindAll(e => e.Slot.Equals(slot));
    }

    public static SkinEntity Get(string id) {
      return instance[id];
    }

    public static int Amount() {
      return instance.entities.Count;
    }

    public static SkinEntity Get(int i) {
      return instance.entities[i];
    }
  }
}
