﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;

namespace Example.SpineExample {
  public class SpineExampleFactory : EntityFactory {

    private static SpineExampleFactory instance;

    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(string modelId, Vector2 position, string id = null) {
      Entity entity = instance.CreateEntityInner(modelId, position, id);
      return entity;
    }
  }
}
