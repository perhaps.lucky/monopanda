﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.GlobalTime;
using MonoPanda.Input;
using MonoPanda.States;
using MonoPanda.UserSettings;
using MonoPanda.Utils;

namespace Example.States {
  public class ExampleInputState : State {

    private const int SQUARE_SPEED = 250;

    private Vector2 squarePosition;
    private bool squareHidden;

    public ExampleInputState(string id) : base(id) { }

    public override void Initialize() {
      squarePosition = new Vector2(Settings.Window.Width / 2, Settings.Window.Height - 50);
    }

    public override void Terminate() {
      
    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsHeld
        && squarePosition.X >= 0)
        squarePosition += new Vector2(-SQUARE_SPEED, 0) * Time.ElapsedCalculated;

      if (InputManager.GetKeyInput(Inputs.Input3D).IsHeld
        && squarePosition.X <= Settings.Window.Width - 50)
        squarePosition += new Vector2(SQUARE_SPEED, 0) * Time.ElapsedCalculated;

      if (InputManager.GetKeyInput(Inputs.Input4F).IsPressed)
        squareHidden = true;

      if (InputManager.GetKeyInput(Inputs.Input5G).IsReleased)
        squareHidden = false;

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Content);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesMain);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Inputs example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Shows reactions to different types of input actions (held/press/release).", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A/D - move square position to left/right", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "F - (on press) hide square", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240), "G - (on release) show square", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (content example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (states example)", Color.Gold, 20);

      if (!squareHidden)
        DrawUtils.DrawTexture(Textures.Square, squarePosition);

    }
  }
}
