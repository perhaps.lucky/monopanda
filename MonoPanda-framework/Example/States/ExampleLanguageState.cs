﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Configuration;
using MonoPanda.Input;
using MonoPanda.Languages;
using MonoPanda.Mods;
using MonoPanda.States;
using MonoPanda.UserSettings;

namespace Example.States {
  public class ExampleLanguageState : State {

    private int languageIndex;

    public ExampleLanguageState(string id) : base(id) { }

    public override void Initialize() {
      languageIndex = Language.GetLanguagesNames().IndexOf(Settings.Language.Language);
      // If language was set to language that was added by one of mods and then mod got deactived,
      // game will load default language instead. Modded language will also not be on list of
      // languages available in game.
      if (languageIndex == -1)
        languageIndex = Language.GetLanguagesNames().IndexOf(Config.Language.Language);
    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed) {
        languageIndex--;
        if (languageIndex == -1)
          languageIndex = Language.GetLanguagesNames().Count - 1;
      }


      if (InputManager.GetKeyInput(Inputs.Input3D).IsPressed) {
        languageIndex++;
        if (languageIndex == Language.GetLanguagesNames().Count)
          languageIndex = 0;
      }

      if (InputManager.GetKeyInput(Inputs.Input4F).IsPressed) {
        var languageId = Language.GetLanguagesNames()[languageIndex];
        Language.LoadLanguage(languageId);
      }

      if (InputManager.GetKeyInput(Inputs.Input5G).IsReleased)
        Settings.Save();

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Mods);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Content);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Language example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Shows line loading from language files and language settings. \nSee mod example state for more.", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A/D - switch language (does not apply the change)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "F - apply language", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240), "G - save language settings (current language will be loaded on next startup)", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (mods example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (content example)", Color.Gold, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 310), "Language: " + Language.GetLanguagesNames()[languageIndex], Color.Navy, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 340), "Example: \n" + Language.Get("example"), Color.Navy, 23);

      // Yep that's not how mods should be ever working
      // So let's assume this mod added that line in some more civilized way.
      // Furthermore translation mod shouldn't be here either, but this way it will show how game behaves when main language is missing.
      if (ModManager.GetActiveMods().Find(m => m.Name == "Example-Language-NewLine" && m.Active) != null
        || ModManager.GetActiveMods().Find(m => m.Name == "Example-Language-NewLineTranslation" && m.Active) != null)
        FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 400), "Example from mod: \n" + Language.Get("example.mod"), Color.Navy, 23);
    }
  }
}
