﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Content;
using MonoPanda.Input;
using MonoPanda.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.States {
  public class ExampleLoadingScreenState : State {

    private ContentRequest loading;

    public ExampleLoadingScreenState(string id) : base(id) {
    }

    public override void Initialize() {
      
    }

    public override void Terminate() {
      
    }

    public override void Update() {
      // terminating state to stop the music
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Tree, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Pathfinding);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed)
        unloadContent();

      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed)
        startLoading();

    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Loading screen example", Color.GreenYellow, 40);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - forcefully unload everything", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - start loading", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (tree example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (pathfinding example)", Color.Gold, 20);

      if (isLoadingOn()) {
        FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(200, 300), "Loading status: " + (float)loading.ItemsLoaded / (float)loading.ItemsToLoad * 100 + "% [" + loading.ItemsLoaded + "/" + loading.ItemsToLoad + "]" , Color.AliceBlue, 25);
      }

    }

    private bool isLoadingOn() {
      return loading != null && loading.Processing;
    }

    private void unloadContent() {
      foreach (var id in ContentManager.getContentPackage(ContentPackages.Loading).Items)
        ContentManager.getItem(id).unload();
    }

    private void startLoading() {
      loading = ContentManager.RequestPackageLoad(ContentPackages.Loading);
    }
  }
}
