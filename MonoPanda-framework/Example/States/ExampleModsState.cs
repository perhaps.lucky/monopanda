﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.Mods;
using MonoPanda.States;
using MonoPanda.UI;
using System;

namespace Example.States {
  public class ExampleModsState : State {

    private UISystem ui;

    public ExampleModsState(string id) : base(id) { }

    public override void Initialize() {
      ui = new UISystem();
      refreshTable();
    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Sound);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Language, true, true);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      ui.Update();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Mods example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "This is table with currently loaded mods. You can scan for new mods, then activate/deactivate each of them. \nYou can also change the order of loading. \nAfter saving changes you will need to restart the game (altho language mods are partially working).", Color.Yellow, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (sound example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (language example)", Color.Gold, 20);

      ui.Draw();
    }

    private void refreshTable() {
      ui.Clear();
      Vector2 pos = new Vector2(100, 150);
      foreach (var mod in ModManager.GetAllMods()) {
        var modWrapper = new ModWrapper(mod.Name, refreshTable);
        ui.AddObject(new Text(Fonts.ExampleFont, pos, mod.Name, mod.Active ? Color.Yellow : Color.Black, 20));
        ui.AddObject(new TextButton(Fonts.ExampleFont, pos + new Vector2(300, 0), mod.Active ? "Deactivate" : "Activate", 20, modWrapper.SwitchActive, Color.Cyan, Color.Yellow, Color.Red));
        ui.AddObject(new TextButton(Fonts.ExampleFont, pos + new Vector2(400, 0), "Move down", 20, modWrapper.MoveDown, Color.Cyan, Color.Yellow, Color.Red));
        ui.AddObject(new TextButton(Fonts.ExampleFont, pos + new Vector2(500, 0), "Move up", 20, modWrapper.MoveUp, Color.Cyan, Color.Yellow, Color.Red));
        pos += new Vector2(0, 30);
      }

      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(500, 400), "Scan mods", 25, scanForMods, Color.Cyan, Color.Yellow, Color.Red));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(500, 500), "Save changes", 25, saveChanges, Color.Cyan, Color.Yellow, Color.Red));
    }

    private void saveChanges() {
      ModManager.SaveMods();
    }

    private void scanForMods() {
      ModManager.RemoveDeletedMods();
      ModManager.ScanForNewMods();
      refreshTable();
    }


    private class ModWrapper {
      private string name;
      private Action refreshAction;


      public ModWrapper(string name, Action refreshAction) {
        this.name = name;
        this.refreshAction = refreshAction;
      }

      public void SwitchActive() {
        var mod = ModManager.GetAllMods().Find(m => m.Name.Equals(name));
        mod.Active = !mod.Active;
        refreshAction.Invoke();
      }

      public void MoveDown() {
        var allModsList = ModManager.GetAllMods();
        var mod = allModsList.Find(m => m.Name.Equals(name));
        var index = allModsList.IndexOf(mod);

        if (index == allModsList.Count - 1)
          return;

        allModsList.Remove(mod);
        allModsList.Insert(index + 1, mod);
        refreshAction.Invoke();
      }

      public void MoveUp() {
        var allModsList = ModManager.GetAllMods();
        var mod = allModsList.Find(m => m.Name.Equals(name));
        var index = allModsList.IndexOf(mod);

        if (index == 0)
          return;

        allModsList.Remove(mod);
        allModsList.Insert(index - 1, mod);
        refreshAction.Invoke();
      }

    }
  }
}
