﻿using MonoPanda.States;

using Example.PathfindingExample;

using MonoPanda.BitmapFonts;

using Consts;

using Microsoft.Xna.Framework;

using MonoPanda.Input;
using MonoPanda.Pathfinding;
using MonoPanda;
using MonoPanda.Systems;
using MonoPanda.GlobalTime;
using MonoPanda.ECS;
using MonoPanda.Console;

namespace Example.States {
  public class ExamplePathfindingState : State {
    public const int TILES_SIDE_LENGTH = 40;
    private const int PAWNS_AMOUNT = 100;
    private const int CAMERA_SPEED = 500;

    private EntityComponentSystem ecs;

    private Entity followEntity;

    public ExamplePathfindingState(string id) : base(id) { }

    public override void Initialize() {
      ecs = EntityComponentSystem.LoadFromJson("Pathfinding_Example/entity-component-system.json");
      PathfindingExampleFactory.CreateInstance<PathfindingExampleFactory>(
        "Pathfinding_Example/pathfinding-entity-factory.json", ecs);

      initializeTiles();
      for (int i = 0; i < PAWNS_AMOUNT; i++)
        PathfindingExampleFactory.CreateEntity("pawn", new Vector2());

      followEntity = ecs.CreateEntity(new Vector2(0, 0));
      ecs.GetSystem<CameraMovementSystem>().Follow(followEntity);

      ConsoleSystem.SetECS(ecs);
    }

    public override void Terminate() {
    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.LoadingScreen, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Spine, true);

      if (InputManager.GetKeyInput(Inputs.Input1A).IsHeld)
        followEntity.Position += new Vector2(-CAMERA_SPEED, 0) * Time.ElapsedCalculated;

      if (InputManager.GetKeyInput(Inputs.Input2S).IsHeld)
        followEntity.Position += new Vector2(0, CAMERA_SPEED) * Time.ElapsedCalculated;

      if (InputManager.GetKeyInput(Inputs.Input3D).IsHeld)
        followEntity.Position += new Vector2(CAMERA_SPEED, 0) * Time.ElapsedCalculated;

      if (InputManager.GetKeyInput(Inputs.Input6W).IsHeld)
        followEntity.Position += new Vector2(0, -CAMERA_SPEED) * Time.ElapsedCalculated;

      if (InputManager.ScrolledUp())
        ecs.GetSystem<CameraZoomSystem>().NextZoomStep();

      if (InputManager.ScrolledDown())
        ecs.GetSystem<CameraZoomSystem>().PreviousZoomStep();

      ecs.Update();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Pathfinding example", Color.GreenYellow, 40);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (loading screen example)",
        Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (spine example)", Color.Gold,
        20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 80), "LMB - order to move", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 110), "RMB - switch wall", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 110), "Entity count: " + ecs.GetAllEntities().Count,
        Color.AliceBlue, 20);

      ecs.Draw();
    }

    private void initializeTiles() {
      PathfindingSystem pathfindingSystem = ecs.GetSystem<TileSystem>().PathfindingSystem;
      for (var y = 0; y < TILES_SIDE_LENGTH; y++) {
        for (var x = 0; x < TILES_SIDE_LENGTH; x++) {
          Entity createdTileEntity =
            PathfindingExampleFactory.CreateEntity("tile", new Vector2(-250 + x * 51, -250 + y * 51), x + "," + y);
          createdTileEntity.GetComponent<TileComponent>().Position = new Point(x, y);
          if (y == 0)
            createFirstRowNode(x, createdTileEntity, pathfindingSystem);
          else
            createNextRowNode(x, y, createdTileEntity, pathfindingSystem);
        }
      }
    }

    private void createFirstRowNode(int x, Entity entity, PathfindingSystem pathfindingSystem) {
      var tileComponent = entity.GetComponent<TileComponent>();
      var entityOnTheLeft = ecs.GetEntityOptional(x - 1 + "," + 0);
      if (entityOnTheLeft.IsPresent()) {
        var nodeOnTheLeft = entityOnTheLeft.Get().GetComponent<TileComponent>().PathfindingNode;
        Node createdNode = tileComponent.PathfindingNode = pathfindingSystem.AddNode(nodeOnTheLeft);
        createdNode.Data = tileComponent;
      } else {
        // x == 0
        Node createdNode = tileComponent.PathfindingNode = pathfindingSystem.AddNode();
        createdNode.Data = tileComponent;
      }
    }

    private void createNextRowNode(int x, int y, Entity entity, PathfindingSystem pathfindingSystem) {
      var tileComponent = entity.GetComponent<TileComponent>();
      var nodeAbove = ecs.GetEntity(x + "," + (y - 1)).GetComponent<TileComponent>().PathfindingNode;
      Node createdNode = tileComponent.PathfindingNode = pathfindingSystem.AddNode(nodeAbove);
      createdNode.Data = tileComponent;
      // Create edge to node on the left
      ecs.GetEntityOptional((tileComponent.Position.X - 1) + "," + tileComponent.Position.Y)
        .IfPresent(neighbour =>
          pathfindingSystem.CreateEdge(neighbour.GetComponent<TileComponent>().PathfindingNode.ID,
            tileComponent.PathfindingNode.ID, 1)
        );
    }
  }
}