﻿using Consts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.Sound;
using MonoPanda.States;
using MonoPanda.UI;
using MonoPanda.UserSettings;
using System.Collections.Generic;

namespace Example.States {
  public class ExampleSettingsState : State {

    private UISystem ui;

    private List<DisplayMode> displayModes;
    private int currentDisplayIndex;
    
    public ExampleSettingsState(string id) : base(id) { }

    public override void Initialize() {
      SoundManager.PlayMusic(Song.Example, true);
      ui = new UISystem();

      displayModes = new List<DisplayMode>(GraphicsAdapter.DefaultAdapter.SupportedDisplayModes);
      currentDisplayIndex = displayModes.FindIndex(dm => dm.Width == Settings.Window.Width && dm.Height == Settings.Window.Height);
      if (currentDisplayIndex == -1)
        currentDisplayIndex = 0;

      refreshUI();
    }

    public override void Terminate() {
      SoundManager.StopMusic();
    }

    public override void Update() {
      // terminating state to stop the music
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Timers, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Sound, true);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      ui.Update();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Settings example", Color.GreenYellow, 40);
      
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (timers example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (sound example)", Color.Gold, 20);

      ui.Draw();
    }

    private void refreshUI() {
      ui.Clear();

      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(20, 100), "Sound volume:", Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(150, 100), "<", 20, SFXDown, Color.Cyan, Color.Yellow, Color.Red));
      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(165, 100), Settings.Sound.SFXVolume.ToString(), Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(195, 100), ">", 20, SFXUp, Color.Cyan, Color.Yellow, Color.Red));

      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(20, 130), "Music volume:", Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(150, 130), "<", 20, musicDown, Color.Cyan, Color.Yellow, Color.Red));
      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(165, 130), Settings.Sound.MusicVolume.ToString(), Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(195, 130), ">", 20, musicUp, Color.Cyan, Color.Yellow, Color.Red));

      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(20, 160), "Resolution:", Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(150, 160), "<", 20, previousResolution, Color.Cyan, Color.Yellow, Color.Red));
      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(165, 160), displayModes[currentDisplayIndex].Width + " x " + displayModes[currentDisplayIndex].Height, Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(250, 160), ">", 20, nextResolution, Color.Cyan, Color.Yellow, Color.Red));

      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(20, 190), "Fullscreen:", Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(150, 190), "<", 20, switchFullscreen, Color.Cyan, Color.Yellow, Color.Red));
      ui.AddObject(new Text(Fonts.ExampleFont, new Vector2(165, 190), Settings.Window.Fullscreen ? "yes" : "no", Color.Azure, 20));
      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(250, 190), ">", 20, switchFullscreen, Color.Cyan, Color.Yellow, Color.Red));

      ui.AddObject(new TextButton(Fonts.ExampleFont, new Vector2(20, 250), "Save settings", 25, saveSettings, Color.Cyan, Color.Yellow, Color.Red));

    }

    private void saveSettings() {
      Settings.Save();
    }

    private void switchFullscreen() {
      GameSettings.SetFullscreen(!Settings.Window.Fullscreen);
      refreshUI();
    }

    private void nextResolution() {
      currentDisplayIndex++;
      if (currentDisplayIndex == displayModes.Count)
        currentDisplayIndex = 0;

      DisplayMode dm = displayModes[currentDisplayIndex];
      GameSettings.SetResolution(dm.Width, dm.Height);

      refreshUI();
    }

    private void previousResolution() {
      currentDisplayIndex--;
      if (currentDisplayIndex == -1)
        currentDisplayIndex = displayModes.Count - 1;

      DisplayMode dm = displayModes[currentDisplayIndex];
      GameSettings.SetResolution(dm.Width, dm.Height);

      refreshUI();
    }

    private void SFXUp() {
      SoundManager.AdjustSFXVolume(10);
      SoundManager.PlaySFX(SFX.Example);
      refreshUI();
    }

    private void SFXDown() {
      SoundManager.AdjustSFXVolume(-10);
      SoundManager.PlaySFX(SFX.Example);
      refreshUI();
    }

    private void musicUp() {
      SoundManager.AdjustMusicVolume(10);
      refreshUI();
    }

    private void musicDown() {
      SoundManager.AdjustMusicVolume(-10);
      refreshUI();
    }

  }
}
