﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.Sound;
using MonoPanda.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.States {
  public class ExampleSoundState : State {

    public ExampleSoundState(string id) : base(id) { }

    public override void Initialize() {

    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed) {
        SoundManager.PlaySFX(SFX.Example);
      }


      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed) {
        SoundManager.PlayMusic(Song.Example, true);
      }

      if (InputManager.GetKeyInput(Inputs.Input3D).IsPressed) {
        SoundManager.StopMusic();
      }

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Settings);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Mods);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Sound example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "Simple SFX and music example", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - play SFX", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - play music (looped)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240), "D - stop music", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (settings example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (mods example)", Color.Gold, 20);

    }
  }
}