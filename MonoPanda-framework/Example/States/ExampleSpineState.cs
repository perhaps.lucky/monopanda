﻿using Consts;
using Example.SpineExample;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.Input;
using MonoPanda.States;
using MonoPanda.UI;
using System.Collections.Generic;

namespace Example.States {
  public class ExampleSpineState : State {

    private EntityComponentSystem ecs;
    private Entity creature;

    private UISystem ui;

    public ExampleSpineState(string id) : base(id) { }

    public override void Initialize() {
      ecs = EntityComponentSystem.LoadFromJson("Spine_Example/entity-component-system.json");

      SpineExampleFactory.CreateInstance<SpineExampleFactory>("Spine_Example/spine-entity-factory.json", ecs);
      ColorsRepository.CreateRepository<ColorsRepository>("Spine_Example/colors.json");
      SkinsRepository.CreateRepository<SkinsRepository>("Spine_Example/skins.json");

      creature = SpineExampleFactory.CreateEntity("creature", new Vector2(0, 0));
      creature.GetComponent<SpineComponent>().PlayAnimation(0, "breath", true, 0f);
      creature.GetComponent<SpineComponent>().PlayAnimation(1, "stand", true, 0f);

      ui = new UISystem();

      List<ISelectionItem> colors = new List<ISelectionItem>();
      colors.AddRange(ColorsRepository.GetAll());
      ui.AddObject(new TextSelection(Fonts.ExampleFont, new Vector2(600, 150), 30, 100, "Fur color", Color.GreenYellow, Color.BlueViolet, Color.MediumPurple, colors, 25, setFurColor));

      List<ISelectionItem> hats = new List<ISelectionItem>();
      hats.AddRange(SkinsRepository.GetAllBySlot("hat"));
      ui.AddObject(new TextSelection(Fonts.ExampleFont, new Vector2(600, 250), 30, 100, "Hat", Color.GreenYellow, Color.BlueViolet, Color.MediumPurple, hats, 25, setHat));

      List<ISelectionItem> eyes = new List<ISelectionItem>();
      eyes.AddRange(SkinsRepository.GetAllBySlot("eyes"));
      ui.AddObject(new TextSelection(Fonts.ExampleFont, new Vector2(600, 350), 30, 100, "Eyes", Color.GreenYellow, Color.BlueViolet, Color.MediumPurple, eyes, 25, setEyes));

      List<ISelectionItem> mouths = new List<ISelectionItem>();
      mouths.AddRange(SkinsRepository.GetAllBySlot("mouth"));
      ui.AddObject(new TextSelection(Fonts.ExampleFont, new Vector2(600, 450), 30, 100, "Mouth", Color.GreenYellow, Color.BlueViolet, Color.MediumPurple, mouths, 25, setMouth));

      ui.ForceOnChangeActions();
    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Pathfinding);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.ECS);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      ecs.Update();
      ui.Update();

      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed) {
        creature.GetComponent<SpineComponent>().PlayAnimation(1, "walk", true);
      }

      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed) {
        creature.GetComponent<SpineComponent>().PlayAnimation(1, "dance", true);
      }

      if (InputManager.GetKeyInput(Inputs.Input3D).IsPressed) {
        creature.GetComponent<SpineComponent>().PlayAnimation(1, "stand", true);
      }
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Spine example", Color.GreenYellow, 40);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (Pathfinding example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (ECS example)", Color.Gold, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A,S,D - Change animations", Color.AliceBlue, 20);

      ecs.Draw();
      ui.Draw();
    }

    private void setFurColor(ISelectionItem colorEntity) {
      creature.GetComponent<SpineComponent>().ApplyColor("body_fur", ((ColorEntity)colorEntity).Color);
    }

    private void setHat(ISelectionItem skinEntity) {
      creature.GetComponent<SpineComponent>().ApplySkin("hat", ((SkinEntity)skinEntity).SkinId);
    }

    private void setEyes(ISelectionItem skinEntity) {
      creature.GetComponent<SpineComponent>().ApplySkin("eyes", ((SkinEntity)skinEntity).SkinId);
    }

    private void setMouth(ISelectionItem skinEntity) {
      creature.GetComponent<SpineComponent>().ApplySkin("mouth", ((SkinEntity)skinEntity).SkinId);
    }
  }
}