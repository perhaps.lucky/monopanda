﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.States;
using System;

namespace Example.States {
  public class ExampleStatesMainState : State {

    private string initTime;
    private StateRequest stateRequest;

    public ExampleStatesMainState(string id) : base(id) { }

    public override void Initialize() {
      initTime = DateTime.Now.ToString("HH:mm:ss");
    }

    public override void Terminate() {
      stateRequest = null;
    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesSecond);

      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesSecond, true);

      if (InputManager.GetKeyInput(Inputs.Input3D).IsPressed)
        stateRequest = StateManager.PrepareState(Consts.States.StatesSecond, false);

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Input);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Tiled);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "States example - main state", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "This state and its code represents changing between states. \nIt is set as initial state in config file.", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - go to second state, don't terminate this state", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - go to second state, terminate this state", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 240),
        $"D - request initialize 2nd state [STATUS: {getStateStatus()}]", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (input example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (tiled example)", Color.Gold, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 150), "This state has been initialized at: \n" + initTime, Color.Aquamarine, 25);
    }

    private String getStateStatus() {
      if (stateRequest == null)
        return "no request";
      
      if (stateRequest.Processing)
        return "processing";

      if (stateRequest.InQueue)
        return "in queue";
      
      if (stateRequest.Finished)
        return "finished";

      return "shouldn't happen lol";
    }
  }
}
