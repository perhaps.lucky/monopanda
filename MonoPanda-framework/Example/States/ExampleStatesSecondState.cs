﻿using Consts;
using Microsoft.Xna.Framework;
using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.States;
using System;

namespace Example.States {
  public class ExampleStatesSecondState : State {

    private string initTime;

    public ExampleStatesSecondState(string id) : base(id) { }

    public override void Initialize() {
      initTime = DateTime.Now.ToString("HH:mm:ss");
    }

    public override void Terminate() {

    }

    public override void Update() {
      if (InputManager.GetKeyInput(Inputs.Input1A).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesMain);

      if (InputManager.GetKeyInput(Inputs.Input2S).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesMain, false, true);
    }

    public override void Draw() {
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "States example - second state", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70), "This is second state, initialized from main state", Color.Yellow, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 150), "Actions: ", Color.AntiqueWhite, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 180), "A - go back to main state (if it got terminated, it will be initialized again)", Color.AliceBlue, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 210), "S - go back to main state and reinitialize", Color.AliceBlue, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(500, 150), "This state has been initialized at: \n" + initTime, Color.Aquamarine, 25);
    }
  }
}
