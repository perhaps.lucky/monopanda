﻿using Consts;

using Example.TiledExample;

using Microsoft.Xna.Framework;

using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.States;
using MonoPanda.Tiled;
using MonoPanda.Tiled.Handlers;

using System.Drawing;

using Color = Microsoft.Xna.Framework.Color;

namespace Example.States {
  public class ExampleTiledState : State {
    private EntityComponentSystem ecs;


    public ExampleTiledState(string id) : base(id) {
    }

    public override void Initialize() {
      ecs = EntityComponentSystem.LoadFromJson("Tiled_Example/tiled-ecs.json");
      ecs.Camera.Zoom = 1.5f;
      ecs.Camera.Position = new Vector2(128, 128);
      TiledFactory.CreateInstance<TiledFactory>("Tiled_Example/tiled-factory.json", ecs);

      TiledFactory.CreateEntity("controlled-entity", new Vector2(0, 0));

      new TiledMapLoader()
        .AddTileHandler("tileset",
          (tile, tileMapPosition, worldPosition, layer) => TiledFactory.CreateTile(worldPosition, tile, layer))
        .AddObjectHandlerByName("house",
          ((position, o, layer) => TiledFactory.CreateHouse(position, o, layer)))
        .AddObjectHandlerByType("colorParameterSpriteThing",
          postInit: ((entity, o, layer) =>
            entity.GetComponent<DrawComponent>().Color = o.GetColorProperty("color", Color.White)))
        .AddObjectHandlerByType("text",
          (position, o, layer) => TiledFactory.CreateTextObject(position, o, layer))
        .AddObjectHandlerByType("trigger", (position, o, layer) => TiledFactory.CreateTriggerObject(position, o),
          (entity, o, layer) => entity.GetComponent<TriggerComponent>().TargetEntity = o.GetObjectProperty("target"))
        .LoadMap("tiled-map");
    }

    public override void Terminate() {
      ecs.Destroy();
    }

    public override void Update() {
      Entity house = ecs.GetEntity("house");

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.StatesMain);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.Tree);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();

      ecs.Update();
    }

    public override void Draw() {
      ecs.Draw();

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Tiled example", Color.GreenYellow, 40);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (states example)", Color.Gold, 20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (tree example)", Color.Gold,
        20);
    }
  }
}