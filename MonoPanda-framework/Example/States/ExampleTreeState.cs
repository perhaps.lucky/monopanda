﻿using Consts;

using Example.TreeExample;

using Microsoft.Xna.Framework;

using MonoPanda;
using MonoPanda.BitmapFonts;
using MonoPanda.ECS;
using MonoPanda.Input;
using MonoPanda.States;

namespace Example.States {
  public class ExampleTreeState : State {
    private EntityComponentSystem ECS;

    public ExampleTreeState(string id) : base(id) {
    }

    public override void Initialize() {
      ECS = EntityComponentSystem.LoadFromJson("Tree_Example/tree-ecs.json");
      TreeFactory.CreateInstance<TreeFactory>("Tree_Example/tree-factory.json", ECS);
      TreeFactory.CreateEntity("controlled-entity", new Vector2(-200, 200));
      TreeFactory.CreateEntity("tree", Vector2.Zero);
    }

    public override void Terminate() {
      ECS.Destroy();
    }

    public override void Update() {
      ECS.Update();

      if (InputManager.GetKeyInput(Inputs.NextState).IsPressed)
        StateManager.SetActiveState(Consts.States.Tiled, true);

      if (InputManager.GetKeyInput(Inputs.PreviousState).IsPressed)
        StateManager.SetActiveState(Consts.States.LoadingScreen, true);

      if (InputManager.GetKeyInput(Inputs.Exit).IsPressed)
        GameMain.ExitGame();
    }

    public override void Draw() {
      ECS.Draw();

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 20), "Tree example", Color.GreenYellow, 40);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 70),
        "A factory with single object with two separated draw components.", Color.Yellow, 20);

      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 500), "M - next state (tiled example)", Color.Gold,
        20);
      FontRenderer.DrawText(Fonts.ExampleFont, new Vector2(20, 530), "N - previous state (loading screen example)",
        Color.Gold, 20);
    }
  }
}