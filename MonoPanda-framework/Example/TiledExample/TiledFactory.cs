﻿using Microsoft.Xna.Framework;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.Tiled.Json;
using MonoPanda.Utils;

namespace Example.TiledExample {
  public class TiledFactory : EntityFactory {

    private static TiledFactory instance;

    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(string modelId, Vector2 position, string id = null) {
      Entity entity = instance.CreateEntityInner(modelId, position, id);
      return entity;
    }

    public static Entity CreateTile(Vector2 position, int id, TiledLayer layer) {
      Entity entity = instance.CreateEntityInner("tile", position);
      var drawComponent = entity.GetComponent<DrawComponent>();
      drawComponent.SetSprite(id);
      drawComponent.Color = ColorUtils.HexStringToColor(layer.TintColor);
      drawComponent.Depth -= 0.01f * layer.LayerIndex;
      return entity;
    }

    public static Entity CreateHouse(Vector2 position, TiledObject o, TiledLayer layer) {
      Entity entity = instance.CreateEntityInner("house", position, "house");
      var drawComponent = entity.GetComponent<DrawComponent>();
      drawComponent.Depth -= 0.01f * layer.LayerIndex;
      return entity;
    }

    public static Entity CreateTextObject(Vector2 position, TiledObject o, TiledLayer layer) {
      Entity entity = instance.CreateEntityInner("text-object", position);
      var textComponent = entity.GetComponent<TextComponent>();
      textComponent.Depth -= 0.01f * layer.LayerIndex;
      textComponent.Text = o.Text.Text;
      textComponent.FontColor = o.Text.GetColor();
      textComponent.FontSize = o.Text.PixelSize;
      textComponent.PositionOffset = new Vector2(-o.Width / 2, -o.Height / 2);
      return entity;
    }

    public static Entity CreateTriggerObject(Vector2 position, TiledObject o) {
      Entity entity = instance.CreateEntityInner("trigger", position);
      var collisionComponent = entity.GetComponent<CollisionComponent>();
      collisionComponent.BoundingRectangle = new Rectangle(-(int)o.Width / 2, -(int)o.Height / 2, (int)o.Width, (int)o.Height);
      return entity;
    }
    
    
  }
}
