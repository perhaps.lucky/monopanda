﻿using MonoPanda.ECS;

namespace Example.TiledExample {
  public class TriggerComponent : EntityComponent {
    public Entity TargetEntity { get; set; }
  }
}