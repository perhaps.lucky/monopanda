﻿using Example.ECS;

using MonoPanda.Components;
using MonoPanda.ECS;

namespace Example.TiledExample {
  public class TriggerSystem : EntitySystem {
    private Entity controlledEntity;

    public TriggerSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void OnComponentAdd(EntityComponent component) {
      if (component is ControlComponent)
        controlledEntity = component.Entity;
    }

    public override void Update() {
      foreach (TriggerComponent component in ECS.GetAllComponents<TriggerComponent>()) {
        if (component.TargetEntity != null) {
          component.Entity.GetComponentOptional<CollisionComponent>().IfPresent(collisionComponent => {
            component.TargetEntity.GetComponent<DrawComponent>().IsVisible =
              collisionComponent.IsColliding(controlledEntity);
          });
        }
      }
    }
  }
}