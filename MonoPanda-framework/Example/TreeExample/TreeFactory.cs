using Microsoft.Xna.Framework;

using MonoPanda.ECS;

namespace Example.TreeExample {
  public class TreeFactory : EntityFactory {
    private static TreeFactory instance;

    protected override void SetAsInstance() {
      instance = this;
    }

    public static Entity CreateEntity(string modelId, Vector2 position) {
      return instance.CreateEntityInner(modelId, position);
    }
  }
}