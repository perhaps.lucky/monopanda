﻿using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Utils;
using System;
using System.Collections.Generic;

namespace MonoPanda.BitmapFonts {
  public class BitmapFont : IDisposable {
    public FontXml FontFile { get; private set; }
    public Texture2D Texture { get; private set; }
    public Dictionary<char, FontChar> CharacterMap { get; private set; }

    public BitmapFont(string fntPath) {
      FontFile = ContentUtils.LoadXml<FontXml>(fntPath);
      Texture = TextureUtils.ChangeColorsForBitmapFont(ContentUtils.LoadTexture(fntPath.Replace(".fnt", ".png")));

      CharacterMap = new Dictionary<char, FontChar>();
      foreach(FontChar c in FontFile.Chars) {
        char ch = (char)c.ID;
        CharacterMap.Add(ch, c);
      }

    }

    public void Dispose() {
      Texture.Dispose();
    }
  }
}
