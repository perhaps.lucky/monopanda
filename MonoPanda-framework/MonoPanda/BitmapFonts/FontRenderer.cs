﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.UserSettings;
using MonoPanda.Utils;
using System.Text.RegularExpressions;

namespace MonoPanda.BitmapFonts {
  public class FontRenderer {

    /// <summary>
    /// Draws text, scales it and so on. <br/>
    /// Returns vector2 representing position of last letter (right-bottom corner).
    /// </summary>
    /// <param name="fontId">Id of the font from content manager</param>
    /// <param name="position">Position of start of text. <b>If scaleToWindowSize is true(default) it will be scaled to user settings!</b></param>
    /// <param name="txt">Text to draw</param>
    /// <param name="color">Color of text to draw</param>
    /// <param name="fontSize">Size of the font, used to scale source textures</param>
    /// <param name="scaleToWindowSize">If true then position, size and word-wrapping will be scaled to user settings.</param>
    /// <param name="maxLineWidth">Max width of line of text. If set to -1 (default), text will not be limited. Causes word-wrapping. <b>Warning: If word doesn't fit in single line, it will stop drawing and log warn!</b></param>
    public static Vector2 DrawText(string fontId, Vector2 position, string txt, Color color, int fontSize = 32, bool scaleToWindowSize = true, int maxLineWidth = -1, float depth = 0, SpriteBatch spriteBatch = null, bool ignoreColorTags = false) {
      DrawParams drawParams = new DrawParams(fontId, position, color, fontSize, scaleToWindowSize, maxLineWidth, depth, spriteBatch, ignoreColorTags);

      string[] words = txt.Split(' ');
      foreach (string word in words) {
        if (tryHandleColorTag(word, drawParams))
          continue;

        if (maxLineWidth != -1 && drawParams.OffsetY + countWordAdvance(word, drawParams) > drawParams.MaxWidthScaled) {
          if (drawParams.OffsetX == 0) {
            Log.log(LogCategory.TextDrawing, LogLevel.Warn, "Word: " + word + " is too long with current width limitation: " + maxLineWidth + ". Drawing stopped!");
            return new Vector2(drawParams.StartX + drawParams.OffsetX, drawParams.StartY + drawParams.OffsetY + (int)(drawParams.FontSize * drawParams.WindowScale));
          }
          drawSymbol('\n', drawParams);
        }

        foreach (char c in word) {
          drawSymbol(c, drawParams);
        }

        drawSymbol(' ', drawParams);
      }

      return new Vector2(drawParams.StartX + drawParams.OffsetX, drawParams.StartY + drawParams.OffsetY + (int)(drawParams.FontSize * drawParams.WindowScale));
    }

    private static void drawSymbol(char c, DrawParams drawParams) {
      if (c == '\n') {
        drawParams.OffsetY += (int)(drawParams.FontSize * drawParams.WindowScale);
        drawParams.OffsetX = 0;
      }

      FontChar fontChar;
      if (drawParams.Font.CharacterMap.TryGetValue(c, out fontChar)) {
        var sourceRectangle = new Rectangle(fontChar.X, fontChar.Y, fontChar.Width, fontChar.Height);
        var pos = new Vector2(drawParams.StartX + drawParams.OffsetX + fontChar.XOffset, drawParams.StartY + drawParams.OffsetY + fontChar.YOffset);

        drawParams.SpriteBatch.Draw(drawParams.Font.Texture, pos, sourceRectangle, drawParams.DrawColor, 0, Vector2.Zero, drawParams.FontSizeScale, SpriteEffects.None, drawParams.Depth);
        drawParams.OffsetX += (int)(fontChar.XAdvance * drawParams.FontSizeScale);
      }
    }

    private static int countWordAdvance(string word, DrawParams drawParams) {
      int sum = 0;
      foreach (char c in word) {
        if (c == '\n')
          return sum;

        FontChar fontChar;
        if (drawParams.Font.CharacterMap.TryGetValue(c, out fontChar)) {
          sum += (int)(fontChar.XAdvance * drawParams.FontSizeScale);
        }

      }
      return sum;
    }

    private static float getWindowScale() {
      return (float)Settings.Window.Height / Config.Window.Height;
    }

    private static bool tryHandleColorTag(string word, DrawParams drawParams) {
      if (isColorTag(word)) {
        try {
          string colorDescription = word.Substring(2, word.IndexOf('}') - 2);
          if (drawParams.IgnoreColorTags)
            return true;
          if (colorDescription.Contains(","))
            drawParams.DrawColor = ParameterUtils.GetColor(colorDescription);
          else
            drawParams.DrawColor = ReflectionUtils.GetStaticProperty<Color>(typeof(Color), colorDescription);
          return true;
        } catch {
          Log.log(LogCategory.TextDrawing, LogLevel.Warn, "Incorrect color tag: " + word);
          return false;
        }
      }
      return false;
    }

    private static bool isColorTag(string word) {
      var pattern = "%{.*}%";
      return Regex.IsMatch(word, pattern);
    }

    private class DrawParams {
      public BitmapFont Font { get; set; }
      public int FontSize { get; set; }
      public float WindowScale { get; set; }
      public float FontSizeScale { get; set; }
      public int StartX { get; set; }
      public int StartY { get; set; }
      public int OffsetX { get; set; }
      public int OffsetY { get; set; }
      public Color DrawColor { get; set; }
      public int MaxWidthScaled { get; set; }
      public float Depth { get; set; }
      public SpriteBatch SpriteBatch { get; set; }
      public bool IgnoreColorTags { get; set; }

      public DrawParams(string fontId, Vector2 position, Color color, int fontSize, bool scaleToWindowSize, int maxWidth, float depth, SpriteBatch spriteBatch, bool ignoreColorTags) {
        Font = ContentManager.Get<BitmapFont>(fontId, forceLoad: true); // force loaded because changes in texture must be called on main thread
        WindowScale = scaleToWindowSize ? getWindowScale() : 1.0f;
        FontSizeScale = (float)fontSize / Font.FontFile.Info.Size * WindowScale;
        StartX = (int)(position.X * WindowScale);
        StartY = (int)(position.Y * WindowScale);
        OffsetX = 0;
        OffsetY = 0;
        DrawColor = color;
        FontSize = fontSize;
        MaxWidthScaled = (int)(maxWidth * WindowScale);
        Depth = depth;
        SpriteBatch = spriteBatch != null ? spriteBatch : GameMain.SpriteBatch;
        IgnoreColorTags = ignoreColorTags;
      }

    }

  }
}
