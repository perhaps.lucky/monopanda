﻿using MonoPanda.Commands.Macros;
using MonoPanda.Configuration;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class AddMacroCommand : Command {

    public AddMacroCommand() {
      HelpCategoryName = HelpCategories.Macro;
      ShortHelpDescription = "Add new user macro";
      ExtendedHelp = "addmacro %{Cyan}% [macro_name] %{Plum}% [macro_commands] \n\n "
                    + "%{Cyan}% macro_name %{White}% - name for saved macro (if existing, macro will be overrided)\n "
                    + "%{Plum}% macro_commands %{White}% - commands executed by macro. \n"
                    + "       Use quote symbols to send all commands as single parameter. \n"
                    + "       You can separate commands using %{Yellow}% ; \n"
                    + "       To use quote symbol inside commands list, escape it like: %{Yellow}%  \\\"\n"
                    + "       If you want escaped quote symbol, write it like: %{Yellow}%  \\\\\"\n"
                    + "       You can add parameters to your macro by using dollar symbol and number of parameter (starting with 1), e.g. %{Yellow}% $1\n\n"
                    + "Macros can be called just like commands and can be used to quickly execute long sets of commands.\n"
                    + "They are remember between game sessions. Their list is stored in: %{Lime}% " + Config.Console.UserMacrosJson;
    }

    public override string Execute(List<string> parameters) {
      var name = parameters[0].ToLower();
      var command = parameters[1];
      var overrided = MacroManager.AddMacro(name, command);
      return (overrided ? "Overrided" : "Added") + " macro: %{Lime}% " + name;
    }
  }
}
