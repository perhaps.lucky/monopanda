﻿using MonoPanda.ECS;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public abstract class Command {

    public CommandSystem CommandSystem { get; set; }
    protected EntityComponentSystem ECS { get => CommandSystem.ECS; }
    public string ShortHelpDescription { get; set; } = "No description available";
    public string ExtendedHelp { get; set; } = "No help available";
    public bool ShowInHelp { get; set; } = true;
    public string HelpCategoryName { get; set; } = "Unknown category";

    public abstract string Execute(List<string> parameters);

  }
}
