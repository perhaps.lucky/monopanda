﻿using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using MonoPanda.ECS;
using System;
using MonoPanda.Commands.Macros;

namespace MonoPanda.Commands {
  public class CommandSystem {
    private const char COMMAND_SPLIT_SYMBOL = ';';
    private const char ESCAPE_SYMBOL = '\\';
    private const char PARAMETERS_SPLIT_SYMBOL = ' ';
    private const char QUOTE_SYMBOL = '\"';

    public Dictionary<string, Command> CommandHandlers { get; set; }
    public EntityComponentSystem ECS { get; private set; }

    public void Initialize() {
      CommandHandlers = new Dictionary<string, Command>();
      foreach (Type commandType in
        Assembly
          .GetAssembly(typeof(Command))
          .GetTypes()
          .Where(type => type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(Command)))) {
        string commandName = commandType.Name.Replace("Command", "").ToLower();
        CommandHandlers.Add(commandName, (Command)Activator.CreateInstance(commandType));
        CommandHandlers[commandName].CommandSystem = this;
      }
    }

    public void SetECS(EntityComponentSystem ecs) {
      ECS = ecs;
    }

    /// <summary>
    /// Executes command or user macro.
    /// </summary>
    /// <param name="macros">If it's set to true, it will try to find macro after failing to find command</param>
    public string ExecuteCommand(string fullCommand, bool macros = true) {
      List<string> commands = splitIntoCommands(fullCommand);
      string response = "";
      foreach (string command in commands) {
        try {
          var commandTrim = command.Trim();
          string commandName = getCommandName(commandTrim).ToLower();
          response +=
            CommandHandlers.ContainsKey(commandName)
            ? CommandHandlers[commandName].Execute(getCommandParameters(commandTrim))
            : macros ? tryExecuteMacro(commandTrim) : commandName + " - command not found.";
          response += '\n';
        } catch (Exception e) {
          // Prevent crashes caused by bad scripts e.g. typos in console
          response += "Command " + command + " threw exception: " + e.ToString();
        }
      }

      return response.Remove(response.Length - 1); // remove last new line symbol
    }

    private string tryExecuteMacro(string fullCommand) {
      if (MacroManager.IsInitialized()) {
        string commandName = getCommandName(fullCommand).ToLower();
        List<string> parameters = getCommandParameters(fullCommand);
        return MacroManager
                  .GetMacro(commandName)
                  .IfPresentGet(macroCommand => {
                    for (int i = 0; i < parameters.Count; i++) {
                      macroCommand = macroCommand.Replace("$" + (i + 1), parameters[i]);
                    }
                    return ExecuteCommand(macroCommand, false);
                  })
                  .OrElse(commandName + " - command not found.");
      }
      return getCommandName(fullCommand).ToLower() + " - command not found.";
    }

    private List<string> splitIntoCommands(string fullCommand) {
      List<string> commands = new List<string>();
      int commandIndex = 0;
      bool inQuotes = false;
      for (int i = 0; i < fullCommand.Length; i++) {
        if (fullCommand[i] == COMMAND_SPLIT_SYMBOL && !inQuotes) {
          commands.Add(fullCommand.Substring(commandIndex, i - commandIndex));
          commandIndex = i + 1;
        }

        if (fullCommand[i] == QUOTE_SYMBOL && (i == 0 || fullCommand[i - 1] != ESCAPE_SYMBOL))
          inQuotes = !inQuotes;
      }
      commands.Add(fullCommand.Substring(commandIndex));

      return commands;

    }

    private string getCommandName(string command) {
      if (command.Contains(PARAMETERS_SPLIT_SYMBOL))
        return command.Substring(0, command.IndexOf(PARAMETERS_SPLIT_SYMBOL));
      else
        return command;
    }

    private List<string> getCommandParameters(string command) {
      string parameters = removeCommandName(command);
      return createParametersList(parameters);
    }

    private string removeCommandName(string fullCommand) {
      if (fullCommand.Contains(PARAMETERS_SPLIT_SYMBOL))
        return fullCommand.Substring(fullCommand.IndexOf(PARAMETERS_SPLIT_SYMBOL) + 1);
      else
        return string.Empty;
    }

    private List<string> createParametersList(string parameters) {
      List<string> readyParameters = new List<string>();
      if (parameters.Equals(string.Empty))
        return readyParameters;
      int paramIndex = 0;
      bool inQuotes = false;
      for (int i = 0; i < parameters.Length; i++) {
        if (parameters[i] == PARAMETERS_SPLIT_SYMBOL && !inQuotes) {
          readyParameters.Add(getParameter(parameters, paramIndex, i));
          paramIndex = i + 1;
        }
        if (parameters[i] == QUOTE_SYMBOL && (i == 0 || parameters[i - 1] != ESCAPE_SYMBOL))
          inQuotes = !inQuotes;
      }
      readyParameters.Add(getParameter(parameters, paramIndex, parameters.Length));

      return readyParameters;
    }

    private string getParameter(string parameters, int paramIndex, int currentIndex) {
      var parameter = parameters.Substring(paramIndex, currentIndex - paramIndex);
      if (parameter.StartsWith(QUOTE_SYMBOL.ToString()) && parameter.EndsWith(QUOTE_SYMBOL.ToString()))
        parameter = parameter.Substring(1, parameter.Length - 2);
      parameter = removeEscapeSymbol(parameter);
      return parameter;
    }

    private string removeEscapeSymbol(string parameter) {
      return parameter.Replace("\\\"", "\"");
    }
  }
}
