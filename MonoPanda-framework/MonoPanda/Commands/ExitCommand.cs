﻿using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class ExitCommand : Command {

    public ExitCommand() {
      HelpCategoryName = HelpCategories.Game;
      ShortHelpDescription = "Quits the game";
      ExtendedHelp = "exit \n\n Simple as that, slower than ALT+F4 ;)";
    }

    public override string Execute(List<string> parameters) {
      GameMain.ExitGame();
      return "What are you trying to prove by not quitting the game like normal being.";
    }
  }
}
