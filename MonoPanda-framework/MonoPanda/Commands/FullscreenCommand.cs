﻿using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class FullscreenCommand : Command {

    public FullscreenCommand() {
      HelpCategoryName = HelpCategories.Window;
      ShortHelpDescription = "Switch fullscreen on/off";
      ExtendedHelp = "fullscreen %{Plum}% ([on/off])\n\n"
                   + "%{Plum}% on/off %{White}% - if not present or wrong value, fullscreen mode will be switched";
    }

    public override string Execute(List<string> parameters) {
      if (parameters.Count <= 0 || !handleParam(parameters[0])) {
        // no param / wrong param value - perform switch
        GameSettings.SetFullscreen(!GameMain.GraphicsDeviceManager.IsFullScreen);
      }

      return "Fullscreen " + (GameMain.GraphicsDeviceManager.IsFullScreen ? " %{Lime}% on" : " %{Red}% off");
    }

    // Returns true if parameter value is valid
    private bool handleParam(string value) {
      if (value.ToLower().Equals("on")) {
        GameSettings.SetFullscreen(true);
        return true;
      } else if (value.ToLower().Equals("off")) {
        GameSettings.SetFullscreen(false);
        return true;
      }
      return false;
    }
  }
}
