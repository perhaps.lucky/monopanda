﻿using MonoPanda.Commands.Macros;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class RemoveMacroCommand : Command {

    public RemoveMacroCommand() {
      HelpCategoryName = HelpCategories.Macro;
      ShortHelpDescription = "Remove user macro";
      ExtendedHelp = "removemacro %{Cyan}% [macro_name] \n\n"
                   + "%{Cyan}% macro_name %{White}% - name of macro to delete";

    }

    public override string Execute(List<string> parameters) {
      var name = parameters[0].ToLower();
      MacroManager.RemoveMacro(name);
      return "Removed macro: %{Lime}% " + name;
    }
  }
}
