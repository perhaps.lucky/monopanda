﻿using MonoPanda.ECS;
using MonoPanda.ParameterConvert;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace MonoPanda.Commands {
  public class SetEntityAttributeCommand : Command {

    public SetEntityAttributeCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Change value of entity attribute";
      ExtendedHelp = "setentityattribute %{Cyan}% [entity_name] %{Plum}% [component_name] %{Lime}% [attribute_name] %{Yellow}% [value] \n\n "
                    + "%{Cyan}% entity_name %{White}% - id of entity.\n "
                    + "%{Plum}% component_name %{White}% - short identificator of component (e.g. %{Yellow}% DrawComponent %{White}% ). \n"
                    + "%{Lime}% attribute_name %{White}% - name of attribute which value will be changed. \n"
                    + "%{Yellow}% value %{White}% - new value for attribute.";
    }

    public override string Execute(List<string> parameters) {
      var entityName = parameters[0];
      var componentName = parameters[1];
      var propertyName = parameters[2];
      var value = parameters[3];

      Optional<Entity> entity = ECS.GetEntityOptional(entityName);
      if (!entity.IsPresent())
        return "Entity not found: " + entityName;

      EntityComponent component = entity.Get().GetComponentByIdentifier(componentName);
      if (component == null)
        return "Component not found: " + componentName;
      Type componentType = component.GetType();

      PropertyInfo propertyInfo = componentType.GetProperty(propertyName);
      if (propertyInfo == null)
        return "Property not found: " + propertyName;

      ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
      if (attribute != null) {
        propertyInfo.SetValue(component, attribute.Convert(value));
      } else {
        propertyInfo.SetValue(component, Convert.ChangeType(value, propertyInfo.PropertyType, CultureInfo.InvariantCulture));
      }

      return "%{LimeGreen}%Set ok";
    }
  }
}
