﻿using MonoPanda.GlobalTime;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoPanda.Commands {
  public class SetWorldTimeSpeedCommand : Command {

    public SetWorldTimeSpeedCommand() {
      HelpCategoryName = HelpCategories.World;
      ShortHelpDescription = "Sets world time speed value";
      ExtendedHelp = "setworldtimespeed %{Yellow}% ([value]) \n\n"
        + " %{Yellow}% value %{White}% - new value of world time speed (if not present, value will be set to 1.0)";
    }

    public override string Execute(List<string> parameters) {
      if (parameters.Count > 0) {
        var newValue = parameters[0];
        Time.WorldTimeSpeed = float.Parse(newValue, CultureInfo.InvariantCulture);
      } else {
        Time.WorldTimeSpeed = 1.0f;
      }

      return "World Time Speed set to: %{Yellow}% " + Time.WorldTimeSpeed;
    }
  }
}
