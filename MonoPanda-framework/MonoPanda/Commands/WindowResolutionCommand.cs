﻿using MonoPanda.Console;
using MonoPanda.ECS;
using MonoPanda.Systems;
using System.Collections.Generic;

namespace MonoPanda.Commands {
  public class WindowResolutionCommand : Command {

    public WindowResolutionCommand() {
      HelpCategoryName = HelpCategories.Window;
      ShortHelpDescription = "Changes window resolution";
      ExtendedHelp = "windowresolution %{DarkSeaGreen}% [width] %{LightBlue}% [height]";
    }

    public override string Execute(List<string> parameters) {
      var width = int.Parse(parameters[0]);
      var height = int.Parse(parameters[1]);

      GameSettings.SetResolution(width, height);
      Optional<EntityComponentSystem>.Of(ECS).IfPresent(ECS => reinitializeResolutionInECS(ECS));
      ConsoleSystem.RefreshBackground();
      return "Resolution changed to: " + width + "x" + height;
    }

    private void reinitializeResolutionInECS(EntityComponentSystem ECS) {
      ECS.Camera.ReinitializeOrigin();
      ECS.GetSystemOptional<DrawSystemSpine>().IfPresent(drawSystemSpine => drawSystemSpine.ReinitializeProjection());
    }
  }
}
