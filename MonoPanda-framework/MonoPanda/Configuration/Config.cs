﻿using IniParser.Model;
using MonoPanda.Logger;
using MonoPanda.Utils;

namespace MonoPanda.Configuration {
  public class Config {

    private static readonly string CONFIG_LOCATION = "Content/MonoPanda/panda-config.ini";

    private static Config instance;
    public static WindowConfig Window => getInstance().window;
    public static MouseConfig Mouse => getInstance().mouse;
    public static ContentConfig Content => getInstance().config;
    public static InputConfig Input => getInstance().input;
    public static SoundConfig Sound => getInstance().sound;
    public static LanguageConfig Language => getInstance().language;
    public static ModsConfig Mods => getInstance().mods;
    public static StatesConfig States => getInstance().states;
    public static TimeConfig Time => getInstance().time;
    public static DebugConfig Debug => getInstance().debug;
    public static SpineConfig Spine => getInstance().spine;
    public static GraphicsConfig Graphics => getInstance().graphics;
    public static ConsoleConfig Console => getInstance().console;

    private readonly WindowConfig window;
    private readonly MouseConfig mouse;
    private readonly ContentConfig config;
    private readonly InputConfig input;
    private readonly SoundConfig sound;
    private readonly LanguageConfig language;
    private readonly ModsConfig mods;
    private readonly StatesConfig states;
    private readonly TimeConfig time;
    private readonly DebugConfig debug;
    private readonly SpineConfig spine;
    private readonly GraphicsConfig graphics;
    private readonly ConsoleConfig console;

    private Config() {
      Log.log(LogCategory.Config, LogLevel.Info, "Initializing engine config.");
      IniData configIniData = IniUtils.getIniData(CONFIG_LOCATION);
      window = new WindowConfig(configIniData);
      mouse = new MouseConfig(configIniData);
      config = new ContentConfig(configIniData);
      input = new InputConfig(configIniData);
      sound = new SoundConfig(configIniData);
      language = new LanguageConfig(configIniData);
      mods = new ModsConfig(configIniData);
      states = new StatesConfig(configIniData);
      time = new TimeConfig(configIniData);
      debug = new DebugConfig(configIniData);
      spine = new SpineConfig(configIniData);
      graphics = new GraphicsConfig(configIniData);
      console = new ConsoleConfig(configIniData);

      instance = this;
      Log.log(LogCategory.Config, LogLevel.Info, "Initializing engine config succeeded!");
    }

    private static Config getInstance() {
      if (instance == null)
        instance = new Config();

      return instance;
    }

  }
}
