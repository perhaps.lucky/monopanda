﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class ConsoleConfig : DefaultIniSectionLoader {

    public readonly bool ConsoleActive;
    public readonly string ConsoleButton;
    public readonly string ConsoleFont;
    public readonly int ConsoleFontSize;
    public readonly int ConsoleDefaultHeight;
    public readonly int ConsoleMaxLines;
    public readonly int CommandHistoryMax;
    public readonly string UserMacrosJson;

    public ConsoleConfig(IniData iniData) : base(iniData, "Console") {
    }
  }
}