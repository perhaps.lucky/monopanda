﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class ContentConfig : DefaultIniSectionLoader {

    public readonly string ContentItemsJsonPath;
    public readonly string ContentPackagesJsonPath;
    public readonly bool ContentUnloadActive;
    public readonly double ContentUnloadTime;

    public ContentConfig(IniData iniData) : base(iniData, "Content") { }
  }
}
