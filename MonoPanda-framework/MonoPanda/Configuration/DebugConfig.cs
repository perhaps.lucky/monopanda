﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class DebugConfig : DefaultIniSectionLoader {

    public readonly bool DebugTools;
    public readonly bool VisibilityRectangles;
    public readonly bool CollisionRectangles;
    public readonly bool CollisionSystemWorld;
    public readonly bool CameraSystemCenterRectangle;
    public readonly bool LightSystemGraphicsDeviceClear;
    public readonly bool LightSystemDebug;

    public DebugConfig(IniData iniData) : base(iniData, "Debug") { }
  }
}