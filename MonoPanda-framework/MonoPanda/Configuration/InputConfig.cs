﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class InputConfig : DefaultIniSectionLoader {

    public readonly string DefaultKeybindsJson;
    public readonly string UserKeybindsJson;

    public InputConfig(IniData iniData) : base(iniData, "Input") { }
  }
}
