﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class LanguageConfig : DefaultIniSectionLoader {

    public readonly string LanguagesDirectory;
    public readonly string Language;

    public LanguageConfig(IniData iniData) : base(iniData, "Language") { }

  }
}
