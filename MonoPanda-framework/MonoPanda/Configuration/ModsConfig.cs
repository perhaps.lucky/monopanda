﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class ModsConfig : DefaultIniSectionLoader {

    public readonly string ModsFolder;
    public readonly string CoreModFolderName;
    public readonly string ModsSettingsJson;
    public readonly string ModInfoJson;

    public ModsConfig(IniData iniData) : base(iniData, "Mods") { }
  }
}
