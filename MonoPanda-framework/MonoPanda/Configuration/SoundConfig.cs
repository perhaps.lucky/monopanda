﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class SoundConfig : DefaultIniSectionLoader {

    public readonly string SoundVolumeJsonPath;
    public readonly int SFXVolume;
    public readonly int MusicVolume;

    public SoundConfig(IniData iniData) : base(iniData, "Sound") { }
  }
}
