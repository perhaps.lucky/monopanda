﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class SpineConfig : DefaultIniSectionLoader {

    public readonly string EmptySkinName;

    public SpineConfig(IniData iniData) : base(iniData, "Spine") { }
  }
}