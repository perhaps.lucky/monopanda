﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class WindowConfig : DefaultIniSectionLoader {

    public readonly int Width;
    public readonly int Height;
    public readonly bool Fullscreen;
    public readonly bool AllowUserSettings;

    public WindowConfig(IniData iniData) : base(iniData, "Window") { }
  }
}
