﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoPanda.BitmapFonts;
using MonoPanda.Commands;
using MonoPanda.Commands.Macros;
using MonoPanda.Configuration;
using MonoPanda.ECS;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.UI;
using MonoPanda.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.Console {
  public class ConsoleSystem {
    private static ConsoleSystem instance;
    
    public bool ConsoleVisible { get; set; }

    private Keys consoleKey;
    private int consoleHeight;
    private int linesOnScreen;
    private int scrollIndex;
    private Texture2D consoleBackground;
    private bool consoleKeyPressed;

    private TextBox consoleInput;
    private CommandSystem commandSystem;
    private LinkedList<string> consoleLines;
    private LinkedList<string> commandHistory;
    private int commandHistoryIndex;

    private bool writeLogs;

    private ConsoleSystem() {
      commandSystem = new CommandSystem();
      consoleLines = new LinkedList<string>();
      commandHistory = new LinkedList<string>();
      commandHistoryIndex = -1;
    }

    public static ConsoleSystem getInstance() {
      if (instance == null)
        instance = new ConsoleSystem();
      return instance;
    }

    public static void SetECS(EntityComponentSystem ecs) {
      getInstance().commandSystem.SetECS(ecs);
    }

    public static void initialize() {
      var instance = getInstance();
      Enum.TryParse(Config.Console.ConsoleButton, out instance.consoleKey);
      instance.consoleHeight = Config.Console.ConsoleDefaultHeight;
      instance.createConsoleBackgroundTexture();
      instance.createConsoleInput();
      instance.commandSystem.Initialize();
      instance.countVisibleLines();
      MacroManager.Initialize();
    }

    public static void update() {
      var instance = getInstance();
      if (Keyboard.GetState().GetPressedKeys().Contains(instance.consoleKey) && !instance.consoleKeyPressed) {
        instance.ConsoleVisible = !instance.ConsoleVisible;
        instance.consoleInput.Focused = instance.ConsoleVisible && Keyboard.GetState().IsKeyUp(Keys.LeftShift);        
        instance.consoleKeyPressed = true;

      } else if (!Keyboard.GetState().GetPressedKeys().Contains(instance.consoleKey)) {
        instance.consoleKeyPressed = false;
      }

      if (instance.ConsoleVisible) {
        instance.consoleInput.Update();
        instance.updateScrollInputs();
        instance.updateHistoryInputs();
      }
    }


    public static void draw() {
      var instance = getInstance();
      if (instance.ConsoleVisible) {
        instance.drawConsoleBackground();
        instance.consoleInput.Draw();
        instance.drawConsoleLines();
      }
    }

    public static bool isConsoleOpen() {
      return Config.Console.ConsoleActive && getInstance().ConsoleVisible;
    }

    public static void SetHeight(int height) {
      instance.consoleHeight = height;
      instance.createConsoleBackgroundTexture();
      instance.consoleInput.Position = new Vector2(5, height - Config.Console.ConsoleFontSize - 5);
      instance.countVisibleLines();
    }

    /// <summary>
    /// Use in case of changing resolution
    /// </summary>
    public static void RefreshBackground() {
      instance.createConsoleBackgroundTexture();
    }

    public static void SetWriteLogs(bool value) {
      instance.writeLogs = value;
    }

    public static bool GetWriteLogs () {
      if (instance != null)
        return instance.writeLogs;
      return false;
    }

    public static void WriteToConsole(string text) {
      instance.writeToConsole(text);
    }

    private void executeCommand() {
      var commandExecutionResponse = commandSystem.ExecuteCommand(consoleInput.Text);
      writeToConsole("%{LimeGreen}% " + consoleInput.Text);
      writeToConsole(commandExecutionResponse);
      Log.log(LogCategory.Console, LogLevel.Debug, commandExecutionResponse);
      commandHistory.AddFirst(consoleInput.Text);
      if (commandHistory.Count > Config.Console.CommandHistoryMax)
        commandHistory.RemoveLast();

      consoleInput.Clear();
      scrollIndex = 0;
      commandHistoryIndex = -1;
    }

    private void writeToConsole(string text) {
      string[] lines = text.Split('\n');

      foreach (var line in lines) {
        consoleLines.AddFirst(line);
        if (consoleLines.Count > Config.Console.ConsoleMaxLines)
          consoleLines.RemoveLast();
      }
    }

    private void drawConsoleBackground() {
      GameMain.SpriteBatch.Draw(consoleBackground, Vector2.Zero, Color.White);
    }

    private void drawConsoleLines() {
      var endIndex = scrollIndex + linesOnScreen;
      var fontSize = Config.Console.ConsoleFontSize;
      int visibleLine = 0;
      for (int i = scrollIndex; i < endIndex; i++) {
        if (consoleLines.ElementAtOrDefault(i) != null)
          FontRenderer.DrawText(Config.Console.ConsoleFont, new Vector2(5, consoleHeight - (fontSize + 5) * (visibleLine + 2)), consoleLines.ElementAt(i), Color.White, fontSize, false);
        visibleLine++;
      }
    }

    private void createConsoleBackgroundTexture() {
      int consoleWidth = GameMain.GraphicsDeviceManager.GraphicsDevice.Viewport.Width;
      var texture = TextureUtils.CreateRectangleTexture(consoleWidth, consoleHeight, new Color(50, 50, 50, 200));
      setConsoleBackground(texture);
    }

    private void setConsoleBackground(Texture2D texture) {
      if (consoleBackground != null)
        consoleBackground.Dispose();
      consoleBackground = texture;
    }

    private void createConsoleInput() {
      var fontSize = Config.Console.ConsoleFontSize;
      consoleInput = new TextBox(Config.Console.ConsoleFont, new Vector2(5, consoleHeight - fontSize - 5), Color.White, fontSize);
      consoleInput.ClickToFocus = false;
      consoleInput.OnEnter += executeCommand;
    }

    private void countVisibleLines() {
      linesOnScreen = consoleHeight / (Config.Console.ConsoleFontSize + 5) - 1;
    }

    private void updateScrollInputs() {
      if (InputManager.GetCurrentKeyboardState().IsKeyDown(Keys.PageUp) &&
        InputManager.GetOldKeyboardState().IsKeyUp(Keys.PageUp))
        scrollIndex += linesOnScreen / 2;

      if (InputManager.GetCurrentKeyboardState().IsKeyDown(Keys.PageDown) &&
          InputManager.GetOldKeyboardState().IsKeyUp(Keys.PageDown))
        scrollIndex -= linesOnScreen / 2;
    }

    private void updateHistoryInputs() {
      if (InputManager.GetCurrentKeyboardState().IsKeyDown(Keys.Up) &&
          InputManager.GetOldKeyboardState().IsKeyUp(Keys.Up)) {
        commandHistoryIndex++;
        if (commandHistoryIndex + 1 > commandHistory.Count) {
          commandHistoryIndex--; // undo the change, don't write
        } else {
          consoleInput.Text = commandHistory.ElementAt(commandHistoryIndex);
        }
      }

      if (InputManager.GetCurrentKeyboardState().IsKeyDown(Keys.Down) &&
          InputManager.GetOldKeyboardState().IsKeyUp(Keys.Down)) {
        commandHistoryIndex--;
        if (commandHistoryIndex < 0) {
          commandHistoryIndex = -1;
          consoleInput.Clear();
        } else {
          consoleInput.Text = commandHistory.ElementAt(commandHistoryIndex);
        }
      }
    }
  }
}
