﻿namespace MonoPanda.Content {
  public enum ContentItemType {
    Texture2D, SoundEffect, Song, BitmapFont, SpriteSheet, SpineAsset, TiledMap
  }
}
