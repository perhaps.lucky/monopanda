﻿using MonoPanda.Logger;
using MonoPanda.Multithreading;

/// <summary>
/// Thread request system performing loading of ContentItems
/// </summary>
namespace MonoPanda.Content {
  public class ContentLoader : ThreadRequestSystem {
    private static ContentLoader instance;

    public static void PutRequest(ThreadRequest request) {
      if (instance == null) {
        instance = new ContentLoader();
        instance.Initialize();
      }

      instance.QueueRequest(request);
      Log.log(LogCategory.Content, LogLevel.Info, "ContentLoader queued request: " + request.ToString());
    }

    public static void loadPackage(ContentPackage package) {
      foreach (string id in package.Items) {
        loadItem(id);
      }
    }

    public static void loadItem(string id) {
      ContentManager.getItem(id).load();
    }

    protected override bool Accepts(ThreadRequest request) {
      return request is ContentRequest;
    }

    protected override void Process(ThreadRequest threadRequest) {
      var request = threadRequest as ContentRequest;
      request.ItemsLoaded = 0;
      if (request.IsPackageRequest) {
        Log.log(LogCategory.Content, LogLevel.Debug,
          "Processing request id: " + request.ContentId + " as content package request.");
        var package = ContentManager.getContentPackage(request.ContentId);
        request.ItemsToLoad = package.Items.Count;
        foreach (string id in package.Items) {
          loadItem(id);
          request.ItemsLoaded++;
        }
      } else {
        Log.log(LogCategory.Content, LogLevel.Debug,
          "Processing request id: " + request.ContentId + " as content item request.");
        request.ItemsToLoad = 1;
        loadItem(request.ContentId);
        request.ItemsLoaded++;
      }

      Log.log(LogCategory.Content, LogLevel.Info, "ContentLoader finished request id: " + request.ContentId);
      return;
    }
  }
}