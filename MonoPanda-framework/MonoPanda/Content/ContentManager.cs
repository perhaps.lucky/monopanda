﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.Utils;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MonoPanda.Content {
  public class ContentManager {

    private static ContentManager instance;

    private List<ContentItem> contentItems;
    private List<ContentPackage> contentPackages;

    private ContentManager() { }

    /// <summary>
    /// Returns item by id or null if it wasn't loaded yet. <br/>
    /// If item wasn't loaded, it will get requested to ContentLoader.
    /// </summary>
    /// <typeparam name="T">class of item</typeparam>
    /// <param name="id">id of item</param>
    public static T Get<T>(string id, bool forceLoad = false) {
      var item = getItem(id);
      if(item != null)
        return getItem(id).getItem<T>(forceLoad);

      Log.log(LogCategory.Content, LogLevel.Warn, "Content item id: " + id + " has not been found! A null will be returned.");
      return default;
    }

    public static ContentRequest RequestPackageLoad(string id) {
      ContentRequest request = new ContentRequest(id, true);
      ContentLoader.PutRequest(request);
      return request;
    }

    public static void initialize() {
      ContentManager instance = getInstance();
      instance.contentItems = loadContentItems();
      instance.contentPackages = loadContentPackages();
      loadRequestedPackages();
    }

    public static void unload() {
      foreach (ContentItem item in getInstance().contentItems)
        item.unload();
    }

    private static List<ContentItem> loadContentItems() {
      Log.log(LogCategory.Content, LogLevel.Info, "Loading content items.");
      List<ContentItem> finalList = new List<ContentItem>();

      ModsUtils.ForEachModName(modName => loadContentItemsFromMod(modName, finalList));

      return finalList;
    }

    private static void loadContentItemsFromMod(string modName, List<ContentItem> targetList) {
      Log.log(LogCategory.Content, LogLevel.Info, "Loading content items from mod: " + modName);
      foreach (ContentItem item in targetList)
        checkIfModOverridesFile(modName, item);

      string contentItemsJson = ModsUtils.GetModPath(modName, Config.Content.ContentItemsJsonPath);
      if (File.Exists(contentItemsJson))
        loadItemsFromMod(contentItemsJson, modName, targetList);
      
    }

    private static void checkIfModOverridesFile(string modName, ContentItem item) {
      string expectedPath = ModsUtils.GetModPath(modName, item.FilePath);

      if (File.Exists(expectedPath)) {
        Log.log(LogCategory.Content, LogLevel.Debug, "Mod: " + modName + " contains file for item: " + item.Id + ". New path set to: " + expectedPath);
        item.FullPath = expectedPath;
      }
    }

    private static void loadItemsFromMod(string jsonPath, string modName, List<ContentItem> targetList) {
      List<ContentItem> itemsFromJson = ContentUtils.LoadJson<List<ContentItem>>(jsonPath);
      foreach (ContentItem item in itemsFromJson) {
        Log.log(LogCategory.Content, LogLevel.Debug, "Loading new content item info for id: " + item.Id);
        item.FullPath = ModsUtils.GetModPath(modName, item.FilePath);
        addReplaceItem(item, targetList);
      }
    }

    private static List<ContentPackage> loadContentPackages() {
      Log.log(LogCategory.Content, LogLevel.Info, "Loading content packages.");
      List<ContentPackage> finalList = new List<ContentPackage>();

      ModsUtils.ForEachModName(modName => loadContentPackagesFromMod(modName, finalList));

      return finalList;
    }

    private static void loadContentPackagesFromMod(string modName, List<ContentPackage> targetList) {
      string jsonPath = ModsUtils.GetModPath(modName, Config.Content.ContentPackagesJsonPath);
      if (File.Exists(jsonPath)) {
        List<ContentPackage> fromMod = ContentUtils.LoadJson<List<ContentPackage>>(jsonPath);
        foreach (ContentPackage newPackage in fromMod)
          addReplacePackage(newPackage, targetList);
      }
    }

    private static void addReplacePackage(ContentPackage package, List<ContentPackage> targetList) {
      var old = targetList.Find(p => p.Id == package.Id);
      if (old == null)
        targetList.Add(package);
      else {
        targetList.Remove(old);
        targetList.Add(package);
      }
    }

    private static void addReplaceItem(ContentItem item, List<ContentItem> targetList) {
      var old = targetList.Find(i => i.Id == item.Id);
      if (old == null)
        targetList.Add(item);
      else {
        Log.log(LogCategory.Content, LogLevel.Warn, "Content item id: " + item.Id + " replaces old item with same id! Make sure you wanted this to happen.");
        targetList.Remove(old);
        targetList.Add(item);
      }
    }

    private static ContentManager getInstance() {
      if (instance == null)
        instance = new ContentManager();

      return instance;
    }

    public static ContentItem getItem(string id) {
      return getInstance().contentItems.Find(item => item.Id.Equals(id));
    }

    public static ContentPackage getContentPackage(string id) {
      return getInstance().contentPackages.Find(item => item.Id.Equals(id));
    }

    public static void update() {
      foreach (ContentItem item in getInstance().contentItems) {
        item.checkUnload();
      }
    }

    private static void loadRequestedPackages() {
      foreach (ContentPackage package in instance.contentPackages) {
        if (package.LoadOnStart)
          ContentLoader.loadPackage(package);
      }
    }

  }
}
