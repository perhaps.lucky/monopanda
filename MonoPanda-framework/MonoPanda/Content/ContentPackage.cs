﻿using System.Collections.Generic;

namespace MonoPanda.Content {
  public class ContentPackage {

    public string Id { get; set; }
    public List<string> Items { get; set; }
    public bool LoadOnStart { get; set; }

  }
}
