﻿using MonoPanda.Multithreading;

namespace MonoPanda.Content {
  public class ContentRequest : ThreadRequest {

    public string ContentId { get; private set; }
    public bool IsPackageRequest { get; private set; }
    public int ItemsToLoad { get; set; }
    public int ItemsLoaded { get; set; }

    public ContentRequest(string contentId, bool isPackageRequest = false) {
      ContentId = contentId;
      IsPackageRequest = isPackageRequest;
      Priority = isPackageRequest ? 1 : 2;
    }

    public override string ToString() {
      return "ContentRequest id: " + ContentId;
    }

  }
}
