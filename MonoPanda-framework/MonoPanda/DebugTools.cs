﻿using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.Input;
using MonoPanda.Languages;
using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.Sound;
using MonoPanda.Timers;
using MonoPanda.UserSettings;
using MonoPanda.Utils;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MonoPanda {
  public class DebugTools {
    private static DebugTools instance;

    private ContentManager contentManager;
    private GameMain gameMain;
    private GameSettings gameSettings;
    private Config config;
    private Settings settings;
    private InputManager inputManager;
    private SoundManager soundManager;
    private ModManager modManager;

    private Dictionary<string, string> languageDictionary;
    
    private DebugTools() {
      contentManager = ReflectionUtils.GetInstanceOf<ContentManager>();
      gameMain = ReflectionUtils.GetInstanceOf<GameMain>();
      gameSettings = ReflectionUtils.GetInstanceOf<GameSettings>();
      config = ReflectionUtils.GetInstanceOf<Config>();
      settings = ReflectionUtils.GetInstanceOf<Settings>();
      inputManager = ReflectionUtils.GetInstanceOf<InputManager>();
      soundManager = ReflectionUtils.GetInstanceOf<SoundManager>();
      modManager = ReflectionUtils.GetInstanceOf<ModManager>();
    }

    public static DebugTools getInstance() {
      if (instance == null)
        instance = new DebugTools();

      return instance;
    }

    public void update() {
      languageDictionary = ReflectionUtils.GetStaticField<Dictionary<string, string>>(typeof(Language), "languageData");

      if (InputManager.GetLeftMouseButton().IsPressed) {
        Log.log(LogCategory.MousePosition, LogLevel.Debug, "Position logged: " + InputManager.GetMouseWindowPosition()); 
      }

    } // put a breakpoint here


  }
}
