﻿using Microsoft.Xna.Framework;
using MonoPanda.UserSettings;
using MonoPanda.Utils;

namespace MonoPanda.ECS {
  public class Camera {
    public Vector2 Position { get { return position; } set { position = value; calculateMatrix(); } }
    public float Zoom { get { return zoom; } set { zoom = value; calculateMatrix(); } }
    public Vector2 Origin { get { return origin; } set { origin = value; calculateMatrix(); } }
    /// <summary>
    /// Info: buggy af.
    /// </summary>
    public float Rotation { get { return rotation; } set { rotation = value; calculateMatrix(); } }
    public Matrix TransformationMatrix { get; private set; }

    private Vector2 position;
    private float zoom;
    private Vector2 origin;
    private float rotation;

    public Camera() {
      position = new Vector2(0f);
      zoom = 1f;
      rotation = 0f;
      origin = new Vector2(Settings.Window.Width / 2f, Settings.Window.Height / 2f);
      calculateMatrix();
    }

    public void ReinitializeOrigin() {
      origin = new Vector2(Settings.Window.Width / 2f, Settings.Window.Height / 2f);
      calculateMatrix();
    }

    public Vector2 WorldToScreen(Vector2 worldPosition) {
      return Vector2.Transform(worldPosition, TransformationMatrix);
    }

    public Vector2 ScreenToWorld(Vector2 screenPosition) {
      // Transformation inversion didn't work here because of scale Z issue.
      Rectangle visibleArea = GetVisibleArea();
      Vector2 topLeftCorner = new Vector2(visibleArea.Left, visibleArea.Top);
      return topLeftCorner + screenPosition / zoom;
    }

    public Rectangle GetVisibleArea() {
      var windowSize = GraphicsUtils.GetWindowSize();

      int x = (int)(Position.X - windowSize.Width / 2 / Zoom);
      int y = (int)(Position.Y - windowSize.Height / 2 / Zoom);
      int width = (int)(windowSize.Width / Zoom);
      int height = (int)(Settings.Window.Height / Zoom);
      Rectangle visibleArea = new Rectangle(x, y, width, height);

      if (Rotation != 0)
        visibleArea = RectangleUtils.ApplyRotation(visibleArea, Rotation, Vector2.Zero);

      return visibleArea;
    }

    private void calculateMatrix() {
      TransformationMatrix =
        Matrix.CreateTranslation(new Vector3(-(position - origin), 0f)) *
        Matrix.CreateTranslation(new Vector3(-origin, 0f)) *
        Matrix.CreateRotationZ(rotation) *
        Matrix.CreateScale(new Vector3(new Vector2(zoom), 0)) *
        Matrix.CreateTranslation(new Vector3(origin,0f));
    }
  }
}
 