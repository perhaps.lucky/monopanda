﻿using MonoPanda.Logger;
using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.BehaviourComponent;
using MonoPanda.ParameterConvert;

namespace MonoPanda.Components {
  public class BehaviourComponent : EntityComponent {
    public Behaviour Behaviour { get; set; }

    public override void Init(Dictionary<string, object> parameters) {
      if (parameters.ContainsKey("BehaviourClass")) {
        Type type = Type.GetType(parameters["BehaviourClass"].ToString());
        Behaviour behaviour = (Behaviour)Activator.CreateInstance(type, null);
        behaviour.Entity = this.Entity;
        Behaviour = behaviour;

        if (parameters.ContainsKey("BehaviourParameters")) {
          Dictionary<string, object> behaviourParams = ((JObject)parameters["BehaviourParameters"]).ToObject<Dictionary<string, object>>();
          ApplyAutoParameters(type, behaviour, behaviourParams);
          behaviour.Init(behaviourParams);
        }
      }

    }

    public override void PostInit() {
      if (Behaviour != null)
        Behaviour.PostInit();
    }

    private void ApplyAutoParameters(Type type, Behaviour behaviour, Dictionary<string, object> parameters) {
      foreach (KeyValuePair<string, object> parameter in parameters) {
        try {
          PropertyInfo propertyInfo = type.GetProperty(parameter.Key);
          if (propertyInfo != null) {
            ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
            if (attribute != null) {
              propertyInfo.SetValue(behaviour, attribute.Convert(parameter.Value.ToString()));
              continue;
            }
          }
          propertyInfo.SetValue(behaviour, Convert.ChangeType(parameter.Value, propertyInfo.PropertyType));
        } catch (InvalidCastException) {
          Log.log(LogCategory.BehaviourSystem, LogLevel.Debug, "Property: " + parameter.Key + " failed the cast in ApplyAutoParameters. ApplyParameters method inside behaviour should be used.");
        } catch (NullReferenceException) {
          Log.log(LogCategory.BehaviourSystem, LogLevel.Debug, "Tried to load a missing property: " + parameter.Key + " from json. Property will be ignored!");
        }
      }
    }

    public override string ToString() {
      return "%{LemonChiffon}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Behaviour: %{White}% " + (Behaviour != null ? Behaviour.ToString() : "none");
    }
  }
}
