﻿using System.Collections.Generic;

namespace MonoPanda.ECS.Components.BehaviourComponent {
  public class Behaviour {

    public Entity Entity { get; set; }

    public virtual void Init(Dictionary<string, object> parameters) { }

    public virtual void PostInit() { }

    public virtual void Update() { }

  }
}
