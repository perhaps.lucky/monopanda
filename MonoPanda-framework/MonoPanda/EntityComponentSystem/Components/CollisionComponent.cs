using Humper;

using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.ParameterConvert;
using MonoPanda.Systems;

namespace MonoPanda.Components {
  public class CollisionComponent : EntityComponent {
    [RectangleParameter] public Rectangle BoundingRectangle { get; set; }
    public IBox HumperBox { get; set; }
    
    public Optional<MoveComponent> MoveComponentOptional { get; private set; }
    public Optional<SolidComponent> SolidComponentOptional { get; private set; }

    public bool IsColliding(Entity entity) {
      var component = entity.GetComponent<CollisionComponent>();
      if (component != null)
        return IsColliding(component);
      return false;
    }

    public bool IsColliding(CollisionComponent component) {
      if (HumperBox == null || component.HumperBox == null)
        return false;
      return HumperBox.Bounds.Intersects(component.HumperBox.Bounds);
    }

    public bool Contains(Vector2 position) {
      if (HumperBox == null)
        return false;

      var collisionRectangle = new Rectangle(
        (int)(Entity.Position.X + BoundingRectangle.X * Entity.Scale),
        (int)(Entity.Position.Y + BoundingRectangle.Y * Entity.Scale),
        (int)(BoundingRectangle.Width * Entity.Scale),
        (int)(BoundingRectangle.Height * Entity.Scale));

      return collisionRectangle.Contains(position);
    }

    public void UpdateBoundingRectangle(Rectangle rectangle) {
      BoundingRectangle = rectangle;
      RefreshBoundingRectangle();
    }

    public void RefreshBoundingRectangle() {
      Entity.EntityComponentSystem.GetSystem<CollisionSystem>().RemoveBox(HumperBox);
      HumperBox = null;
    }

    public override void PostInit() {
      MoveComponentOptional = Entity.GetComponentOptional<MoveComponent>();
      SolidComponentOptional = Entity.GetComponentOptional<SolidComponent>();
    }

    public override string ToString() {
      return "%{GreenYellow}% " + this.ShortIdentifier + ": \n"
             + "%{PaleGoldenrod}% OffsetRectangle: %{White}% " + BoundingRectangle;
    }
  }
}