﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.Logger;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class DrawComponent : EntityComponent {
    public string ContentId { private get; set; }
    [VectorParameter]
    public Vector2 Origin { get { return origin; } set { origin = value; HasSetOrigin = true; } }
    [ColorParameter]
    public Color Color { get; set; } = Color.White;
    public float Rotation { get; set; }
    public float Scale { get; set; } = 1f;
    public SpriteEffects SpriteEffect { get; set; }
    public float Depth { get; set; }
    public IDraw DrawTarget { get => drawTarget; set => setDrawTarget(value); }

    private IDraw drawTarget;
    public bool IsVisible { get; set; } = true;
    [VectorParameter]
    public Vector2 Size { get; set; }

    private Vector2 origin;
    public bool HasSetOrigin { get; private set; }

    public override void Init(Dictionary<string, object> parameters) {
      ContentItemType itemType = ContentManager.getItem(ContentId).ItemType;
      switch (itemType) {
        case ContentItemType.Texture2D:
          drawTarget = new Sprite(ContentId);
          break;
        case ContentItemType.SpriteSheet:
          drawTarget = new SpriteSheetSprite(ContentId);
          break;
        case ContentItemType.SpineAsset:
          drawTarget = new SpineSprite(ContentId);
          break;
      }
    }
    
    public override void Destroy() {
      if(drawTarget is IDisposable)
        ((IDisposable)drawTarget).Dispose();
    }
    
    public void Draw(SpriteBatch spriteBatch) {
      drawTarget?.Draw(spriteBatch, this);
    }

    public void SetSprite(int spriteIndex) {
      if (drawTarget is SpriteSheetSprite) {
        ((SpriteSheetSprite) drawTarget).UpdateSourceRectangle(spriteIndex);
        return;
      }
      Log.log(LogCategory.DrawingSystems, LogLevel.Warn, $"DrawTarget in DrawComponent in Entity: {Entity.Id} is not a spritesheet!");
    }

    private void setDrawTarget(IDraw newDrawTarget) {
      if(drawTarget is IDisposable)
        ((IDisposable)drawTarget).Dispose();
      drawTarget = newDrawTarget;
    }

    public override string ToString() {
      return "%{Gold}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% ContentId: %{White}% " + ContentId + "\n"
        + "%{PaleGoldenrod}% Origin: %{White}% " + Origin + "\n"
        + "%{PaleGoldenrod}% Rotation: %{White}% " + Rotation + "\n"
        + "%{PaleGoldenrod}% Scale: %{White}% " + Scale + "\n"
        + "%{PaleGoldenrod}% Depth: %{White}% " + Depth + "\n"
        + "%{PaleGoldenrod}% SpriteEffects: %{White}% " + SpriteEffect.ToString() + "\n"
        + "%{PaleGoldenrod}% DrawTarget: %{White}% " + DrawTarget.ToString() + "\n"
        + "%{PaleGoldenrod}% Visible: " + (IsVisible ? "%{Lime}% Yes" : "%{OrangeRed}% No") + "\n"
        + "%{PaleGoldenrod}% Size: %{White}% " + Size;
    }
  }
}
