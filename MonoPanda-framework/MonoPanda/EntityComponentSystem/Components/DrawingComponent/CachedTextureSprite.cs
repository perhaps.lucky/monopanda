﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Components;

using System;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class CachedTextureSprite : IDraw, IDisposable {
    public Texture2D Texture { get; private set; }

    public CachedTextureSprite(Texture2D texture) {
      Texture = texture;
    }

    public void Draw(SpriteBatch spriteBatch, DrawComponent component) {
      if (Texture != null && !Texture.IsDisposed && component.IsVisible) {
#pragma warning disable CS0618 // Type or member is obsolete, but I like this method 😥
        spriteBatch.Draw(
          Texture,
          position: component.Entity.Position,
          color: component.Color,
          rotation: component.Rotation + component.Entity.Rotation,
          scale: new Vector2(component.Scale * component.Entity.Scale),
          origin: component.HasSetOrigin ? component.Origin : new Vector2(Texture.Width / 2, Texture.Height / 2),
          effects: component.SpriteEffect,
          layerDepth: component.Depth,
          sourceRectangle: null);
#pragma warning restore CS0618
      }
    }

    public void Dispose() {
      Texture?.Dispose();
    }
  }
}