﻿using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Components;
using System.Drawing;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public interface IDraw {
    void Draw(SpriteBatch spriteBatch, DrawComponent component);
  }
}
