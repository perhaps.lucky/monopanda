﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.Spine.MonoPanda;
using MonoPanda.Utils;

using Spine;

using System;
using System.Collections.Generic;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class SpineSprite : IDraw {
    private const string ACTIVE_SKIN_ID = "activeSkin";

    public string AssetId { get; set; }
    public Skeleton Skeleton { get; set; }

    private List<string> skins = new List<string>(); // list of id of skins from skeletonData that are active
    private bool skinsChanged;

    private Dictionary<string, Color> slotColors = new Dictionary<string, Color>();
    private bool colorsChanged;

    public SpineSprite(string assetId) {
      AssetId = assetId;
    }

    public void ApplyColor(string slotId, Color color) {
      if (slotColors.ContainsKey(slotId))
        slotColors.Remove(slotId);
      slotColors.Add(slotId, color);
      colorsChanged = true;
    }

    public void ApplyColorByPrefix(string prefix, Color color) {
      performByPrefix(prefix, slot => ApplyColor(slot.Data.Name, color));
    }

    public void ApplyColorBySuffix(string suffix, Color color) {
      performBySuffix(suffix, slot => ApplyColor(slot.Data.Name, color));
    }

    public void ApplyColorByFilter(Predicate<Slot> filter, Color color) {
      performByFilter(filter, slot => ApplyColor(slot.Data.Name, color));
    }


    public void ClearColor(string slotId) {
      if (slotColors.ContainsKey(slotId))
        slotColors.Remove(slotId);
      colorsChanged = true;
    }

    public void ClearColorByPrefix(string prefix) {
      performByPrefix(prefix, slot => ClearColor(slot.Data.Name));
    }

    public void ClearColorBySuffix(string suffix) {
      performBySuffix(suffix, slot => ClearColor(slot.Data.Name));
    }

    public void ClearColorByFilter(Predicate<Slot> filter) {
      performByFilter(filter, slot => ClearColor(slot.Data.Name));
    }

    public void AddSkin(string skinId) {
      if (!skins.Contains(skinId)) {
        skins.Add(skinId);
        skinsChanged = true;
      }
    }

    public void RemoveSkin(string skinId) {
      if (skins.Contains(skinId)) {
        skins.Remove(skinId);
        skinsChanged = true;
      }
    }

    public void ApplyState(AnimationState state) {
      SpineAsset asset = ContentManager.Get<SpineAsset>(AssetId);
      if (asset != null) {
        createSkeletonIfNeeded(asset);
        state.Apply(Skeleton);
      }
    }

    public void SetSlotVisible(string slotName, bool visible) {
      if (Skeleton == null) {
        createSkeletonIfNeeded(ContentManager.Get<SpineAsset>(AssetId, true));
      }

      Skeleton.FindSlot(slotName).visible = visible;
    }

    public void SetSlotsVisibleByPrefix(string prefix, bool visible) {
      performByPrefix(prefix, slot => slot.visible = visible);
    }

    public void SetSlotsVisibleBySuffix(string suffix, bool visible) {
      performBySuffix(suffix, slot => slot.visible = visible);
    }

    public void SetSlotsVisibleByFilter(Predicate<Slot> filter, bool visible) {
      performByFilter(filter, slot => slot.visible = visible);
    }

    public void Draw(SpriteBatch spriteBatch, DrawComponent component) {
      // SpineDraw should be used instead
    }

    public void Update(DrawComponent component) {
      SpineAsset asset = ContentManager.Get<SpineAsset>(AssetId);
      if (asset != null) {
        createSkeletonIfNeeded(asset);

        var rotation = component.Rotation + component.Entity.Rotation;
        var position = component.Entity.Position - component.Origin * component.Entity.Scale;
        if (component.HasSetOrigin)
          position = RotationUtils.RotateWithAnchor(position, rotation, component.Origin);

        Skeleton.X = position.X;
        Skeleton.Y = position.Y;
        Skeleton.RootBone.Rotation = -(rotation) * 180.0f / 3.14f;
        Skeleton.ScaleX = Skeleton.ScaleY = component.Scale * component.Entity.Scale;
        Skeleton.R = ColorUtils.ByteToFloat(component.Color.R);
        Skeleton.G = ColorUtils.ByteToFloat(component.Color.G);
        Skeleton.B = ColorUtils.ByteToFloat(component.Color.B);
        Skeleton.A = ColorUtils.ByteToFloat(component.Color.A);

        if (component.SpriteEffect.HasFlag(SpriteEffects.FlipHorizontally)) {
          Skeleton.ScaleX *= -1;
          Skeleton.RootBone.Rotation *= -1;
        }

        if (component.SpriteEffect.HasFlag(SpriteEffects.FlipVertically)) {
          Skeleton.ScaleY *= -1 * (Bone.yDown ? -1 : 1);
          Skeleton.RootBone.Rotation *= -1;
        }

        applySkins(asset);
        applyColors(asset);
        Skeleton.UpdateWorldTransform();
      }
    }

    /// <summary>
    /// Purpose of this check is to refresh Skeleton if asset gets unload.
    /// It's rather rare but it's possible to happen.
    /// Then again I would rethink design if object stays outside the screen for 30min (default time).
    /// Maybe it should never be unloaded.
    /// </summary>
    public void CheckIfContentUnloaded() {
      if (!ContentManager.getItem(AssetId).IsLoaded && Skeleton != null) {
        Log.log(LogCategory.Spine, LogLevel.Warn,
          $"Spine asset got unloaded, Skeleton with AssetId: {AssetId} will be reloaded.");
        Skeleton = null;
      }
    }

    public void DrawSpine(SkeletonRenderer renderer) {
      SpineAsset asset = ContentManager.Get<SpineAsset>(AssetId);
      if (asset != null && Skeleton != null) {
        renderer.Draw(Skeleton);
      }
    }

    private void performByPrefix(string prefix, Action<Slot> actionOnSlot) {
      if (Skeleton == null) {
        createSkeletonIfNeeded(ContentManager.Get<SpineAsset>(AssetId, true));
      }

      foreach (Slot slot in Skeleton.Slots) {
        if (slot.Data.Name.StartsWith(prefix))
          actionOnSlot.Invoke(slot);
      }
    }

    private void performBySuffix(string suffix, Action<Slot> actionOnSlot) {
      if (Skeleton == null) {
        createSkeletonIfNeeded(ContentManager.Get<SpineAsset>(AssetId, true));
      }

      foreach (Slot slot in Skeleton.Slots) {
        if (slot.Data.Name.EndsWith(suffix))
          actionOnSlot.Invoke(slot);
      }
    }

    private void performByFilter(Predicate<Slot> filter, Action<Slot> actionOnSlot) {
      if (Skeleton == null) {
        createSkeletonIfNeeded(ContentManager.Get<SpineAsset>(AssetId, true));
      }

      foreach (Slot slot in Skeleton.Slots.FindAll(filter)) {
        actionOnSlot.Invoke(slot);
      }
    }

    private void createSkeletonIfNeeded(SpineAsset asset) {
      if (Skeleton == null) {
        Skeleton = new Skeleton(asset.SkeletonData);
        skinsChanged = true;
        colorsChanged = true;
        return;
      }

      Slot slot = Skeleton.DrawOrder.Find(s => s.Attachment is RegionAttachment);
      AtlasRegion region = (slot?.Attachment as RegionAttachment)?.RendererObject as AtlasRegion;
      if (region == null)
        return;
      Texture2D texture = region.page.rendererObject as Texture2D;
      if (texture.IsDisposed) {
        Skeleton = new Skeleton(asset.SkeletonData);
        skinsChanged = true;
        colorsChanged = true;
      }
    }

    private void applySkins(SpineAsset asset) {
      if (!skinsChanged)
        return;

      Skin skin = new Skin(ACTIVE_SKIN_ID);
      foreach (string skinId in skins)
        applySkin(skinId, skin);

      switchSkeletonSkin(skin);
      skinsChanged = false;
      colorsChanged = true; // applying skins reset colors so they need to be re-applied
    }

    private void applySkin(string skinId, Skin skin) {
      Skin skinToApply = Skeleton.Data.FindSkin(skinId);
      if (skinToApply == null) {
        if (!skinId.Equals(Config.Spine.EmptySkinName))
          Log.log(LogCategory.Spine, LogLevel.Warn,
            "Skin id: " + skinId + " not found for spine asset: " + AssetId + ". Skin will not be applied!");
        return;
      }

      skin.AddSkin(skinToApply);
    }

    private void switchSkeletonSkin(Skin newSkin) {
      Skeleton.Skin = newSkin;
      Skeleton.SetSlotsToSetupPose();
    }

    private void applyColors(SpineAsset asset) {
      if (!colorsChanged)
        return;

      foreach (Slot slot in Skeleton.Slots.Items) {
        resetSlotColor(slot);
      }

      foreach (KeyValuePair<string, Color> slotToColor in slotColors) {
        Slot slot = Skeleton.FindSlot(slotToColor.Key);
        if (slot != null) {
          applyColorToSlot(slot, slotToColor.Value);
        }
      }

      colorsChanged = false;
    }

    private void applyColorToSlot(Slot slot, Color color) {
      slot.R = ColorUtils.ByteToFloat(color.R);
      slot.G = ColorUtils.ByteToFloat(color.G);
      slot.B = ColorUtils.ByteToFloat(color.B);
      slot.A = ColorUtils.ByteToFloat(color.A);
    }

    private void resetSlotColor(Slot slot) {
      slot.R = 1f;
      slot.G = 1f;
      slot.B = 1f;
      slot.A = 1f;
    }
  }
}


/*
 *      Leaving this poor attempt at transfering it to SpriteBatch. Didn't work well but maybe one day it will be necessary to move it.
 *      Who knows.
 * 
 *
 *
 * private void drawAttachment(RegionAttachment attachment, SpriteBatch spriteBatch, DrawComponent component) {
      AtlasRegion region = attachment.RendererObject as AtlasRegion;
      Texture2D texture = region.page.rendererObject as Texture2D;

      if (texture.IsDisposed) {
        Skeleton = null; // reload skeleton to use new texture from content manager
        return;
      }

      Skeleton.UpdateWorldTransform();

      spriteBatch.Draw(
        texture,
        position: new Vector2(component.Entity.Position.X + region.offsetX + attachment.X, component.Entity.Position.Y + region.offsetY + attachment.Y),
        sourceRectangle: new Rectangle(region.x, region.y, region.width, region.height),
        color: ColorUtils.MixColors(component.Color, new Color(attachment.R, attachment.B, attachment.G, attachment.A)),
        rotation: (component.SpriteEffect == SpriteEffects.FlipHorizontally ? 1 : -1) * (component.Rotation + attachment.Rotation) * 3.14159f / 180.0f,
        scale: new Vector2(component.Scale * component.Entity.Scale * attachment.ScaleX, component.Scale * component.Entity.Scale * attachment.ScaleY),
        origin: component.HasSetOrigin ? component.Origin : new Vector2(region.width * 0.5f, region.height * 0.5f),
        effects: component.SpriteEffect,
        layerDepth: component.Depth);
    }

  */