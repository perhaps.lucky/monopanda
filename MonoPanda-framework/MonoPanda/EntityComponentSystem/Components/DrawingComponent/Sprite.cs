﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Components;
using MonoPanda.Content;
using MonoPanda.Utils;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class Sprite : IDraw {
    public string TextureId { get; set; }

    public Sprite(string textureId) {
      TextureId = textureId;
    }

    public void Draw(SpriteBatch spriteBatch, DrawComponent component) {
      Texture2D texture = ContentManager.Get<Texture2D>(TextureId);
      if (texture != null && component.IsVisible) {
#pragma warning disable CS0618 // Type or member is obsolete, but I like this method 😥
        spriteBatch.Draw(
          texture,
          position: component.Entity.Position,
          color: component.Color,
          rotation: component.Rotation + component.Entity.Rotation,
          scale: new Vector2(component.Scale * component.Entity.Scale),
          origin: component.HasSetOrigin ? component.Origin : new Vector2(texture.Width / 2, texture.Height / 2),
          effects: component.SpriteEffect,
          layerDepth: component.Depth,
          sourceRectangle: null);
#pragma warning restore CS0618
      }
    }
  }
}
