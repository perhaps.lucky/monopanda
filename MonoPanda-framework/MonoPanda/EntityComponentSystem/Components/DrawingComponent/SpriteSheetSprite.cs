﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Components;
using MonoPanda.Content;
using MonoPanda.SpriteSheets;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class SpriteSheetSprite : IDraw {
    public string TextureId { get; set; }
    public Rectangle SourceRectangle { get; set; }

    public SpriteSheetSprite(string textureId) {
      TextureId = textureId;
    }

    public void Draw(SpriteBatch spriteBatch, DrawComponent component) {
      SpriteSheet spriteSheet = ContentManager.Get<SpriteSheet>(TextureId);
      if (spriteSheet != null && component.IsVisible) {
        if (SourceRectangle.Equals(Rectangle.Empty))
          UpdateSourceRectangle(0);
        
        spriteBatch.Draw(
          spriteSheet.Texture,
          position: component.Entity.Position,
          sourceRectangle: SourceRectangle,
          color: component.Color,
          rotation: component.Rotation + component.Entity.Rotation,
          scale: new Vector2(component.Scale * component.Entity.Scale),
          origin: component.HasSetOrigin ? component.Origin : new Vector2(SourceRectangle.Width / 2, SourceRectangle.Height / 2),
          effects: component.SpriteEffect,
          layerDepth: component.Depth);
      }
    }

    public void UpdateSourceRectangle(string sourceRectangleFilename) {
      SpriteSheet spriteSheet = ContentManager.Get<SpriteSheet>(TextureId, true);
      SourceRectangle = spriteSheet.SpriteSheetDescription.frames.Find(f => f.filename.Equals(sourceRectangleFilename)).frame.ToRectangle();
    }

    public void UpdateSourceRectangle(int frame) {
      SpriteSheet spriteSheet = ContentManager.Get<SpriteSheet>(TextureId, true);
      SourceRectangle = spriteSheet.SpriteSheetDescription.frames[frame].frame.ToRectangle();
    }
  }
}
