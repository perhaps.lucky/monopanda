﻿using Microsoft.Xna.Framework.Graphics;
using MonoPanda.BitmapFonts;
using MonoPanda.Components;

namespace MonoPanda.ECS.Components.DrawingComponent {
  public class TextSprite : IDraw {

    public void Draw(SpriteBatch spriteBatch, DrawComponent component) {
      if (component.IsVisible) {
        component.Entity.GetComponentOptional<TextComponent>().IfPresentAnd(
          textComponent => textComponent.Active,
          textComponent =>
            FontRenderer.DrawText(
              textComponent.Font,
              textComponent.Entity.Position + textComponent.PositionOffset,
              textComponent.Text,
              textComponent.FontColor,
              textComponent.FontSize,
              false,
              -1,
              textComponent.Depth,
              spriteBatch)
        );
      }
    }

  }
}
