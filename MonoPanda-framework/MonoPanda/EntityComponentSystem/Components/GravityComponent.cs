﻿using MonoPanda.ECS;
using MonoPanda.Logger;

namespace MonoPanda.Components {
  public class GravityComponent : EntityComponent {
    public float GravityMultiplier { get; set; } = 1f;
    
    public Optional<MoveComponent> MoveComponentOptional { get; private set; }

    public override void PostInit() {
      MoveComponentOptional = Entity.GetComponentOptional<MoveComponent>();
      if(!MoveComponentOptional.IsPresent())
        Log.log(LogCategory.EntityFactory, LogLevel.Warn, $"Entity: {Entity.Id} is missing MoveComponent. GravityComponent will have no effect.");
    }

    public override string ToString() {
      return "%{GhostWhite}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% GravityMultiplier: %{White}% " + GravityMultiplier;
    }
  }
}
