﻿using MonoPanda.ECS;

namespace MonoPanda.Components {
  public class GravityFieldComponent : EntityComponent {
    public float Radius { get; set; }
    public float Power { get; set; }

    public override string ToString() {
      return "%{DeepSkyBlue}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Radius: %{White}% " + Radius + "\n"
        + "%{PaleGoldenrod}% Power: %{White}% " + Power;
    }
  }
}
