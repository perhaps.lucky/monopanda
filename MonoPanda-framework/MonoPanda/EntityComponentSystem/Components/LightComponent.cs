﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.ECS.Components.LightComponent;
using MonoPanda.ParameterConvert;
using MonoPanda.UtilityClasses;
using MonoPanda.Utils;

using Penumbra;

using System;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class LightComponent : EntityComponent {
    public string ContentId { get; set; }

    public float ConeDecay {
      get => coneDecay;
      set {
        coneDecay = value;
        if (Light is Spotlight) ((Spotlight)Light).ConeDecay = value;
      }
    }

    public float Intensity { get => Light.Intensity; set => Light.Intensity = value; }
    public float Radius { get => Light.Radius; set => Light.Radius = value; }
    public float Rotation { get => Light.Rotation; set => Light.Rotation = value; }

    [VectorParameter] public Vector2 Scale { get => Light.Scale; set => Light.Scale = value; }

    [ColorParameter] public Color Color { get => Light.Color; set => Light.Color = value; }

    public ShadowType ShadowType {
      get => Light.ShadowType;
      set {
        Light.ShadowType = value;
        ShadowTypeSet = true;
      }
    }

    public bool ShadowTypeSet { get; private set; }

    [VectorParameter] public Vector2 OffsetPosition { get; set; }

    private LightType type;
    private float coneDecay;

    public Light Light {
      get => light;
      set {
        OldLight = light;
        LightTypeChanged = true;
        light = value;
      }
    }

    // information for LightSystem to remove old light
    public bool LightTypeChanged { get; set; }
    public Light OldLight { get; private set; }
    private Light light;

    private float savedIntensity;

    private Observer observer;

    public LightComponent() {
      Light = new PointLight();
    }

    public override void Init(Dictionary<string, object> parameters) {
      if (parameters.ContainsKey("Type")) {
        Enum.TryParse(parameters["Type"].ToString(), out LightType lightType);
        setType(lightType);
      }

      // must be set after light is created
      if (parameters.ContainsKey("ShadowType")) {
        Enum.TryParse(parameters["ShadowType"].ToString(), out ShadowType shadowType);
        ShadowType = shadowType;
      }

      Light.Position = Entity.Position + OffsetPosition;
      observer = Entity.RegisterObserver(new Action(() => { Light.Position = Entity.Position + OffsetPosition; }));
    }

    public override void Destroy() {
      Entity.UnregisterObserver(observer);
    }

    protected override void SetActive() {
      Intensity = savedIntensity;
    }

    protected override void SetInactive() {
      savedIntensity = Intensity;
      Intensity = 0;
    }

    private void setType(LightType newLightType) {
      if (newLightType == type)
        return;

      Light newLight = null;
      switch (newLightType) {
        case LightType.PointLight:
          newLight = new PointLight();
          break;
        case LightType.Spotlight:
          newLight = new Spotlight();
          ((Spotlight)newLight).ConeDecay = coneDecay;
          break;
        case LightType.TexturedLight:
          newLight = new TexturedLight();
          // texture applied in LightSystem
          break;
      }

      newLight.Intensity = Light.Intensity;
      newLight.Radius = Light.Radius;
      newLight.Rotation = Light.Rotation;
      newLight.Scale = Light.Scale;
      newLight.Color = Light.Color;
      newLight.ShadowType = Light.ShadowType;

      Light = newLight;
      type = newLightType;
    }

    public void AddIgnoredHull(Hull hull) {
      if (!Light.IgnoredHulls.Contains(hull))
        Light.IgnoredHulls.Add(hull);
    }

    public void AddIgnoredHull(ShadowHullComponent component) => AddIgnoredHull(component.Hull);

    public void AddIgnoredHull(Entity entity) {
      Entity.GetComponentOptional<ShadowHullComponent>().IfPresent(AddIgnoredHull);
    }

    public void RemoveIgnoredHull(Hull hull) {
      Light.IgnoredHulls.Remove(hull);
    }

    public void RemoveIgnoredHull(ShadowHullComponent component) => RemoveIgnoredHull(component.Hull);

    public void RemoveIgnoredHull(Entity entity) {
      Entity.GetComponentOptional<ShadowHullComponent>().IfPresent(RemoveIgnoredHull);
    }

    public override string ToString() {
      return "%{Moccasin}% " + this.ShortIdentifier + ":\n"
             + "%{PaleGoldenrod}% Type: %{White}% " + type.ToString() + "\n"
             + "%{PaleGoldenrod}% Intensity: %{White}% " + Intensity + "\n"
             + "%{PaleGoldenrod}% Radius: %{White}% " + Radius + "\n"
             + "%{PaleGoldenrod}% Rotation: %{White}% " + Rotation + "\n"
             + "%{PaleGoldenrod}% Scale: %{White}% " + Scale;
    }
  }
}