﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class MoveComponent : EntityComponent {

    /// <summary>
    /// Affects Velocity. It does not change within default systems.
    /// </summary>
    [VectorParameter]
    public Vector2 Acceleration { get; set; }

    /// <summary>
    /// Describes a movement in current update. Will naturally go to 0.
    /// </summary>
    [VectorParameter]
    public Vector2 Velocity { get; set; }

    /// <summary>
    /// Individual multiplier of MaxAccelerationToVelocity value from MoveSystem
    /// </summary>
    public float MaxAccelerationToVelocityMultiplier { get; set; } = 1f;

    /// <summary>
    /// Individual multiplier of VelocityLossMultiplier value from MoveSystem.
    /// </summary>
    public float VelocityLossMultiplier { get; set; } = 1f;

    /// <summary>
    /// Prevents from losing velocity
    /// </summary>
    public bool LockVelocity { get; set; }

    /// <summary>
    /// Position from last update
    /// </summary>
    public Vector2 LastPosition { get; set; }

    /// <summary>
    /// Setting another component to this field will cause this entity to move in exactly same way as mimicked entity (in MoveSystem).
    /// After movement, this field is set back to null.
    /// </summary>
    public MoveComponent Mimicking { get; set; }

    public override string ToString() {
      return "%{LightCyan}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Acceleration: %{White}% " + Acceleration + "\n"
        + "%{PaleGoldenrod}% Velocity: %{White}% " + Velocity;
    }

    public override void Init(Dictionary<string, object> parameters) {
      LastPosition = Entity.Position;
    }
  }
}
