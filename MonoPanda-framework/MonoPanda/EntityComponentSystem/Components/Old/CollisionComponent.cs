﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.Flag;
using MonoPanda.Logger;
using MonoPanda.Old.CollisionComponentDetails;
using MonoPanda.Old.ECS.Components.CollisionComponentDetails;
using MonoPanda.UtilityClasses;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;

namespace MonoPanda.Old.Components {
  public class CollisionComponent : EntityComponent {
    public Rectangle BoundingRectangle { get; set; }
    public CollisionMovementParameters CollisionMovementParams { get; set; }

    // used for checks if update is needed
    public OneTimeFlag UpdateRequested { get; set; }
    private Observer observer;

    public override void Init(Dictionary<string, object> parameters) {
      UpdateRequested = new OneTimeFlag(Entity.Id + "_CollisionComponentUpdateRequested",
        LogCategory.CollisionDetection);
      BoundingRectangle =
        ParameterUtils.GetRectangleFromParameters(parameters, "BoundingRectangle");
      CollisionMovementParams = new CollisionMovementParameters(this);
      observer =
        Entity.RegisterObserver(new Action(() => { UpdateRequested.Reinitialize(); }));
    }

    public override void Destroy() {
      Entity.UnregisterObserver(observer);
    }

    public bool IsColliding(CollisionComponent other, bool precise = true) {
      if (!precise) {
        return CollisionMovementParams.MovementRectangle.Intersects(other.CollisionMovementParams
          .MovementRectangle);
      }

      CollisionDetails collisionDetails = GetCollisionDetails(other);
      return collisionDetails.IsColliding;
    }

    public CollisionDetails GetCollisionDetails(CollisionComponent other) {
      if (!CollisionMovementParams.MovementRectangle.Intersects(other.CollisionMovementParams
        .MovementRectangle))
        return CollisionDetails.NoCollision;

      return new CollisionDetails(CollisionMovementParams, other.CollisionMovementParams);
    }

    public bool IsColliding(Entity entity, bool precise = true) {
      var component = entity.GetComponent<CollisionComponent>();
      if (component != null)
        return IsColliding(component, precise);
      return false;
    }

    public CollisionDetails GetCollisionDetails(Entity entity) {
      var component = entity.GetComponent<CollisionComponent>();
      if (component != null)
        return GetCollisionDetails(component);
      return CollisionDetails.NoCollision;
    }

    public bool Contains(Vector2 position) {
      return CollisionMovementParams.CurrentCollisionRectangle.Contains(position);
    }

    public override string ToString() {
      return "%{GreenYellow}% " + this.ShortIdentifier + ": \n"
             + "%{PaleGoldenrod}% OffsetRectangle: %{White}% " + BoundingRectangle;
    }
  }
}