﻿using Microsoft.Xna.Framework;

using MonoPanda.GlobalTime;
using MonoPanda.Logger;
using MonoPanda.Old.ECS.Components.CollisionComponentDetails;
using MonoPanda.Old.Systems;

namespace MonoPanda.Old.CollisionComponentDetails {
  public class CollisionDetails {
    public bool IsColliding { get; private set; }
    public CollisionRectangle ThisCollisionRectangle { get; private set; }
    public CollisionRectangle OtherCollisionRectangle { get; private set; }
    public CollisionRectangle ThisPreviousRectangle { get; private set; }
    public CollisionRectangle OtherPreviousRectangle { get; private set; }

    /// <summary>
    /// Direction of collision, e.g. Top means that "This" object hit "Other" object from top.
    /// </summary>
    public CollisionDirection Direction { get; private set; }

    private CollisionDetails() { } // not colliding

    public static CollisionDetails NoCollision => new CollisionDetails();

    public CollisionDetails(CollisionMovementParameters thisParams,
      CollisionMovementParameters otherParams) {
      float timePassed = Time.ElapsedMillis;
      //int timeSteps = 100;
      int timeSteps = (int)(timePassed / CollisionSystem.CollisionDetailsTimeStep);
      float thisUpdateDistance = thisParams.Distance / timeSteps;
      float otherUpdateDistance = otherParams.Distance / timeSteps;
      Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
        "This update distance: " + thisUpdateDistance);
      Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
        "Other update distance: " + otherUpdateDistance);

      for (int i = 0; i < timeSteps; i++) {
        var thisRectPos = getPosition(thisParams, thisUpdateDistance * (i + 1));
        var otherRectPos = getPosition(otherParams, otherUpdateDistance * (i + 1));

        var thisRect = getRectangle(thisRectPos, thisParams);
        var otherRect = getRectangle(otherRectPos, otherParams);

        if (thisRect.Intersects(otherRect)) {
          ThisCollisionRectangle = thisRect;
          OtherCollisionRectangle = otherRect;
          ThisPreviousRectangle = thisParams.PreviousCollisionRectangle;
          OtherPreviousRectangle = otherParams.PreviousCollisionRectangle;
          Direction = getCollisionDirection();
          Log.log(LogCategory.CollisionDetection, LogLevel.Debug, "Direction: " + Direction);
          Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
            "This bottom: " + ThisCollisionRectangle.Bottom);
          Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
            "Other top: " + OtherCollisionRectangle.Top);
          Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
            "Previous this bottom: " + ThisPreviousRectangle.Bottom);
          Log.log(LogCategory.CollisionDetection, LogLevel.Debug,
            "Previous other top: " + OtherPreviousRectangle.Top);
          IsColliding = true;

          return;
        }
      }
      // not colliding :c
    }

    private Vector2 getPosition(CollisionMovementParameters param, float updateDistance) {
      return param.PreviousCollisionRectangle.Center + param.MovementDirection * updateDistance;
    }

    private CollisionRectangle getRectangle(Vector2 position, CollisionMovementParameters param) {
      return new CollisionRectangle(
        position.X - param.PreviousCollisionRectangle.Width / 2,
        position.Y - param.PreviousCollisionRectangle.Height / 2,
        param.PreviousCollisionRectangle.Width,
        param.PreviousCollisionRectangle.Height);
    }

    private CollisionDirection getCollisionDirection() {
      if (collisionFromTop())
        return CollisionDirection.Top;
      if (collisionFromBottom())
        return CollisionDirection.Bottom;
      if (collisionFromLeft())
        return CollisionDirection.Left;
      if (collisionFromRight())
        return CollisionDirection.Right;


      return CollisionDirection.NoDirection;
    }

    private bool collisionFromTop() {
      return ThisCollisionRectangle.Bottom >= OtherCollisionRectangle.Top
             && ThisPreviousRectangle.Bottom < OtherPreviousRectangle.Top;
    }

    private bool collisionFromBottom() {
      return ThisCollisionRectangle.Top <= OtherCollisionRectangle.Bottom
             && ThisPreviousRectangle.Top > OtherPreviousRectangle.Bottom;
    }

    private bool collisionFromLeft() {
      return ThisCollisionRectangle.Right >= OtherCollisionRectangle.Left
             && ThisPreviousRectangle.Right < OtherPreviousRectangle.Left;
    }

    private bool collisionFromRight() {
      return ThisCollisionRectangle.Left <= OtherCollisionRectangle.Right
             && ThisPreviousRectangle.Left > OtherPreviousRectangle.Right;
    }
  }
}