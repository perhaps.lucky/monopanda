﻿namespace MonoPanda.Old.ECS.Components.CollisionComponentDetails {
  public enum CollisionDirection {
    Top, Bottom, Right, Left, NoDirection
  }
}
