﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.Utils;

namespace MonoPanda.Old.ECS.Components.CollisionComponentDetails {
  public class CollisionMovementParameters {

    public CollisionRectangle CurrentCollisionRectangle { get; set; }
    public CollisionRectangle PreviousCollisionRectangle { get; set; }
    public bool NoMovement { get; set; }
    public CollisionRectangle MovementRectangle { get; set; }
    public Vector2 MovementDirection { get; set; }
    public float Distance { get; set; }

    public CollisionMovementParameters(MonoPanda.Old.Components.CollisionComponent component) {
      var scale = component.Entity.Scale;
      var offsetRectangle = component.BoundingRectangle;
      var width = component.BoundingRectangle.Width;
      var height = component.BoundingRectangle.Height;
      var moveComponent = component.Entity.GetComponent<MoveComponent>();
      var rotation = component.Entity.Rotation;

      CurrentCollisionRectangle = createRectangleWithOffset(component.Entity.Position, offsetRectangle, scale, rotation, width, height);

      if (moveComponent == null) {
        PreviousCollisionRectangle = MovementRectangle = CurrentCollisionRectangle;
        NoMovement = true;
      } else {
        PreviousCollisionRectangle = createRectangleWithOffset(moveComponent.LastPosition, offsetRectangle, scale, rotation, width, height);
        MovementRectangle = CollisionRectangle.Union(PreviousCollisionRectangle, CurrentCollisionRectangle);
      }

      if (PreviousCollisionRectangle.Center != CurrentCollisionRectangle.Center)
        MovementDirection = VectorUtils.GetDirectionVector(PreviousCollisionRectangle.Center, CurrentCollisionRectangle.Center);
      else
        MovementDirection = new Vector2(0, 0);
      Distance = Vector2.Distance(PreviousCollisionRectangle.Center, CurrentCollisionRectangle.Center);
    }

    private CollisionRectangle createRectangleWithOffset(Vector2 position, Rectangle offsetRectangle, float scale, float rotation, float width, float height) {
      var scaledWidth = width * scale;
      var scaledHeight = height * scale;

      CollisionRectangle rectangle = new CollisionRectangle(
        position.X + offsetRectangle.X * scale - scaledWidth / 2,
        position.Y + offsetRectangle.Y * scale - scaledHeight / 2,
        scaledWidth,
        scaledHeight);

      // Rotating is a bad idea in general, for something like that one should use pixel-perfect collision or rectangles that can be actually rotated
      /*if (rotation != 0)
       rectangle = RectangleUtils.ApplyRotation(rectangle, rotation, new Vector2(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2));*/

      return rectangle;
    }
  }
}
