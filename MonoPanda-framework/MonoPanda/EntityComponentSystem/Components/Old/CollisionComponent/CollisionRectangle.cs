﻿using Microsoft.Xna.Framework;
using System;

namespace MonoPanda.Old.ECS.Components.CollisionComponentDetails {

  // I just need rectangles that live in beautiful universe of float precision
  public struct CollisionRectangle : IEquatable<CollisionRectangle> {

    public float X;
    public float Y;
    public float Width;
    public float Height;

    public CollisionRectangle(float x, float y, float width, float height) {
      X = x;
      Y = y;
      Width = width;
      Height = height;
    }

    public static CollisionRectangle FromRectangle(Rectangle rectangle) {
      return new CollisionRectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
    }

    public float Bottom { get { return Y + Height; } }
    public float Top { get { return Y; } }
    public float Right { get { return X + Width; } }
    public float Left { get { return X; } }
    public Vector2 Center { get { return new Vector2(X + Width / 2, Y + Height / 2); } }

    public bool Intersects(CollisionRectangle other) {
      if (this.Bottom < other.Top || this.Top > other.Bottom || this.Left > other.Right || this.Right < other.Left)
        return false;
      return true;
    }

    public bool Equals(CollisionRectangle other) {
      return
        this.X == other.X
        && this.Y == other.Y
        && this.Width == other.Width
        && this.Height == other.Height;
    }

    public static bool operator ==(CollisionRectangle a, CollisionRectangle b) {
      return a.Equals(b);
    }
    public static bool operator !=(CollisionRectangle a, CollisionRectangle b) {
      return !a.Equals(b);
    }

    public Rectangle ToRectangle() {
      return new Rectangle((int)X, (int)Y, (int)Width, (int)Height);
    }

    public static CollisionRectangle Union(CollisionRectangle one, CollisionRectangle another) {
      var top = Math.Min(one.Top, another.Top);
      var bottom = Math.Max(one.Bottom, another.Bottom);
      var left = Math.Min(one.Left, another.Left);
      var right = Math.Max(one.Right, another.Right);

      var width = right - left;
      var height = bottom - top;

      return new CollisionRectangle(left, top, width, height);
    }

    public override bool Equals(object obj) {
      if (obj is CollisionRectangle)
        return Equals((CollisionRectangle)obj);
      return false;

    }

    public override int GetHashCode() {
      return base.GetHashCode();
    }

    public override string ToString() {
      return "{X: " + X + " Y: " + Y + " Width: " + Width + " Height: " + Height + "}";
    }

    public bool Contains(Vector2 position) {
      return
           position.X >= Left
        && position.X <= Right
        && position.Y <= Bottom
        && position.Y >= Top;
    }
  }
}