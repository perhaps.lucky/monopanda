﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;
using MonoPanda.Logger;
using MonoPanda.Utils;
using Newtonsoft.Json.Linq;
using Penumbra;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class ShadowHullComponent : EntityComponent {
    public Hull Hull { get => hull; set { HullChanged = true; OldHull = hull; hull = value; } }
    public bool HullChanged { get; set; }
    public Hull OldHull { get; private set; }

    private Hull hull;

    public ShadowHullComponent() {}

    public override void Init(Dictionary<string, object> parameters) {
      if (parameters.ContainsKey("PointsOffsets")) {
        List<Vector2> points = new List<Vector2>();
        foreach (object point in (JArray)parameters["PointsOffsets"]) {
          Vector2 offsetPoint = ParameterUtils.GetVector2(point);
          points.Add(offsetPoint);
        }
        Hull = new Hull(points);

        if (!Hull.Valid) {
          Log.log(LogCategory.LightSystem, LogLevel.Warn, "Invalid hull! Hulls must have at least 3 points and they should form a simple polygon.");
          Log.log(LogCategory.LightSystem, LogLevel.Warn, points);
        }
      }
    }

    public void ChangePoints(List<Vector2> points) {
      Hull = new Hull(points);
    }

    protected override void SetActive() {
      Hull.Enabled = true;
    }

    protected override void SetInactive() {
      Hull.Enabled = false;
    }

    public override string ToString() {
      return "%{SkyBlue}% " + this.ShortIdentifier;
    }

  }
}
