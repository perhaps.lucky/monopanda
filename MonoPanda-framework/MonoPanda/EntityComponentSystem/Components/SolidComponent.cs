﻿using MonoPanda.ECS;

namespace MonoPanda.Components {
  public class SolidComponent : EntityComponent {
    public override string ToString() {
      return "%{Tomato}% " + this.ShortIdentifier;
    }
  }
}
