﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using MonoPanda.ECS;
using MonoPanda.Systems;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class SoundEmitterComponent : EntityComponent {
    public AudioEmitter AudioEmitter { get; private set; }
    public float PositionZ { get; set; }
    public float DistanceOfMaxVolume { get; set; } = -1f;
    public float DistanceOfMinVolume { get; set; } = -1f;

    public override void Init(Dictionary<string, object> parameters) {
      AudioEmitter = new AudioEmitter();
    }

    public SoundEffectInstance PlaySFX(string SFX) {
      return Entity.EntityComponentSystem.GetSystemOptional<SoundSystem>()
        .IfPresentGet(soundSystem => soundSystem.PlaySFX(SFX, AudioEmitter))
        .OrElse(null);
    }

    public override void Destroy() {
      AudioEmitter.Position = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
    }

    public override string ToString() {
      return "%{PeachPuff}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Z: %{White}% " + PositionZ + "\n"
        + "%{PaleGoldenrod}% DistanceOfMinVolume: %{White}% " + DistanceOfMinVolume + "\n"
        + "%{PaleGoldenrod}% DistanceOfMaxVolume: %{White}% " + DistanceOfMaxVolume;
    }
  }
}
