﻿using Microsoft.Xna.Framework.Audio;
using MonoPanda.ECS;
using MonoPanda.Systems;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class SoundListenerComponent : EntityComponent {

    public AudioListener AudioListener { get; private set; }
    public float PositionZ { get; set; }

    public override void Init(Dictionary<string, object> parameters) {
      AudioListener = new AudioListener();
    }

    public void Activate() {
      Entity.EntityComponentSystem.GetSystemOptional<SoundSystem>().IfPresent((soundSystem) => {
        soundSystem.ActiveAudioListener = AudioListener;
      });
    }

    public override string ToString() {
      return "%{Plum}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Z: %{White}% " + PositionZ;
    }
  }
}
