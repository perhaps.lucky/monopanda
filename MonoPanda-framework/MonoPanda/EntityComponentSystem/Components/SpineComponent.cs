﻿using Microsoft.Xna.Framework;

using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.Logger;
using MonoPanda.Spine.MonoPanda;

using Spine;

using System;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class SpineComponent : EntityComponent {
    public AnimationState AnimationState { get; private set; }
    public float DefaultAnimationMix { get; set; } = 0.2f;

    public SpineSprite Sprite { get => sprite; }

    private SpineSprite sprite;
    private Dictionary<string, string> skinSlots = new Dictionary<string, string>();
    private Dictionary<int, string> animationNames = new Dictionary<int, string>();

    public override void PostInit() {
      setSprite();
      AnimationState = createAnimationState();
    }

    /// <summary>
    /// Applies colours to specified slot.
    /// </summary>
    public void ApplyColor(string slotName, Color color) {
      sprite.ApplyColor(slotName, color);
    }

    public void ApplyColorByPrefix(string prefix, Color color) {
      sprite.ApplyColorByPrefix(prefix, color);
    }

    public void ApplyColorBySuffix(string suffix, Color color) {
      sprite.ApplyColorBySuffix(suffix, color);
    }

    public void ApplyColorByFilter(Predicate<Slot> filter, Color color) {
      sprite.ApplyColorByFilter(filter, color);
    }

    /// <summary>
    /// Sets colour back to default.
    /// </summary>
    public void ClearColor(string slotName) {
      sprite.ClearColor(slotName);
    }

    public void ClearColorByPrefix(string prefix) {
      sprite.ClearColorByPrefix(prefix);
    }

    public void ClearColorBySuffix(string suffix) {
      sprite.ClearColorBySuffix(suffix);
    }

    public void ClearColorByFilter(Predicate<Slot> filter) {
      sprite.ClearColorByFilter(filter);
    }

    /// <summary>
    /// Applies skin to a slot, e.g. hat. If slot is already taken by another skin, the old skin will be removed.
    /// </summary>
    public void ApplySkin(string slotId, string skinId) {
      if (skinSlots.ContainsKey(slotId)) {
        RemoveSkin(skinSlots[slotId]);
        skinSlots.Remove(slotId);
      }

      skinSlots.Add(slotId, skinId);
      AddSkin(skinId);
    }

    /// <summary>
    /// Adds skin (if found) to list of skins and triggers skin reload on next update
    /// </summary>
    public void AddSkin(string skinId) {
      sprite.AddSkin(skinId);
    }

    /// <summary>
    /// Removes skin (if found) from list of skins and triggers skin reload on next update
    /// </summary>
    public void RemoveSkin(string skinId) {
      sprite.RemoveSkin(skinId);
    }

    /// <summary>
    /// Sets slot visibility
    /// </summary>
    public void SetSlotVisible(string slotName, bool visible) {
      sprite.SetSlotVisible(slotName, visible);
    }

    /// <summary>
    /// Sets visibility for many slots by their prefix
    /// </summary>
    public void SetSlotsVisibleByPrefix(string prefix, bool visible) {
      sprite.SetSlotsVisibleByPrefix(prefix, visible);
    }

    public void SetSlotsVisibleBySuffix(string suffix, bool visible) {
      sprite.SetSlotsVisibleBySuffix(suffix, visible);
    }

    public void SetSlotsVisibleByFilter(Predicate<Slot> filter, bool visible) {
      sprite.SetSlotsVisibleByFilter(filter, visible);
    }


    /// <summary>
    /// Plays spine animation
    /// </summary>
    /// <param name="trackNumber">number of track</param>
    /// <param name="animationName">name of spine animation</param>
    /// <param name="loop">should the animation be looped</param>
    /// <param name="forceRestart">if left as false, animation will not be restarted if it's already playing on the track</param>
    /// <param name="lockAnimationAfterEnd">if true, animation will be still considered as currently played animation after it ends, preventing it starting again if PlayAnimation is called constantly</param>
    public Optional<TrackEntry> PlayAnimation(int trackNumber, string animationName, bool loop = false,
      float mixIn = -1, float mixOut = -1, bool forceRestart = false, bool lockAnimationAfterEnd = false) {
      if (IsAnimationPlaying(trackNumber, animationName) && !forceRestart)
        return Optional<TrackEntry>.Empty();

      mixIn = mixIn < 0 ? DefaultAnimationMix : mixIn;
      mixOut = mixOut < 0 ? DefaultAnimationMix : mixOut;
      animationNames[trackNumber] = animationName;

      if (loop) {
        AnimationState.SetEmptyAnimation(trackNumber, mixIn);
        TrackEntry trackEntry = AnimationState.AddAnimation(trackNumber, animationName, true, 0f);
        trackEntry.MixDuration = mixIn;
        return Optional<TrackEntry>.Of(trackEntry);
      } else {
        AnimationState.SetEmptyAnimation(trackNumber, mixIn);
        TrackEntry trackEntry = AnimationState.AddAnimation(trackNumber, animationName, false, 0f);
        trackEntry.MixDuration = mixIn;
        AnimationState.AddEmptyAnimation(trackNumber, mixOut, trackEntry.Animation.Duration);
        if (!lockAnimationAfterEnd) {
          trackEntry.End += entry => {
            StopAnimation(entry.trackIndex);
          };
        }

        return Optional<TrackEntry>.Of(trackEntry);
      }
    }

    public void StopAnimation(int trackNumber, float mixDuration = 0.1f) {
      AnimationState.SetEmptyAnimation(trackNumber, mixDuration);
      animationNames.Remove(trackNumber);
    }

    /// <summary>
    /// Returns animation name played on track
    /// </summary>
    public Optional<string> GetAnimationName(int trackNumber) {
      if (!animationNames.ContainsKey(trackNumber))
        return Optional<string>.Empty();
      return Optional<string>.Of(animationNames[trackNumber]);
    }

    public bool IsAnimationPlaying(int trackNumber, string animationName) {
      return GetAnimationName(trackNumber).IfPresentGet(animName => animName.Equals(animationName))
        .OrElse(false);
    }

    public void ApplyState() {
      sprite.ApplyState(AnimationState);
    }

    public bool IsAnyAnimationPlaying(int trackNumber) {
      return GetAnimationName(trackNumber).IsPresent();
    }

    private AnimationState createAnimationState() {
      SpineAsset asset = ContentManager.Get<SpineAsset>(sprite.AssetId, true);
      AnimationStateData animationStateData = new AnimationStateData(asset.SkeletonData);
      return new AnimationState(animationStateData);
    }

    private void setSprite() {
      try {
        sprite = Entity.GetComponent<DrawComponent>().DrawTarget as SpineSprite;
      } catch (Exception e) {
        Log.log(LogCategory.DrawingSystems, LogLevel.Error,
          "Entity " + Entity.Id +
          " tried to add SpineComponent but it doesn't have a DrawComponent that it can relate to! Entity must have DrawComponent with DrawTarget of class SpineSprite! Exception message: " +
          e.Message);
      }
    }

    public override string ToString() {
      var str = "%{Aquamarine}% " + this.ShortIdentifier + ":\n"
                + "%{PaleGoldenrod}% Skin Slots: \n";

      foreach (var keyValue in skinSlots) {
        str += keyValue.Key + ": " + keyValue.Value + "\n";
      }

      return str;
    }
  }
}