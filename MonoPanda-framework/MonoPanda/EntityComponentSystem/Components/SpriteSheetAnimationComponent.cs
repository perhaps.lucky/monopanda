﻿using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.Logger;
using MonoPanda.Timers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.Components {
  public class SpriteSheetAnimationComponent : EntityComponent {
    public Dictionary<string, SpriteSheetAnimation> Animations { get; set; }
    public SpriteSheetAnimation CurrentAnimation { get; set; }
    /// <summary>
    /// Frame within current animation. For frame number in sheet, use CurrentAnimations.frames[CurrentFrame]
    /// </summary>
    public int CurrentFrame { get; set; }
    public RepeatingTimer FrameTimer { get; set; }
    public bool AnimationFinished { get; set; } = true;

    private SpriteSheetSprite relatedSpriteSheet;

    public override void Init(Dictionary<string, object> parameters) {
      Animations = ((JObject)parameters["Animations"]).ToObject<Dictionary<string, SpriteSheetAnimation>>();
      setRelatedSprite();

      if (parameters.ContainsKey("StartAnimation"))
        PlayAnimation((string)parameters["StartAnimation"]);
    }

    /// <summary>
    /// Plays animation specified by id
    /// </summary>
    public void PlayAnimation(string id) {
      if (!AnimationFinished)
        StopAnimation();

      CurrentAnimation = Animations[id];
      reverseIfNeeded(CurrentAnimation);
      AnimationFinished = false;
      FrameTimer = new RepeatingTimer(Animations[id].FrameDuration, affectedByWorldTime: true);
      FrameTimer.Initialize(true);
      CurrentFrame = 0;
      UpdateSpriteSheetSprite();
    }

    /// <summary>
    /// Stops currently played animation
    /// </summary>
    public void StopAnimation() {
      AnimationFinished = true;
      FrameTimer.Dispose();
      FrameTimer = null;
    }

    /// <summary>
    /// Updates sourceRectangle inside related SpriteSheetSprite, based on CurrentFrame and CurrentAnimation.
    /// </summary>
    public void UpdateSpriteSheetSprite() {
      if (relatedSpriteSheet != null)
        relatedSpriteSheet.UpdateSourceRectangle(CurrentAnimation.Frames[CurrentFrame]);
    }

    private void reverseIfNeeded(SpriteSheetAnimation animation) {
      if (CurrentAnimation.isReversed) {
        Array.Reverse(CurrentAnimation.Frames);
        CurrentAnimation.isReversed = false;
      }
    }

    private void setRelatedSprite() {
      try {
        relatedSpriteSheet = Entity.GetComponent<DrawComponent>().DrawTarget as SpriteSheetSprite;
      } catch (Exception e) {
        Log.log(LogCategory.DrawingSystems, LogLevel.Error, "Entity " + Entity.Id + " tried to add SpriteSheetAnimationComponent but it doesn't have a DrawComponent that it can relate to! Entity must have DrawComponent with DrawTarget of class SpriteSheetSprite first! Exception message: " + e.Message);
      }
    }

    public override string ToString() {
      var str = "%{LightYellow}% " + this.ShortIdentifier + ":\n";
      
      if(CurrentAnimation != null)
        str += "%{PaleGoldenrod}% Animation: %{White}% " + Animations.FirstOrDefault(x => x.Value == CurrentAnimation).Key +
        " [" + CurrentFrame + "/" + CurrentAnimation.Frames.Length + "]" + (CurrentAnimation.IsLooping ? " Looping " : "")
        + (CurrentAnimation.IsReversing ? " Reversing " : "") + " Frame duration: " + CurrentAnimation.FrameDuration;

      return str;
    }
  }
}
