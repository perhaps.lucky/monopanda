﻿using Microsoft.Xna.Framework;
using MonoPanda.ECS;
using MonoPanda.Flag;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;
using System.Collections.Generic;

namespace MonoPanda.Components {
  public class TextComponent : EntityComponent {
    [VectorParameter]
    public Vector2 PositionOffset { get; set; }
    public string Text { get; set; }
    public string Font { get; set; }
    public int FontSize { get; set; }
    [ColorParameter]
    public Color FontColor { get; set; }
    public float Depth { get; set; }

    public OneTimeFlag DrawComponentNotCreated { get; set; }

    public override void Init(Dictionary<string, object> parameters) {
      DrawComponentNotCreated = new OneTimeFlag();
    }

    public override string ToString() {
      return "%{Turquoise}% " + this.ShortIdentifier + ":\n"
        + "%{PaleGoldenrod}% Text: %{White}% " + Text;
    }
  }
}
