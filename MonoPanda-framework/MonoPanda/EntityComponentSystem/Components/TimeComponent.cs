﻿using MonoPanda.ECS;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Components {
  public class TimeComponent : EntityComponent {
    public float TimeMultiplier {
      get => timeMultiplier;
      set {
        float oldValue = timeMultiplier;
        timeMultiplier = value;
        if (timeChangeInformer != null)
          timeChangeInformer.Inform(oldValue, true);
      }
    }

    private float timeMultiplier = 1.0f;

    public Informer TimeChangeInformer {
      get {
        if (timeChangeInformer == null)
          timeChangeInformer = new Informer();
        return timeChangeInformer;
      }
    }

    private Informer timeChangeInformer;
  }
}