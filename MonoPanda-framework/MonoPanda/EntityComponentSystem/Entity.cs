﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS.Systems.Camera;
using MonoPanda.UtilityClasses;

using System;
using System.Collections.Generic;

namespace MonoPanda.ECS {
  public class Entity : ICameraFollowable {

    public string Id { get; private set; }
    public Vector2 Position { get => position; set { position = value; informObservers(); } }
    public float Scale { get => scale; set { scale = value; informObservers(); } }
    public float Rotation { get => rotation; set { rotation = value; informObservers(); } }
    public bool IsInVisibleArea { get; set; }
    public bool IsDestroyed { get; private set; }

    private Vector2 position;
    private float scale = 1f;
    private float rotation;
    
    public EntityComponentSystem EntityComponentSystem { get; private set; }
    private List<EntityComponent> components;

    private Informer informer;

    public Entity(string id, Vector2 position, EntityComponentSystem ecs) {
      informer = new Informer();
      Id = id;
      Position = position;
      EntityComponentSystem = ecs;
      components = new List<EntityComponent>();
    }

    public void Destroy() {
      foreach (EntityComponent component in components) {
        component.Destroy();
        EntityComponentSystem.ComponentRemoved(component);
      }
      EntityComponentSystem.RemoveEntity(this);
      IsDestroyed = true;
    }

    public void AddComponent(EntityComponent component) {
      components.Add(component);
      component.Entity = this;
      component.TypeIdentifier = component.GetType().FullName;
      if(component.ShortIdentifier == null)
        component.ShortIdentifier = component.GetType().Name;
      EntityComponentSystem.ComponentAdded(component);
    }

    public void RemoveComponent<T>() where T : EntityComponent {
      var component = GetComponent<T>();
      if (component != null) {
        RemoveComponent(component);
        EntityComponentSystem.ComponentRemoved(component);
      }
    }

    public void RemoveComponent(EntityComponent component) {
      component.Destroy();
      components.Remove(component);
    }

    public bool HasComponent<T>() where T : EntityComponent {
      return GetComponent<T>() != null;
    }

    public T GetComponent<T>() where T : EntityComponent {
      return components.Find(c => c is T) as T;
    }

    public EntityComponent GetComponentByIdentifier(string shortIdentifier) {
      return components.Find(c => c.ShortIdentifier.Equals(shortIdentifier));
    }

    public Optional<T> GetComponentOptional<T>() where T : EntityComponent {
      return Optional<T>.Of(components.Find(c => c is T) as T);
    }

    public List<T> GetAllComponents<T>() where T : EntityComponent {
      return components.FindAll(c => c is T).ConvertAll(c => c as T);
    }

    public List<EntityComponent> GetAllComponents() {
      return new List<EntityComponent>(components);
    }

    public T GetComponentById<T>(string id) where T : EntityComponent {
      return components.Find(c => c is T && c.ShortIdentifier.Equals(id)) as T;
    }

    public Observer RegisterObserver(Delegate action) {
      return informer.RegisterObserver(action);
    }

    public void UnregisterObserver(Observer observer) {
      informer.UnregisterObserver(observer);
    }
    
    public void PostInit() {
      foreach (EntityComponent component in components) {
        component.PostInit();
      }
    }

    private void informObservers() {
      informer.Inform();
    }
    
    public Vector2 GetPosition() {
      return Position;
    }

    public override string ToString() {
      var str = "%{Cyan}% " + Id + "\n"
        + "%{PaleGoldenrod}% Position: %{White}% " + Position.ToString() + "\n"
        + "%{PaleGoldenrod}% Scale: %{White}% " + Scale.ToString() + "\n"
        + "%{PaleGoldenrod}% Rotation: %{White}% " + Scale.ToString() + "\n"
        + "%{PaleGoldenrod}% Visible: " + (IsInVisibleArea ? "%{Lime}% Yes" : "%{OrangeRed}% No") + "\n";

      foreach (var component in components) {
        str += component.ToString() + "\n";
      }

      return str.Substring(0, str.Length-1);
    }
  }
}
