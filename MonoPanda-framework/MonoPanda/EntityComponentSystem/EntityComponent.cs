﻿using System.Collections.Generic;

namespace MonoPanda.ECS {
  public abstract class EntityComponent {

    public bool Active { get => active; set { active = value; if (value) SetActive(); else SetInactive(); } }
    public Entity Entity { get; set; }
    public string Id { get; set; }
    public string TypeIdentifier { get; set; }
    public string ShortIdentifier { get; set; }

    private bool active = true;

    /// <summary>
    /// This method should not be called outside Entity class.
    /// Use this to dispose object from component if necessary (or perform other actions).
    /// </summary>
    public virtual void Destroy() {

    }

    /// <summary>
    /// This method will be called after creation of object and should be used to map
    /// any parameter that cannot be converted by reflection.
    /// </summary>
    public virtual void Init(Dictionary<string, object> parameters) {

    }
    
    /// <summary>
    /// This method will be called after all components are created
    /// </summary>
    public virtual void PostInit() {
      
    }

    protected virtual void SetActive() {

    }

    protected virtual void SetInactive() {

    }

    public override string ToString() {
      return "%{Tomato}% " + this.ShortIdentifier;
    }
  }
}
