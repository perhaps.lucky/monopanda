﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS.Json;
using MonoPanda.Flag;
using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MonoPanda.ECS {
  public class EntityComponentSystem {

    public Camera Camera { get; private set; }

    private int anonymousId;

    private List<Entity> entities;
    private List<EntitySystem> systems;

    private InnerEntityUpdateSystem innerEntityUpdateSystem;

    private Dictionary<string, List<EntityComponent>> componentTypeToList;
    
    private OneTimeFlag firstUpdate;


    /// <summary>
    /// Creates system basing on modable json.
    /// </summary>
    /// <param name="jsonPath">Path inside the mod folder</param>
    /// <returns></returns>
    public static EntityComponentSystem LoadFromJson(string jsonPath) {
      List<EntitySystemJsonModel> models = new List<EntitySystemJsonModel>();
      ModsUtils.ForEachModFile(jsonPath, filePath => loadMod(filePath, models));

      EntityComponentSystem ecs = new EntityComponentSystem();
      foreach (var model in models) {
        ecs.AddSystem(createSystemFromModel(model));
      }
      return ecs;
    }

    public EntityComponentSystem() {
      entities = new List<Entity>();
      systems = new List<EntitySystem>();
      innerEntityUpdateSystem = new InnerEntityUpdateSystem(this);
      Camera = new Camera();
      componentTypeToList = new Dictionary<string, List<EntityComponent>>();
      firstUpdate = new OneTimeFlag();
    }

    /// <summary>
    /// Creates entity with specified id and position.
    /// </summary>
    public Entity CreateEntity(string id, Vector2 position) {
      Entity entity = new Entity(id, position, this);
      entities.Add(entity);
      entityCreated(entity);
      return entity;
    }

    /// <summary>
    /// Creates anonymous entity with id being a number from sequence.
    /// </summary>
    public Entity CreateEntity(Vector2 position) {
      return CreateEntity(getAnonymousId(), position);
    }

    /// <summary>
    /// Destroys entity by id. Returns true if entity was found and destroyed.
    /// </summary>
    public bool DestroyEntity(string id) {
      Entity entity = GetEntity(id);
      if (entity != null)
        return DestroyEntity(entity);
      return false;
    }

    /// <summary>
    /// Destroys entity and removes it from the system. Returns true or throws exception.
    /// </summary>
    public bool DestroyEntity(Entity entity) {
      entity.Destroy();
      return true;
    }

    /// <summary>
    /// Removes entity from the system. <b>Does not call destroy!</b>
    /// </summary>
    public void RemoveEntity(Entity entity) {
      entities.Remove(entity);
      entityDestroyed(entity);
    }

    /// <summary>
    /// Returns entity with specified id or null if entity isn't in the system.
    /// </summary>
    public Entity GetEntity(string id) {
      foreach (Entity entity in entities) {
        if (entity.Id.Equals(id))
          return entity;
      }
      return null;
    }

    public Optional<Entity> GetEntityOptional(string id) {
      return Optional<Entity>.Of(GetEntity(id));
    }

    public List<Entity> GetEntityByPrefix(string prefix) {
      return entities.FindAll(e => e.Id.StartsWith(prefix));
    }

    /// <summary>
    /// Adds entity system.
    /// </summary>
    public void AddSystem(EntitySystem entitySystem) {
      systems.Add(entitySystem);
      entitySystem.ECS = this;
    }

    /// <summary>
    /// Returns entity system of class T.
    /// </summary>
    public T GetSystem<T>() where T : EntitySystem {
      return systems.Find(s => s is T) as T;
    }

    public Optional<T> GetSystemOptional<T>() where T : EntitySystem {
      return Optional<T>.Of(GetSystem<T>());
    }

    public EntitySystem GetSystemByIdentifier(string shortIdentifier) {
      return systems.Find(s => s.ShortIdentifier.Equals(shortIdentifier));
    }

    public List<EntitySystem> GetAllSystems() {
      return systems;
    }

    /// <summary>
    /// Removes system of class T.
    /// </summary>
    public void RemoveSystem<T>() where T : EntitySystem {
      T entitySystem = GetSystem<T>();
      if (entitySystem != null) {
        entitySystem.Destroy();
        systems.Remove(entitySystem);
      }
    }

    public void Update() {
      if (firstUpdate.Check()) {
        onFirstUpdate();
      }
      innerEntityUpdateSystem.update();
      foreach (EntitySystem system in systems) {
        if (system.Active && checkSystemUpdateTimer(system))
          system.Update();
      }
    }

    public void Draw() {
      foreach (EntitySystem system in systems) {
        if (system.Active)
          system.Draw();
      }
    }

    /// <summary>
    /// Call destroy on every entity and system inside.
    /// </summary>
    public void Destroy() {
      foreach (EntitySystem system in systems) {
        system.Destroy();
      }
      systems.Clear();
      
      foreach (Entity entity in entities.ToList()) {
        entity.Destroy();
      }   
    }

    /// <summary>
    /// Return all entities in the system.
    /// </summary>
    public List<Entity> GetAllEntities() {
      return entities;
    }

    /// <summary>
    /// Returns all components of specified type.
    /// </summary>
    public List<T> GetAllComponents<T>() where T : EntityComponent {
      List<T> list = new List<T>();
      var componentType = typeof(T).FullName;
      ensureComponentTypeListExists(componentType);
      foreach (var component in componentTypeToList[componentType]) {
        if (component.Active)
          list.Add(component as T);
      }

      return list;
    }

    public void ComponentAdded(EntityComponent component) {
      addComponentToTypeList(component);

      foreach (var system in systems) {
        system.OnComponentAdd(component);
      }
    }

    public void ComponentRemoved(EntityComponent component) {
      removeComponentFromTypeList(component);

      foreach (var system in systems) {
        system.OnComponentRemove(component);
      }
    }

    private void entityCreated(Entity entity) {
      foreach (var system in systems) {
        system.OnEntityCreate(entity);
      }
    }

    private void entityDestroyed(Entity entity) {
      foreach (var system in systems) {
        system.OnEntityDestroy(entity);
      }
    }

    private string getAnonymousId() {
      return (anonymousId++).ToString();
    }

    private static void loadMod(string filePath, List<EntitySystemJsonModel> targetList) {
      List<EntitySystemJsonModel> modLoadedModels = ContentUtils.LoadJson<List<EntitySystemJsonModel>>(filePath);
      foreach (var modModel in modLoadedModels) {
        addOrOverrideModel(modModel, targetList);
      }
    }

    private static void addOrOverrideModel(EntitySystemJsonModel model, List<EntitySystemJsonModel> targetList) {
      var foundModel = targetList.Find(m => m.ClassName.Equals(model.ClassName));
      if (foundModel != null) {
        combineSystemModels(foundModel, model);
      } else {
        targetList.Add(model);
      }
    }

    private static void combineSystemModels(EntitySystemJsonModel baseModel, EntitySystemJsonModel newModel) {
      if (newModel.UpdateInterval > 0)
        baseModel.UpdateInterval = newModel.UpdateInterval;
      foreach (KeyValuePair<string, object> keyValue in newModel.Parameters) {
        baseModel.Parameters[keyValue.Key] = keyValue.Value;
      }

    }

    private static EntitySystem createSystemFromModel(EntitySystemJsonModel model) {
      Type type = Type.GetType(model.ClassName);
      EntitySystem system = (EntitySystem)Activator.CreateInstance(type, new object[] { model.ClassName, model.UpdateInterval });

      if (model.Parameters != null) {
        applyParametersFromModel(model, system, type);
        system.Initialize(model.Parameters);
      }

      system.ShortIdentifier = type.Name;
      return system;
    }

    private bool checkSystemUpdateTimer(EntitySystem system) {
      return system.UpdateTimer == null || system.UpdateTimer.Check(!system.TimerLogs);
    }

    private static void applyParametersFromModel(EntitySystemJsonModel model, EntitySystem system, Type type) {
      if (model.Parameters != null) {
        foreach (KeyValuePair<string, object> parameter in model.Parameters) {
          try {
            PropertyInfo propertyInfo = type.GetProperty(parameter.Key);
            if (propertyInfo != null) {
              ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
              if (attribute != null) {
                propertyInfo.SetValue(system, attribute.Convert(parameter.Value.ToString()));
                continue;
              }
            }
            propertyInfo.SetValue(system, Convert.ChangeType(parameter.Value, propertyInfo.PropertyType));
          } catch (InvalidCastException) {
            // This happens when we need some specific conversion
            // For example, in DrawSystem, strings from json get converted into values of static fields of each class
            // system.ApplyParameters is a method that should be used for cases like above.
            Log.log(LogCategory.ECS, LogLevel.Debug, "Property: " + parameter.Key + " failed the cast in applyParametersFromModel. ApplyParameters method inside system should be used.");
          } catch (NullReferenceException) {
            Log.log(LogCategory.ECS, LogLevel.Debug, "Tried to load a missing property: " + parameter.Key + " from json.");
          }
        }
      }
    }

    private void addComponentToTypeList(EntityComponent component) {
      ensureComponentTypeListExists(component.TypeIdentifier);
      componentTypeToList[component.TypeIdentifier].Add(component);
    }

    private void removeComponentFromTypeList(EntityComponent component) {
      componentTypeToList[component.TypeIdentifier].Remove(component);
    }

    private void ensureComponentTypeListExists(string componentType) {
      if (!componentTypeToList.ContainsKey(componentType))
        componentTypeToList.Add(componentType, new List<EntityComponent>());
    }
    
    private void onFirstUpdate() {
      foreach (EntitySystem system in systems) {
        if (system.Active)
          system.OnFirstUpdate();
      }
    }

    public override string ToString() {
      var str = "EntityComponentSystem: \n"
        + "Entities: " + entities.Count + "\n"
        + "Systems: " + systems.Count + "\n"
        + "Components per type: \n";

      var total = 0;
      foreach (var keyValue in componentTypeToList) {
        str += keyValue.Key + ": " + keyValue.Value.Count + "\n";
        total += keyValue.Value.Count;
      }
      str += "Total components: " + total;
      return str;
    }
  }
}
