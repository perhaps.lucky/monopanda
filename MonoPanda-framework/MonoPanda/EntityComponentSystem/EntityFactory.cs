﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS.Json;
using MonoPanda.Logger;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace MonoPanda.ECS {
  public abstract class EntityFactory {
    private const string CLASS_NAME_PARAMETER_NAME = "ClassName";

    public EntityFactoryModel Default { get; set; }
    public List<EntityFactoryModel> Models { get; set; }
    public EntityComponentSystem ECS { get; private set; }

    public static T CreateInstance<T>(string jsonPath, EntityComponentSystem ECS) where T : EntityFactory {
      EntityFactory finalFactory = (T)Activator.CreateInstance(typeof(T));
      finalFactory.Models = new List<EntityFactoryModel>();

      ModsUtils.ForEachModFile(jsonPath, filePath => loadMod(filePath, finalFactory));

      finalFactory.Models = createCombinedModels(finalFactory);

      finalFactory.ECS = ECS;
      finalFactory.SetAsInstance();
      return finalFactory as T;
    }


    private static void loadMod(string filePath, EntityFactory targetFactory) {
      EntityFactory modFactory = ContentUtils.LoadJson<ModFactory>(filePath);
      applyFactory(modFactory, targetFactory);
    }

    private static void applyFactory(EntityFactory sourceFactory, EntityFactory targetFactory) {
      if (sourceFactory.Default != null) {
        if (targetFactory.Default != null)
          targetFactory.Default = createCombinedModel(targetFactory.Default, sourceFactory.Default);
        else
          targetFactory.Default = sourceFactory.Default;
      }

      if (sourceFactory.Models != null)
        foreach (EntityFactoryModel model in sourceFactory.Models) {
          var oldModel = targetFactory.Models.Find(m => m.Id.Equals(model.Id));
          if (oldModel != null) {
            targetFactory.Models.Remove(oldModel);
            targetFactory.Models.Add(createCombinedModel(oldModel, model));
          } else {
            targetFactory.Models.Add(model);
          }
        }
    }

    private static List<EntityFactoryModel> createCombinedModels(EntityFactory sourceFactory) {
      if (sourceFactory.Default == null)
        return sourceFactory.Models;

      List<EntityFactoryModel> resultList = new List<EntityFactoryModel>();
      foreach (var model in sourceFactory.Models) {
        resultList.Add(createCombinedModel(sourceFactory.Default, model));
      }

      return resultList;
    }

    private static EntityFactoryModel createCombinedModel(EntityFactoryModel defaultModel,
      EntityFactoryModel processedModel) {
      EntityFactoryModel combinedModel = new EntityFactoryModel();
      appendModel(defaultModel, combinedModel);
      appendModel(processedModel, combinedModel);
      combinedModel.Id = processedModel.Id;
      return combinedModel;
    }

    private static void appendModel(EntityFactoryModel sourceModel, EntityFactoryModel targetModel) {
      foreach (KeyValuePair<string, Dictionary<string, object>> sourceModelComponent in sourceModel.Components) {
        if (targetModel.Components.ContainsKey(sourceModelComponent.Key))
          appendParameters(sourceModelComponent.Value, targetModel, sourceModelComponent.Key);
        else {
          targetModel.Components.Add(sourceModelComponent.Key, new Dictionary<string, object>());
          appendParameters(sourceModelComponent.Value, targetModel, sourceModelComponent.Key);
        }
      }
    }

    private static void appendParameters(Dictionary<string, object> sourceModelComponent,
      EntityFactoryModel targetModel, string componentClass) {
      foreach (KeyValuePair<string, object> parameter in sourceModelComponent) {
        targetModel.Components[componentClass][parameter.Key] = parameter.Value;
      }
    }

    public Entity CreateEntityInner(string modelId, Vector2 position, string id = null) {
      Entity entity = id != null ? ECS.CreateEntity(id, position) : ECS.CreateEntity(position);
      EntityFactoryModel model = Models.Find(m => modelId.Equals(m.Id));

      if (model == null)
        return entity;

      foreach (KeyValuePair<string, Dictionary<string, object>> keyValue in model.Components) {
        createComponentFromModel(keyValue.Key, keyValue.Value).IfPresent(component => {
          entity.AddComponent(component);
          component.Init(keyValue.Value);
        });
      }

      entity.PostInit();
      return entity;
    }

    private Optional<EntityComponent> createComponentFromModel(string componentId,
      Dictionary<string, object> parameters) {
      string typeName = parameters.ContainsKey(CLASS_NAME_PARAMETER_NAME)
        ? (string)parameters[CLASS_NAME_PARAMETER_NAME]
        : componentId;
      Type type = Type.GetType(typeName);
      EntityComponent component = (EntityComponent)Activator.CreateInstance(type);

      if (parameters.ContainsKey(CLASS_NAME_PARAMETER_NAME)) {
        // if class name is present as parameter, use componentId as short identifier
        component.ShortIdentifier = componentId;
      }
      
      applyParametersToComponent(component, parameters, type);
      return Optional<EntityComponent>.Of(component);
    }

    private void applyParametersToComponent(EntityComponent component, Dictionary<string, object> parameters,
      Type type) {
      if (parameters != null) {
        foreach (KeyValuePair<string, object> parameter in parameters) {
          // I removed try-catch and instead ensured the conversion can happen.
          // If there is a cast exception anyway, this code needs more trashy updates.
          // try-catching is slow and in this case it makes a lot of difference
          PropertyInfo propertyInfo = type.GetProperty(parameter.Key);
          if (propertyInfo != null) {
            ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
            if (attribute != null) {
              propertyInfo.SetValue(component, attribute.Convert(parameter.Value.ToString()));
              continue;
            }
          }

          if (propertyInfo == null ||
              (!propertyInfo.PropertyType.IsPrimitive && propertyInfo.PropertyType != typeof(string)))
            continue;
          propertyInfo.SetValue(component, Convert.ChangeType(parameter.Value, propertyInfo.PropertyType));
        }
      }
    }

    protected abstract void SetAsInstance();

    /// <summary>
    /// This class is used to have object that will store mod informations in process of loading the factory.
    /// </summary>
    private class ModFactory : EntityFactory {
      protected override void SetAsInstance() {
        throw new InvalidOperationException();
      }
    }
  }
}