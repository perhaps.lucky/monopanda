﻿using MonoPanda.Timers;

using System.Collections.Generic;

namespace MonoPanda.ECS {
  public abstract class EntitySystem {
    public bool Active { get; set; }
    public bool TimerLogs { get; set; }

    public EntityComponentSystem ECS { get; set; }
    public RepeatingTimer UpdateTimer { get; private set; }
    public string ShortIdentifier { get; set; }

    public EntitySystem(string id, int updateInterval = -1) {
      Active = true;
      if (updateInterval > 0) {
        UpdateTimer = new RepeatingTimer(updateInterval, id);
        UpdateTimer.Initialize(!TimerLogs);
      }
    }

    public virtual void Update() {
    }

    public virtual void Draw() {
    }

    public virtual void OnComponentAdd(EntityComponent component) {
    }

    public virtual void OnComponentRemove(EntityComponent component) {
    }

    public virtual void OnEntityCreate(Entity entity) {
    }

    public virtual void OnEntityDestroy(Entity entity) {
    }

    public virtual void OnFirstUpdate() {
    }
    
    public virtual void Destroy() {
      ECS = null;
    }

    /// <summary>
    /// This method gets called when system is created from json.
    /// Method should be used to apply parameters that can't be directly converted, e.g.
    /// In DrawSystem, strings from json are converted to values from static fields.
    /// </summary>
    /// <param name="parameters"></param>
    public virtual void Initialize(Dictionary<string, object> parameters) {
    }
  }
}