﻿using System.Collections.Generic;

namespace MonoPanda.ECS.Json {
  public class EntityFactoryModel {
    public string Id { get; set; }
    public Dictionary<string, Dictionary<string, object>> Components { get; set; } = new Dictionary<string, Dictionary<string, object>>();
  }
}
