﻿using System.Collections.Generic;

namespace MonoPanda.ECS.Json {
  public class EntitySystemJsonModel {
    public string ClassName { get; set; }
    public int UpdateInterval { get; set; }
    public Dictionary<string, object> Parameters { get; set; }
  }
}
