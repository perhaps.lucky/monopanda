﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.ECS.Systems.Camera;
using MonoPanda.GlobalTime;
using MonoPanda.Utils;

namespace MonoPanda.Systems {
  public class CameraMovementSystem : EntitySystem {
    public float CameraSpeedMultiplier { get; set; } = 4f;

    private Optional<ICameraFollowable> followedObject;


    public CameraMovementSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public void Follow(ICameraFollowable fObject) {
      followedObject = Optional<ICameraFollowable>.Of(fObject);
    }

    public void StopFollowing() {
      followedObject = Optional<ICameraFollowable>.Empty();
    }

    public override void Update() {
      ECS.Camera.Position = calculateNewPosition();
    }

    private Vector2 calculateNewPosition() {
      var targetPosition = calculateTargetPosition();
      var currentPosition = ECS.Camera.Position;
      var distance = Vector2.Distance(currentPosition, targetPosition);

      if (distance == 0f) {
        return targetPosition;
      }

      var angle = VectorUtils.GetDirectionVector(currentPosition, targetPosition);
      return currentPosition + angle * (distance * Time.ElapsedCalculated * CameraSpeedMultiplier);
    }

    private Vector2 calculateTargetPosition() {
      return followedObject.IfPresentGet(ICameraFollowable.GetPosition).OrElse(ECS.Camera.Position);
    }
  }
}