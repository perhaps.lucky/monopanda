﻿using Microsoft.Xna.Framework;

namespace MonoPanda.ECS.Systems.Camera {
  public class FollowedPosition : ICameraFollowable {
    private Vector2 position;

    public FollowedPosition(Vector2 position) {
      this.position = position;
    }

    public Vector2 GetPosition() {
      return position;
    }
  }
}