﻿using Microsoft.Xna.Framework;

namespace MonoPanda.ECS.Systems.Camera {
  public interface ICameraFollowable {
    Vector2 GetPosition();

    public static Vector2 GetPosition(ICameraFollowable iCameraFollowable) {
      return iCameraFollowable.GetPosition();
    }
  }
}