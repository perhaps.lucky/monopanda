﻿using MonoPanda.ECS;
using MonoPanda.Extensions;
using MonoPanda.GlobalTime;
using MonoPanda.Logger;
using MonoPanda.ParameterConvert;

using System;
using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class CameraZoomSystem : EntitySystem {
    private const int NO_STEP_SET = -1;

    public float Zoom { get; set; } = 1f;
    public float MinZoomDifference { get; set; } = 0.001f;
    [JArrayParameter(typeof(List<float>))] public List<float> ZoomSteps { get; set; } = new List<float>();
    public int Step { get; set; } = NO_STEP_SET;

    private int maxStep = NO_STEP_SET;

    public CameraZoomSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Initialize(Dictionary<string, object> parameters) {
      if (ZoomSteps.IsNotEmpty()) {
        maxStep = ZoomSteps.Count - 1;
        if (Step == NO_STEP_SET) {
          Step = ZoomSteps.Count / 2;
        }
      }
    }

    public override void OnFirstUpdate() {
      if (ZoomSteps.IsNotEmpty()) {
        ECS.Camera.Zoom = Zoom = ZoomSteps[Step];
      }
    }

    public void NextZoomStep() {
      if (ZoomSteps.IsEmpty()) {
        logMissingConfiguration();
        return;
      }

      Step = Math.Min(++Step, maxStep);
      Zoom = ZoomSteps[Step];
    }

    public void PreviousZoomStep() {
      if (ZoomSteps.IsEmpty()) {
        logMissingConfiguration();
        return;
      }

      Step = Math.Max(--Step, 0);
      Zoom = ZoomSteps[Step];
    }

    public override void Update() {
      var currentZoom = ECS.Camera.Zoom;

      if (currentZoom != Zoom) {
        updateZoom(currentZoom);
      }
    }

    private void updateZoom(float currentZoom) {
      var difference = Zoom - currentZoom;
      if (Math.Abs(difference) < MinZoomDifference) {
        ECS.Camera.Zoom = Zoom;
      } else {
        ECS.Camera.Zoom += difference * Time.ElapsedCalculated;
      }
    }

    private void logMissingConfiguration() {
      Log.log(LogCategory.CameraSystem, LogLevel.Warn,
        $"Missing steps configuration, operations `NextZoomStep` and `PreviousZoomStep` are not available.");
    }
  }
}