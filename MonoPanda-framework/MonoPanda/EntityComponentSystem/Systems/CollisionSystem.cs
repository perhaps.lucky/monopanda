using Humper;
using Humper.Responses;

using Microsoft.Xna.Framework;

using MonoPanda.BitmapFonts;
using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.ECS;
using MonoPanda.HumperPanda;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;
using System.Net;

namespace MonoPanda.Systems {
  public class CollisionSystem : EntitySystem {
    private World world; // humper

    // offset point where humper world starts (every object will add this offset to calculations)
    public Vector2 WorldOffset { get; private set; }

    // size of the humper world
    public Vector2 WorldSize { get; private set; }

    public float CellSize { get; private set; }

    public CollisionSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Initialize(Dictionary<string, object> parameters) {
      var worldBounds = ParameterUtils.GetRectangleFromParameters(parameters, "WorldBounds");
      WorldOffset = new Vector2(worldBounds.X, worldBounds.Y);
      WorldSize = new Vector2(worldBounds.Width, worldBounds.Height);

      CellSize = ParameterUtils.GetFromParameters(parameters, "CellSize", 64F);

      world = new World(worldBounds.Width, worldBounds.Height, CellSize);
    }

    public override void Update() {
      foreach (var collisionComponent in ECS.GetAllComponents<CollisionComponent>()) {
        updateOrCreateBox(collisionComponent);
      }
    }

    public override void Draw() {
      if (Config.Debug.CollisionRectangles || Config.Debug.CollisionSystemWorld)
        requestDrawDebug();
    }

    public void RecreateWorld(Vector2 offset, Vector2 size, float cellSize) {
      this.WorldOffset = offset;
      this.WorldSize = size;
      this.CellSize = cellSize;
      world = new World(size.X, size.Y, cellSize);
      restartComponents();
    }

    public void RecreateWorld(Vector2 offset, Vector2 size) {
      RecreateWorld(offset, size, this.CellSize);
    }

    public void RecreateWorld(Vector2 offset) {
      RecreateWorld(offset, this.WorldSize);
    }

    public bool RemoveBox(IBox box) {
      return world.Remove(box);
    }

    private void restartComponents() {
      foreach (var collisionComponent in ECS.GetAllComponents<CollisionComponent>())
        collisionComponent.HumperBox = null;
    }

    private void updateOrCreateBox(CollisionComponent component) {
      if (component.HumperBox == null) {
        component.HumperBox = createBoxFromCollisionComponent(component);
      }

      if (component.MoveComponentOptional.IsPresent() && positionChanged(component)) {
        component.HumperBox.Move(countNewPosition(component), handleCollision);
        updateEntityPosition(component);
      }
    }

    private bool positionChanged(CollisionComponent component) {
      return component.MoveComponentOptional.Get().LastPosition != component.Entity.Position;
    }

    private ICollisionResponse handleCollision(ICollision collision) {
      if (collidedWithSolid(collision)) {
        return new SlideResponse(collision);
      }

      return new CrossResponse(collision);
    }

    private bool collidedWithSolid(ICollision collision) {
      return ((CollisionComponent)collision.Other.Data).SolidComponentOptional.IfPresentGet(solid => solid.Active).OrElse(false);
    }

    private void updateEntityPosition(CollisionComponent component) {
      component.Entity.Position = new Vector2(
          component.HumperBox.X - component.BoundingRectangle.X * component.Entity.Scale + WorldOffset.X,
          component.HumperBox.Y - component.BoundingRectangle.Y * component.Entity.Scale + WorldOffset.Y
        );
    }
    
    private IBox createBoxFromCollisionComponent(CollisionComponent component) {
      IBox box = world.Create(
        component.Entity.Position.X + component.BoundingRectangle.X * component.Entity.Scale - WorldOffset.X,
        component.Entity.Position.Y + component.BoundingRectangle.Y * component.Entity.Scale - WorldOffset.Y,
        component.BoundingRectangle.Width * component.Entity.Scale,
        component.BoundingRectangle.Height * component.Entity.Scale);
      box.Data = component;
      return box;
    }

    private Vector2 countNewPosition(CollisionComponent component) {
      return new Vector2(
        component.Entity.Position.X + component.BoundingRectangle.X * component.Entity.Scale - WorldOffset.X,
        component.Entity.Position.Y + component.BoundingRectangle.Y * component.Entity.Scale - WorldOffset.Y
      );
    }

    private void requestDrawDebug() {
      var bounds = world.Bounds;
      world.DrawDebug((int)bounds.X, (int)bounds.Y, (int)bounds.Width, (int)bounds.Height, drawCell,
        drawBox,
        drawString);
    }

    private void drawCell(int x, int y, int w, int h, float alpha) {
      if (Config.Debug.CollisionSystemWorld) {
        DrawUtils.DrawRectangle(ECS,
          new Rectangle((int)(x + WorldOffset.X), (int)(y + WorldOffset.Y), w, h),
          new Color(241, 232, 188, ColorUtils.FloatToByte(alpha) / 4));
      }
    }

    private void drawBox(IBox box) {
      if (Config.Debug.CollisionRectangles) {
        DrawUtils.DrawRectangle(ECS,
          new Rectangle((int)(box.X + WorldOffset.X), (int)(box.Y + WorldOffset.Y), (int)box.Width,
            (int)box.Height),
          new Color(147, 250, 255, 75));
      }
    }

    private void drawString(string message, int x, int y, float alpha) {
      if (Config.Debug.CollisionSystemWorld) {
        FontRenderer.DrawText(
          Config.Console.ConsoleFont,
          ECS.Camera.WorldToScreen(new Vector2(x + WorldOffset.X, y + WorldOffset.Y)),
          message,
          Color.Cyan,
          12,
          false,
          -1,
          0.5f,
          GameMain.SpriteBatch);
      }
    }
  }

  public static class HumperExtensions {
    public static IMovement Move(this IBox iBox, Vector2 move,
      Func<ICollision, ICollisionResponse> filter) {
      return iBox.Move(move.X, move.Y, filter);
    }
  }
}