﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Tools;
using MonoPanda.Logger;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class DrawSystem : EntitySystem {
    private SpriteBatch spriteBatch;

    [StaticFieldParameter(typeof(Effect))] public Effect Effect { get; set; }

    [StaticFieldParameter(typeof(RasterizerState))]
    public RasterizerState RasterizerState { get; set; }

    [StaticFieldParameter(typeof(DepthStencilState))]
    public DepthStencilState DepthStencilState { get; set; }

    [StaticFieldParameter(typeof(SamplerState))]
    public SamplerState SamplerState { get; set; }

    [StaticFieldParameter(typeof(BlendState))]
    public BlendState BlendState { get; set; }

    [EnumParameter(typeof(SpriteSortMode))]
    public SpriteSortMode SortMode { get; set; } = SpriteSortMode.Deferred;

    public List<DrawRectangle> Rectangles { get; set; }

    public DrawSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      spriteBatch = new SpriteBatch(GameMain.GraphicsDeviceManager.GraphicsDevice);
      Rectangles = new List<DrawRectangle>();
    }

    public override void Draw() {
      Matrix transformMatrix = ECS.Camera.TransformationMatrix;
      Rectangle visibleArea = ECS.Camera.GetVisibleArea();
      beginLightSystemDraw();
      spriteBatch.Begin(SortMode, BlendState, SamplerState, DepthStencilState, RasterizerState,
        Effect, transformMatrix);
      drawComponents(visibleArea);
      drawRectangles(visibleArea);
      spriteBatch.End();
      clearRectangles();
    }

    private void drawComponents(Rectangle visibleArea) {
      Log.log(LogCategory.DrawingSystems, LogLevel.Debug,
        "Visible area: " + visibleArea.ToString());

      foreach (DrawComponent component in ECS.GetAllComponents<DrawComponent>()) {
        if (component.Entity.IsInVisibleArea) {
          Log.log(LogCategory.DrawingSystems, LogLevel.Debug,
            "Entity: " + component.Entity.Id + " qualifies for draw.");
          component.Draw(spriteBatch);
        }
      }
    }

    private void drawRectangles(Rectangle visibleArea) {
      foreach (DrawRectangle rectangle in Rectangles) {
        if (visibleArea.Intersects(rectangle.Rectangle.ToRectangle())) {
          rectangle.Draw(spriteBatch);
        }
      }
    }

    private void clearRectangles() {
      foreach (DrawRectangle rectangle in Rectangles)
        rectangle.Dispose();
      Rectangles.Clear();
    }

    private T reflectOrReturnNull<T>(Dictionary<string, object> parameters, string key) {
      if (!parameters.ContainsKey(key))
        return default;

      return ReflectionUtils.GetStaticField<T>(typeof(T), (string)parameters[key]);
    }

    private void beginLightSystemDraw() {
      LightSystem lightSystem = ECS.GetSystem<LightSystem>();
      if (lightSystem != null)
        lightSystem.BeginDraw();
    }

    public override string ToString() {
      return "%{PowderBlue}% DrawSystem: \n"
             + "%{PaleGoldenrod}% Effect: %{White}% " + (Effect != null ? Effect.Name : "") + "\n"
             + "%{PaleGoldenrod}% RasterizerState: %{White}% " +
             (RasterizerState != null ? RasterizerState.Name : "") + "\n"
             + "%{PaleGoldenrod}% DepthStencilState: %{White}% " +
             (DepthStencilState != null ? DepthStencilState.Name : "") + "\n"
             + "%{PaleGoldenrod}% SamplerState: %{White}% " +
             (SamplerState != null ? SamplerState.Name : "") + "\n"
             + "%{PaleGoldenrod}% BlendState: %{White}% " +
             (BlendState != null ? BlendState.Name : "") + "\n"
             + "%{PaleGoldenrod}% SortMode: %{White}% " + SortMode.ToString();
    }
  }
}