﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using MonoPanda.ECS.Tools;
using MonoPanda.Logger;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using Spine;

using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class DrawSystemSpine : EntitySystem {
    [StaticFieldParameter(typeof(Effect))] public Effect Effect { get; set; }

    [StaticFieldParameter(typeof(RasterizerState))]
    public RasterizerState RasterizerState { get; set; }

    [StaticFieldParameter(typeof(DepthStencilState))]
    public DepthStencilState DepthStencilState { get; set; }

    [StaticFieldParameter(typeof(SamplerState))]
    public SamplerState SamplerState { get; set; }

    [StaticFieldParameter(typeof(BlendState))]
    public BlendState BlendState { get; set; }

    private SpriteSortMode sortMode = SpriteSortMode.Deferred;

    public List<DrawRectangle> Rectangles { get; set; }

    private SpriteBatch spriteBatch;
    private SkeletonRenderer skeletonRenderer;
    private BasicEffect skeletonRendererEffect;

    private bool spriteBatchWorking;
    private bool skeletonRendererWorking;

    private List<DrawComponent> drawComponentsSorted;

    public DrawSystemSpine(string id, int updateInterval = -1) : base(id, updateInterval) {
      spriteBatch = new SpriteBatch(GameMain.GraphicsDeviceManager.GraphicsDevice);
      skeletonRenderer = new SkeletonRenderer(GameMain.GraphicsDeviceManager.GraphicsDevice);
      skeletonRenderer.PremultipliedAlpha = false;
      skeletonRendererEffect = skeletonRenderer.Effect as BasicEffect;
      skeletonRendererEffect.Projection = Matrix.CreateOrthographicOffCenter(0,
        GameMain.GraphicsDeviceManager.GraphicsDevice.Viewport.Width,
        GameMain.GraphicsDeviceManager.GraphicsDevice.Viewport.Height, 0, 0, 1);
      Rectangles = new List<DrawRectangle>();
    }

    public void ReinitializeProjection() {
      skeletonRendererEffect.Projection = Matrix.CreateOrthographicOffCenter(0,
        GameMain.GraphicsDeviceManager.GraphicsDevice.Viewport.Width,
        GameMain.GraphicsDeviceManager.GraphicsDevice.Viewport.Height, 0, 0, 1);
    }

    public override void Update() {
      skeletonRendererEffect.View = ECS.Camera.TransformationMatrix;
      drawComponentsSorted = new List<DrawComponent>();
      foreach (DrawComponent component in ECS.GetAllComponents<DrawComponent>()) {
        if (component.Entity.IsInVisibleArea) {
          Log.log(LogCategory.DrawingSystems, LogLevel.Debug,
            $"Entity: {component.Entity.Id} qualifies for draw.");
          drawComponentsSorted.Add(component);
          if (component.DrawTarget is SpineSprite) {
            (component.DrawTarget as SpineSprite).Update(component);
          }
        }

        if (component.DrawTarget is SpineSprite) {
          (component.DrawTarget as SpineSprite).CheckIfContentUnloaded();
        }
      }

      drawComponentsSorted.Sort((com1, com2) => com1.Depth <= com2.Depth ? 1 : -1);
    }

    public override void Draw() {
      Rectangle visibleArea = ECS.Camera.GetVisibleArea();

      beginLightSystemDraw();
      drawComponents(visibleArea);
      drawRectangles(visibleArea);
      end();
      clearRectangles();
    }

    private void drawComponents(Rectangle visibleArea) {
      Log.log(LogCategory.DrawingSystems, LogLevel.Debug, $"Visible area: {visibleArea}");

      if (drawComponentsSorted != null)
        foreach (DrawComponent component in drawComponentsSorted) {
          drawComponent(component);
        }
    }

    private void drawComponent(DrawComponent component) {
      if (component.DrawTarget is SpineSprite)
        drawSpineComponent(component);
      else {
        enableSpriteBatch();
        component.Draw(spriteBatch);
      }
    }

    private void drawSpineComponent(DrawComponent component) {
      enableSkeletonRenderer();
      if (component.IsVisible)
        (component.DrawTarget as SpineSprite).DrawSpine(skeletonRenderer);
    }

    private void enableSpriteBatch() {
      if (spriteBatchWorking)
        return;

      if (skeletonRendererWorking) {
        skeletonRenderer.End();
        skeletonRendererWorking = false;
      }

      spriteBatch.Begin(sortMode, BlendState, SamplerState, DepthStencilState, RasterizerState, Effect,
        ECS.Camera.TransformationMatrix);
      spriteBatchWorking = true;
    }

    private void enableSkeletonRenderer() {
      if (skeletonRendererWorking)
        return;

      if (spriteBatchWorking) {
        spriteBatch.End();
        spriteBatchWorking = false;
      }

      skeletonRenderer.Begin();
      skeletonRendererWorking = true;
    }

    private void end() {
      if (spriteBatchWorking) {
        spriteBatch.End();
        spriteBatchWorking = false;
      }

      if (skeletonRendererWorking) {
        skeletonRenderer.End();
        skeletonRendererWorking = false;
      }
    }

    private void drawRectangles(Rectangle visibleArea) {
      enableSpriteBatch();
      foreach (DrawRectangle rectangle in Rectangles) {
        if (visibleArea.Intersects(rectangle.Rectangle.ToRectangle()))
          rectangle.Draw(spriteBatch);
      }
    }

    private void clearRectangles() {
      foreach (DrawRectangle rectangle in Rectangles)
        rectangle.Dispose();
      Rectangles.Clear();
    }

    private T reflectOrReturnNull<T>(Dictionary<string, object> parameters, string key) {
      if (!parameters.ContainsKey(key))
        return default;

      return ReflectionUtils.GetStaticField<T>(typeof(T), (string)parameters[key]);
    }

    private void beginLightSystemDraw() {
      LightSystem lightSystem = ECS.GetSystem<LightSystem>();
      if (lightSystem != null)
        lightSystem.BeginDraw();
    }

    public override string ToString() {
      return "%{PowderBlue}% DrawSystemSpine: \n"
             + "%{PaleGoldenrod}% Effect: %{White}% " + (Effect != null ? Effect.Name : "") + "\n"
             + "%{PaleGoldenrod}% RasterizerState: %{White}% " + (RasterizerState != null ? RasterizerState.Name : "") +
             "\n"
             + "%{PaleGoldenrod}% DepthStencilState: %{White}% " +
             (DepthStencilState != null ? DepthStencilState.Name : "") + "\n"
             + "%{PaleGoldenrod}% SamplerState: %{White}% " + (SamplerState != null ? SamplerState.Name : "") + "\n"
             + "%{PaleGoldenrod}% BlendState: %{White}% " + (BlendState != null ? BlendState.Name : "");
    }
  }
}