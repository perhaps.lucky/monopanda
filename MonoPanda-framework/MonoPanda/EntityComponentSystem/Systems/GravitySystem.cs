﻿using Microsoft.Xna.Framework;

using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class GravitySystem : EntitySystem {
    public bool GlobalGravityActive { get; set; } = true;
    public float GlobalGravityPower { get; set; } = 100.0f;
    public bool AffectSourcePowerByDistance { get; set; } = true;
    public float MaxPowerDistanceMultiplier { get; set; } = 0.4f;

    [DirectionVectorParameter] public Vector2 GlobalGravityDirection { get; set; } = new Vector2(0, 1);
    private Tick tick;

    public GravitySystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      tick = new Tick();
    }

    public override void Update() {
      double elapsedTime = tick.Get();
      List<GravityFieldComponent> gravityFieldComponents = ECS.GetAllComponents<GravityFieldComponent>();

      foreach (GravityComponent gravityComponent in ECS.GetAllComponents<GravityComponent>()) {
        Optional<MoveComponent> moveComponentOptional = gravityComponent.MoveComponentOptional;
        if (!moveComponentOptional.IsPresent())
          continue;

        if (GlobalGravityActive)
          affectByGlobalGravity(moveComponentOptional.Get(), gravityComponent, elapsedTime);

        affectByGravityFields(moveComponentOptional.Get(), gravityComponent, elapsedTime, gravityFieldComponents);
      }
    }

    public void SetGlobalGravityDirection(string direction) {
      GlobalGravityDirection = ParameterUtils.DirectionStringToVector(direction);
    }

    private void affectByGlobalGravity(MoveComponent moveComponent, GravityComponent gravityComponent,
      double elapsedTime) {
      moveComponent.Velocity +=
        GlobalGravityDirection
        * GlobalGravityPower
        * gravityComponent.GravityMultiplier
        * (float)elapsedTime
        * Time.EntityTimeSpeed(moveComponent);
    }

    private void affectByGravityFields(MoveComponent moveComponent, GravityComponent gravityComponent,
      double elapsedTime, List<GravityFieldComponent> gravityFieldComponents) {
      foreach (GravityFieldComponent gravityFieldComponent in gravityFieldComponents) {
        float distance = Vector2.Distance(gravityComponent.Entity.Position, gravityFieldComponent.Entity.Position);

        if (distance < gravityFieldComponent.Radius) {
          Vector2 direction =
            VectorUtils.GetDirectionVector(gravityComponent.Entity.Position, gravityFieldComponent.Entity.Position);
          moveComponent.Velocity +=
            direction
            * (AffectSourcePowerByDistance ? countPower(distance, gravityFieldComponent) : gravityFieldComponent.Power)
            * gravityComponent.GravityMultiplier
            * (float)elapsedTime
            * Time.EntityTimeSpeed(moveComponent);
        }
      }
    }

    private float countPower(float distance, GravityFieldComponent gravityFieldComponent) {
      var distanceWithMaxPower = gravityFieldComponent.Radius * MaxPowerDistanceMultiplier;
      if (distance <= distanceWithMaxPower)
        return gravityFieldComponent.Power;

      var distanceFromRadius = gravityFieldComponent.Radius - distance;

      var power = distanceFromRadius * gravityFieldComponent.Power / distanceWithMaxPower;
      //var power = distanceWithMaxPower / distance * gravityFieldComponent.Power;
      //var power = distanceDelta * gravityFieldComponent.Power / distanceWithMaxPower;
      return power;
    }

    public override string ToString() {
      return "%{LightCoral}% GravitySystem: \n"
             + "%{PaleGoldenrod}% GlobalGravityActive: %{White}% " + GlobalGravityActive + "\n"
             + "%{PaleGoldenrod}% GlobalGravityPower: %{White}% " + GlobalGravityPower + "\n"
             + "%{PaleGoldenrod}% AffectSourcePowerByDistance: %{White}% " + AffectSourcePowerByDistance + "\n"
             + "%{PaleGoldenrod}% MaxPowerDistanceMultiplier: %{White}% " + MaxPowerDistanceMultiplier + "\n"
             + "%{PaleGoldenrod}% GlobalGravityDirection: %{White}% " + GlobalGravityDirection.ToString();
    }
  }
}