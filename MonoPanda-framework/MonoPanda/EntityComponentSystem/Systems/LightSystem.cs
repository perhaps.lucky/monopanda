﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;
using Penumbra;
using System;
using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class LightSystem : EntitySystem {

    [ColorParameter]
    public Color AmbientColor { get => penumbra.AmbientColor; set => penumbra.AmbientColor = value; }
    [EnumParameter(typeof(ShadowType))]
    public ShadowType DefaultShadowType;

    private PenumbraComponent penumbra;

    public LightSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
      penumbra = new PenumbraComponent(GameMain.GetInstance());
      penumbra.Debug = Config.Debug.LightSystemDebug;
    }

    public override void Initialize(Dictionary<string, object> parameters) {
      penumbra.Initialize();
    }

    public override void OnComponentRemove(EntityComponent component) {
      if (component is LightComponent) {
        penumbra.Lights.Remove(((LightComponent)component).Light);
      }
    }

    /// <summary>
    /// Should be called in draw system before this Draw()
    /// </summary>
    public void BeginDraw() {
      if (Active) {
        penumbra.BeginDraw();
        // Default GraphicsDevice.Clear is called outside the ECS, before start of penumbra, resulting in background being unaffected by lights
        // Leaving this as optional debug, as typically game will have it's own background.
        if (Config.Debug.LightSystemGraphicsDeviceClear)
          GameMain.GraphicsDeviceManager.GraphicsDevice.Clear(Color.SlateGray);
      }
    }

    public override void Update() {
      penumbra.Update(Time.CurrentGameTime);
      penumbra.Transform = ECS.Camera.TransformationMatrix;

      foreach (LightComponent component in ECS.GetAllComponents<LightComponent>()) {
        if (component.LightTypeChanged) replaceLight(component);
        if (component.Light is TexturedLight) tryGetTexture(component);
        if (!component.ShadowTypeSet) setDefaultShadowType(component);
      }

      foreach (ShadowHullComponent component in ECS.GetAllComponents<ShadowHullComponent>()) {
        component.Hull.Position = new Vector2((int)component.Entity.Position.X, (int)component.Entity.Position.Y);
        if (component.HullChanged) replaceHull(component);
      }

      penumbra.Update(Time.CurrentGameTime);
    }

    public override void Draw() {
      penumbra.Draw(Time.CurrentGameTime);
    }

    /// <summary>
    /// If type of light changed, remove old light and add new one.
    /// </summary>
    private void replaceLight(LightComponent component) {
      penumbra.Lights.Remove(component.OldLight);
      penumbra.Lights.Add(component.Light);
      component.LightTypeChanged = false;
    }

    /// <summary>
    /// Get texture from ContentManager and set it in Light object (can be null).
    /// </summary>
    private void tryGetTexture(LightComponent component) {
      Texture2D texture = ContentManager.Get<Texture2D>(component.ContentId);
      ((TexturedLight)component.Light).Texture = texture;
    }

    private void setDefaultShadowType(LightComponent component) {
      component.ShadowType = DefaultShadowType;
    }

    private void replaceHull(ShadowHullComponent component) {
      if (component.OldHull != null) 
        penumbra.Hulls.Remove(component.OldHull);
      penumbra.Hulls.Add(component.Hull);
      component.HullChanged = false;
    }

    public override string ToString() {
      return "%{LightSteelBlue}% LightSystem: \n"
        + "%{PaleGoldenrod}% AmbientColor: %{White}% " + AmbientColor.ToString() + "\n"
        + "%{PaleGoldenrod}% DefaultShadowType: %{White}% " + DefaultShadowType.ToString();
    }

  }
}
