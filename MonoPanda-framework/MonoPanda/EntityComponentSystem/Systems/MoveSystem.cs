﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;

namespace MonoPanda.Systems {
  public class MoveSystem : EntitySystem {

    public float MaxAccelerationToVelocity { get; set; } = 100f;
    public float VelocityLoss { get; set; } = 100f;

    public MoveSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (MoveComponent component in ECS.GetAllComponents<MoveComponent>()) {
        if(!component.LockVelocity) loseVelocity(component);
        addAccelerationToVelocity(component);
        if(component.Velocity != Vector2.Zero)
          move(component);
      }
    }

    private void loseVelocity(MoveComponent component) {
      Vector2 acceleration = component.Acceleration;
      Vector2 velocity = component.Velocity;
      float velocityLoss = VelocityLoss * component.VelocityLossMultiplier;
      float timeMultiplier = Time.EntityTimeSpeed(component);

      if (velocity.X > 0 && acceleration.X <= 0) {
        velocity = accelerateLeft(velocity, velocityLoss, 0, timeMultiplier);
      } else if (velocity.X < 0 && acceleration.X >= 0) {
        velocity = accelerateRight(velocity, velocityLoss, 0, timeMultiplier);
      }

      if (velocity.Y > 0 && acceleration.Y <= 0) {
        velocity = accelerateUp(velocity, velocityLoss, 0, timeMultiplier);
      } else if (velocity.Y < 0 && acceleration.Y >= 0) {
        velocity = accelerateDown(velocity, velocityLoss, 0, timeMultiplier);
      }

      component.Velocity = velocity;
    }

    private void addAccelerationToVelocity(MoveComponent component) {
      Vector2 acceleration = component.Acceleration;
      Vector2 velocity = component.Velocity;
      float maxAccelerationToVelocity = MaxAccelerationToVelocity * component.MaxAccelerationToVelocityMultiplier;
      float timeMultiplier = Time.EntityTimeSpeed(component);

      if (acceleration.X < 0)
        velocity = accelerateLeft(velocity, -acceleration.X, maxAccelerationToVelocity, timeMultiplier);

      if (acceleration.X > 0)
        velocity = accelerateRight(velocity, acceleration.X, maxAccelerationToVelocity, timeMultiplier);

      if (acceleration.Y < 0)
        velocity = accelerateUp(velocity, -acceleration.Y, maxAccelerationToVelocity, timeMultiplier);

      if (acceleration.Y > 0)
        velocity = accelerateDown(velocity, acceleration.Y, maxAccelerationToVelocity, timeMultiplier);

      component.Velocity = velocity;
    }

    private void move(MoveComponent component) {
      component.LastPosition = component.Entity.Position;
      component.Entity.Position += component.Velocity * Time.ElapsedCalculated * Time.EntityTimeSpeed(component);
      if (component.Mimicking != null) {
        component.Entity.Position += component.Mimicking.Velocity * Time.ElapsedCalculated * Time.EntityTimeSpeed(component);
        component.Mimicking = null;
      }

    }

    private Vector2 accelerateLeft(Vector2 velocity, float accelerationValue, float accelerationLimit, float timeMultiplier) {
      velocity.X += -accelerationValue * Time.ElapsedCalculated * timeMultiplier;
      if (velocity.X < -accelerationLimit)
        velocity.X = -accelerationLimit;
      return velocity;
    }

    private Vector2 accelerateRight(Vector2 velocity, float accelerationValue, float accelerationLimit, float timeMultiplier) {
      velocity.X += accelerationValue * Time.ElapsedCalculated * timeMultiplier;
      if (velocity.X > accelerationLimit)
        velocity.X = accelerationLimit;
      return velocity;
    }

    private Vector2 accelerateUp(Vector2 velocity, float accelerationValue, float accelerationLimit, float timeMultiplier) {
      velocity.Y += -accelerationValue * Time.ElapsedCalculated * timeMultiplier;
      if (velocity.Y < -accelerationLimit)
        velocity.Y = -accelerationLimit;
      return velocity;
    }

    private Vector2 accelerateDown(Vector2 velocity, float accelerationValue, float accelerationLimit, float timeMultiplier) {
      velocity.Y += accelerationValue * Time.ElapsedCalculated * timeMultiplier;
      if (velocity.Y > accelerationLimit)
        velocity.Y = accelerationLimit;
      return velocity;
    }

    public override string ToString() {
      return "%{Cyan}% MoveSystem: \n"
        + "%{PaleGoldenrod}% MaxAccelerationToVelocity: %{White}% " + MaxAccelerationToVelocity + "\n"
        + "%{PaleGoldenrod}% VelocityLoss: %{White}% " + VelocityLoss;
    }
  }
}
