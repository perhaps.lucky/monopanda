﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.ECS;
using MonoPanda.Old.ECS.Components.CollisionComponentDetails;
using MonoPanda.Utils;
using System.Collections.Generic;

namespace MonoPanda.Old.Systems {
  public class CollisionSystem : EntitySystem {

    public static float CollisionDetailsTimeStep = 0.1f;

    public CollisionSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Initialize(Dictionary<string, object> parameters) {
      if (parameters.ContainsKey("CollisionDetailsTimeStep"))
        CollisionDetailsTimeStep = (float)parameters["CollisionDetailsTimeStep"];
    }

    public override void Update() {
      foreach (MonoPanda.Old.Components.CollisionComponent component in ECS.GetAllComponents<MonoPanda.Old.Components.CollisionComponent>()) {
        if (isRectanglesUpdateNecessary(component)) updateCurrentRectangle(component);
        if (Config.Debug.CollisionRectangles) requestDrawRectangles(component);
      }
    }

    private void updateCurrentRectangle(MonoPanda.Old.Components.CollisionComponent component) {
      CollisionMovementParameters parameters = new CollisionMovementParameters(component);
      component.CollisionMovementParams = parameters;
    }

    private bool isRectanglesUpdateNecessary(MonoPanda.Old.Components.CollisionComponent component) {
      return component.UpdateRequested.Check();
    }

    private void requestDrawRectangles(MonoPanda.Old.Components.CollisionComponent component) {
      DrawUtils.DrawRectangle(ECS, component.CollisionMovementParams.CurrentCollisionRectangle, new Color(50, 50, 250, 100));
      //RectangleUtils.DrawRectangle(ECS, component.CollisionMovementParams.MovementRectangle, new Color(250, 100, 100, 30));
    }

    public override string ToString() {
      return "%{Moccasin}% CollisionSystem: \n"
        + "%{PaleGoldenrod}% CollisionDetailsTimeStep: %{White}% " + CollisionDetailsTimeStep;
    }
  }
}
