﻿using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;

namespace MonoPanda.Systems {
  public class SpineAnimationSystem : EntitySystem {
    public SpineAnimationSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (SpineComponent component in ECS.GetAllComponents<SpineComponent>()) {
        component.AnimationState.Update(Time.ElapsedMillis * Time.EntityTimeSpeed(component) / 1000.0f);
        if(component.Entity.IsInVisibleArea)
          component.ApplyState();
      }
    }

    public override string ToString() {
      return "%{LightCyan}% SpineAnimationSystem";
    }
  }
}
