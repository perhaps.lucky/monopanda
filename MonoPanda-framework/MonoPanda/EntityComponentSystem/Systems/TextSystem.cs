﻿using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;

using System.Collections.Generic;

namespace MonoPanda.Systems {
  public class TextSystem : EntitySystem {
    public TextSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (TextComponent textComponent in ECS.GetAllComponents<TextComponent>()) {
        if (textComponent.DrawComponentNotCreated.Check())
          textComponent.Entity.AddComponent(createDrawComponent(textComponent.Depth));
      }
    }

    private DrawComponent createDrawComponent(float depth) {
      DrawComponent component = new DrawComponent();
      component.DrawTarget = new TextSprite();
      component.Depth = depth; // Necessary for DrawSystemSpine which does sorting outside the batch
      return component;
    }

    public override string ToString() {
      return "%{Snow}% TextSystem";
    }
  }
}