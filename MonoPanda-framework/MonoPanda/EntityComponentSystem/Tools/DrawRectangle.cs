﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Old.ECS.Components.CollisionComponentDetails;
using MonoPanda.Utils;

using System;

namespace MonoPanda.ECS.Tools {
  public class DrawRectangle : IDisposable {

    public CollisionRectangle Rectangle {
      get => rectangle;
      set {
        rectangle = value;
        needsTextureReinit = true;
      }
    }

    private CollisionRectangle rectangle;

    public Color Color {
      get => color;
      set {
        color = value;
        needsTextureReinit = true;
      }
    }

    private Color color;

    public float Depth {
      get => depth;
      set {
        depth = value;
        needsTextureReinit = true;
      }
    }

    private float depth;

    public int Alpha {
      get => alpha;
      set {
        alpha = value;
        needsTextureReinit = true;
      }
    }

    private int alpha;

    public int BorderWidth {
      get => borderWidth;
      set {
        borderWidth = value;
        needsTextureReinit = true;
      }
    }

    private int borderWidth;

    private Texture2D texture;

    private bool needsTextureReinit;

    public DrawRectangle(CollisionRectangle rectangle, Color color, float depth, int alpha = 125, int borderWidth = 2) {
      Rectangle = rectangle;
      Color = color;
      Depth = depth;
      Alpha = alpha;
      BorderWidth = borderWidth;
    }

    public DrawRectangle(Rectangle rectangle, Color color, float depth) : this(
      CollisionRectangle.FromRectangle(rectangle), color, depth) {
    }

    public void Draw(SpriteBatch spriteBatch) {
      if (needsTextureReinit) {
        initTexture();
      }

#pragma warning disable CS0618 // Type or member is obsolete
      spriteBatch.Draw(
        texture,
        position: new Vector2(Rectangle.X, Rectangle.Y),
        color: Color.White,
        rotation: 0,
        scale: new Vector2(1f),
        layerDepth: Depth, sourceRectangle: null, origin: Vector2.Zero, effects: SpriteEffects.None);
#pragma warning restore CS0618 // Type or member is obsolete
    }

    public void Dispose() {
      if (texture != null)
        texture.Dispose();
    }

    private void initTexture() {
      Dispose();
      Color fill = new Color(color.R, color.B, color.G, (byte)alpha);
      Color border = new Color(color.R, color.B, color.G, (byte)255);
      texture = TextureUtils.CreateRectangleTexture((int)Rectangle.Width, (int)Rectangle.Height, fill, borderWidth, border);
    }
  }
}