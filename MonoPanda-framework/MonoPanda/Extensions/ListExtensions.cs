﻿using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.Extensions {
  static class ListExtensions {
    public static bool IsEmpty<T>(this List<T> list) {
      return list == null || !list.Any();
    }

    public static bool IsNotEmpty<T>(this List<T> list) {
      return !IsEmpty(list);
    }
  }
}