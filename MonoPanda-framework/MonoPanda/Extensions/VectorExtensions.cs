﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS.Systems.Camera;

namespace MonoPanda.Extensions {
  static class VectorExtensions {
    public static FollowedPosition ToFollowedPosition(this Vector2 vector2) {
      return new FollowedPosition(vector2);
    }
  }
}