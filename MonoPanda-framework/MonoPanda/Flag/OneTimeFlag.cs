﻿using MonoPanda.Logger;

namespace MonoPanda.Flag {
  /// <summary>
  /// Simple flag object that will return true just once.
  /// Can be reinitialized afterwards.
  /// </summary>
  public class OneTimeFlag {
    private bool value = false;
    private string id;
    private LogCategory logCategory;
    private LogLevel logLevel;

    public OneTimeFlag(string id = null, LogCategory logCategory = LogCategory.Flags, LogLevel logLevel = LogLevel.Debug) {
      this.id = id;
      this.logCategory = logCategory;
      this.logLevel = logLevel;
    }

    public bool Check() {
      if (!value) {
        Log.log(logCategory, logLevel, flagName() + " checked and returned true.");
        value = true;
        return true;
      }
      return false;
    }

    public void Reinitialize() {
      Log.log(logCategory, logLevel, flagName() + " reinitialized.");
      value = false;
    }

    private string flagName() {
      if (id != null)
        return "Flag id: " + id;
      return "Anonymous flag";
    }

  }
}
