﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Configuration;
using MonoPanda.Initializers;
using MonoPanda.UserSettings;
using MonoPanda.Utils;
using MonoPanda.GlobalTime;
using MonoPanda.Timers;
using MonoPanda.Logger;
using MonoPanda.Content;
using MonoPanda.Input;
using MonoPanda.Sound;
using MonoPanda.Languages;
using MonoPanda.Mods;
using MonoPanda.States;
using MonoPanda.Multithreading;
using System;
using MonoPanda.Console;

namespace MonoPanda {
  public class GameMain : Game {

    private static GameMain instance;

    public static GraphicsDeviceManager GraphicsDeviceManager => GetInstance().graphicsDeviceManager;
    public static SpriteBatch SpriteBatch => GetInstance().spriteBatch;

    private GraphicsDeviceManager graphicsDeviceManager;
    private SpriteBatch spriteBatch;

    public GameMain() {
      Content.RootDirectory = "Content";
      graphicsDeviceManager = new GraphicsDeviceManager(this);
      this.IsFixedTimeStep = Config.Graphics.IsFixedTimeStep;
      this.TargetElapsedTime = TimeSpan.FromMilliseconds(Config.Graphics.TargetElapsedTimeMillis);
      graphicsDeviceManager.SynchronizeWithVerticalRetrace = Config.Graphics.SynchronizeWithVerticalRetrace;
      instance = this;
    }

    protected override void Initialize() {
      Log.initialize();

      Log.log(LogCategory.Startup, LogLevel.Info, "Starting up the game!");

      spriteBatch = new SpriteBatch(GraphicsDevice);
      ModManager.initialize();
      GraphicsInitializer.initialize();
      InputManager.Initialize();
      SoundManager.initialize();
      Language.LoadLanguage(Settings.Language.Language);

      Log.log(LogCategory.Startup, LogLevel.Info, "Ensuring all settings are provided.");
      Settings.Save();

      if (Config.Console.ConsoleActive)
        ConsoleSystem.initialize();

      base.Initialize();
    }

    protected override void LoadContent() {
      ContentManager.initialize();
      MouseCursorInitializer.initialize();

      Log.log(LogCategory.Startup, LogLevel.Info, "Startup complete!");
      StateManager.Initialize();
    }

    protected override void UnloadContent() {
      ContentManager.unload();
      ThreadRequestSystem.StopAllSystems();
      StateManager.Terminate();
    }

    protected override void Update(GameTime gameTime) {
      Time.update(gameTime); // get rid of passing gameTime parameter
      InputManager.Update();
      ContentManager.update();
      Log.update();
      StateManager.Update();

      if (Config.Debug.DebugTools)
        DebugTools.getInstance().update();

      if (Config.Console.ConsoleActive)
        ConsoleSystem.update();

      base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime) {
      GraphicsDevice.Clear(Color.SlateGray);

      spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

      StateManager.Draw();

      if (GameSettings.getInstance().DrawCustomCursor && GameSettings.getInstance().IsCursorVisible)
        DrawUtils.DrawTexture(GameSettings.getInstance().CustomCursorContentId, InputManager.GetMouseWindowPosition());

      if (Config.Console.ConsoleActive)
        ConsoleSystem.draw();

      spriteBatch.End();
      base.Draw(gameTime);
    }

    public static void ExitGame() {
      GetInstance().Exit();
    }

    public static GameMain GetInstance() {
      return instance;
    }
  }
}
