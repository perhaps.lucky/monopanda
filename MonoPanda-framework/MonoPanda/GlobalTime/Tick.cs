﻿using MonoPanda.Configuration;
using System;

namespace MonoPanda.GlobalTime {
  /// <summary>
  /// This class returns the time difference between Get() calls. First call will always return 0 as there is no time stored.
  /// This should be used whenever there is a need for update based on passing time, that is not called on every frame.
  /// </summary>
  public class Tick {

    private TimeSpan lastTick;

    /// <summary>
    /// Returns a difference of time (affected by Config.Time.ElapsedCalculatedMultiplier) since last time it was called. <br/>
    /// <b>Keep in mind that it should be only used once in each Update. Every next call will return 0.</b>
    /// </summary>
    /// <returns></returns>
    public double Get() {
      var currentTick = Time.CurrentGameTime.TotalGameTime;

      if (lastTick == default) {
        lastTick = currentTick;
        return 0f;
      }

      double calculatedDifference = (currentTick.TotalMilliseconds - lastTick.TotalMilliseconds) * Config.Time.ElapsedCalculatedMultiplier;
      lastTick = currentTick;
      return calculatedDifference;
    }


  }
}
