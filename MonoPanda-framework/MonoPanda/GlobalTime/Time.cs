﻿using Microsoft.Xna.Framework;

using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.ECS;
using MonoPanda.UtilityClasses;

using System;

namespace MonoPanda.GlobalTime {
  public class Time {
    public static GameTime CurrentGameTime { get; private set; }
    public static float ElapsedMillis => (float)CurrentGameTime.ElapsedGameTime.TotalMilliseconds;
    public static float ElapsedCalculated => (float)calculateElapsed();

    public static double TotalElapsedMillis { get; private set; } = 0;

    /// <summary>
    /// This can be used whenever there is a time-affecting feature. Timers and systems can be affected by this value.
    /// </summary>
    public static float WorldTimeSpeed { get => worldTimeSpeed; set => setWorldTimeSpeed(value); }

    private static float worldTimeSpeed = 1.0f;

    private static Informer worldTimeChangeInformer = new Informer();

    /// <summary>
    /// This can be used like WorldTimeSpeed, with the difference that entity with TimeComponent have another way for individual time speed control
    /// </summary>
    public static float EntityTimeSpeed(Entity entity) {
      if (entity == null)
        return WorldTimeSpeed;

      var entityMultiplier = 1.0f;
      entity.GetComponentOptional<TimeComponent>().IfPresent(c => entityMultiplier = c.TimeMultiplier);
      return WorldTimeSpeed * entityMultiplier;
    }

    public static float EntityTimeSpeed(EntityComponent component) {
      return EntityTimeSpeed(component.Entity);
    }

    public static void update(GameTime gameTime) {
      CurrentGameTime = gameTime;
      TotalElapsedMillis = gameTime.TotalGameTime.TotalMilliseconds;
    }

    public static Observer RegisterWorldTimeSpeedObserver(Delegate action) {
      return worldTimeChangeInformer.RegisterObserver(action);
    }

    public static void UnregisterWorldTimeSpeedObserver(Observer observer) {
      worldTimeChangeInformer.UnregisterObserver(observer);
    }

    private static double calculateElapsed() {
      return CurrentGameTime.ElapsedGameTime.TotalMilliseconds * Config.Time.ElapsedCalculatedMultiplier;
    }

    private static void setWorldTimeSpeed(float value) {
      var oldWorldTimeSpeed = worldTimeSpeed;
      worldTimeSpeed = value;
      worldTimeChangeInformer.Inform(oldWorldTimeSpeed, false);
    }
  }
}