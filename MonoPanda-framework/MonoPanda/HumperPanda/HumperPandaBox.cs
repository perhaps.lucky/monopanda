using Humper;
using Humper.Base;
using Humper.Responses;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.HumperPanda {
  /// <summary>
  /// Necessary implementation because <see cref="Humper.Box">Box class</see> class is directly connected with <see cref="Humper.World">World</see> and not <see cref="Humper.IWorld">it's interface</see>.
  /// CopyPaste, except this time it uses HumperPandaWorld
  /// </summary>
  public class HumperPandaBox : IBox {
    private HumperPandaWorld world;
    private RectangleF bounds;
    private Enum tags;

    public HumperPandaBox(HumperPandaWorld world, float x, float y, float width, float height) {
      this.world = world;
      this.bounds = new RectangleF(x, y, width, height);
    }

    public RectangleF Bounds => this.bounds;

    public object Data { get; set; }

    public float Height => this.Bounds.Height;

    public float Width => this.Bounds.Width;

    public float X => this.Bounds.X;

    public float Y => this.Bounds.Y;

    public IMovement Simulate(
      float x,
      float y,
      Func<ICollision, ICollisionResponse> filter) {
      return this.world.Simulate(this, x, y, filter);
    }

    public IMovement Simulate(
      float x,
      float y,
      Func<ICollision, CollisionResponses> filter) {
      return this.Move(x, y,
        (Func<ICollision, ICollisionResponse>)(col =>
          col.Hit == null ? (ICollisionResponse)null : CollisionResponse.Create(col, filter(col))));
    }

    public IMovement Move(float x, float y, Func<ICollision, ICollisionResponse> filter) {
      IMovement movement = this.Simulate(x, y, filter);
      this.bounds.X = movement.Destination.X;
      this.bounds.Y = movement.Destination.Y;
      this.world.Update((IBox)this, movement.Origin);
      return movement;
    }

    public IMovement Move(float x, float y, Func<ICollision, CollisionResponses> filter) {
      IMovement movement = this.Simulate(x, y, filter);
      this.bounds.X = movement.Destination.X;
      this.bounds.Y = movement.Destination.Y;
      this.world.Update((IBox)this, movement.Origin);
      return movement;
    }

    public IBox AddTags(params Enum[] newTags) {
      foreach (Enum newTag in newTags)
        this.AddTag(newTag);
      return (IBox)this;
    }

    public IBox RemoveTags(params Enum[] newTags) {
      foreach (Enum newTag in newTags)
        this.RemoveTag(newTag);
      return (IBox)this;
    }

    private void AddTag(Enum tag) {
      if (this.tags == null) {
        this.tags = tag;
      } else {
        Type type = this.tags.GetType();
        if ((object)Enum.GetUnderlyingType(type) != (object)typeof(ulong))
          this.tags = (Enum)Enum.ToObject(type,
            (object)(Convert.ToInt64((object)this.tags) | Convert.ToInt64((object)tag)));
        else
          this.tags = (Enum)Enum.ToObject(type,
            (object)(ulong)((long)Convert.ToUInt64((object)this.tags) | (long)Convert.ToUInt64((object)tag)));
      }
    }

    private void RemoveTag(Enum tag) {
      if (this.tags == null)
        return;
      Type type = this.tags.GetType();
      if ((object)Enum.GetUnderlyingType(type) != (object)typeof(ulong))
        this.tags = (Enum)Enum.ToObject(type,
          (object)(Convert.ToInt64((object)this.tags) & ~Convert.ToInt64((object)tag)));
      else
        this.tags = (Enum)Enum.ToObject(type,
          (object)(ulong)((long)Convert.ToUInt64((object)this.tags) & ~(long)Convert.ToUInt64((object)tag)));
    }

    public bool HasTag(params Enum[] values) => this.tags != null &&
                                                ((IEnumerable<Enum>)values).Any<Enum>(
                                                  (Func<Enum, bool>)(value => this.tags.HasFlag(value)));

    public bool HasTags(params Enum[] values) => this.tags != null &&
                                                 ((IEnumerable<Enum>)values).All<Enum>(
                                                   (Func<Enum, bool>)(value => this.tags.HasFlag(value)));
  }
}