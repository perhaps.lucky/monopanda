﻿using Humper;
using Humper.Base;
using Humper.Responses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.HumperPanda {
  
  /// <summary>
  /// This class is a copy of <see cref="Humper.World">oryginal IWorld implementation</see>.
  /// For some reason Hit method returns a single collision information. Because of it, other collisions are originally ignored. That causes problems when there are collisions that end with CrossResponse - entity can pass through other objects as long as this CrossResponse is returned from first collision.
  /// I passed filter that decides of collision response to Hit method. If filter returns CrossResponse, that collision is ignored.
  /// This is probably not smartest and optimal fix, but it works as much as I want it to.
  /// </summary>
  public class HumperPandaWorld : IWorld{
    private Grid grid;

    public HumperPandaWorld(float width, float height, float cellSize = 64f) => this.grid = new Grid((int) Math.Ceiling((double) width / (double) cellSize), (int) Math.Ceiling((double) height / (double) cellSize), cellSize);

    public RectangleF Bounds => new RectangleF(0.0f, 0.0f, this.grid.Width, this.grid.Height);

    public IBox Create(float x, float y, float width, float height)
    {
      HumperPandaBox box = new HumperPandaBox(this, x, y, width, height);
      this.grid.Add((IBox) box);
      return (IBox) box;
    }

    public IEnumerable<IBox> Find(float x, float y, float w, float h)
    {
      x = Math.Max(0.0f, Math.Min(x, this.Bounds.Right - w));
      y = Math.Max(0.0f, Math.Min(y, this.Bounds.Bottom - h));
      return this.grid.QueryBoxes(x, y, w, h);
    }

    public IEnumerable<IBox> Find(RectangleF area) => this.Find(area.X, area.Y, area.Width, area.Height);

    public bool Remove(IBox box) => this.grid.Remove(box);

    public void Update(IBox box, RectangleF from) => this.grid.Update(box, from);

    public IHit Hit(Vector2 point, IEnumerable<IBox> ignoring = null)
    {
      IEnumerable<IBox> first = this.Find(point.X, point.Y, 0.0f, 0.0f);
      if (ignoring != null)
        first = first.Except<IBox>(ignoring);
      foreach (IBox other in first)
      {
        IHit hit = Humper.Hit.Resolve(point, other);
        if (hit != null) {
          return hit;
        }
      }
      return (IHit) null;
    }

    public IHit Hit(Vector2 origin, Vector2 destination, IEnumerable<IBox> ignoring = null)
    {
      Vector2 location = Vector2.Min(origin, destination);
      Vector2 vector2 = Vector2.Max(origin, destination);
      RectangleF rectangleF = new RectangleF(location, vector2 - location);
      IEnumerable<IBox> first = this.Find(rectangleF.X, rectangleF.Y, rectangleF.Width, rectangleF.Height);
      if (ignoring != null)
        first = first.Except<IBox>(ignoring);
      IHit than = (IHit) null;
      foreach (IBox other in first)
      {
        IHit hit = Humper.Hit.Resolve(origin, destination, other);
        if (hit != null && (than == null || hit.IsNearest(than, origin)))
          than = hit;
      }
      return than;
    }

    public IHit Hit(RectangleF origin, RectangleF destination, IEnumerable<IBox> ignoring = null) {
      throw new NotImplementedException();
    }

    public IHit Hit(RectangleF origin, RectangleF destination, IEnumerable<IBox> ignoring = null, HumperPandaBox box = null, Func<ICollision, ICollisionResponse> filter = null)
    {
      RectangleF rectangleF = new RectangleF(origin, destination);
      IEnumerable<IBox> first = this.Find(rectangleF.X, rectangleF.Y, rectangleF.Width, rectangleF.Height);
      if (ignoring != null)
        first = first.Except<IBox>(ignoring);
      IHit than = (IHit) null;
      foreach (IBox other in first)
      {
        IHit hit = Humper.Hit.Resolve(origin, destination, other);
        if (hit != null && (than == null || hit.IsNearest(than, origin.Location))) {
          // Here goes some additional nonsense
          Collision collision = new Collision() {Box = (IBox)box, Hit = hit, Goal = destination, Origin = origin};
          if (filter.Invoke(collision).GetType() != typeof(CrossResponse)) // check type of response from filter
            than = hit; // normal behaviour
        }
      }
      return than;
    }

    public IMovement Simulate(Box box, float x, float y, Func<ICollision, ICollisionResponse> filter) {
      throw new NotImplementedException();
    }

    public IMovement Simulate(
      HumperPandaBox box,
      float x,
      float y,
      Func<ICollision, ICollisionResponse> filter)
    {
      RectangleF bounds = box.Bounds;
      RectangleF rectangleF = new RectangleF(x, y, box.Width, box.Height);
      List<IHit> hitList = new List<IHit>();
      Movement movement = new Movement();
      movement.Origin = bounds;
      movement.Goal = rectangleF;
      List<IHit> hits = hitList;
      List<IBox> ignoring = new List<IBox>();
      ignoring.Add((IBox) box);
      HumperPandaBox box1 = box;
      RectangleF origin = bounds;
      RectangleF destination = rectangleF;
      Func<ICollision, ICollisionResponse> filter1 = filter;
      movement.Destination = this.Simulate(hits, ignoring, box1, origin, destination, filter1);
      movement.Hits = (IEnumerable<IHit>) hitList;
      return (IMovement) movement;
    }

    private RectangleF Simulate(
      List<IHit> hits,
      List<IBox> ignoring,
      HumperPandaBox box,
      RectangleF origin,
      RectangleF destination,
      Func<ICollision, ICollisionResponse> filter)
    {
      IHit hit = this.Hit(origin, destination, (IEnumerable<IBox>) ignoring, box, filter);
      if (hit != null)
      {
        hits.Add(hit);
        RectangleF origin1 = new RectangleF(hit.Position, origin.Size);
        Collision collision = new Collision()
        {
          Box = (IBox) box,
          Hit = hit,
          Goal = destination,
          Origin = origin
        };
        ICollisionResponse collisionResponse = filter((ICollision) collision);
        if (collisionResponse != null && destination != collisionResponse.Destination)
        {
          ignoring.Add(hit.Box);
          return this.Simulate(hits, ignoring, box, origin1, collisionResponse.Destination, filter);
        }
      }
      return destination;
    }

    public void DrawDebug(
      int x,
      int y,
      int w,
      int h,
      Action<int, int, int, int, float> drawCell,
      Action<IBox> drawBox,
      Action<string, int, int, float> drawString)
    {
      foreach (IBox queryBox in this.grid.QueryBoxes((float) x, (float) y, (float) w, (float) h))
        drawBox(queryBox);
      foreach (Grid.Cell queryCell in this.grid.QueryCells((float) x, (float) y, (float) w, (float) h))
      {
        int num1 = queryCell.Count();
        float num2 = num1 > 0 ? 1f : 0.4f;
        drawCell((int) queryCell.Bounds.X, (int) queryCell.Bounds.Y, (int) queryCell.Bounds.Width, (int) queryCell.Bounds.Height, num2);
        Action<string, int, int, float> action = drawString;
        string str = num1.ToString();
        RectangleF bounds = queryCell.Bounds;
        int x1 = (int) bounds.Center.X;
        bounds = queryCell.Bounds;
        int y1 = (int) bounds.Center.Y;
        double num3 = (double) num2;
        action(str, x1, y1, (float) num3);
      }
    }
  }
}