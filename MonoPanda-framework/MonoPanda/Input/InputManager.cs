﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using MonoPanda.Configuration;
using MonoPanda.Console;
using MonoPanda.Logger;
using MonoPanda.UI;
using MonoPanda.Utils;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System.Collections.Generic;
using System.IO;

namespace MonoPanda.Input {
  public class InputManager {
    private static InputManager instance;

    private List<KeyInputDataForManager> keyInputs;

    private KeyInputDataForManager lmb;
    private KeyInputDataForManager rmb;
    private KeyInputDataForManager mmb;

    private int oldScrollValue;
    private bool scrolledUp;
    private bool scrolledDown;

    private KeyboardState oldKeyboardState;
    private KeyboardState keyboardState;

    private InputManager() { }

    /// <summary>
    /// Returns key data for input id.
    /// </summary>
    public static KeyInputData GetKeyInput(string inputId, IFocus context = null) {
      if (context != IFocus.Focus)
        return new KeyInputData(); // check in different context -> all false

      KeyInputData dataFound = getInstance().keyInputs.Find(key => key.Id.Equals(inputId));

      if (dataFound == null) {
        Log.log(LogCategory.Input, LogLevel.Warn,
          "Not found key data for input id: " + inputId + " return empty data (all false).");
        return new KeyInputData();
      }

      return dataFound;
    }

    public static KeyInputData GetLeftMouseButton(IFocus context = null, bool ignoreFocus = false) {
      if (context != IFocus.Focus && !ignoreFocus)
        return new KeyInputData();
      return getInstance().lmb;
    }

    public static KeyInputData GetRightMouseButton(IFocus context = null) {
      if (context != IFocus.Focus)
        return new KeyInputData();
      return getInstance().rmb;
    }

    public static KeyInputData GetMiddleMouseButton(IFocus context = null) {
      if (context != IFocus.Focus)
        return new KeyInputData();
      return getInstance().mmb;
    }

    public static Vector2 GetMouseWindowPosition() {
      return Mouse.GetState().Position.ToVector2();
    }

    public static bool ScrolledUp() {
      return getInstance().scrolledUp;
    }

    public static bool ScrolledDown() {
      return getInstance().scrolledDown;
    }

    public static void Initialize() {
      Log.log(LogCategory.Startup, LogLevel.Info, "Initializing input manager.");
      InputManager instance = getInstance();
      instance.loadKeyInputs();
      instance.lmb = new KeyInputDataForManager();
      instance.rmb = new KeyInputDataForManager();
      instance.mmb = new KeyInputDataForManager();
    }

    public static void Update() {
      InputManager instance = getInstance();
      instance.updateKeyInputs();
      instance.updateMouseInputs();
      instance.updateMouseScroll();
      instance.updateKeyboardStates();
    }

    /// <summary>
    /// Changes key bound to input. <br/>
    /// <b>Note:</b> Change is made on runtime and will not be saved on its own.
    /// </summary>
    /// <param name="id">id of input</param>
    /// <param name="key">key enum/number</param>
    public static void SetUserKeybind(string id, Keys key) {
      Log.log(LogCategory.Input, LogLevel.Info, "Input: " + id + " is now set to key: " + key.ToString());
      GetKeyInput(id).Key = key;
    }

    /// <summary>
    /// Saves current settings. Settings will be loaded again on next startup.
    /// </summary>
    public static void SaveUserKeybinds() {
      Log.log(LogCategory.Input, LogLevel.Info, "Saving user keybinds.");
      JArray array = new JArray();
      foreach (var keyInput in getInstance().keyInputs) {
        JObject jsonKeyInput = new JObject();
        jsonKeyInput.Add(new JProperty("Id", keyInput.Id));
        jsonKeyInput.Add(new JProperty("Key", keyInput.Key));
        array.Add(jsonKeyInput);
      }

      using (StreamWriter file = File.CreateText(Config.Input.UserKeybindsJson))
      using (JsonTextWriter writer = new JsonTextWriter(file)) {
        array.WriteTo(writer);
      }
    }

    /// <summary>
    /// Restores keybinds defined in keybinds_default.json.
    /// <b>Note:</b> Change is made on runtime and will not be saved on its own.
    /// </summary>
    public static void RestoreDefaultKeybinds() {
      Log.log(LogCategory.Input, LogLevel.Info, "Restoring default keybinds.");
      getInstance().keyInputs = ContentUtils.LoadJson<List<KeyInputDataForManager>>(Config.Input.DefaultKeybindsJson);
    }

    public static KeyboardState GetCurrentKeyboardState() {
      return getInstance().keyboardState;
    }

    public static KeyboardState GetOldKeyboardState() {
      return getInstance().oldKeyboardState;
    }

    private void updateKeyInputs() {
      List<Keys> pressedKeys = new List<Keys>(Keyboard.GetState().GetPressedKeys());
      foreach (var input in keyInputs) {
        updateKeyInput(input, pressedKeys.Contains(input.Key));
      }
    }

    private void updateKeyInput(KeyInputDataForManager input, bool isButtonDown) {
      if (isButtonDown) {
        // Case: noticed button that wasn't pressed is now pressed
        if (!input.IsPressed && !input.WasPressedOnLastCheck) {
          Log.log(LogCategory.Input, LogLevel.Debug, "Input: " + input.Id + " got pressed.");
          input.IsPressed = true;
          input.WasPressedOnLastCheck = true;
          input.IsHeld = true;
          input.IsReleased = false;
          return;
        }

        // Case: noticed button that was pressed is still down
        if (input.WasPressedOnLastCheck) {
          Log.log(LogCategory.Input, LogLevel.Debug, "Input: " + input.Id + " is held.");
          input.IsPressed = false;
          return;
        }
      } else {
        // Case: button was down and now is up
        if (input.WasPressedOnLastCheck) {
          Log.log(LogCategory.Input, LogLevel.Debug, "Input: " + input.Id + " got released");
          input.IsPressed = false;
          input.IsHeld = false;
          input.IsReleased = true;
          input.WasReleasedOnLastCheck = true;
          input.WasPressedOnLastCheck = false;
          return;
        }

        // Case: button is up but was released last update
        if (input.WasReleasedOnLastCheck) {
          input.IsReleased = false;
          input.WasReleasedOnLastCheck = false;
        }
      }
    }

    private void updateMouseInputs() {
      var mouseState = Mouse.GetState();
      updateKeyInput(lmb, mouseState.LeftButton == ButtonState.Pressed);
      updateKeyInput(rmb, mouseState.RightButton == ButtonState.Pressed);
      updateKeyInput(mmb, mouseState.MiddleButton == ButtonState.Pressed);
    }

    private void updateMouseScroll() {
      scrolledUp = false;
      scrolledDown = false;

      var currentScrollValue = Mouse.GetState().ScrollWheelValue;
      if (currentScrollValue > oldScrollValue)
        scrolledUp = true;
      else if (currentScrollValue < oldScrollValue)
        scrolledDown = true;

      oldScrollValue = currentScrollValue;
    }

    public static InputManager getInstance() {
      if (instance == null)
        instance = new InputManager();
      return instance;
    }

    private void loadKeyInputs() {
      try {
        keyInputs = ContentUtils.LoadJson<List<KeyInputDataForManager>>(Config.Input.UserKeybindsJson);
        Log.log(LogCategory.Input, LogLevel.Info, "User keybinds file loaded.");
      } catch (FileNotFoundException) {
        Log.log(LogCategory.Input, LogLevel.Info, "No user keybinds file found. Loading default keybinds.");
        keyInputs = ContentUtils.LoadJson<List<KeyInputDataForManager>>(Config.Input.DefaultKeybindsJson);
      }

      Log.log(LogCategory.Input, LogLevel.Debug, "Loaded " + getInstance().keyInputs.Count + " key inputs");
    }

    private void updateKeyboardStates() {
      oldKeyboardState = keyboardState;
      keyboardState = Keyboard.GetState();
    }

    // package-private fields for C# 2020
    private class KeyInputDataForManager : KeyInputData {
      public new bool IsPressed { get { return isPressed; } set { isPressed = value; } }
      public new bool IsHeld { get { return isHeld; } set { isHeld = value; } }
      public new bool IsReleased { get { return isReleased; } set { isReleased = value; } }
      public bool WasPressedOnLastCheck { get { return wasPressed; } set { wasPressed = value; } }
      public bool WasReleasedOnLastCheck { get { return wasReleased; } set { wasReleased = value; } }
    }
  }
}