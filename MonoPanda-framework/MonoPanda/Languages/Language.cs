﻿using IniParser.Model;

using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.UserSettings;
using MonoPanda.Utils;

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MonoPanda.Languages {
  public class Language {
    private static Dictionary<string, string> languageData;

    /// <summary>
    /// Get a line from currently loaded language.
    /// </summary>
    public static string Get(string id) {
      Log.log(LogCategory.Language, LogLevel.Debug, "Get: " + id);
      if (languageData.ContainsKey(id))
        return languageData[id];

      Log.log(LogCategory.Language, LogLevel.Warn, "Line id: " + id + " is missing! Returning id.");
      return id;
    }

    public static void LoadLanguage(string language) {
      if (!GetLanguagesNames().Contains(language)) {
        if (language.Equals(Config.Language.Language)) {
          Log.log(LogCategory.Language, LogLevel.Info, "No default language file.");
          return;
        }

        Log.log(LogCategory.Language, LogLevel.Warn, "Requested language: " + language + " is missing. Loading default language!");
        LoadLanguage(Config.Language.Language);
        return;
      }

      Log.log(LogCategory.Language, LogLevel.Info, "Loading language: " + language);
      languageData = new Dictionary<string, string>();
      ModsUtils.ForEachModName(modName => loadMod(modName, language));

      Settings.Language.Language = language;
    }

    private static void loadMod(string mod, string language) {
      var languagesPath = ModsUtils.GetModPath(mod, Config.Language.LanguagesDirectory);
      if (!Directory.Exists(languagesPath))
        return;

      Log.log(LogCategory.Language, LogLevel.Info, "Loading languages from mod: " + mod);
      var activeLanguagePath = languagesPath + language + ".ini";
      if (File.Exists(activeLanguagePath)) {
        load(mod, activeLanguagePath);
      } else {
        Log.log(LogCategory.Language, LogLevel.Info, "Language: " + language + " not found for mod: " + mod + ". Loading default language.");
        load(mod, languagesPath + Config.Language.Language + ".ini");
      }
    }

    private static void load(string mod, string languageFilePath) {
      if (!File.Exists(languageFilePath))
        return;

      var iniData = IniUtils.getIniDataNoSections(languageFilePath);
      foreach (KeyData keyData in iniData.Sections.GetSectionData(IniUtils.FAKE_INI_SECTION_NAME).Keys) {
        if (!languageData.ContainsKey(keyData.KeyName)) {
          Log.log(LogCategory.Language, LogLevel.Debug, "Added line id: " + keyData.KeyName + ", value: " + keyData.Value);
          languageData.Add(keyData.KeyName, keyData.Value);
        } else {
          Log.log(LogCategory.Language, LogLevel.Info, "Mod: " + mod + " replaced line id: " + keyData.KeyName);
          Log.log(LogCategory.Language, LogLevel.Debug, "\nOld value: " + languageData[keyData.KeyName] + "\nNew Value: " + keyData.Value);
          languageData.Remove(keyData.KeyName);
          languageData.Add(keyData.KeyName, keyData.Value);
        }
      }
    }

    /// <summary>
    /// Returns all available languages from all mods.
    /// </summary>
    public static List<string> GetLanguagesNames() {
      List<string> languages = new List<string>();

      ModsUtils.ForEachModName(modName => {
        var langDirPath = ModsUtils.GetModPath(modName, Config.Language.LanguagesDirectory);
        if (Directory.Exists(langDirPath))
          foreach (string lang in Directory.GetFiles(langDirPath, "*.ini").Select(Path.GetFileName))
            if (!languages.Contains(lang.Replace(".ini", "")))
              languages.Add(lang.Replace(".ini", ""));
      });

      return languages;
    }
  }
}
