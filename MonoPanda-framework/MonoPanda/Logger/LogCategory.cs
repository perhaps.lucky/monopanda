﻿namespace MonoPanda.Logger {
  /// <summary>
  /// Categories of logs. Each category has a set LogLevel in loggers.json file. <br/>
  /// You can add new log categories by editing this enum and adding level information to loggers.json.
  /// </summary>
  public enum LogCategory {
    Startup, Timers, TimersSystem, Config, Ini, Test, Flags, Content, Input, MousePosition, Sound, Language, Mods, States, TextDrawing, DrawingSystems,
    ECS,
    EntityFactory,
    Spine,
    LightSystem,
    BehaviourSystem,
    CollisionDetection,
    CameraSystem,
    UI,
    Console,
    Repository,
    ThreadRequest
  }
}
