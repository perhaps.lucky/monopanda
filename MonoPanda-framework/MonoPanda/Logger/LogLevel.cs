﻿namespace MonoPanda.Logger {
  /// <summary>
  /// Log level describe how important the logged information is:<br/>
  /// <b>Off</b> - this should not be used for logging, unless you want information to be logged regardless of settings in loggers.json file<br/>
  /// <b>Error</b> - events that cause application crash<br/>
  /// <b>Warn</b> - events that aren't causing crash, but might cause wrong behaviour (e.g. content cannot be loaded)<br/>
  /// <b>Info</b> - purely informational logs, e.g. start of some system<br/>
  /// <b>Debug</b> - detailed informations that you don't want to be logged in normal execution<br/>
  /// <i>The above are just a suggestion of usage and you can freely edit the enum to match your needs, as long as you keep loggers.json in mind.<br/>
  /// <u>Note that level order in enum matters - setting one on right means that logs from ones on left will be received.</u></i>
  /// </summary>
  public enum LogLevel {
    Off, Error, Warn, Info, Debug
  }
}
