﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Utils;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MonoPanda.Mods {
  public class ModManager {
    private static ModManager instance;

    private List<ModSettings> modSettings;

    private ModManager() { }

    public static void initialize() {
      var instance = getInstance();
      instance.modSettings = ContentUtils.LoadJson<List<ModSettings>>(Config.Mods.ModsSettingsJson);
      RemoveDeletedMods();
      ScanForNewMods();
      SaveMods();
    }

    public static List<ModSettings> GetAllMods() {
      return getInstance().modSettings;
    }

    public static List<ModSettings> GetActiveMods(bool includeCoreMod = false) {
      if (!includeCoreMod)
        return GetAllMods().FindAll(m => m.Active);

      List<ModSettings> mods = new List<ModSettings>();
      mods.Add(new ModSettings(Config.Mods.CoreModFolderName, true));
      mods.AddRange(GetActiveMods());
      return mods;
    }

    /// <summary>
    /// Saves current list from mod manager. If change was made past-initialization, it will need restart to load changes.
    /// </summary>
    public static void SaveMods() {
      File.WriteAllText(Config.Mods.ModsSettingsJson, JsonConvert.SerializeObject(GetAllMods(), Formatting.Indented));
      Log.log(LogCategory.Mods, LogLevel.Info, "Mods settings saved from ModManager list.");
      Log.log(LogCategory.Mods, LogLevel.Debug, "Saved list: ", GetAllMods());
    }

    /// <summary>
    /// Saves list from parameter. Requires restart to make changes.
    /// </summary>
    public static void SaveMods(List<ModSettings> modSettings) {
      getInstance().modSettings = modSettings;
      File.WriteAllText(Config.Mods.ModsSettingsJson, JsonConvert.SerializeObject(modSettings, Formatting.Indented));
      Log.log(LogCategory.Mods, LogLevel.Info, "Mods settings saved from external list");
      Log.log(LogCategory.Mods, LogLevel.Debug, "Saved list: ", modSettings);
    }

    /// <summary>
    /// Scans for new mods and adds them to list in ModManager instance. <br/>
    /// Returns list with new mods.
    /// </summary>
    /// <param name="save">if true it will also save the list from instance</param>
    public static List<ModSettings> ScanForNewMods(bool save = false) {
      Log.log(LogCategory.Mods, LogLevel.Info, "Scanning for new mods...");
      List<ModSettings> newMods = new List<ModSettings>();
      foreach (string folderName in Directory.EnumerateDirectories(Config.Mods.ModsFolder).Select(Path.GetFileName)) {
        if (File.Exists(ModsUtils.GetModPath(folderName, Config.Mods.ModInfoJson))
          && getInstance().modSettings.Find(m => m.Name == folderName) == null) {
          Log.log(LogCategory.Mods, LogLevel.Info, "Found new mod: " + folderName);
          var mod = new ModSettings(folderName, false);
          newMods.Add(mod);
          getInstance().modSettings.Add(mod);
        }
      }

      if (save && newMods.Count > 0) SaveMods();
      return newMods;
    }

    public static ModInfo GetModInfo(string modName) {
      string path = ModsUtils.GetModPath(modName, Config.Mods.ModInfoJson);
      if (File.Exists(path))
        return ContentUtils.LoadJson<ModInfo>(ModsUtils.GetModPath(modName, Config.Mods.ModInfoJson));

      Log.log(LogCategory.Mods, LogLevel.Warn, "Mod info not found for mod name: " + modName + ". Returning empty object!");
      return new ModInfo();
    }

    public static ModManager getInstance() {
      if (instance == null)
        instance = new ModManager();
      return instance;
    }

    /// <summary>
    /// Removes mods that are on the list, but they are missing either folder or mod-info.json file in mod folder.
    /// </summary>
    public static void RemoveDeletedMods() {
      List<ModSettings> remove = new List<ModSettings>();
      foreach (string modName in getInstance().modSettings.Select(m => m.Name)) {
        if (!Directory.Exists(Config.Mods.ModsFolder + modName)
          || !File.Exists(ModsUtils.GetModPath(modName, Config.Mods.ModInfoJson))) {
          Log.log(LogCategory.Mods, LogLevel.Info, "Mod named: " + modName + " is missing in file system and will be deleted from mod list.");
          var mod = GetAllMods().Find(m => m.Name == modName);
          remove.Add(mod);
        }
      }

      GetAllMods().RemoveAll(m => remove.Contains(m));
      SaveMods();
    }
  }
}
