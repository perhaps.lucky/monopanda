﻿using Priority_Queue;

namespace MonoPanda.Multithreading {
  public class ThreadRequest : FastPriorityQueueNode {
    public bool Processing { get; set; }
    public bool InQueue { get; set; }
    public bool Finished { get; set; }
  }
}
