﻿using MonoPanda.Logger;

using Priority_Queue;
using System.Collections.Generic;
using System.Threading;
using System;

namespace MonoPanda.Multithreading {
  public abstract class ThreadRequestSystem {
    private static List<ThreadRequestSystem> threadRequestSystems = new List<ThreadRequestSystem>();

    private List<SimplePriorityQueue<ThreadRequest>> queues;
    private List<Thread> threads;
    private int maxQueues;
    private ThreadPriority threadPriority;

    public void Initialize(int threadsMax = 1, ThreadPriority threadPriority = default) {
      if (!threadRequestSystems.Contains(this))
        threadRequestSystems.Add(this);

      maxQueues = threadsMax;
      this.threadPriority = threadPriority;

      queues = new List<SimplePriorityQueue<ThreadRequest>>();
      threads = new List<Thread>();
    }

    public void QueueRequest(ThreadRequest request) {
      if (!Accepts(request)) {
        Log.log(LogCategory.ThreadRequest, LogLevel.Warn, $"Request not accepted by ThreadRequestSystem!");
      }
      
      request.Finished = false; // ensure so it won't be true if used again
      request.InQueue = true;

      int queueId = findQueueIdWithFewestElements();
      if (queues.Count - 1 < queueId) {
        queues.Add(new SimplePriorityQueue<ThreadRequest>());
        threads.Add(new Thread(Run));
      }

      queues[queueId].Enqueue(request, request.Priority);
      if (!threads[queueId].IsAlive) {
        threads[queueId] = new Thread(Run);
        threads[queueId].Start(queueId);
        threads[queueId].Priority = threadPriority;
      }
    }

    public void Run(object queueId) {
      var queue = queues[(int)queueId];
      while (true) {
        if (queue.Count == 0)
          return;

        ThreadRequest request = queue.Dequeue();
        request.Processing = true;
        Process(request);
        request.Processing = false;
        request.Finished = true;
        request.InQueue = false;
      }
    }

    public void Stop() {
      foreach (var thread in threads) {
        if (thread.IsAlive)
          thread.Abort();
      }
    }

    public static void StopAllSystems() {
      foreach (var system in threadRequestSystems)
        system.Stop();
    }

    protected abstract bool Accepts(ThreadRequest request);
    protected abstract void Process(ThreadRequest request);

    private int findQueueIdWithFewestElements() {
      if (queues.Count < maxQueues)
        return queues.Count;

      int max = int.MaxValue;
      int id = -1;
      foreach (SimplePriorityQueue<ThreadRequest> queue in queues) {
        if (queue.Count == 0)
          return queues.IndexOf(queue);

        if (queue.Count < max) {
          max = queue.Count;
          id = queues.IndexOf(queue);
        }
      }
      return id;
    }
  }
}
