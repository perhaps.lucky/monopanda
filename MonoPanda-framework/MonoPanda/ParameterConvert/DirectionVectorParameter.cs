﻿using MonoPanda.Utils;

namespace MonoPanda.ParameterConvert {
  public sealed class DirectionVectorParameter : ParameterConvertAttribute {
    public override object Convert(string parameterValue) {
      return ParameterUtils.DirectionStringToVector(parameterValue);
    }
  }
}
