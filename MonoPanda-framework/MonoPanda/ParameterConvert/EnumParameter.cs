﻿using System;

namespace MonoPanda.ParameterConvert {
  public class EnumParameter: ParameterConvertAttribute{

    private Type enumType;

    public EnumParameter(Type enumType) {
      this.enumType = enumType;
    }

    public override object Convert(string parameterValue) {
      return Enum.Parse(enumType, parameterValue);
    }
  }
}
