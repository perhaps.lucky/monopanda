﻿using Newtonsoft.Json;

using System;
using System.Reflection;

namespace MonoPanda.ParameterConvert {
  public class JArrayParameter : ParameterConvertAttribute {

    private Type objectType;

    public JArrayParameter(Type objectType) {
      this.objectType = objectType;
    }

    public override object Convert(string parameterValue) {
      MethodInfo method = typeof(JsonConvert).GetMethod("DeserializeObject", 1, new Type[] { typeof(string) });
      method = method.MakeGenericMethod(objectType);
      return method.Invoke(null, new object[] { parameterValue });
    }
  }
}
