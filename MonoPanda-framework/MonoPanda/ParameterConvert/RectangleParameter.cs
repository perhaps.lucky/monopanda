﻿using Microsoft.Xna.Framework;

using MonoPanda.Utils;

namespace MonoPanda.ParameterConvert {
  public sealed class RectangleParameter : ParameterConvertAttribute {
    public override object Convert(string parameterValue) {
      return ParameterUtils.GetRectangle(parameterValue);
    }
  }
}