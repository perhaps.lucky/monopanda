﻿using MonoPanda.Utils;

using System;
using System.Reflection;

namespace MonoPanda.ParameterConvert {
  public sealed class StaticFieldParameter : ParameterConvertAttribute {

    private Type type;

    public StaticFieldParameter(Type type) {
      this.type = type;
    }

    public override object Convert(string parameterValue) {
      MethodInfo method = typeof(ReflectionUtils).GetMethod(nameof(ReflectionUtils.GetStaticField));
      MethodInfo generic = method.MakeGenericMethod(type);
      object[] parameters = { type, parameterValue };
      return generic.Invoke(null, parameters);
    }
  }
}
