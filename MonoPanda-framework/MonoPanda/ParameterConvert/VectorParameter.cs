﻿using MonoPanda.Utils;

namespace MonoPanda.ParameterConvert {
  public sealed class VectorParameter : ParameterConvertAttribute {
    public override object Convert(string parameterValue) {
      return ParameterUtils.GetVector2(parameterValue);
    }
  }
}
