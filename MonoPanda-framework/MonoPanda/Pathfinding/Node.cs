﻿using System.Collections.Generic;

namespace MonoPanda.Pathfinding {
  public class Node {
    public List<Edge> Edges { get; set; }
    public int IsleID { get; set; }
    public int ID { get; private set; }
    public object Data { get; set; }

    public Node(int id) {
      Edges = new List<Edge>();
      ID = id;
    }
  }
}
