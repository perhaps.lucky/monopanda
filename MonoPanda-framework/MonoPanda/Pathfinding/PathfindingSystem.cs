﻿using MonoPanda.Multithreading;

using System;
using System.Collections.Generic;
using System.Linq;

namespace MonoPanda.Pathfinding {
  public class PathfindingSystem : ThreadRequestSystem {
    private List<Node> nodes;

    private int NewIsleID {
      get {
        currentIsleID++;
        return currentIsleID;
      }
    }

    private int currentIsleID;

    private int NewNodeID {
      get {
        currentNodeID++;
        return currentNodeID;
      }
    }

    private int currentNodeID;
    private Heuristic heuristic;
    public List<int> DeactivatedNodes { get; private set; }

    public PathfindingSystem(Heuristic heuristic) {
      nodes = new List<Node>();
      this.heuristic = heuristic;
      DeactivatedNodes = new List<int>();
    }

    public Node AddNode(Node parent = null, int cost = 1) {
      Node node = new Node(NewNodeID);
      node.IsleID = parent == null || DeactivatedNodes.Contains(parent.ID) ? NewIsleID : parent.IsleID;
      nodes.Add(node);
      if (parent != null)
        CreateEdge(node, parent, cost);
      return node;
    }

    public Node AddNode(int parentID, int cost = 1) {
      Node parent = GetNode(parentID);
      return AddNode(parent, cost);
    }

    /// <summary>
    /// Node added by this method should previously exist in system.<br/>
    /// If node has saved edges, they will recreated.<br/>
    /// It rememebers last existing nodes, so it's not good for switching nodes on/off.
    /// </summary>
    public Node AddPreviouslyExistingNode(Node node) {
      nodes.Add(node);
      List<Edge> oldEdges = node.Edges;
      node.Edges = new List<Edge>();
      foreach (Edge oldEdge in oldEdges) {
        var otherEnd = oldEdge.GetOtherEnd(node);
        if (GetNode(otherEnd.ID) != null) // check if it currently exist in system
          CreateEdge(otherEnd, node);
      }

      return node;
    }

    public Edge CreateEdge(Node from, Node to, int cost = 1) {
      Edge edge = new Edge(from, to, cost);
      from.Edges.Add(edge);
      to.Edges.Add(edge);
      if (from.IsleID != to.IsleID)
        fillIsle(to, from.IsleID);
      return edge;
    }

    public Edge CreateEdge(int fromID, int toID, int cost = 1) {
      Node from = GetNode(fromID);
      Node to = GetNode(toID);
      return CreateEdge(from, to, cost);
    }

    public void RemoveEdge(Edge edge) {
      edge.To.Edges.Remove(edge);
      edge.From.Edges.Remove(edge);
      if (!canReach(edge.From, edge.To))
        fillIsle(edge.To, NewIsleID);
    }

    public void RemoveNode(Node node) {
      foreach (Edge edge in node.Edges) {
        edge.GetOtherEnd(node).Edges.Remove(edge);
      }

      nodes.Remove(node);
      checkIsles(getNeighbourNodes(node));
    }

    public void RemoveNode(int id) {
      Node node = GetNode(id);
      RemoveNode(node);
    }

    public void RemoveNode(Predicate<Node> predicate) {
      Node node = GetNode(predicate);
      RemoveNode(node);
    }

    public void DeactivateNode(Node node) {
      DeactivatedNodes.Add(node.ID);
      checkIsles(getNeighbourNodes(node));
    }

    public void ActivateNode(Node node) {
      DeactivatedNodes.Remove(node.ID);
      List<Node> neighbours = getNeighbourNodes(node);
      // Find neighbour(if present) and copy his IsleID 
      // (prevents situation where all neighbours have same IsleID that is different
      // than activated node's IsleID = no fill gets called and node ends with old id)
      Optional<Node>.Of(neighbours.Find(n => !DeactivatedNodes.Contains(n.ID)))
        .IfPresent(neighbour => node.IsleID = neighbour.IsleID);
      checkIsles(neighbours);
    }

    public Node GetNode(int ID) {
      return nodes.Find(n => n.ID == ID);
    }

    public Node GetNode(Predicate<Node> predicate) {
      return nodes.Find(predicate);
    }

    protected override bool Accepts(ThreadRequest request) {
      return request is PathfindingRequest;
    }

    protected override void Process(ThreadRequest request) {
      var pathfindingRequest = request as PathfindingRequest;
      SearchInstance searchInstance =
        new SearchInstance(pathfindingRequest.Start, pathfindingRequest.Target, this, heuristic);
      searchInstance.Search();

      pathfindingRequest.FoundPath = searchInstance.FoundPath;
      pathfindingRequest.Path.Clear();
      foreach (int nodeID in searchInstance.Path) {
        pathfindingRequest.Path.Add(GetNode(nodeID));
      }
    }

    private void fillIsle(Node startNode, int isleID) {
      Queue<Node> nodesQueue = new Queue<Node>();
      nodesQueue.Enqueue(startNode);
      while (nodesQueue.Count > 0) {
        Node currentNode = nodesQueue.Dequeue();
        currentNode.IsleID = isleID;
        foreach (Node neighbour in currentNode.Edges.Select(n => n.GetOtherEnd(currentNode))) {
          if (neighbour.IsleID != isleID
              && !DeactivatedNodes.Contains(neighbour.ID)
              && !nodesQueue.Contains(neighbour))
            nodesQueue.Enqueue(neighbour);
        }
      }
    }

    private bool canReach(Node from, Node to) {
      SearchInstance searchInstance = new SearchInstance(from, to, this, heuristic);
      searchInstance.Search(true);
      return searchInstance.FoundPath;
    }

    private List<Node> getNeighbourNodes(Node node) {
      List<Node> neighbours = new List<Node>();
      foreach (Edge edge in node.Edges) {
        var neighbourNode = edge.GetOtherEnd(node);
        if (!DeactivatedNodes.Contains(neighbourNode.ID))
          neighbours.Add(neighbourNode);
      }

      return neighbours;
    }

    private void checkIsles(List<Node> nodes) {
      foreach (Node one in nodes) {
        foreach (Node another in nodes) {
          if (one.IsleID != another.IsleID) {
            if (canReach(one, another))
              fillIsle(another, one.IsleID);
          } else {
            if (!canReach(one, another))
              fillIsle(another, NewIsleID);
          }
        }
      }
    }
  }
}