﻿using System;
using System.IO;

namespace MonoPanda {
  public static class Program {

    [STAThread]
    static void Main() {
      using (var game = new GameMain()) {
#if DEBUG
        game.Run();
#else
        try {
          game.Run();
        } catch (Exception e) {
          StreamWriter crashFile = new StreamWriter("crash.txt");
          crashFile.WriteLine("Exception type: " + e.GetType().ToString());
          crashFile.WriteLine("Message: " + e.Message);
          crashFile.WriteLine("Stack trace:\n" + e.StackTrace);
          crashFile.Close();
        }
#endif
      }
    }
  }
}
