﻿using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.ParameterConvert;
using MonoPanda.Utils;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace MonoPanda.Repository {
  public abstract class Repository<T> where T : RepositoryEntity {

    protected List<T> entities = new List<T>();

    public static void CreateRepository<R>(string jsonPath) where R : Repository<T>, new() {
      R instance = new R();
      instance.load(jsonPath);
    }

    public T GetInner(string id) {
      return entities.Find(e => id.Equals(e.Id));
    }

    public T this[string key] { get => GetInner(key); }

    private void load(string jsonPath) {
      List<Dictionary<string, object>> dictionaries = new List<Dictionary<string, object>>();
      ModsUtils.ForEachModFile(jsonPath, filePath => loadMod(filePath, dictionaries));
      convertDictionariesToEntities(dictionaries);
      initialize();
      setInstance(this);
    }

    private void initialize() {
      foreach (T entity in entities) {
        entity.Initialize();
      }
    }

    private void loadMod(string filePath, List<Dictionary<string, object>> targetDictionaries) {
      List<Dictionary<string, object>> loadedDictionaries = ContentUtils.LoadJson<List<Dictionary<string, object>>>(filePath);
      foreach (Dictionary<string, object> dictionary in loadedDictionaries) {
        loadDictionary(dictionary, targetDictionaries);
      }
    }

    private void loadDictionary(Dictionary<string, object> dictionary, List<Dictionary<string, object>> targetDictionaries) {
      Dictionary<string, object> existingDictionary = targetDictionaries.Find(e => e["Id"].Equals(dictionary["Id"]));
      if (existingDictionary != null) {
        foreach (KeyValuePair<string, object> keyValue in dictionary) {
          existingDictionary[keyValue.Key] = keyValue.Value;
        }
      } else {
        targetDictionaries.Add(dictionary);
      }
    }

    private void convertDictionariesToEntities(List<Dictionary<string, object>> dictionaries) {
      foreach (Dictionary<string, object> dictionary in dictionaries) {
        entities.Add(convertDictionaryToEntity(dictionary));
      }
    }

    private T convertDictionaryToEntity(Dictionary<string, object> dictionary) {
      T entity = (T)Activator.CreateInstance(typeof(T));
      foreach (KeyValuePair<string, object> property in dictionary) {
        applyProperty(property.Key, property.Value, entity);
      }
      return entity;
    }

    private void applyProperty(string propertyName, object value, T target) {
      PropertyInfo propertyInfo = typeof(T).GetProperty(propertyName);
      if (propertyInfo == null) {
        Log.log(LogCategory.Repository, LogLevel.Warn, "Not found property: " + propertyName + " for class: " + typeof(T).FullName + "[Id: " + target.Id + "]");
        return;
      }

      ParameterConvertAttribute attribute = propertyInfo.GetCustomAttribute<ParameterConvertAttribute>();
      if (attribute != null) {
        propertyInfo.SetValue(target, attribute.Convert(value.ToString()));
        return;
      }

      propertyInfo.SetValue(target, Convert.ChangeType(value, propertyInfo.PropertyType));
    }

    protected abstract void setInstance(Repository<T> repository);
  }
}