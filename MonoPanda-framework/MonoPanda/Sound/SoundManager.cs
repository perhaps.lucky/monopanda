﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

using MonoPanda.Configuration;
using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.Mods;
using MonoPanda.UserSettings;
using MonoPanda.Utils;

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MonoPanda.Sound {
  public class SoundManager {

    private static SoundManager instance;

    private Dictionary<string, int> volumesDictionary;
    private string currentlyPlayedSong;

    public static void initialize() {
      var instance = getInstance();
      instance.volumesDictionary = new Dictionary<string, int>();
      ModsUtils.ForEachModName(modName => loadMod(modName));
    }

    private static SoundManager getInstance() {
      if (instance == null)
        instance = new SoundManager();
      return instance;
    }

    /// <summary>
    /// Plays sound effect without considering 3D effect.
    /// </summary>
    public static SoundEffectInstance PlaySFX(string id) {
      Log.log(LogCategory.Sound, LogLevel.Info, "Playing sfx: " + id);
      var instance = ContentManager.Get<SoundEffect>(id, true).CreateInstance();
      instance.Volume = 1.0f * (Settings.Sound.SFXVolume / 100.0f) * getVolumeFromDictionary(id);
      Log.log(LogCategory.Sound, LogLevel.Debug, "Effective SFX volume: " + instance.Volume);
      instance.Play();
      return instance;
    }

    /// <summary>
    /// Plays music.
    /// </summary>
    public static void PlayMusic(string id, bool loop = true) {
      Log.log(LogCategory.Sound, LogLevel.Info, "Playing music: " + id + (loop ? "[LOOP]" : ""));
      StopMusic();

      // volume of MediaPlayer is changed each time to match combination of individual track-settings and global user-settings.
      MediaPlayer.Volume = 1.0f * (Settings.Sound.MusicVolume / 100.0f) * getVolumeFromDictionary(id);
      Log.log(LogCategory.Sound, LogLevel.Debug, "Effective music volume: " + MediaPlayer.Volume);

      var song = ContentManager.Get<Song>(id, true);
      MediaPlayer.Play(song);
      MediaPlayer.IsRepeating = loop;
      getInstance().currentlyPlayedSong = id;
    }

    public static void StopMusic() {
      if (MediaPlayer.State == MediaState.Playing) {
        MediaPlayer.Volume = 0; // MediaPlayer.Stop() does ugly cut sound at the end, and Volume is set each time music starts anyway
        getInstance().currentlyPlayedSong = null;
      }
    }

    public static string GetCurrentlyPlayedSongId() {
      return getInstance().currentlyPlayedSong;
    }

    /// <summary>
    /// Sets current sound effect volume. <b>Does not save in user settings (use <i>Settings.save()</i>)</b>
    /// </summary>
    public static void SetSFXVolume(int volume) {
      Log.log(LogCategory.Sound, LogLevel.Info, "SFX volume set to: " + volume);
      Settings.Sound.SFXVolume = volume;
    }

    /// <summary>
    /// Sets current music volume. <b>Does not save in user settings (use <i>Settings.save()</i>)</b>
    /// </summary>
    public static void SetMusicVolume(int volume) {
      Log.log(LogCategory.Sound, LogLevel.Info, "Music volume set to: " + volume);
      Settings.Sound.MusicVolume = volume;

      if (GetCurrentlyPlayedSongId() != null)
        MediaPlayer.Volume = 1.0f * (Settings.Sound.MusicVolume / 100.0f) * getVolumeFromDictionary(GetCurrentlyPlayedSongId());
    }

    public static void AdjustSFXVolume(int change) {
      Settings.Sound.SFXVolume += change;

      if (Settings.Sound.SFXVolume > 100)
        Settings.Sound.SFXVolume = 100;

      if (Settings.Sound.SFXVolume < 0)
        Settings.Sound.SFXVolume = 0;

      Log.log(LogCategory.Sound, LogLevel.Info, "SFX volume set to: " + Settings.Sound.SFXVolume);
    }

    public static void AdjustMusicVolume(int change) {
      Settings.Sound.MusicVolume += change;

      if (Settings.Sound.MusicVolume > 100)
        Settings.Sound.MusicVolume = 100;

      if (Settings.Sound.MusicVolume < 0)
        Settings.Sound.MusicVolume = 0;

      if (GetCurrentlyPlayedSongId() != null)
        MediaPlayer.Volume = 1.0f * (Settings.Sound.MusicVolume / 100.0f) * getVolumeFromDictionary(GetCurrentlyPlayedSongId());
    }

    private static float getVolumeFromDictionary(string id) {
      var dictionary = getInstance().volumesDictionary;
      if (dictionary.ContainsKey(id)) {
        if (dictionary[id] < 0) {
          Log.log(LogCategory.Sound, LogLevel.Warn, "Value in sound volume dictionary is lower than 0! 1.0f will be returned (id: " + id + ")");
          return 1.0f;
        }
        return dictionary[id] / 100.0f;
      }
      return 1.0f;
    }

    private static void loadMod(string modName) {
      string fullPath = ModsUtils.GetModPath(modName, Config.Sound.SoundVolumeJsonPath);
      if (File.Exists(fullPath)) {
        Dictionary<string, int> modDictionary = ContentUtils.LoadJson<Dictionary<string, int>>(fullPath);

        foreach (KeyValuePair<string, int> keyValue in modDictionary) {
          instance.volumesDictionary[keyValue.Key] = keyValue.Value;
        }
      }
    }


  }
}
