﻿namespace MonoPanda.Spine.MonoPanda {
  public class ExternalAttachment {
    public string SlotName { get; set; }
    public float X { get; set; }
    public float Y { get; set; }
    public float Rotation { get; set; }
    public float Width { get; set; }
    public float Height { get; set; }
    public float ScaleX { get; set; } = 1.0f;
    public float ScaleY { get; set; } = 1.0f;

    public string ContentId { get; set; }
    public string AtlasFileName { get; set; }
    
    public string MeshSkinName { get; set; }
  }
}
