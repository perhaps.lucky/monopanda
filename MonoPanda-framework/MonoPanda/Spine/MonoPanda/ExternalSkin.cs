﻿using System.Collections.Generic;

namespace MonoPanda.Spine.MonoPanda {
  public class ExternalSkin {
    public string Name { get; set; }
    public List<ExternalAttachment> Attachments { get; set; } = new List<ExternalAttachment>();
  }
}
