﻿using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Content;
using MonoPanda.SpriteSheets;

using Spine;

namespace MonoPanda.Spine.MonoPanda {
  public class HackRendererObject {
    public string ContentId { get; set; }
    public string AtlasFileName { get; set; }

    private bool isSpriteSheet;

    public static HackRendererObject FromExternalAttachment(ExternalAttachment externalAttachment) {
      HackRendererObject o = new HackRendererObject();
      o.ContentId = externalAttachment.ContentId;
      o.AtlasFileName = externalAttachment.AtlasFileName != null
        ? externalAttachment.AtlasFileName
        : externalAttachment.SlotName;
      o.isSpriteSheet = ContentManager.getItem(externalAttachment.ContentId).ItemType == ContentItemType.SpriteSheet;
      return o;
    }

    public Texture2D GetTexture() {
      if (!isSpriteSheet) {
        return ContentManager.Get<Texture2D>(ContentId);
      }

      var spriteSheet = ContentManager.Get<SpriteSheet>(ContentId);
      if (spriteSheet != null) {
        return spriteSheet.Texture;
      }

      return null;
    }

    public void CountUvUs(RegionAttachment regionAttachment) {
      if (!isSpriteSheet) {
        regionAttachment.SetUVs(0, 0, 1, 1, false);
        return;
      }

      SpriteSheet spriteSheet = ContentManager.Get<SpriteSheet>(ContentId, true);
      SpriteSheetFrame spriteSheetFrame =
        spriteSheet.SpriteSheetDescription.frames.Find(f => f.filename.Equals(AtlasFileName));
      float u = spriteSheetFrame.frame.x / (float)spriteSheet.Texture.Width;
      float v = spriteSheetFrame.frame.y / (float)spriteSheet.Texture.Height;
      float u2 = (spriteSheetFrame.frame.x + spriteSheetFrame.frame.w) / (float)spriteSheet.Texture.Width;
      float v2 = (spriteSheetFrame.frame.y + spriteSheetFrame.frame.h) / (float)spriteSheet.Texture.Height;
      regionAttachment.SetUVs(u, v, u2, v2, false);
    }

    public void CountUvUs(MeshAttachment meshAttachment, MeshAttachment sourceMesh) {
      UvURegion sourceRegion = UvURegion.FromAtlasRegion((AtlasRegion)sourceMesh.RendererObject);
      UvURegion spriteSheetRegion = getSpriteSheetRegion();
      
      float[] uwus = new float[sourceMesh.UVs.Length];
      for (int i = 0; i < meshAttachment.UVs.Length; i++) {
        var isU = i % 2 == 0;
        var currentValue = meshAttachment.UVs[i];

        // count U or V value inside frame in spine atlas
        var currentValueMinusMin = currentValue - (isU ? sourceRegion.minU : sourceRegion.minV);
        var maxValueMinusMin = (isU ? sourceRegion.maxU : sourceRegion.maxV) - (isU ? sourceRegion.minU : sourceRegion.minV);
        var newValue = currentValueMinusMin / maxValueMinusMin;

        if (!isSpriteSheet) {
          // if not sprite sheet, then file should be same as atlas frame
          uwus[i] = newValue;
          continue;
        }

        // count U or V position in spritesheet
        var valueInSpriteSheet =
          newValue * ((isU ? spriteSheetRegion.maxU : spriteSheetRegion.maxV) -
                      (isU ? spriteSheetRegion.minU : spriteSheetRegion.minV)) +
          (isU ? spriteSheetRegion.minU : spriteSheetRegion.minV);
        uwus[i] = valueInSpriteSheet;
      }

      meshAttachment.UVs = uwus;
    }

    private UvURegion getSpriteSheetRegion() {
      if (isSpriteSheet) {
        SpriteSheet spriteSheet = ContentManager.Get<SpriteSheet>(ContentId, true);
        SpriteSheetFrame spriteSheetFrame =
          spriteSheet.SpriteSheetDescription.frames.Find(f => f.filename.Equals(AtlasFileName));
        return UvURegion.FromSpriteSheetFrame(spriteSheetFrame, spriteSheet);
      }

      return UvURegion.Default();
    }

    private struct UvURegion {
      public float minU, minV, maxU, maxV;

      public static UvURegion Default() {
        return new UvURegion { minU = 0, minV = 0, maxU = 1, maxV = 1 };
      }

      public static UvURegion FromAtlasRegion(AtlasRegion region) {
        return new UvURegion { minU = region.u, maxU = region.u2, minV = region.v, maxV = region.v2 };
      }

      public static UvURegion FromSpriteSheetFrame(SpriteSheetFrame frame, SpriteSheet spriteSheet) {
        return new UvURegion {
          minU = frame.frame.x / (float)spriteSheet.Texture.Width,
          maxU = (frame.frame.x + frame.frame.w) / (float)spriteSheet.Texture.Width,
          minV = frame.frame.y / (float)spriteSheet.Texture.Height,
          maxV = (frame.frame.y + frame.frame.h) / (float)spriteSheet.Texture.Height
        };
      }
    }
  }
}