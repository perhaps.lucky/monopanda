﻿using Microsoft.Xna.Framework.Graphics;

using MonoPanda.Content;
using MonoPanda.Logger;
using MonoPanda.SpriteSheets;
using MonoPanda.Utils;

using Spine;

using System;
using System.Collections.Generic;
using System.IO;

namespace MonoPanda.Spine.MonoPanda {
  public class SpineAsset : IDisposable {
    public SkeletonData SkeletonData { get; private set; }
    private Atlas atlas;

    public SpineAsset(string atlasPath, string relativePath) {
      atlas = new Atlas(atlasPath, new XnaTextureLoader(GameMain.GraphicsDeviceManager.GraphicsDevice));
      SkeletonJson skeletonJson = new SkeletonJson(atlas);
      SkeletonData = skeletonJson.ReadSkeletonData(atlasPath.Replace(".atlas", ".json"));
      addExternalSkins(relativePath.Replace(".atlas", "_skins.json"));
    }

    public void Dispose() {
      if (atlas != null)
        atlas.Dispose();
    }

    private void addExternalSkins(string externalSkinsPath) {
      List<ExternalSkin> skins = loadExternalSkins(externalSkinsPath);
      foreach (ExternalSkin externalSkin in skins) {
        Skin skin = createFromExternalSkin(externalSkin);
        SkeletonData.Skins.Add(skin);
      }
    }

    private Skin createFromExternalSkin(ExternalSkin externalSkin) {
      Skin skin = new Skin(externalSkin.Name);
      foreach (ExternalAttachment attachment in externalSkin.Attachments) {
        addAttachmentToSkin(attachment, skin);
      }

      return skin;
    }

    private void addAttachmentToSkin(ExternalAttachment externalAttachmentData, Skin skin) {
      Attachment attachment;
      if (externalAttachmentData.MeshSkinName == null) {
        attachment = createRegionAttachment(externalAttachmentData);
      } else {
        attachment = createMeshAttachment(externalAttachmentData);
      }

      int slotIndex = SkeletonData.FindSlotIndex(externalAttachmentData.SlotName);
      skin.SetAttachment(slotIndex++, externalAttachmentData.SlotName, attachment);
    }

    private RegionAttachment createRegionAttachment(ExternalAttachment attachment) {
      RegionAttachment regionAttachment = new RegionAttachment(attachment.SlotName);

      regionAttachment.Width = regionAttachment.RegionWidth = regionAttachment.RegionOriginalWidth =
        attachment.Width > 0 ? attachment.Width : getAttachmentWidth(attachment); // no idea what i'm doing
      regionAttachment.Height = regionAttachment.RegionHeight = regionAttachment.RegionOriginalHeight =
        attachment.Height > 0 ? attachment.Height : getAttachmentHeight(attachment);
      regionAttachment.X = attachment.X;
      regionAttachment.Y = attachment.Y;
      regionAttachment.Rotation = attachment.Rotation;
      HackRendererObject hackRendererObject = HackRendererObject.FromExternalAttachment(attachment);
      regionAttachment.RendererObject = hackRendererObject;
      hackRendererObject.CountUvUs(regionAttachment);
      regionAttachment.ScaleX = attachment.ScaleX;
      regionAttachment.ScaleY = attachment.ScaleY;
      regionAttachment.UpdateOffset();
      return regionAttachment;
    }

    private MeshAttachment createMeshAttachment(ExternalAttachment attachment) {
      MeshAttachment meshAttachment = new MeshAttachment(attachment.SlotName);

      HackRendererObject hackRendererObject = HackRendererObject.FromExternalAttachment(attachment);
      meshAttachment.RendererObject = hackRendererObject;

      Skin copyFromSkin = SkeletonData.Skins.Find(s => s.Name.Equals(attachment.MeshSkinName));
      int slotIndex = SkeletonData.FindSlotIndex(attachment.SlotName);
      MeshAttachment copyFromAttachment = (MeshAttachment)copyFromSkin.GetAttachment(slotIndex, attachment.SlotName);

      // copy everything
      meshAttachment.UVs = copyFromAttachment.UVs;
      meshAttachment.Triangles = copyFromAttachment.Triangles;
      meshAttachment.Edges = copyFromAttachment.Edges;
      meshAttachment.Vertices = copyFromAttachment.Vertices;
      meshAttachment.HullLength = copyFromAttachment.HullLength;
      meshAttachment.RegionOffsetX = copyFromAttachment.RegionOffsetX;
      meshAttachment.RegionOffsetY = copyFromAttachment.RegionOffsetY;
      meshAttachment.WorldVerticesLength = copyFromAttachment.WorldVerticesLength;
      meshAttachment.Bones = copyFromAttachment.Bones;
      meshAttachment.RegionDegrees = copyFromAttachment.RegionDegrees;
      meshAttachment.RegionRotate = copyFromAttachment.RegionRotate;
      meshAttachment.RegionU = copyFromAttachment.RegionU;
      meshAttachment.RegionU2 = copyFromAttachment.RegionU2;
      meshAttachment.RegionUVs = copyFromAttachment.RegionUVs;
      meshAttachment.RegionV = copyFromAttachment.RegionV;
      meshAttachment.RegionV2 = copyFromAttachment.RegionV2;
      meshAttachment.Height = copyFromAttachment.Height;
      meshAttachment.RegionHeight = copyFromAttachment.RegionHeight;
      meshAttachment.RegionOriginalHeight = copyFromAttachment.RegionOriginalHeight;
      meshAttachment.Width = copyFromAttachment.Width;
      meshAttachment.RegionWidth = copyFromAttachment.RegionWidth;
      meshAttachment.RegionOriginalWidth = copyFromAttachment.RegionOriginalWidth;
      
      hackRendererObject.CountUvUs(meshAttachment, copyFromAttachment);

      return meshAttachment;
    }

    private List<ExternalSkin> loadExternalSkins(string externalSkinsPath) {
      List<ExternalSkin> finalList = new List<ExternalSkin>();
      ModsUtils.ForEachModName(modName => {
        string path = ModsUtils.GetModPath(modName, externalSkinsPath);
        if (!File.Exists(path))
          return;

        List<ExternalSkin> modList = ContentUtils.LoadJson<List<ExternalSkin>>(path);
        applySkins(modList, finalList);
      });
      return finalList;
    }

    private void applySkins(List<ExternalSkin> source, List<ExternalSkin> target) {
      foreach (ExternalSkin skin in source) {
        var existingSkin = target.Find(s => s.Name.Equals(skin.Name));
        if (existingSkin != null)
          target.Remove(existingSkin);

        target.Add(skin);
      }
    }

    private float getAttachmentWidth(ExternalAttachment attachment) {
      ContentItem contentItem = ContentManager.getItem(attachment.ContentId);
      if (contentItem != null) {
        contentItem.load();
        switch (contentItem.ItemType) {
          case ContentItemType.SpriteSheet:
            return contentItem.getItem<SpriteSheet>().SpriteSheetDescription.frames.Find(f =>
                f.filename.Equals(attachment.AtlasFileName != null ? attachment.AtlasFileName : attachment.SlotName))
              .sourceSize.w;
          case ContentItemType.Texture2D:
            return contentItem.getItem<Texture2D>().Width;
        }

        Log.log(LogCategory.Spine, LogLevel.Warn,
          "Content item: " + attachment.ContentId +
          " is incorrect type for external attachment. Width will be set to 0.");
        return 0;
      }

      Log.log(LogCategory.Spine, LogLevel.Warn,
        "Content item: " + attachment.ContentId + " not found. Width will be set to 0.");
      return 0;
    }

    private float getAttachmentHeight(ExternalAttachment attachment) {
      ContentItem contentItem = ContentManager.getItem(attachment.ContentId);
      if (contentItem != null) {
        contentItem.load();
        switch (contentItem.ItemType) {
          case ContentItemType.SpriteSheet:
            return contentItem.getItem<SpriteSheet>().SpriteSheetDescription.frames.Find(f =>
                f.filename.Equals(attachment.AtlasFileName != null ? attachment.AtlasFileName : attachment.SlotName))
              .sourceSize.h;
          case ContentItemType.Texture2D:
            return contentItem.getItem<Texture2D>().Height;
        }

        Log.log(LogCategory.Spine, LogLevel.Warn,
          "Content item: " + attachment.ContentId +
          " is incorrect type for external attachment. Height will be set to 0.");
        return 0;
      }

      Log.log(LogCategory.Spine, LogLevel.Warn,
        "Content item: " + attachment.ContentId + " not found. Height will be set to 0.");
      return 0;
    }
  }
}