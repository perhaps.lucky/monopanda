﻿using Microsoft.Xna.Framework;

namespace MonoPanda.SpriteSheets {
  public class JsonRectangle {
    public int x { get; set; }
    public int y { get; set; }
    public int w { get; set; }
    public int h { get; set; }

    public Rectangle ToRectangle() {
      return new Rectangle(x, y, w, h);
    }
  }
}
