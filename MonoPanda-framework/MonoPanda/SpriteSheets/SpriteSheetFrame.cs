﻿namespace MonoPanda.SpriteSheets {
  public class SpriteSheetFrame {
    public string filename { get; set; }
    public bool rotated { get; set; }
    public bool trimmed { get; set; }
    public JsonRectangle frame { get; set; }
    public JsonRectangle spriteSourceSize { get; set; }
    public JsonRectangle sourceSize { get; set; }
  }
}
