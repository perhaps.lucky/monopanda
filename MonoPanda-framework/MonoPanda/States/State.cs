﻿namespace MonoPanda.States {
  public abstract class State {
    public string Id { get; private set; }
    public bool Initialized { get; set; }

    public State(string id) {
      Id = id;
    }

    public abstract void Initialize();

    public abstract void Update();

    public abstract void Draw();

    public abstract void Terminate();
  }
}
