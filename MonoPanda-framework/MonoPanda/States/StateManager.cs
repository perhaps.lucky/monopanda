﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Utils;
using System;
using System.Collections.Generic;

namespace MonoPanda.States {
  public class StateManager {
    private static StateManager instance;
    private State activeState;
    private List<State> states;
    private StateLoader stateLoader;

    private StateManager() {
      stateLoader = new StateLoader();
    }

    public static void Initialize() {
      var instance = GetInstance();
      instance.states = new List<State>();
      instance.stateLoader.Initialize();
      Dictionary<string, string> dicc = ContentUtils.LoadJson<Dictionary<string, string>>(Config.States.StatesJsonPath);

      foreach (KeyValuePair<string, string> keyValuePair in dicc) {
        Type type = Type.GetType(keyValuePair.Value);
        State state = (State)Activator.CreateInstance(type, keyValuePair.Key);
        instance.states.Add(state);
      }

      SetActiveState(Config.States.InitialStateId);
    }

    public static void Update() {
      GetInstance().activeState.Update();
    }

    public static void Draw() {
      GetInstance().activeState.Draw();
    }

    public static void SetActiveState(string id, bool terminateCurrentState = false, bool reinitialize=false) {
      if (terminateCurrentState) {
        Log.log(LogCategory.States, LogLevel.Info, "Terminating state id: " + GetInstance().activeState.Id);
        GetInstance().activeState.Terminate();
        GetInstance().activeState.Initialized = false;
      }

      State state = getState(id);
      Log.log(LogCategory.States, LogLevel.Info, "Setting active state to: " + id);
      GetInstance().activeState = state;
      if (!state.Initialized) {
        Log.log(LogCategory.States, LogLevel.Info, "Initializing state id: " + id);
        state.Initialize();
        state.Initialized = true;
      } else if (reinitialize) {
        Log.log(LogCategory.States, LogLevel.Info, "Reinitializing state id: " + id);
        state.Terminate();
        state.Initialize();
      }
    }

    public static StateRequest PrepareState(string id, bool reinitialize = true) {
      StateRequest request = new StateRequest(getState(id), reinitialize);
      instance.stateLoader.QueueRequest(request);
      return request;
    }

    private static State getState(string id) {
      return GetInstance().states.Find(s => id.Equals(s.Id));
    }

    public static StateManager GetInstance() {
      if (instance == null)
        instance = new StateManager();
      return instance;
    }

    public static void Terminate() {
      foreach (State state in GetInstance().states) {
        if (state.Initialized)
          state.Terminate();
      }
    }


  }
}
