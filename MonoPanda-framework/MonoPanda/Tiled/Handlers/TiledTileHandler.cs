﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.Tiled.Json;

using System;

namespace MonoPanda.Tiled.Handlers {
  public class TiledTileHandler {
    private Func<int, Point, Vector2, TiledLayer, Entity> create;
    private Action<TiledLayer, int> postInit;
    public TiledTileHandler(Func<int, Point, Vector2, TiledLayer, Entity> create, Action<TiledLayer, int> postInit = null) {
      this.create = create;
      this.postInit = postInit;
    }

    public Entity HandleCreate(int tile, Point mapTilePosition, Vector2 worldPosition, TiledLayer layer) {
      return create.Invoke(tile, mapTilePosition, worldPosition, layer);
    }

    public void HandlePostInit(TiledLayer layer, int tileIndex) {
      if (postInit != null)
        postInit.Invoke(layer, tileIndex); 
    }
  }
}