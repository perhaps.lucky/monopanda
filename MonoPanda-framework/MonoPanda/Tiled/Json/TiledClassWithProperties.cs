﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.Utils;

using System.Collections.Generic;

namespace MonoPanda.Tiled.Json {
  public abstract class TiledClassWithProperties {
    protected abstract List<TiledProperty> getPropertyList();
    protected abstract TiledMap getMap();

    public bool GetBoolProperty(string propertyName, bool valueIfNotPresent = false) {
      object value = getProperty(propertyName, "bool");
      return (bool?) value ?? valueIfNotPresent;
    }

    public Color GetColorProperty(string propertyName, Color valueIfNotPresent) {
      object value = getProperty(propertyName, "color");
      return value == null ? valueIfNotPresent : ColorUtils.HexStringToColor((string)value);
    }

    public int GetIntProperty(string propertyName, int valueIfNotPresent = 0) {
      object value = getProperty(propertyName, "int");
      return (int)((long?) value ?? valueIfNotPresent);
    }

    public float GetFloatProperty(string propertyName, float valueIfNotPresent = 0) {
      object value = getProperty(propertyName, "float");
      return (float?) value ?? valueIfNotPresent;
    }

    public Entity GetObjectProperty(string propertyName) {
      object value = getProperty(propertyName, "object");
      return value == null ? null : getMap().IdToEntity[(int)(long)value];
    }

    public string GetStringProperty(string propertyName, string valueIfNotPresent = "") {
      object value = getProperty(propertyName, "string");
      return value == null ? valueIfNotPresent : (string)value;
    }

    private object getProperty(string propertyName, string propertyType) {
      foreach (TiledProperty property in getPropertyList()) {
        if (property.Name == propertyName && property.Type == propertyType)
          return property.Value;
      }

      return null;
    }
  }
}