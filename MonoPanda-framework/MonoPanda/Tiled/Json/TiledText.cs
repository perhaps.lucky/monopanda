﻿using Microsoft.Xna.Framework;

using MonoPanda.Utils;

namespace MonoPanda.Tiled.Json {
  public class TiledText {
    public string Text { get; set; }
    public bool Wrap { get; set; }
    public string Color { get; set; } = "#000000";
    public int PixelSize { get; set; } = 16;
    public string FontFamily { get; set; }

    public Color GetColor() {
      return ColorUtils.HexStringToColor(Color);
    }
  }
}