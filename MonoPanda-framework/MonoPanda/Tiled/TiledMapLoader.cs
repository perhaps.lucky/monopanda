﻿using Microsoft.Xna.Framework;

using MonoPanda.Content;
using MonoPanda.ECS;
using MonoPanda.Tiled.Content;
using MonoPanda.Tiled.Handlers;
using MonoPanda.Tiled.Json;

using System;
using System.Collections.Generic;

namespace MonoPanda.Tiled {
  public class TiledMapLoader {
    private Vector2 mapPositionOffset;

    private TiledMap map;

    private Dictionary<string, TiledTileHandler> tileHandlers;
    private Dictionary<string, TiledLayerHandler> layerHandlers;
    private Dictionary<string, TiledObjectHandler> objectHandlersByName;
    private Dictionary<string, TiledObjectHandler> objectHandlersByType;

    public TiledMapLoader() {
      tileHandlers = new Dictionary<string, TiledTileHandler>();
      layerHandlers = new Dictionary<string, TiledLayerHandler>();
      objectHandlersByName = new Dictionary<string, TiledObjectHandler>();
      objectHandlersByType = new Dictionary<string, TiledObjectHandler>();
    }


    public void LoadMap(string contentId) {
      prepareMap(contentId);
      foreach (TiledLayer layer in map.Layers) {
        loadLayer(layer);
      }

      postInit();
    }

    public TiledMapLoader SetMapPositionOffset(Vector2 position) {
      this.mapPositionOffset = position;
      return this;
    }

    public TiledMapLoader AddTileHandler(string tilesetName, Func<int, Point, Vector2, TiledLayer, Entity> create,
      Action<TiledLayer, int> postInit = null) {
      tileHandlers.Add(tilesetName, new TiledTileHandler(create, postInit));
      return this;
    }

    public TiledMapLoader AddLayerHandler(string layerName, Action<TiledLayer> create = null,
      Action<TiledLayer> postInit = null) {
      layerHandlers.Add(layerName, new TiledLayerHandler(create, postInit));
      return this;
    }

    public TiledMapLoader AddObjectHandlerByName(string objectName,
      Func<Vector2, TiledObject, TiledLayer, Entity> create = null,
      Action<Entity, TiledObject, TiledLayer> postInit = null) {
      objectHandlersByName.Add(objectName, new TiledObjectHandler(create, postInit));
      return this;
    }

    public TiledMapLoader AddObjectHandlerByType(string objectType,
      Func<Vector2, TiledObject, TiledLayer, Entity> create = null,
      Action<Entity, TiledObject, TiledLayer> postInit = null) {
      objectHandlersByType.Add(objectType, new TiledObjectHandler(create, postInit));
      return this;
    }

    private void prepareMap(string contentId) {
      map = ContentManager.Get<TiledMapContentItem>(contentId, true).TiledMap;
      for (var i = 0; i < map.Layers.Count; i++) {
        map.Layers[i].LayerIndex = i;
        map.Layers[i].Map = map;
        if (map.Layers[i].Type == TiledLayerType.objectgroup) {
          foreach (TiledObject o in map.Layers[i].Objects) {
            o.Map = map;
          }
        }
      }
    }

    private void postInit() {
      foreach (TiledLayer layer in map.Layers) {
        handleLayerPostInit(layer);
        switch (layer.Type) {
          case TiledLayerType.tilelayer:
            postInitTileLayer(layer);
            break;
          case TiledLayerType.objectgroup:
            postInitObjectLayer(layer);
            break;
        }
      }
    }

    private void postInitTileLayer(TiledLayer layer) {
      for (var i = 0; i < layer.Data.Length; i++) {
        int tileId = layer.Data[i];
        if (tileId == 0)
          continue;
        var tileSet = getTileSet(tileId);
        if (tileHandlers.ContainsKey(tileSet.Name)) {
          tileHandlers[tileSet.Name].HandlePostInit(layer, i);
        }
      }
    }

    private void postInitObjectLayer(TiledLayer layer) {
      foreach (TiledObject o in layer.Objects) {
        if (!map.IdToEntity.ContainsKey(o.Id))
          continue;

        Entity entity = map.IdToEntity[o.Id];

        if (objectHandlersByName.ContainsKey(o.Name)) {
          objectHandlersByName[o.Name].HandlePostInit(entity, o, layer);
        }

        if (objectHandlersByType.ContainsKey(o.Type)) {
          objectHandlersByType[o.Type].HandlePostInit(entity, o, layer);
        }
      }
    }


    private void loadLayer(TiledLayer layer) {
      switch (layer.Type) {
        case TiledLayerType.tilelayer:
          loadTileLayer(layer);
          break;
        case TiledLayerType.objectgroup:
          loadObjectLayer(layer);
          break;
      }
    }

    private void loadTileLayer(TiledLayer layer) {
      handleLayerCreate(layer);
      for (var i = 0; i < layer.Data.Length; i++) {
        int tileId = layer.Data[i];
        if (tileId == 0)
          continue;
        var worldPosition = calculateTilePosition(layer, i);
        var tileMapPosition = new Point(i % (int)layer.Width, i / (int)layer.Width);
        var tileSet = getTileSet(tileId);
        if (tileHandlers.ContainsKey(tileSet.Name)) {
          Entity createdEntity = tileHandlers[tileSet.Name]
            .HandleCreate(calculateTileIdWithinSet(tileId, tileSet), tileMapPosition, worldPosition, layer);
          layer.IndexToCreatedEntity.Add(i, createdEntity);
        }
      }
    }

    private void loadObjectLayer(TiledLayer layer) {
      foreach (TiledObject o in layer.Objects) {
        loadObject(o, layer);
      }
    }

    private void loadObject(TiledObject o, TiledLayer layer) {
      var position = calculateObjectPosition(o, layer);
      if (objectHandlersByName.ContainsKey(o.Name)) {
        Entity createdEntity = objectHandlersByName[o.Name].HandleCreate(position, o, layer);
        if (createdEntity != null)
          map.IdToEntity.Add(o.Id, createdEntity);
        return;
      }

      var type = getTypeOrTypeFromTileset(o);
      if (objectHandlersByType.ContainsKey(type)) {
        Entity createdEntity = objectHandlersByType[type].HandleCreate(position, o, layer);
        if (createdEntity != null)
          map.IdToEntity.Add(o.Id, createdEntity);
      }
    }

    private Vector2 calculateTilePosition(TiledLayer layer, int i) {
      int tileX = i % (int)layer.Width;
      int tileY = i / (int)layer.Height;
      return mapPositionOffset + layer.GetOffset() + new Vector2(tileX * map.TileWidth + map.TileWidth / 2,
        tileY * map.TileHeight + map.TileHeight / 2);
    }

    private Vector2 calculateObjectPosition(TiledObject o, TiledLayer layer) {
      return mapPositionOffset + layer.GetOffset() + new Vector2(o.X + o.Width / 2, o.Y + o.Height / 2);
    }

    private TiledTileSet getTileSet(int tileId) {
      var tilesets = map.Tilesets;
      for (var i = 0; i < tilesets.Count; i++) {
        if (i == tilesets.Count - 1) {
          return tilesets[i];
        }

        if (tileId >= tilesets[i].FirstGid && tileId < tilesets[i + 1].FirstGid) {
          return tilesets[i];
        }
      }

      throw new Exception("Shouldn't happen if everything is okay with tiled project."); // or so I believe
    }

    private int calculateTileIdWithinSet(int tileId, TiledTileSet tileSet) {
      return tileId - tileSet.FirstGid;
    }

    private void handleLayerCreate(TiledLayer layer) {
      if (layerHandlers.ContainsKey(layer.Name)) {
        layerHandlers[layer.Name].HandleCreate(layer);
      }
    }

    private void handleLayerPostInit(TiledLayer layer) {
      if (layerHandlers.ContainsKey(layer.Name)) {
        layerHandlers[layer.Name].HandlePostInit(layer);
      }
    }

    private string getTypeOrTypeFromTileset(TiledObject o) {
      if (o.Type.Length > 0)
        return o.Type; // type present, return it

      var tileset = getTileSet(o.GID);
      return tileset.FindTileByGID(o.GID).Type;
    }
  }
}