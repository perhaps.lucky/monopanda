﻿using MonoPanda.ECS;
using MonoPanda.Flag;
using MonoPanda.GlobalTime;
using MonoPanda.Logger;

namespace MonoPanda.Timers {
  /// <summary>
  /// This timer check keeps returning true after waitTime has passed,
  /// but not for longer than workTime.
  /// Cooldown parameters sets internal repeating timer that will 
  /// additionally reduce how often true is returned.
  /// </summary>
  public class PeriodTimer : Timer {

    private int waitTime;
    private RepeatingTimer internalTimer;
    private OneTimeFlag isInternalTimerInitialized;

    private double targetWaitTime;

    public PeriodTimer(int waitTime, int workTime, int cooldown = 0, string id = null, bool affectedByWorldTime = false, Entity entity = null, LogCategory logCategory = LogCategory.Timers) : base(waitTime + workTime, "PeriodTimer", id, affectedByWorldTime, logCategory, entity) {
      this.waitTime = waitTime - cooldown;
      internalTimer = new RepeatingTimer(cooldown);
      isInternalTimerInitialized = new OneTimeFlag(Id);
    }

    public override void Initialize(bool silent = false) {
      base.Initialize(silent);
      targetWaitTime = Time.TotalElapsedMillis + waitTime;
      isInternalTimerInitialized.Reinitialize();
    }

    public override bool Check() {
      if (Time.TotalElapsedMillis < targetWaitTime)
        return false;

      if (isInternalTimerInitialized.Check())
        internalTimer.Initialize();

      if (Time.TotalElapsedMillis < targetTime) {
        Log.log(LogCategory.Timers, LogLevel.Debug, "Timer: " + Id + " is in active period.");
        return internalTimer.Check();
      }
      return false;
    }

    public override void Dispose() {
      base.Dispose();
      internalTimer.Dispose();
    }
  }
}
