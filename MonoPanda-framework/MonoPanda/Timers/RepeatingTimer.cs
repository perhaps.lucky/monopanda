﻿using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.Logger;

namespace MonoPanda.Timers {
  /// <summary>
  /// This timer check returns true every time repeatTime has passed.
  /// </summary>
  public class RepeatingTimer : Timer {

    // private bool initialized;

    public RepeatingTimer(int repeatTime, string id = null, bool affectedByWorldTime = false, Entity entity = null, LogCategory logCategory = LogCategory.Timers) : base(repeatTime, "RepeatingTimer", id, affectedByWorldTime, logCategory, entity) {
    }
    
    // public override void Initialize(bool silent = false) {
    //   base.Initialize(silent);
    //   // initialized = true;
    // }

    /// <summary>
    /// Returns true every time repeatTime has passed
    /// </summary>
    public bool Check(bool silent = true) {
      if (!initialized) return false;
      
      if (Time.TotalElapsedMillis >= targetTime) {
        Initialize(silent);
        return true;
      }
      return false;
    }

    public override bool Check() {
      return Check();
    }

    // public override void Dispose() {
    //   base.Dispose();
    //   initialized = false;
    // }
  }
}
