﻿using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.Logger;

namespace MonoPanda.Timers {
  /// <summary>
  /// This timer check returns true once after the waitTime has passed.
  /// Afterwards it needs another initialization to run.
  /// </summary>
  public class SingleTimeTimer : Timer {

    private bool gotChecked;

    public SingleTimeTimer(int waitTime, string id = null, bool affectedByWorldTime = false, Entity entity = null, LogCategory logCategory = LogCategory.Timers) : base(waitTime, "SingleTimeTimer", id, affectedByWorldTime, logCategory, entity) {
    }

    public override void Initialize(bool silent = false) {
      base.Initialize(silent);
      gotChecked = false;
    }

    /// <summary>
    /// Returns true once when the waitTime has passed.
    /// </summary>
    public override bool Check() {
      if (!initialized) return false;
      if (!gotChecked && Time.TotalElapsedMillis >= this.targetTime) {
        gotChecked = true;
        return true;
      }
      return false;
    }
  }
}
