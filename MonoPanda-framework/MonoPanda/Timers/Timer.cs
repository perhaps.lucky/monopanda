﻿using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.GlobalTime;
using MonoPanda.Logger;
using MonoPanda.UtilityClasses;

using System;

namespace MonoPanda.Timers {
  public abstract class Timer : IDisposable {
    private static int currentId = 0;

    public string Id { get; private set; }

    protected int workTime;

    protected double targetTime;

    protected bool affectedByWorldTime;

    protected bool initialized;

    protected LogCategory logCategory;

    private Observer worldTimeObserver;
    private Observer timeComponentObserver;
    private Entity entity;

    public Timer(int expectedWorkTime, string timerType, string id = null, bool affectedByWorldTime = false,
      LogCategory logCategory = LogCategory.Timers, Entity entity = null) {
      this.workTime = expectedWorkTime;
      this.Id = (id == null ? (currentId++).ToString() : id) + "_" + timerType;
      this.logCategory = logCategory;
      this.affectedByWorldTime = affectedByWorldTime;
      if (affectedByWorldTime) {
        this.worldTimeObserver = Time.RegisterWorldTimeSpeedObserver(new Action<float, bool>(WorldTimeChanged));
        this.entity = entity;
        if (entity != null)
          registerTimeComponent(entity);
      }
    }

    /// <summary>
    /// (Re-)Starts timer.
    /// </summary>
    /// <param name="silent">if true then it won't get logged (used in some system timers)</param>
    public virtual void Initialize(bool silent = false) {
      if (!silent)
        Log.log(logCategory, LogLevel.Info, "Timer id: " + Id + " initialized.");
      targetTime = Time.TotalElapsedMillis +
                   workTime * (affectedByWorldTime ? (1.0 / Time.EntityTimeSpeed(entity)) : 1);
      initialized = true;
    }

    /// <summary>
    /// Called when WorldTimeSpeed has changed (if timer is affected by it)
    /// Calculates new target time by multiplying time left and inverse of speed
    /// </summary>
    protected virtual void WorldTimeChanged(float oldTimeSpeed, bool timeComponent) {
      var timeLeft = targetTime - Time.TotalElapsedMillis;
      targetTime = Time.TotalElapsedMillis + timeLeft * ((1.0 / Time.EntityTimeSpeed(entity)) /
                                                         (1.0 / getOldTimeSpeedValue(oldTimeSpeed,
                                                           timeComponent)));
    }

    public abstract bool Check();

    public virtual void Dispose() {
      if (affectedByWorldTime) {
        Time.UnregisterWorldTimeSpeedObserver(worldTimeObserver);
        if (timeComponentObserver != null)
          entity.GetComponent<TimeComponent>().TimeChangeInformer.UnregisterObserver(timeComponentObserver);
      }

      initialized = false;
    }

    // only one value can change at once
    private float getOldTimeSpeedValue(float oldTimeSpeed, bool timeComponent) {
      if (timeComponent) {
        return oldTimeSpeed * Time.WorldTimeSpeed;
      } else {
        return oldTimeSpeed * (entity != null
          ? entity.GetComponentOptional<TimeComponent>().IfPresentGet(timeComponent => timeComponent.TimeMultiplier)
            .OrElse(1f)
          : 1f);
      }
    }

    private void registerTimeComponent(Entity entity) {
      entity.GetComponentOptional<TimeComponent>().IfPresent(timeComponent => {
        timeComponentObserver =
          timeComponent.TimeChangeInformer.RegisterObserver(new Action<float, bool>(WorldTimeChanged));
      });
    }
  }
}