﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using MonoPanda.BitmapFonts;
using MonoPanda.Input;
using MonoPanda.Logger;
using MonoPanda.Timers;

using System;
using System.Collections.Generic;

namespace MonoPanda.UI {
  public class TextBox : UIFocusableObject, IDisposable {
    public string Text { get { return text; } set { setText(value); } }
    public string FontId { get; set; }
    public Vector2 Position { get; set; }
    public Color Color { get; set; }
    
    public int FontSize {
      get => fontSize;
      set {
        fontSize = value;
        calculateVisibleChars();
      }
    }

    private int fontSize;

    public bool ClickToFocus { get; set; }

    public int Width {
      get => width;
      set {
        width = value;
        calculateVisibleChars();
      }
    }

    private int width = -1;
    public int Height { get; set; } = -1;

    /// <summary>
    /// If empty, pressing enter will add '\n' symbol to Text
    /// </summary>
    public Action OnEnter { get; set; }

    public bool NewLineOnEnter { get; set; } = false;

    public Action OnTab { get; set; }

    private bool isCursorOn;
    private RepeatingTimer cursorTimer;
    private int cursorPosition;

    private string text;

    private int visibleChars = -1; // how many characters can fit in "width"
    private int startChar = 0; // position of first visible character
    private string visibleText;

    public TextBox(string fontId, Vector2 position, Color color, int fontSize = 32) {
      FontId = fontId;
      Position = position;
      Color = color;
      FontSize = fontSize;
      Text = "";
      cursorTimer = new RepeatingTimer(500, logCategory: LogCategory.UI);
      cursorTimer.Initialize(true);
      cursorPosition = 0;
      ClickToFocus = true;
    }

    public override void Update() {
      updateCursor();
      if (Focused) {
        processInputs();
      }

      if (ClickToFocus && Active) {
        updateFocus();
      }
    }

    public override void Draw() {
      string textWithCursor = "";
      if (Focused)
        textWithCursor = isCursorOn
          ? visibleText.Insert(cursorPosition - startChar, "|")
          : visibleText.Insert(cursorPosition - startChar, " ");

      FontRenderer.DrawText(FontId, Position, Focused ? textWithCursor : visibleText, Color, FontSize, false);
    }

    public void Dispose() {
      cursorTimer.Dispose();
    }

    public void Clear() {
      text = "";
      cursorPosition = 0;
      startChar = 0;
      updateVisibleText();
    }

    private void setText(string text) {
      this.text = text;
      cursorPosition = text.Length;
      updateVisibleText();
    }

    private void updateCursor() {
      if (Focused) {
        if (cursorTimer.Check())
          isCursorOn = !isCursorOn;
      } else {
        isCursorOn = false;
      }
    }

    private void processInputs() {
      foreach (Keys key in getPressedKeys()) {
        if (key >= Keys.A && key <= Keys.Z)
          handleLetters(key);
        else if (key >= Keys.D0 && key <= Keys.D9)
          handleNumbers(key);
        else if (key == Keys.Back)
          handleBackspace();
        else if (key == Keys.Space)
          handleSpace();
        else if (key == Keys.Left)
          moveCursorLeft();
        else if (key == Keys.Right)
          moveCursorRight();
        else if (key == Keys.Delete)
          handleDelete();
        else if (key == Keys.Home)
          handleHome();
        else if (key == Keys.End)
          handleEnd();
        else if (key == Keys.OemMinus)
          handleOemMinus();
        else if (key == Keys.OemPlus)
          handleOemPlus();
        else if (key == Keys.OemCloseBrackets)
          handleOemCloseBrackets();
        else if (key == Keys.OemOpenBrackets)
          handleOemOpenBrackets();
        else if (key == Keys.OemSemicolon)
          handleOemSemicolon();
        else if (key == Keys.OemQuotes)
          handleOemQuotes();
        else if (key == Keys.OemPipe)
          handleOemPipe();
        else if (key == Keys.OemComma)
          handleOemComma();
        else if (key == Keys.OemPeriod)
          handleOemPeriod();
        else if (key == Keys.OemQuestion)
          handleOemQuestion();
        else if (key == Keys.Enter)
          handleEnter();
        else if (key == Keys.Tab)
          handleTab();
      }
    }

    private bool shiftPressed() {
      var keyboardState = InputManager.GetCurrentKeyboardState();
      return keyboardState.IsKeyDown(Keys.LeftShift) || keyboardState.IsKeyDown(Keys.RightShift);
    }

    private List<Keys> getPressedKeys() {
      List<Keys> pressedKeys = new List<Keys>();
      var keyboardState = InputManager.GetCurrentKeyboardState();
      var oldKeyboardState = InputManager.GetOldKeyboardState();
      foreach (Keys key in keyboardState.GetPressedKeys()) {
        if (oldKeyboardState.IsKeyUp(key))
          pressedKeys.Add(key);
      }

      return pressedKeys;
    }

    private void handleBackspace() {
      if (cursorPosition > 0 && text.Length > 0) {
        if (shiftPressed()) {
          var previousSpaceIndex = text.Substring(0, cursorPosition).LastIndexOf(" ");
          if (previousSpaceIndex != -1) {
            text = text.Remove(previousSpaceIndex, cursorPosition - previousSpaceIndex);
            cursorPosition = previousSpaceIndex;
            startChar = startChar > visibleChars ? previousSpaceIndex : 0;
            updateVisibleText();
          } else {
            text = text.Remove(0, cursorPosition);
            cursorPosition = 0;
            startChar = 0;
            updateVisibleText();
          }
        } else {
          text = text.Remove(cursorPosition - 1, 1);
          moveCursorLeft();
          updateVisibleText();
        }
      }
    }

    private void handleDelete() {
      if (cursorPosition < text.Length) {
        if (shiftPressed()) {
          var substringNextSpaceIndex = text.Substring(cursorPosition).IndexOf(" ");
          var nextSpaceIndex = substringNextSpaceIndex != -1 ? substringNextSpaceIndex + cursorPosition : text.Length;
          text = text.Remove(cursorPosition, nextSpaceIndex - cursorPosition);
          updateVisibleText();
        } else {
          text = text.Remove(cursorPosition, 1);
          updateVisibleText();
        }
      }
    }

    private void write(string symbol) {
      text = text.Insert(cursorPosition, symbol);
      updateVisibleText();
    }

    private void handleSpace() {
      write(" ");
      moveCursorRight();
    }

    private void handleLetters(Keys key) {
      if (key == Keys.V && InputManager.GetCurrentKeyboardState().IsKeyDown(Keys.LeftControl)) {
        var clipboardContent = TextCopy.ClipboardService.GetTextAsync().Result;
        if (clipboardContent != null) {
          write(clipboardContent);
          moveCursorRight(clipboardContent.Length);
        }

        return;
      }

      if (shiftPressed())
        write(key.ToString().ToUpper());
      else
        write(key.ToString().ToLower());

      moveCursorRight();
    }

    private void handleNumbers(Keys key) {
      if (shiftPressed())
        write(getAlternativeNumberSymbol(key));
      else
        write(key.ToString().Substring(1));

      moveCursorRight();
    }

    private void moveCursorLeft() {
      cursorPosition--;
      if (cursorPosition < 0)
        cursorPosition = 0;

      if (cursorPosition < startChar) {
        startChar--;
        updateVisibleText();
      }
    }

    private void moveCursorRight(int moveBy = 1) {
      cursorPosition += moveBy;
      if (cursorPosition > text.Length)
        cursorPosition = text.Length;

      if (cursorPosition - startChar > visibleChars) {
        startChar++;
        updateVisibleText();
      }
    }

    private void handleHome() {
      cursorPosition = 0;
      startChar = 0;
      updateVisibleText();
    }

    private void handleEnd() {
      cursorPosition = text.Length;
      startChar = cursorPosition > visibleChars ? cursorPosition - visibleChars : 0;
      updateVisibleText();
    }

    private void handleOemMinus() {
      if (shiftPressed())
        write("_");
      else
        write("-");
      moveCursorRight();
    }

    private void handleOemPlus() {
      if (shiftPressed())
        write("+");
      else
        write("=");
      moveCursorRight();
    }

    private void handleOemCloseBrackets() {
      if (shiftPressed())
        write("}");
      else
        write("]");
      moveCursorRight();
    }

    private void handleOemOpenBrackets() {
      if (shiftPressed())
        write("{");
      else
        write("[");
      moveCursorRight();
    }

    private void handleOemQuotes() {
      if (shiftPressed())
        write("\"");
      else
        write("'");
      moveCursorRight();
    }

    private void handleOemPipe() {
      if (shiftPressed())
        write("|");
      else
        write("\\");
      moveCursorRight();
    }

    private void handleOemComma() {
      if (shiftPressed())
        write("<");
      else
        write(",");
      moveCursorRight();
    }

    private void handleOemPeriod() {
      if (shiftPressed())
        write(">");
      else
        write(".");
      moveCursorRight();
    }

    private void handleOemQuestion() {
      if (shiftPressed())
        write("?");
      else
        write("/");
      moveCursorRight();
    }

    private void handleOemSemicolon() {
      if (shiftPressed())
        write(":");
      else
        write(";");
      moveCursorRight();
    }

    private void handleEnter() {
      if (OnEnter == null) {
        if (NewLineOnEnter) {
          write("\n");
          moveCursorRight();
        } else {
          Focused = false;
        }
      } else
        OnEnter.Invoke();
    }

    private void handleTab() {
      if (OnTab != null)
        OnTab.Invoke();
    }

    private string getAlternativeNumberSymbol(Keys key) {
      switch (key) {
        case Keys.D1:
          return "!";
        case Keys.D2:
          return "@";
        case Keys.D3:
          return "#";
        case Keys.D4:
          return "$";
        case Keys.D5:
          return "%";
        case Keys.D6:
          return "^";
        case Keys.D7:
          return "&";
        case Keys.D8:
          return "*";
        case Keys.D9:
          return "(";
        case Keys.D0:
          return ")";
        default:
          return ""; // won't happen
      }
    }

    private void updateFocus() {
      var cursorInArea = isCursorInArea();
      if (Focused && !cursorInArea && InputManager.GetLeftMouseButton(this).IsPressed) 
        Focused = false;

        if (!Focused && cursorInArea && InputManager.GetLeftMouseButton(this, true).IsPressed)
        Focused = true;
    }

    private bool isCursorInArea() {
      var cursorPos = InputManager.GetMouseWindowPosition();
      return cursorPos.X >= Position.X
             && cursorPos.X <= Position.X + Width
             && cursorPos.Y >= Position.Y
             && cursorPos.Y <= Position.Y + Height;
    }

    private void calculateVisibleChars() {
      if (width == -1) {
        visibleChars = Int32.MaxValue;
      } else {
        visibleChars = width / fontSize;
      }
    }

    private void updateVisibleText() {
      visibleText = text.Substring(startChar,
        text.Length - startChar > visibleChars ? visibleChars : text.Length - startChar);
    }

    public override void OnUnfocused() {
      startChar = 0;
      updateVisibleText();
    }

    public override void OnFocused() {
      startChar = cursorPosition - visibleChars >= 0 ? cursorPosition - visibleChars : 0;
      updateVisibleText();
    }
  }
}