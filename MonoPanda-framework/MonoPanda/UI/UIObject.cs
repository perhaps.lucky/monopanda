﻿namespace MonoPanda.UI {
  public abstract class UIObject {

    public bool Visible { get; set; } = true;

    public void UpdateIfVisible() {
      if (Visible)
        Update();
    }

    public void DrawIfVisible() {
      if (Visible)
        Draw();
    }

    public abstract void Update();
    public abstract void Draw();

  }
}
