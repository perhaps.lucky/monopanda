﻿using IniParser.Exceptions;
using IniParser.Model;
using MonoPanda.Utils;
using System.Reflection;

namespace MonoPanda.UserSettings {
  public class Settings {

    private static readonly string SETTINGS_LOCATION = "Content/MonoPanda/user-settings.ini";

    private static Settings instance;
    public static WindowSettings Window => getInstance().windowSettings;
    public static SoundSettings Sound => getInstance().soundSettings;
    public static LanguageSettings Language => getInstance().languageSettings;

    private readonly WindowSettings windowSettings;
    private readonly SoundSettings soundSettings;
    private readonly LanguageSettings languageSettings;

    private Settings() {
      IniData settingsIniData = loadOrCreateSettings();

      windowSettings = new WindowSettings(settingsIniData);
      soundSettings = new SoundSettings(settingsIniData);
      languageSettings = new LanguageSettings(settingsIniData);

      instance = this;
    }

    private IniData loadOrCreateSettings() {
      try {
        return IniUtils.getIniData(SETTINGS_LOCATION);
      } catch (ParsingException e) {
        return new IniData();
      }
    }

    private static Settings getInstance() {
      if (instance == null)
        instance = new Settings();

      return instance;
    }

    public static void Save() {
      IniData iniData = new IniData();

      foreach (PropertyInfo propertyInfo in instance.GetType().GetProperties())
        IniUtils.addToIniData(iniData, propertyInfo.GetValue(null));

      IniUtils.saveIniData(iniData, SETTINGS_LOCATION);
    }


  }
}