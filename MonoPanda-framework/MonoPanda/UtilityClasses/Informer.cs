﻿using System;
using System.Collections.Generic;

namespace MonoPanda.UtilityClasses {
  public class Informer {
    private List<Observer> observers;

    public Observer RegisterObserver(Delegate action) {
      if (observers == null)
        observers = new List<Observer>();

      Observer observer = new Observer(action);
      observers.Add(observer);
      return observer;
    }

    public void UnregisterObserver(Observer observer) {
      observers.Remove(observer);
    }

    public void Inform(params object[] @params) {
      if (observers == null)
        return;

      foreach (Observer observer in observers) {
        observer.Inform(@params);
      }
    }
  }
}