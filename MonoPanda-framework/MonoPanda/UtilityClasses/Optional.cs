﻿using System;

namespace MonoPanda {

  /// <summary>
  /// Was nice idea but didn't really help as I thought
  /// </summary>
  public class Optional<T> {
    private T value;

    public static Optional<T> Of(T value) {
      return new Optional<T>(value);
    }

    /// <summary>
    /// <b>You probably want to use IfPresentGet instead</b> <br/>
    /// This method makes everything slow with exception catch. It's still fine to use if it's not called on every single update.
    /// </summary>
    public static Optional<T> Of(Func<T> funcValue) {
      try {
        T v = funcValue.Invoke();
        return Of(v);
      } catch (NullReferenceException) {
        return Empty();
      }
    }

    public static Optional<T> Empty() {
      return new Optional<T>();
    }

    private Optional(T value) {
      this.value = value;
    }

    private Optional() { }

    public bool IsPresent() {
      return value != null;
    }

    public void IfPresent(Action<T> action) {
      if (IsPresent()) {
        action.Invoke(value);
      }
    }

    public Optional<Y> IfPresentGet<Y>(Func<T, Y> getFunc) {
      if (IsPresent()) {
        return Optional<Y>.Of(getFunc.Invoke(Get()));
      }
      return Optional<Y>.Empty();
    }

    public T Get() {
      return value;
    }

    public T OrElse(T another) {
      if (IsPresent()) {
        return value;
      } else {
        return another;
      }
    }

    public T OrElseGet(Func<T> func) {
      if (IsPresent()) {
        return value;
      } else {
        return func.Invoke();
      }
    }

    public T OrElseThrow(Exception e) {
      if (IsPresent()) {
        return value;
      } else {
        throw e;
      }
    }

    public void IfPresentAnd(Func<T, bool> condition, Action<T> action) {
      if (IsPresent() && condition.Invoke(Get())) {
        action.Invoke(value);
      }
    }
  }
}
