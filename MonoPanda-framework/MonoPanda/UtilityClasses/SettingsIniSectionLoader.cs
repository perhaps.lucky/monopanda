﻿using IniParser.Model;
using MonoPanda.Configuration;
using MonoPanda.Logger;
using System;
using System.Reflection;

namespace MonoPanda.UtilityClasses {
  public abstract class SettingsIniSectionLoader : DefaultIniSectionLoader {

    private KeyDataCollection iniSection;

    public SettingsIniSectionLoader(IniData iniData, string sectionName) : base(iniData, sectionName) {
      this.iniSection = iniData.Sections[sectionName];
      importFromConfig();
    }

    protected void importFromConfig() {
      Object configurationConfig = getRelatedConfigObject();

      foreach (PropertyInfo propertyInfo in GetType().GetProperties()) {
        if (iniSection.ContainsKey(propertyInfo.Name))
          continue;

        Log.log(LogCategory.Ini, LogLevel.Debug, "Property: " + propertyInfo.Name + " is missing in user settings. Importing from config.");
        FieldInfo targetConfigPropertyInfo = configurationConfig.GetType().GetField(propertyInfo.Name);
        propertyInfo.SetValue(this, targetConfigPropertyInfo.GetValue(configurationConfig));
      }
    }

    private Object getRelatedConfigObject() {
      string propertyInConfigName = this.GetType().Name.Replace("Settings", "");
      PropertyInfo configPropertyInfo = typeof(Config).GetProperty(propertyInConfigName);
      return configPropertyInfo.GetValue(null);
    }


  }
}
