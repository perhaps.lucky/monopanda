﻿using Microsoft.Xna.Framework;

namespace MonoPanda.Utils {
  public class ColorUtils {
    public static Color MixColors(Color first, Color second) {
      return new Color(
        (first.R + second.R) / 2,
        (first.G + second.G) / 2,
        (first.B + second.B) / 2,
        (first.A + second.A) / 2);
    }

    public static float ByteToFloat(byte value) {
      return value / 255.0f;
    }

    public static byte FloatToByte(float value) {
      return (byte)(value * 255);
    }

    public static Color HexStringToColor(string hex) {
      System.Drawing.Color systemColor = System.Drawing.ColorTranslator.FromHtml(hex);
      return new Color(systemColor.R, systemColor.G, systemColor.B, systemColor.A);
    }

    public static Color AlphaBlendColors(Color firstColor, Color secondColor) {
      Vector4 first = firstColor.ToVector4();
      Vector4 second = secondColor.ToVector4();

      float resultAlpha = first.W + second.W * (1 - first.W);
      Vector4 result = new Vector4(
        alphaBlendCalculateColorChannel(first.X, first.W, second.X, second.W, resultAlpha),
        alphaBlendCalculateColorChannel(first.Y, first.W, second.Y, second.W, resultAlpha),
        alphaBlendCalculateColorChannel(first.Z, first.W, second.Z, second.W, resultAlpha),
        resultAlpha);
      return new Color(result);
    }

    private static float alphaBlendCalculateColorChannel(float first, float firstAlpha, float second, float secondAlpha,
      float resultAlpha) {
      return (first * firstAlpha + second * secondAlpha * (1 - firstAlpha)) / resultAlpha;
    }
  }
}