﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoPanda.Content;
using MonoPanda.Systems;
using MonoPanda.ECS;
using MonoPanda.ECS.Tools;
using MonoPanda.Old.ECS.Components.CollisionComponentDetails;

namespace MonoPanda.Utils {
  public class DrawUtils {

    public static void DrawTexture(string id, Vector2 position, SpriteBatch spriteBatch = null) {
      DrawTexture(id, position, Color.White, spriteBatch);
    }

    public static void DrawTexture(string id, Vector2 position, Color color, SpriteBatch spriteBatch = null) {
      Texture2D texture = ContentManager.Get<Texture2D>(id);
      if (texture != null) {
        if (spriteBatch != null)
          spriteBatch.Draw(texture, position, color);
        else
          GameMain.SpriteBatch.Draw(texture, position, color);
      }
    }
    
    public static void DrawTexture(string id, Vector2 position, Color color, float rotation,
      Vector2 scale, Vector2 origin, SpriteEffects spriteEffects, float layerDepth, Rectangle? sourceRectangle,
      SpriteBatch spriteBatch = null) {
      Texture2D texture = ContentManager.Get<Texture2D>(id);
      if (texture != null) {
        if (spriteBatch != null) {
          spriteBatch.Draw(
            texture,
            position,
            color: color,
            rotation: rotation,
            scale: scale,
            origin: origin,
            effects: spriteEffects,
            layerDepth: layerDepth,
            sourceRectangle: sourceRectangle);
        } else {
          GameMain.SpriteBatch.Draw(
            texture,
            position,
            color: color,
            rotation: rotation,
            scale: scale,
            origin: origin,
            effects: spriteEffects,
            layerDepth: layerDepth,
            sourceRectangle: sourceRectangle);
        }
      }
    }

    /// <summary>
    /// Request a draw of rectangle. <br/>
    /// <b>Use only for dev purposes.</b>
    /// </summary>
    public static void DrawRectangle(EntityComponentSystem ecs, Rectangle rectangle, Color color, float depth = 0) {
      DrawSystem drawSystem = ecs.GetSystem<DrawSystem>();
      if (drawSystem == null) {
        DrawSystemSpine drawSystemSpine = ecs.GetSystem<DrawSystemSpine>();
        if (drawSystemSpine == null)
          return;

        drawSystemSpine.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
        return;
      }

      drawSystem.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
    }

    public static void DrawRectangle(EntityComponentSystem ecs, CollisionRectangle rectangle, Color color, float depth = 0) {
      DrawSystem drawSystem = ecs.GetSystem<DrawSystem>();
      if (drawSystem == null) {
        DrawSystemSpine drawSystemSpine = ecs.GetSystem<DrawSystemSpine>();
        if (drawSystemSpine == null)
          return;

        drawSystemSpine.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
        return;
      }

      drawSystem.Rectangles.Add(new DrawRectangle(rectangle, color, depth));
    }
  }
}
