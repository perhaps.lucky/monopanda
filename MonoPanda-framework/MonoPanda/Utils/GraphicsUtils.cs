﻿using MonoPanda.Configuration;
using MonoPanda.UserSettings;
using System.Drawing;

namespace MonoPanda.Utils {
  public class GraphicsUtils {
    public static void ApplyChanges() {
      GameMain.GraphicsDeviceManager.PreferredBackBufferWidth = Settings.Window.Width;
      GameMain.GraphicsDeviceManager.PreferredBackBufferHeight = Settings.Window.Height;
      GameMain.GraphicsDeviceManager.IsFullScreen = Settings.Window.Fullscreen;
      
      GameMain.GraphicsDeviceManager.ApplyChanges();
    }

    public static Size GetWindowSize() {
      if (Config.Window.AllowUserSettings)
        return new Size(Settings.Window.Width, Settings.Window.Height);
      else
        return new Size(Config.Window.Width, Config.Window.Height);
    }
  }
}
