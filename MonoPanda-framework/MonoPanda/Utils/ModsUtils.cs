﻿using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Mods;

using System;
using System.IO;
using System.Linq;

namespace MonoPanda.Utils {
  public class ModsUtils {
    public static void ForEachModFile(string relativePath, Action<string> doAction, bool includeCoreMod = true) {
      bool found = false;
      foreach (string modName in ModManager.GetActiveMods(includeCoreMod).Select(m => m.Name)) {
        string fullPath = GetModPath(modName, relativePath);
        if (File.Exists(fullPath)) {
          found = true;
          doAction.Invoke(fullPath);
        }
      }

      if (!found) {
        Log.log(LogCategory.Mods, LogLevel.Warn, $"File: {relativePath} not found in any of mods!");
      }
    }

    public static void ForEachModName(Action<string> action, bool includeCoreMod = true) {
      foreach (string modName in ModManager.GetActiveMods(includeCoreMod).Select(m => m.Name)) {
        action.Invoke(modName);
      }
    }

    public static void ForEachMod(Action<ModSettings> action, bool includeCoreMod = true) {
      foreach (ModSettings mod in ModManager.GetActiveMods(includeCoreMod)) {
        action.Invoke(mod);
      }
    }


    public static string GetModPath(string modName, string pathToFile) {
      return Config.Mods.ModsFolder + modName + "/" + pathToFile;
    }
  }
}