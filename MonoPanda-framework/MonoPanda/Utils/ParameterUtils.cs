﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Globalization;

namespace MonoPanda.Utils {
  public class ParameterUtils {
    public static Vector2 DirectionStringToVector(string value) {
      switch (value) {
        case "Down":
          return new Vector2(0, 1);
        case "Up":
          return new Vector2(0, -1);
        case "Left":
          return new Vector2(-1, 0);
        case "Right":
          return new Vector2(1, 0);
        default:
          return new Vector2();
      }
    }

    public static Vector2 GetVector2(object value) {
      string valueAsString = value.ToString();
      string[] values = valueAsString.Split(',');

      if (values.Length == 1)
        return new Vector2(float.Parse(values[0], CultureInfo.InvariantCulture));
      else
        return new Vector2(float.Parse(values[0], CultureInfo.InvariantCulture), float.Parse(values[1], CultureInfo.InvariantCulture));
    }

    public static Color GetColor(string value) {
      string[] values = value.Split(',');
      int R = int.Parse(values[0]);
      int G = int.Parse(values[1]);
      int B = int.Parse(values[2]);
      int A = values.Length > 3 ? int.Parse(values[3]) : 255;

      return new Color(R, G, B, A);
    }

    public static Rectangle GetRectangle(string value, Vector2 offsetTo = default, bool offset = false) {
      string[] values = value.Split(',');
      int X = int.Parse(values[0]);
      int Y = int.Parse(values[1]);
      int width = int.Parse(values[2]);
      int height = int.Parse(values[3]);

      if (offset)
        return new Rectangle((int)offsetTo.X + X, (int)offsetTo.Y + Y, width, height);
      else
        return new Rectangle(X, Y, width, height);
    }

    public static Rectangle GetRectangleFromParameters(Dictionary<string, object> parameters, string key) {
      if (parameters.ContainsKey(key)) {
        return GetRectangle(parameters[key].ToString());
      }
      return default;
    }

    public static Vector2 GetVector2FromParameters(Dictionary<string, object> parameters, string key) {
      if (parameters.ContainsKey(key)) {
        return GetVector2(parameters[key].ToString());
      }
      return default;
    }

    public static Color GetColorFromParameters(Dictionary<string, object> parameters, string key) {
      if (parameters.ContainsKey(key)) {
        return GetColor(parameters[key].ToString());
      }
      return new Color(255,255,255,255);
    }

    public static T GetFromParameters<T>(Dictionary<string, object> parameters, string key, T defaultTo = default) {
      if (parameters.ContainsKey(key)) {
        return (T)parameters[key];
      }
      return defaultTo;
    }

  }
}
