﻿using Microsoft.Xna.Framework;

using MonoPanda.Old.ECS.Components.CollisionComponentDetails;

using System.Linq;

namespace MonoPanda.Utils {
  public class RectangleUtils {
    /// <summary>
    /// Applies rotation to the corners and then creates a new rectangle object that contains the points. <br/>
    /// <b>Not actual rotation, as XNA Rectangles can't be rotated</b>
    /// </summary>
    public static Rectangle ApplyRotation(Rectangle rectangle, float rotation,
      Vector2 rotationOrigin) {
      Vector2 leftTop = new Vector2(rectangle.Left, rectangle.Top);
      Vector2 rightTop = new Vector2(rectangle.Right, rectangle.Top);
      Vector2 leftBottom = new Vector2(rectangle.Left, rectangle.Bottom);
      Vector2 rightBottom = new Vector2(rectangle.Right, rectangle.Bottom);

      leftTop = RotationUtils.RotateWithAnchor(leftTop, rotation, rotationOrigin);
      rightTop = RotationUtils.RotateWithAnchor(rightTop, rotation, rotationOrigin);
      leftBottom = RotationUtils.RotateWithAnchor(leftBottom, rotation, rotationOrigin);
      rightBottom = RotationUtils.RotateWithAnchor(rightBottom, rotation, rotationOrigin);

      float[] x = new float[] {leftTop.X, rightTop.X, leftBottom.X, rightBottom.X};
      float[] y = new float[] {leftTop.Y, rightTop.Y, leftBottom.Y, rightBottom.Y};
      int minX = (int)x.Min();
      int minY = (int)y.Min();
      int width = (int)x.Max() - minX;
      int height = (int)y.Max() - minY;

      return new Rectangle(minX, minY, width, height);
    }

    public static CollisionRectangle ApplyRotation(CollisionRectangle rectangle, float rotation,
      Vector2 rotationOrigin) {
      Vector2 leftTop = new Vector2(rectangle.Left, rectangle.Top);
      Vector2 rightTop = new Vector2(rectangle.Right, rectangle.Top);
      Vector2 leftBottom = new Vector2(rectangle.Left, rectangle.Bottom);
      Vector2 rightBottom = new Vector2(rectangle.Right, rectangle.Bottom);

      leftTop = RotationUtils.RotateWithAnchor(leftTop, rotation, rotationOrigin);
      rightTop = RotationUtils.RotateWithAnchor(rightTop, rotation, rotationOrigin);
      leftBottom = RotationUtils.RotateWithAnchor(leftBottom, rotation, rotationOrigin);
      rightBottom = RotationUtils.RotateWithAnchor(rightBottom, rotation, rotationOrigin);

      float[] x = new float[] {leftTop.X, rightTop.X, leftBottom.X, rightBottom.X};
      float[] y = new float[] {leftTop.Y, rightTop.Y, leftBottom.Y, rightBottom.Y};
      float minX = x.Min();
      float minY = y.Min();
      float width = x.Max() - minX;
      float height = y.Max() - minY;

      return new CollisionRectangle(minX, minY, width, height);
    }
  }
}