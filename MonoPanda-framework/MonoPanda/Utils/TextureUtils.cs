﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoPanda.Utils {
  public class TextureUtils {

    public static Texture2D ChangeColorToTransparent(Texture2D texture, Color changedColor, bool disposeOldTexture = true) {
      Color[] colorData = getColorDataFromTexture(texture);

      if (disposeOldTexture)
        texture.Dispose();

      for (int i = 0; i < texture.Width * texture.Height; i++) {
        if (colorData[i].R == changedColor.R
          && colorData[i].G == changedColor.G
          && colorData[i].B == changedColor.B) {
          colorData[i].A = 0;
        }
      }

      return createTextureFromColorData(colorData, texture.Width, texture.Height);
    }

    public static Texture2D ChangeColorsForBitmapFont(Texture2D texture) {
      Color[] colorData = getColorDataFromTexture(texture);
      texture.Dispose(); // always dispose in this case

      for (int i = 0; i < texture.Width * texture.Height; i++) {
        // assuming all fonts are on black background with white text
        colorData[i].A = colorData[i].R;
      }
      return createTextureFromColorData(colorData, texture.Width, texture.Height);
    }

    public static Texture2D CreateRectangleTexture(int width, int height, Color color) =>
      CreateRectangleTexture(width, height, color, 0, Color.Transparent);

    public static Texture2D CreateRectangleTexture(int width, int height, Color color, int borderWidth,
      Color borderColor) {
      var texture = new Texture2D(GameMain.GraphicsDeviceManager.GraphicsDevice, width, height);
      Color[] textureData = new Color[width * height];
      for (int i = 0; i < textureData.Length; i++) {
        if (i < borderWidth * width || // top
            i % width < borderWidth || // left
            i % width >= width - borderWidth || // right
            i > textureData.Length - borderWidth * width) { // bottom
          textureData[i] = borderColor;
        } else {
          textureData[i] = color;
        }
      }

      texture.SetData(textureData);
      return texture;
    }
    
    /// <summary>
    /// Blends one (source) texture into another (target).
    /// </summary>
    /// <param name="targetColorData">Target texture color data</param>
    /// <param name="targetWidth">Target texture width</param>
    /// <param name="targetRectangle">Target rectangle (where sourceTexture should be placed)</param>
    /// <param name="sourceColorData">Source texture color data</param>
    /// <param name="sourceWidth">Source texture width</param>
    /// <returns>Color data of target texture after adding source texture</returns>
    public static Color[] AlphaBlendTextures(Color[] targetColorData, int targetWidth, Rectangle targetRectangle, Color[] sourceColorData, int sourceWidth) {
      int sourceX = 0;
      int sourceY = 0;
      for (int x = targetRectangle.X; x < targetRectangle.X + targetRectangle.Width; x++) {
        for (int y = targetRectangle.Y; y < targetRectangle.Y + targetRectangle.Height; y++) {
          int targetPixel = y * targetWidth + x;
          int sourcePixel = sourceY * sourceWidth + sourceX;
          Color targetCurrentColor = targetColorData[targetPixel];
          Color sourceColor = sourceColorData[sourcePixel];

          targetColorData[targetPixel] = ColorUtils.AlphaBlendColors(sourceColor, targetCurrentColor);;
          sourceY++;
        }
        sourceX++;
        sourceY = 0;
      }

      return targetColorData;
    }

    private static Color[] getColorDataFromTexture(Texture2D texture) {
      Color[] data = new Color[texture.Width * texture.Height];
      texture.GetData(data);
      return data;
    }

    private static Texture2D createTextureFromColorData(Color[] data, int width, int height) {
      Texture2D texture = new Texture2D(GameMain.GraphicsDeviceManager.GraphicsDevice, width, height);
      texture.SetData(data);
      return texture;
    }

  }
}
